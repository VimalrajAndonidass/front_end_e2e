package Arabic_lang.Java_Files;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import English_Lang.Java_Files.Hotel_Filter;
import junit.framework.Assert;

public class Hotel_Booking_Ar extends Namlatec_Login_Ar_old{
	
void Hotel_Book_Ar() throws InterruptedException, IOException
	{
		booking_Ar();
		image_Ar();
		description_Ar();
		roomtype_Ar();
		cancellation_policy_Ar();
		amenities_ver_Ar();
		policy_ver_Ar();
		book_now_Ar();
		various_section_Validation_Ar();
		
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		System.out.println("Hotel-Bookinng: PASS");
		multiScreens.multiScreenShot(driver);
	}
	
	void booking_Ar() throws IOException
	{
		/*driver.navigate().to("https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver); 
	*/	
		WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div"));	
		Hotel_sel.click(); 
		String exp_hot_name = Hotel_sel.getText();
		multiScreens.multiScreenShot(driver);
		
		//Get handles of the windows
			String MainWindow = driver.getWindowHandle();
			String mainWindowHandle = driver.getWindowHandle();
			Set<String> allWindowHandles = driver.getWindowHandles();
			Iterator<String> iterator = allWindowHandles.iterator();
		
		// Here we will check if child window has other child windows and will fetch the heading of the child window
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
	            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	            		driver.switchTo().window(ChildWindow);
	        }}
			multiScreens.multiScreenShot(driver);
		//Scroll bar navigation
		//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
		
		// Hotel name verification
			Hotel_Filter hot = new Hotel_Filter();
			String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
			
			try
			{
				Assert.assertEquals(exp_hot_name, act_hot_name);
				System.out.println("Hotel name matching:" + exp_hot_name);
			} catch (AssertionError e)
			{
				System.out.println("Hotel name mismatching");
				driver.close();
			}
	}
	
	public void image_Ar()
	{
		//Image verification
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
	}
	

	void description_Ar()
	{
		// Description verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]")).isDisplayed();
				System.out.println("Hotel description is available");
			} catch (Exception e)
			{
				System.out.println("Description missing");
				
			}
	}
	
	void roomtype_Ar()
	{
			
		//Type of rooms section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")).isDisplayed();
				System.out.println("Type of rooms section is available");
			} catch (Exception e)
			{
				System.out.println("Types of rooms section missing");
			}
			
		  //Room type images verification	
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img")).isDisplayed();
				System.out.println("Room type images are available");
			} catch (Exception e)
			{
				System.out.println("Images are missing");
			}
			
		 //Room type sharing details verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div")).isDisplayed();
				System.out.println("Sharing details available");
			} catch (Exception e)
			{
				System.out.println("Sharing details are not available");
			}	
			
		// Amenities section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]")).isDisplayed();
				System.out.println("Amenities section available");
			} catch (Exception e)
			{
				System.out.println("Amenities section not available");
			}
			
		// Rate section verification
			try
			{
				String rate = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).getText();
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).isDisplayed();
				//driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
				System.out.println("Selected currency" + rate);
				System.out.println("Rate section available");
			} catch (Exception e)
			{
				System.out.println("Rate section not available");
			}
		
		// Add Room section verification
			try
			{
				driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
				System.out.println("Add room section available");
			} catch (Exception e)
			{
				System.out.println("Add room section not available");
			}	
			
			((JavascriptExecutor)driver).executeScript("scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
	}		
		// 	Verification of Cancellation policies
	
public void cancellation_policy_Ar()
{
	
	String cancel_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div")).getText();
	System.out.println(cancel_type);
	if (cancel_type.equals("إلغاء مجاني")) {
		// Card Payment verification
			String exp_title1 ="سياسة الإلغاء";
			String exp_title2 ="بطاقه ائتمان";
			String exp_text1 = "إلغاء مجاني قبل الساعة 06:00:00 مساءً (قبل يوم واحد) ، قد يتم تطبيق رسوم الخدمة.\n\n" 
	
			+ "إذا تم الإلغاء بعد الساعة 06:00:00 مساءً (قبل يوم واحد) ، فسيتم تحصيل مبلغ إقامة الليلة الأولى.\n\n" 

			+ "لن يتم رد أي مبلغ إذا تم إلغاؤه بعد وقت تسجيل الوصول.\n\n"

			+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"

			+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.";

			String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
			String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
			String act_text1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]")).getText();
			try
			{
				Assert.assertEquals(exp_title1, act_title1);
				Assert.assertEquals(exp_title2, act_title2);
				Assert.assertEquals(exp_text1, act_text1);
				System.out.println("Cancellation policy for Card payment content and title is matching");
			} catch (Exception e)
			{
				System.out.println("Cancellation policy for Card payment content is not matching");
			}


				// Wire Transfer verification
					String exp_text11 = "تحويل بنكي";
					String exp_text12_ar = "إلغاء مجاني قبل الساعة 06:00:00 مساءً (قبل يوم واحد) ، قد يتم تطبيق رسوم الخدمة\n\n" 
			
					+ "في حالة الإلغاء بعد الساعة 06:00:00 مساءً (قبل يوم واحد) ، سيتم تحصيل مبلغ إقامة الليلة الأولى.\n\n" 

					+ "لن يتم رد أي مبلغ إذا تم إلغاؤه بعد وقت تسجيل الوصول.\n\n"

					+ "إذا لم يظهر الضيف في حجزه المؤكد ، فإننا نتعامل معه على أنه إلغاء ولن يتم رد أي مبلغ.\n\n"

					+ "لن تخضع ساعات تسجيل الوصول المتأخر لإعادة التمويل\n\n"
					
					+ "سيتم إلغاء الحجز المحجوز تلقائيًا في حالة فشل المعاملة.\n\n"
					
					+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"
					
					+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني الملكية مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.\n\n"

					+ "تسري المبالغ المستردة فقط عندما يتم تقديم طلب الإلغاء رسميًا من خلال منصة Namlatic.\n\n"
					
					+ "سوف تستغرق معالجة المبالغ المستردة القابلة للتطبيق من 5 إلى 7 أيام عمل.";
					
					String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
					String act_text12_ar = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
					try
					{
						Assert.assertEquals(exp_text11, act_text11);
						Assert.assertEquals(exp_text12_ar, act_text12_ar);
						System.out.println("Cancellation policy for Wire Transfer content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Wire Transfer content is not matching");
					}

				// Pay at hotel
					((JavascriptExecutor)driver).executeScript("scroll(0,'الدفع في الفندق')");					
					String exp_text21 = "الدفع في الفندق";
					String exp_text22 = "عند إجراء حجز باستخدام خيار \"الدفع في الفندق\" ، سيقوم الفندق المعني بتحصيل المبلغ بالكامل مقابل الحجز في وقت تسجيل الوصول.\n\n" 
			
					+ "يجب تأكيد الحجز قبل 48 ساعة من وقت تسجيل الوصول لتاريخ الرحلة عن طريق البريد الإلكتروني أو مكالمة مع Namlatic ، ويعتبر الحجز مؤكدًا فقط عند التحقق الشخصي ، وإلا فسيكون عرضة للإلغاء بموجب بند سياسة الإلغاء.\n\n" 

					+ "فيما يلي تفاصيل الاتصال:\n"
					
					+ "البريد الإلكتروني: Confirm@namlatic.com\n"
					
					+ "الخط الساخن Namlatic DZ: +213982414415\n\n"
					
					+ "بالنسبة للسائحين الدوليين ، سيتم تحصيل المبلغ بالعملة المحلية أو بأي عملة أخرى ، وفقًا لما يقرره الفندق.\n\n"
					
					+ "لأغراض أمنية ، يجب على المستخدم تقديم رقم التعريف الشخصي المكون من 4 أرقام الذي تم استلامه مع البريد الإلكتروني لتأكيد الحجز الخاص به ، ويجوز لـ Namlatic أو الفندق إلغاء الحجز في حالة إذا كان الرمز الذي تم العثور عليه غير صحيح.\n\n"

					+ "يسمح Namlatic بالإلغاء الجزئي والإلغاء الكامل للغرف المحجوزة قبل تسجيل الوصول.\n\n"
					
					+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء مع \"لا استرداد\"\n\n"
					
					+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي رد جزئي.\n\n"
					
					+ "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic Online Booking.\n\n"

					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"
					
					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.";
					
					String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
					String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
					System.out.println(act_text22);
					
					try
					{
						Assert.assertEquals(exp_text21, act_text21);
						Assert.assertEquals(exp_text22, act_text22);
						System.out.println("Cancellation policy for Pay at hotel content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Pay at hotel content is not matching");
					}
			
				// CIB
					((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
					String exp_text31 = "CIB";
					String exp_text32 = "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic اون لاين للحجز.\n\n" 
			
					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n" 

					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.\n\n"

					+ "سياسة الاسترجاع\n\n"
					
					+ "أي حجوزات يتم إلغاؤها بعد الساعة 6:00 مساءً من خلال منصة Namlatic ، سيتم استرداد جزء من المبلغ المدفوع من خلال بطاقة CIB (قد يتم تطبيق رسوم الإلغاء).\n\n"
					
					+ "قد تختلف رسوم الإلغاء ورسوم الخدمة المطبقة وفقًا للحجز ومكان الإقامة المختار.\n\n"
					
					+ "قد تستغرق عملية استرداد الأموال المطبقة 3-5 أيام عمل.\n\n"
					
					+ "عدم الحضور\n\n"
					
					+ "لن يتم رد أي أموال إذا لم يحضر الشخص إلى الفندق المعني في حجزه المؤكد.\n\n"
					
					+ "لا تقبل Namlatic أي التزام أو مسؤولية عن عواقب وصولك المتأخر أو أي إلغاء أو فرض رسوم عدم حضور من قبل مزود الملكية.\n\n"
					
					+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي استرداد.";

					String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
					String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
					try
					{
						Assert.assertEquals(exp_text31, act_text31);
						Assert.assertEquals(exp_text32, act_text32);
						System.out.println("Cancellation policy for CIB payment method content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for CIB payment method content is not matching");
					}
	}
	else if (cancel_type.equals("غير قابل للاسترجاع"))
	{
		// Card Payment verification
					String exp_title1 ="سياسة الإلغاء";
					String exp_title2 ="بطاقه ائتمان";
					String exp_text1 = "لن يتم رد أي مبلغ خلال أي وقت من الإلغاء.\n\n" 
			
					+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n" 

					+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.";

					String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
					String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
					String act_text1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]")).getText();
					try
					{
						Assert.assertEquals(exp_title1, act_title1);
						Assert.assertEquals(exp_title2, act_title2);
						Assert.assertEquals(exp_text1, act_text1);
						System.out.println("Cancellation policy for Card payment content and title is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Card payment content is not matching");
					}


						// Wire Transfer verification
							String exp_text11 = "تحويل بنكي";
							String exp_text12 = "لن يتم استرداد أي مبلغ في أي وقت من الإلغاء.\n\n" 
					
							+ "لا يسمح Namlatic بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n" 

							+ "3. سيتم إلغاء الحجز المحجوز تلقائيًا في حالة فشل المعاملة.";

							String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
							String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
							try
							{
								Assert.assertEquals(exp_text11, act_text11);
								Assert.assertEquals(exp_text12, act_text12);
								System.out.println("Cancellation policy for Wire Transfer content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for Wire Transfer content is not matching");
							}

						// Pay at hotel
							((JavascriptExecutor)driver).executeScript("scroll(0,'الدفع في الفندق')");					
							String exp_text21 = "الدفع في الفندق";
							String exp_text22 = "عند إجراء حجز باستخدام خيار \"الدفع في الفندق\" ، سيقوم الفندق المعني بتحصيل المبلغ بالكامل مقابل الحجز في وقت تسجيل الوصول.\n\n" 
					
							+ "يجب تأكيد الحجز قبل 48 ساعة من وقت تسجيل الوصول لتاريخ الرحلة عن طريق البريد الإلكتروني أو مكالمة مع Namlatic ، ويعتبر الحجز مؤكدًا فقط عند التحقق الشخصي ، وإلا فسيكون عرضة للإلغاء بموجب بند سياسة الإلغاء.\n\n" 

							+ "فيما يلي تفاصيل الاتصال:\n"
							
							+ "البريد الإلكتروني: Confirm@namlatic.com\n"
							
							+ "الخط الساخن Namlatic DZ: +213982414415\n\n"
							
							+ "بالنسبة للسائحين الدوليين ، سيتم تحصيل المبلغ بالعملة المحلية أو بأي عملة أخرى ، وفقًا لما يقرره الفندق.\n\n"
							
							+ "لأغراض أمنية ، يجب على المستخدم تقديم رقم التعريف الشخصي المكون من 4 أرقام الذي تم استلامه مع البريد الإلكتروني لتأكيد الحجز الخاص به ، ويجوز لـ Namlatic أو الفندق إلغاء الحجز في حالة إذا كان الرمز الذي تم العثور عليه غير صحيح.\n\n"

							+ "يسمح Namlatic بالإلغاء الجزئي والإلغاء الكامل للغرف المحجوزة قبل تسجيل الوصول.\n\n"
							
							+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء مع \"لا استرداد\"\n\n"
							
							+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي رد جزئي.\n\n"
							
							+ "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic Online Booking.\n\n"

							+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"
							
							+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.";
							
							String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
							String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
							try
							{
								Assert.assertEquals(exp_text21, act_text21);
								Assert.assertEquals(exp_text22, act_text22);
								System.out.println("Cancellation policy for Pay at hotel content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for Pay at hotel content is not matching");
							}
					
						// CIB
							((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
							String exp_text31 = "CIB";
							String exp_text32 = "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic اون لاين للحجز.\n\n" 
					
							+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n" 

							+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.\n\n"

							+ "سياسة الاسترجاع\n\n"
							
							+ "لا يمكن إلغاء أو رد الحجوزات التي تمت بشروط الفندق \"غير القابلة للاسترداد\".\n\n"
							
							+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء غير قابل للاسترداد مع \"عدم استرداد الأموال\"";
							
							String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
							String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
							try
							{
								Assert.assertEquals(exp_text31, act_text31);
								Assert.assertEquals(exp_text32, act_text32);
								System.out.println("Cancellation policy for CIB payment method content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for CIB payment method content is not matching");
							}
	}
	}

public void amenities_ver_Ar()
{
				// Amenities section2 verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div")).isDisplayed();
						System.out.println("Amenities section available");
					} catch (Exception e)
					{
						System.out.println("Amenities section not available");
					}
}

public void policy_ver_Ar()
{
				// Hotel Policy verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div")).isDisplayed();
						System.out.println("Hotel Policy section available");
					} catch (Exception e)
					{
						System.out.println("Hotel Policy section not available");
					}
}

public void book_now_Ar() throws InterruptedException, IOException
{				
		Date_room_selection_Ar();
	//only one option should be active in script.
		CIB_Ar();
		//Card_Payment();
		//Wire_Transfer();
}

public void Date_room_selection_Ar() throws InterruptedException, IOException
{
		//From and To Date Selection

		//WebElement date=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
		Thread.sleep(500);
	
		//WebElement navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", navigate);
		
		//	driver.findElement(By.xpath("//*[text()='29']")).click();
			//driver.findElement(By.xpath("//*[text()='30']")).click();
	
			
		//Room selection
			Thread.sleep(2000); 
			((JavascriptExecutor)driver).executeScript("scroll(0,700)");
			WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
			Select room_sel=new Select(element);
			room_sel.selectByVisibleText("1");
			WebElement firstEle=room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);

			//Book now button verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Book Now button available in booking section");
			} catch (Exception e)
			{
				System.out.println("Book Now button not available in Booking section");
			}
			
		//Click on book now button
			WebElement book_now = driver.findElement (By.xpath("//*[(text()='احجز الآن')]"));
					book_now.click();
					multiScreens.multiScreenShot(driver);
	
}					
		//Select payment methods ' CIB '  using radio button.
					public void CIB_Ar() throws IOException, InterruptedException
					{
						if (driver.findElement(By.id("cib")).isEnabled())
						{
							WebElement radio_cib=driver.findElement(By.id("cib"));
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

				//Select Terms and Conditions - check box
					WebElement checkbox_terms=driver.findElement(By.id("terms"));
					checkbox_terms.click();
					multiScreens.multiScreenShot(driver);
					System.out.println("CIB card selected");
						} else
						{
						//Currency selection
							WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
							Thread.sleep(500);
							cur.click();
							WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج دج')]"));
							cur_sel.click();
						
						//CIB selection and proceed
							Date_room_selection_Ar();									
							WebElement radio_cib=driver.findElement(By.id("cib"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
							Thread.sleep(500);
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

						//Select Terms and Conditions - check box
							WebElement checkbox_terms=driver.findElement(By.id("terms"));
							checkbox_terms.click();
							multiScreens.multiScreenShot(driver);
							System.out.println("CIB card selected");
						}
						}
						
					
			public void Card_Payment_Ar() throws IOException, InterruptedException
			{
				WebElement radio_card=driver.findElement(By.id("card"));
				radio_card.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Card Payment selected");
			}
			
			public void Wire_Transfer_Ar() throws IOException, InterruptedException
			{
				WebElement radio_wt=driver.findElement(By.id("wt"));
				radio_wt.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Wire Transfer selected");
			}

public void various_section_Validation_Ar() throws InterruptedException
{
		// Booking section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div")).isDisplayed();
				System.out.println("Booking section available");
			} catch (Exception e)
			{
				System.out.println("Booking section not available");
			}
			
			//Pricing
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]")).isDisplayed();
				System.out.println("Pricing details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Pricing details not available in Booking section");
			}
			
			// Calender
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).isDisplayed();
				System.out.println("Calender details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Calender details not available in Booking section");
			}
			
			//Guest
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Guest details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Guest details not available in Booking section");
			}
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,2000)");

		//Navigating to various section
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
			Thread.sleep(1000);
			//((JavascriptExecutor)driver).executeScript("scroll(0,400)");

			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
			Thread.sleep(2000);
			
		/* //Select reCAPTCHA Check box
			driver.switchTo().defaultContent().findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"));
			System.out.println("test");
			WebElement checkbox_recaptcha=driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
			checkbox_recaptcha.click();
			System.out.println("reCAPTCHA check box is selected"); 
		

		//Click on Proceed To Pay button
			WebElement Proceed_element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"));
			Proceed_element.click(); */
}			
}
