package Arabic_lang.Java_Files;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import English_Lang.Java_Files.Hotel_Booking;
import English_Lang.Java_Files.Hotel_Filter;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Main {
		
	public static void main (String args[]) throws Exception 
	{
	
		//Webdriver declaration
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
			
		 //Login Profile
			Namlatec_Login_Ar_old Nam_Login = new Namlatec_Login_Ar_old();
			Nam_Login.login_Ar();
			
		 //Hotel Filter
			Hotel_Filter_Ar Hotel_Fil = new Hotel_Filter_Ar();
			Hotel_Fil.Hotel_sel_Ar();
			
		//Hotel Booking
			Hotel_Booking_Ar Book = new Hotel_Booking_Ar();
			Book.Hotel_Book_Ar(); 
			
		//My Booking
			My_Booking_Ar MyBook_Ar = new My_Booking_Ar();
			MyBook_Ar.Booking_Ar(); 
		 
		//Cancellation
			Cancel_Booking_Ar CanBook = new Cancel_Booking_Ar();
			CanBook.Can_Book_Ar(); 
		 
		//Edit Profile
			Edit_Profile_Ar Edit_Prof_Ar = new Edit_Profile_Ar();
			Edit_Prof_Ar.Profileedit_Ar(); 
			
		//Signoff
			Signout Sgout = new Signout();
			Sgout.Signoff(); 
		
	}
				
			}
	