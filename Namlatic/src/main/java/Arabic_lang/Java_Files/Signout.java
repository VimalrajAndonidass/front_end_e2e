package Arabic_lang.Java_Files;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebElement;

public class Signout extends Namlatec_Login_Ar_old{
	void Signoff() throws InterruptedException, IOException
	{
	//Signout
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu1=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		Actions builder1=new Actions(driver);
		builder1.moveToElement(menu1).build().perform();
		WebDriverWait wait2=new WebDriverWait(driver,5);
		//wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM wEvDo']")));
		WebElement menu_signout=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[3]"));
		menu_signout.click();
		multiScreens.multiScreenShot(driver);
		System.out.println("Signout: PASS");
	}
}
