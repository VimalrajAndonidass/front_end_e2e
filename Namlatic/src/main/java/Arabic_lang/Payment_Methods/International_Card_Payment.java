package Arabic_lang.Payment_Methods;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class International_Card_Payment {
	public static WebDriver driver = null;

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\Arabic\\Booking\\", "Card_Payment");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void Launch() throws Exception {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\Arabic\\Booking\\" + "Card_Payment.html");

		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException {
		test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);

		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
		// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]"));
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='عربي')]"));
		chglang.click();
	}

	@Test(priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException {
		// Login
		test = report.startTest("Login Functionality and Elastic search verification");
		try {
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}

		// Hotel search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
			Hotel.sendKeys("عنابة");
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement btnclick1 = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
			btnclick1.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Hotel search working fine");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

	@Test(priority = 3, invocationCount = 1)
	void booking() throws IOException, InterruptedException {

		test = report.startTest("Hotel details page laoding and Hotel name verification");
		Thread.sleep(2000);
		WebElement Hotel_sel = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div[1]/div[1]/img"));

		Hotel_sel.click();
		String exp_hot_name = Hotel_sel.getText();
		System.out.println(exp_hot_name);
		multiScreens.multiScreenShot(driver);
		// Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		// Scroll bar navigation
		// ((JavascriptExecutor)driver).executeScript("scroll(0,1601)");

		// Hotel name verification
		// Hotel_Filter hot = new Hotel_Filter();
		Thread.sleep(2000);
		String act_hot_name = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]"))
				.getText();
		try {
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}

	}

	@Test(priority = 4, invocationCount = 1)
	/*
	 * public void booking_confirmation() throws InterruptedException, IOException {
	 * Date_room_selection(); Card(); }
	 */

	public void Date_room_selection() throws InterruptedException, IOException {
		test = report.startTest("Date and Room selection section verification");
		try {
			/*
			 * //From and To Date Selection //WebElement date =
			 * driver.findElement(By.xpath("//*[text()='Description"));
			 * //((JavascriptExecutor)
			 * driver).executeScript("arguments[0].scrollIntoView(true);",date);
			 * Thread.sleep(3000); driver.findElement(By.xpath(
			 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div"
			 * )).click(); Thread.sleep(500);
			 * 
			 * WebElement month = driver.findElement(By.xpath(
			 * "//select[contains(@class,'DateRangePicker__MonthHeaderSelect')]")); Select
			 * month_pick=new Select(month); month_pick.selectByVisibleText("October");
			 * month_pick.getFirstSelectedOption();
			 * 
			 * Thread.sleep(500); WebElement start_date =
			 * driver.findElement(By.xpath("//*[@text()='1']")); start_date.click();
			 * 
			 * Thread.sleep(500); WebElement end_date =
			 * driver.findElement(By.xpath("//*[@text()='10']")); end_date.click();
			 */
			// Room selection
			Thread.sleep(3000);
			WebElement scroll = driver.findElement(By.xpath("//*[text()='أنواع الغرف']"));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
			WebElement element = driver.findElement(
					By.xpath("//select[contains(@class,'sc-oTpcS hKhvoq') or contains (@class,'sc-qQKPx ksBkya')]"));
			Select room_sel = new Select(element);
			room_sel.selectByVisibleText("1");
			room_sel.getFirstSelectedOption();

			/*
			 * WebElement element1 =driver.findElement(By.
			 * xpath("//select[contains(@class,'sc-oTpcS hKhvoq')][2]")); Select
			 * room_sel1=new Select(element1); room_sel1.selectByVisibleText("2");
			 * room_sel1.getFirstSelectedOption();
			 */

			multiScreens.multiScreenShot(driver);
			Thread.sleep(2000);

			test.log(LogStatus.PASS, "Rooms selected successfully");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select Rooms");
		}

		try {
			// Book now button verification
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(2000);
			// WebElement book_scroll
			// =driver.findElement(By.xpath("//select[contains(@class,'sc-pTUKB
			// hbNNSd')]"));
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);",book_scroll);
			Thread.sleep(2000);
			WebElement book_now = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
			Thread.sleep(2000);
			book_now.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");
		}

	}

	@Test(priority = 5, invocationCount = 1)
	public void Card() throws IOException, InterruptedException {
		test = report.startTest("Payment Method verification");
		try {
			WebElement radio_card = driver.findElement(By.id("card"));
			radio_card.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "Card Payment selected");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select International card");
		}

//Click on Proceed To Pay button
		try {
			WebElement book_now_scroll = driver.findElement(By.xpath("//div[contains(text(),'72 ساعة')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", book_now_scroll);
			Thread.sleep(3000);
			WebElement Proceed_element = driver.findElement(By.xpath("//*[text()='احجز الآن']"));
			Proceed_element.click();

			WebElement CC_scroll = driver.findElement(By.xpath("//div[contains(text(),'وصف')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", CC_scroll);

			Thread.sleep(3000);
			// Switch to frame:
			// v1 by containing name, probably the safest option

			WebElement iframe_by_name_contains = driver
					.findElement(By.xpath("//iframe[contains(@name,'__privateStripeFrame')]"));
			// iframe_by_name_contains.
			System.out.println(iframe_by_name_contains.isEnabled());
			driver.switchTo().frame(iframe_by_name_contains);
			Thread.sleep(5000);
			new Actions(driver).moveToElement(driver.findElement(By.xpath("//*[contains(@name, 'cardnumber')]")))
					.click().sendKeys("4242").build().perform();
			// get input field
			System.out.println(driver.findElement(By.name("cardnumber")).getAttribute("placeholder"));
			System.out.println(driver.findElement(By.name("cardnumber")).getAttribute("value"));
			System.out.println(driver.findElement(By.name("cardnumber")).isSelected());

			driver.findElement(By.name("cardnumber")).sendKeys("22222");
			driver.findElement(By.xpath("//*[contains(@value,'')]")).sendKeys("4242 4242 4242 4242");

			Thread.sleep(15000);
			// WebElement cardno=
			// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[3]/form/div[1]"));
			// cardno.sendKeys("4242 4242 4242 4242");

			/*
			 * WebElement card_no = driver.findElement(By.xpath("")); //card_no.click();
			 * System.out.println(card_no.getAttribute("placeholder"));
			 * card_no.sendKeys("4242424242424242");
			 */

			WebElement card_name = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[3]/form/input"));
			card_name.click();
			card_name.sendKeys("Test");
			card_name.sendKeys(Keys.TAB);
			Thread.sleep(10000);

			// driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[3]")));
			WebElement exp_date = driver
					.findElement(By.xpath("//input[@class,'InputElement is-empty Input Input--empty']"));
			// exp_date.click();
			exp_date.sendKeys("0222");

			WebElement cvv = driver.findElement(By.xpath(""));
			// cvv.click();
			cvv.sendKeys("123");

			WebElement Proceed_to_pay = driver.findElement(By.xpath("//input[contains(@placeholder,'CVV)]"));
			Proceed_to_pay.click();

			test.log(LogStatus.PASS, "Payment done successfully via Card Payment");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Payment failed");
		}
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}
