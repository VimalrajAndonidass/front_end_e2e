package English_Lang.Cancellation.WT_Cancellation.NonRefund_Cancel.Ongoing;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;


public class Single_Room {
	public static WebDriver driver = null;


	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\English","English");
	static ExtentTest test;
	static ExtentReports report;
		

	@BeforeTest	
		public void Launch() throws Exception {
			
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\"+"WT_NonRefund_Cancel_Ong_Single_ExtentReportResults.html");
		
	    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	@Test (priority = 0, invocationCount = 1)
	public void startTest() throws IOException
	{
		test = report.startTest("Scenario");
		try
		{
			test.log(LogStatus.PASS, "Ongoing Non Refundable cancellation for WT payment method with Single rooms(English)");
		} catch (AssertionError e)
		{
			
		}
	}
	@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}
	@Test (priority = 2, invocationCount = 1)
	public void Lang_Selection() throws IOException
	{
	test = report.startTest("Language Selection");
	// Language change
	try
	{
				WebElement language = driver.findElement(By.xpath("//*[(text()='Fran�ais')]"));
				language.click();
				WebElement chglang = language.findElement(By.xpath("//*[(text()='English')]"));
				chglang.click();
				test.log(LogStatus.PASS, "English language changed successfully");
	}
	catch(AssertionError e)
	{
		test.log(LogStatus.PASS, "English language not changed");
	}
	}

	@Test (priority = 3, invocationCount = 1)
	public void Login() throws IOException, InterruptedException
	{
	// Login 		
	test = report.startTest("Login Functionality verification");
	try
	{
		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login')]"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
		uname.sendKeys("tnamlatic@gmail.com");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
		pwd.sendKeys("Test123456");
		multiScreens.multiScreenShot(driver);
		pwd.sendKeys(Keys.ENTER);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Login Failed");
	}
	}
	@Test (priority = 4, invocationCount = 1)
	public void Myprofile_Menu() throws IOException, InterruptedException
	{
	// Click on my profile menu		
	test = report.startTest("Select my profile menu");
	try
	{
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span")).click();
		//Actions builder=new Actions(driver);
		//builder.moveToElement(menu).build().perform();
		WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		test.log(LogStatus.PASS, "My booking page - Menu option 2 selected");
	}
	catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Unable to select My Profile Menu");	
	}
	}

//Single room cancellation for WT payment - Ongoing Booking Section
	@Test (priority = 5, invocationCount = 1)
	public void Cancel_Single_Room_Ong() throws IOException, InterruptedException
	{
	// Click on cancel booking on Ongoing section
	test = report.startTest("Select single room and cancel it - Ongoing section(WT)");
	try
	{
		//WebElement scroll = driver.findElement(By.xpath("//div[contains(text(),'NARZLIBWTD8')]"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,1500)");				
		Thread.sleep(3000);
		WebElement cancel = driver.findElement(By.xpath("//div[contains(text(),'NV2Z5TDGL8Q')]/following::span[text()='Wire transfer']/following::div[text()='Cancel booking']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
		cancel.click();
		Thread.sleep(1000);
		
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "WT payment on Ongoing section - Select single room and cancel is working");	
//Single room type cancellation - Ongoing section		
		//print room types
				String room_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[1]")).getText();
				Thread.sleep(1000);
				test.log(LogStatus.PASS, "Print Room type: "+room_type);
				
				//Print room price value
				String room_price = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[2]")).getText();
				Thread.sleep(1000);
				test.log(LogStatus.PASS, "Print Room Price: "+room_price);
				
				
				//Print sharing and room count
				String shar_room = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[3]/div[3]")).getText();
				Thread.sleep(1000);
				test.log(LogStatus.PASS, "Print sharing and room count: "+shar_room);
				
				//Print cancellation types
				String cancel_types = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[5]/div")).getText();
				Thread.sleep(1000);
				test.log(LogStatus.PASS, "Print cancellation type: "+cancel_types);
		
				driver.findElement(By.id("reason-4")).click();
				Thread.sleep(1000);
				//Please mention the reason
		 		driver.findElement(By.xpath("//*[contains(@placeholder, 'Please mention the reason')]")).sendKeys("Test other reason"); //Enter other reason
				Thread.sleep(1000);
		WebElement scroll1 = driver.findElement(By.xpath("//div[contains(text(),'Yes, cancel this booking')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll1);
		scroll1.click();
		Thread.sleep(1000);
		
		// Click on OK button - To close a cancel successful pop-up window.
		((JavascriptExecutor)driver).executeScript("scroll(0,600)");				
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[contains(text(),'OK')]")).click();
		Thread.sleep(1000);
	}
	catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "WT payment on Ongoing section - Select single room and cancel not working");	
	}
	}
	
@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
}
}
