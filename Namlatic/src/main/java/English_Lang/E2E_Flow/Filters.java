package English_Lang.E2E_Flow;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Filters {
	public static WebDriver driver = null;

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\English\\", "Filter_Verfication");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void Launch() throws Exception {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\English\\" + "Filter_verification.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Filter Verification");
		try {
			test.log(LogStatus.PASS, "Filter Verification");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException, InterruptedException {
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);

		// Language change
		Thread.sleep(3000);
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div"));
		chglang.click();
	}

	@Test(priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException {
		// Login
		test = report.startTest("Login Functionality");
		try {
			Thread.sleep(3000);
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123456");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}

		// Hotel search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));
			Hotel.sendKeys("Louie Palace Pondicherry");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");

			String Hotel_name = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"))
					.getText();
			System.out.println(Hotel_name);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Search working fine");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to search hotel with given keyword");
		}
		
		report.endTest(test);

	}

	// --- Hotel Search screen validation---

	@Test(priority = 3)
	public void Sorting() throws IOException, InterruptedException {
		// Price filter verification
		// Title Verification
		test = report.startTest("Sorting verification");
		Thread.sleep(3000);

		String act_Price_title = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/div[1]/div"))
				.getText();
		String exp_Price_title = "Price";
		try {
			Assert.assertEquals(exp_Price_title, act_Price_title);
			test.log(LogStatus.PASS, "Price filter menu available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price filter menu not available");
		}
		multiScreens.multiScreenShot(driver);

		// Lowest price verification
		WebElement lowprice = driver.findElement(
				By.xpath("//*[@id=\"filter_price_low_to_high\"]"));

		try {
			test.log(LogStatus.PASS, "Lowest price is enabled: " + lowprice.isDisplayed());

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Lowest price is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Highest price verifcation
		WebElement highprice = driver.findElement(
				By.xpath("//*[@id=\"filter_price_high_to_low\"]"));

		try {
			test.log(LogStatus.PASS, "Highest price is enabled: " + highprice.isDisplayed());
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Lowest price is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Slider verification
		WebElement slider = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/div[2]/div[1]/div/div"));
		// WebElement slider2 =
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[6]"));
		try {
			test.log(LogStatus.PASS, "Price slider1 is enabled: " + slider.isDisplayed());
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price slider is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Slider navigation
		Actions act = new Actions(driver);
		act.dragAndDropBy(slider, 0, 80).build().perform();
		// act.dragAndDropBy(slider, -20, 0).build().perform();
		multiScreens.multiScreenShot(driver);

		// Price low to high
		String act_lowtohigh = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/div[2]/div[2]/label"))
				.getText();
		String exp_lowtohigh = "price low to high";
		WebElement lowtohigh = driver.findElement(By.id("filter_price_low_to_high"));

		try {
			Assert.assertEquals(exp_lowtohigh, act_lowtohigh);
			System.out.println("Low to high option button is displayed:" + lowtohigh.isDisplayed());
			System.out.println("Low to high option button is selected:" + lowtohigh.isSelected());
			driver.findElement(By.xpath("//*[@id=\"filter_price_low_to_high\"]")).click();
			test.log(LogStatus.PASS, "Price - low to high is displaying and enabled");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price - low to high is missing or not enabled to select");
		}
		multiScreens.multiScreenShot(driver);

		// Price high to low
		String act_hightolow = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
				.getText();
		String exp_hightolow = "price high to low";
		WebElement hightolow = driver.findElement(By.id("filter_price_high_to_low"));

		try {
			Assert.assertEquals(exp_hightolow, act_hightolow);
			System.out.println("High to Low option button is displayed:" + hightolow.isDisplayed());
			System.out.println("High to Low option button is selected:" + hightolow.isSelected());
			driver.findElement(By.xpath("//*[@id=\"filter_price_high_to_low\"]")).click();
			test.log(LogStatus.PASS, "Price - high to low is displaying and enabled");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price - high to low is missing or not enabled to select");

		}
		multiScreens.multiScreenShot(driver);

		// Price filter enable and disable
		// Deselect
		WebElement pricefilter = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/div[1]/div"));
		pricefilter.click();
		Thread.sleep(3000);
		// Select
		pricefilter.click();
		multiScreens.multiScreenShot(driver);

	}

	@Test(priority = 4)
	public void Nearby_filter() throws InterruptedException, IOException
	{
		test=report.startTest("Nearby Filter Verification");
		WebElement Nearby = driver.findElement(By.xpath("//*[contains(text(),'Nearby filter')]"));
		Nearby.click();
		Thread.sleep(3000);
		//Select
		Nearby.click();
		multiScreens.multiScreenShot(driver);

		// Title verification
				String act_pop_title = driver
						.findElement(By.xpath("//*[contains(text(),'Nearby filter')]"))
						.getText();
				String exp_pop_title = "Nearby filter";
				try {
					Assert.assertEquals(exp_pop_title, act_pop_title);
					System.out.println("Nearby title is matching");
					test.log(LogStatus.PASS, "Nearby title is matching");
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Nearby title is not matching");
				}
				multiScreens.multiScreenShot(driver);
				
				// City center (urban) verification
				String act_near1 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[1]/label"))
						.getText();
				
				String exp_near1 = "city center ( urban)";
				System.out.println(act_near1);
				System.out.println(exp_near1);
				try {
					Assert.assertEquals(exp_near1, act_near1);
					test.log(LogStatus.PASS, "City center (urban) is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_city_center\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "City center (urban) is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Beach verification
				String act_near2 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[2]/label"))
						.getText();
				String exp_near2 = "beach";
				try {
					Assert.assertEquals(exp_near2, act_near2);
					test.log(LogStatus.PASS, "Beach is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_beach\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Beach is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Sahara verification
				String act_near3 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[3]/label"))
						.getText();
				String exp_near3 = "sahara";
				try {
					Assert.assertEquals(exp_near3, act_near3);
					test.log(LogStatus.PASS, "Sahara is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_sahara\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Sahara is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Mountain verification
				String act_near4 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[4]/label"))
						.getText();
				String exp_near4 = "mountain";
				try {
					Assert.assertEquals(exp_near4, act_near4);
					test.log(LogStatus.PASS, "Mountain is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_mountain\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Mountiain is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Airport verification
				String act_near5 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[5]/label"))
						.getText();
				String exp_near5 = "airport";
				try {
					Assert.assertEquals(exp_near5, act_near5);
					test.log(LogStatus.PASS, "Airport is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_airport\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Airport is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Spa verification
				String act_near6 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[6]/label"))
						.getText();
				String exp_near6 = "spa resort";
				try {
					Assert.assertEquals(exp_near6, act_near6);
					test.log(LogStatus.PASS, "Spa resort is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_spa_resort\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Spa resort is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
				
				// Transport verification
				String act_near7 = driver
						.findElement(
								By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/div[3]/div[2]/div[7]/label"))
						.getText();
				String exp_near7 = "transport station (metro & train)";
				try {
					Assert.assertEquals(exp_near7, act_near7);
					test.log(LogStatus.PASS, "Transport is available in Nearby filter");
					driver.findElement(By.xpath("//*[@id=\"filter_transport_station\"]")).click();
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Transport resort is not available in Nearby filter");
				}
				multiScreens.multiScreenShot(driver);
	}
	
	@Test(priority = 5, enabled=false)
	public void popular_sr() throws InterruptedException, IOException {
		test = report.startTest("Popular search verification");
		// Popular search popular filter enable and disable
		// Deselect
		WebElement popularfilter = driver.findElement(By.xpath("//*[contains(text(),'Popular Searches')]"));
		popularfilter.click();
		Thread.sleep(3000);
		// Select
		popularfilter.click();
		multiScreens.multiScreenShot(driver);

		// Title verification
		String act_pop_title = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[1]/div"))
				.getText();
		String exp_pop_title = "popular searches";
		try {
			Assert.assertEquals(exp_pop_title, act_pop_title);
			System.out.println("Popular search title is matching");
			test.log(LogStatus.PASS, "Popular search title is matching");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Popular search title is not matching");
		}
		multiScreens.multiScreenShot(driver);

		// Couple Friendly verification
		String act_pop1 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[1]/label"))
				.getText();
		String exp_pop1 = "couple friendly";
		try {
			Assert.assertEquals(exp_pop1, act_pop1);
			test.log(LogStatus.PASS, "Couple friendly is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_couple_friendly\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Couple friendly is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Air Conditioning verification
		String act_pop2 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[2]/label"))
				.getText();
		String exp_pop2 = "air conditioning";
		try {
			Assert.assertEquals(exp_pop2, act_pop2);
			test.log(LogStatus.PASS, "Air Conditioning is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_air_conditioning\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Air Conditioning is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Free Breakfast verification
		String act_pop3 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[3]/label"))
				.getText();
		String exp_pop3 = "free breakfast";
		try {
			Assert.assertEquals(exp_pop3, act_pop3);
			test.log(LogStatus.PASS, "Free Breakfast is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_free_breakfast\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free Breakfast is available in Popular search");
		}
		// Removing hubspot
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		Thread.sleep(2000);
		driver.switchTo().parentFrame(); // Parent Frame
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Free Cancellation Policy verification
		String act_pop4 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[4]/label"))
				.getText();
		System.out.println(act_pop4);
		String exp_pop4 = "free cancellation policy";
		try {
			Assert.assertEquals(exp_pop4, act_pop4);
			test.log(LogStatus.PASS, "Free Cancellation is available in Popular search");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='filter_free_cancellation_policy']")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free Cancellation is available in Popular search");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Pay at hotel verification
		String act_pop5 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[5]/label"))
				.getText();
		String exp_pop5 = "pay at hotel";
		try {
			Assert.assertEquals(exp_pop5, act_pop5);
			test.log(LogStatus.PASS, "Pay at hotel is available in Popular search");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='filter_pay_at_hotel']")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Pay at hotel is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Deselect
		popularfilter.click();
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		Thread.sleep(2000);
	}

	@Test(priority = 6, enabled=false)
	public void Amenities() throws InterruptedException, IOException {
		test = report.startTest("Amenities section verification");
		// Amenities filter enable and disable
		// Deselect
		Thread.sleep(3000);
		WebElement amenitiesfilter = driver.findElement(By.xpath("//*[contains(text(),'Amenities')]"));
		Thread.sleep(3000);
		amenitiesfilter.click();
		Thread.sleep(3000);
		// Select
		amenitiesfilter.click();
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,400)");

		// Title verification
		String act_amn_title = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[1]/div"))
				.getText();
		String exp_amn_title = "amenities";
		try {
			Assert.assertEquals(exp_amn_title, act_amn_title);
			test.log(LogStatus.PASS, "Amenities title is matching");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Amenities title is mismatching");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Free Wi-fi verification
		String act_amn1 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[1]/label"))
				.getText();
		String exp_amn1 = "free wi-fi";
		try {
			Assert.assertEquals(exp_amn1, act_amn1);
			test.log(LogStatus.PASS, "Free Wifi is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_free_wi_fi\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free wifi is not available in Amenities");
		}

		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Power Backup verification
		Thread.sleep(2000);
		String act_amn2 = driver.findElement(By.xpath("//*[contains(text(),'Power Backup')]")).getText();
		// html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[2]/label")).getText();
		String exp_amn2 = "power backup";
		System.out.println(act_amn2);
		try {
			Assert.assertEquals(exp_amn2, act_amn2);

			test.log(LogStatus.PASS, "Power Backup is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_power_backup\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Power Backup is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Gardern verification
		Thread.sleep(2000);
		String act_amn3 = driver.findElement(By.xpath("//*[contains(text(),'Garden/ Backyard')]")).getText();
		String exp_amn3 = "garden/ backyard";
		System.out.println(act_amn3);

		try {
			Assert.assertEquals(exp_amn3, act_amn3);
			test.log(LogStatus.PASS, "Gardern is available in Amenities");

			driver.findElement(By.xpath("//*[@id=\"filter_garden__backyard\"]")).click();
		} catch (AssertionError e) {

			test.log(LogStatus.FAIL, "Gardern is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// SPA verification
		Thread.sleep(2000);
		String act_amn4 = driver.findElement(By.xpath("//*[contains(text(),'Spa')]")).getText();
		String exp_amn4 = "spa";
		try {
			Assert.assertEquals(exp_amn4, act_amn4);

			test.log(LogStatus.PASS, "SPA is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_spa\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "SPA is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Swimming pool verification
		Thread.sleep(2000);
		String act_amn5 = driver.findElement(By.xpath("//*[contains(text(),'Swimming Pool')]")).getText();
		String exp_amn5 = "swimming pool";
		try {
			Assert.assertEquals(exp_amn5, act_amn5);
			test.log(LogStatus.PASS, "Swimming pool is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_swimming_pool\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Swmimming pool is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Gym verification
		Thread.sleep(2000);
		String act_amn6 = driver.findElement(By.xpath("//*[contains(text(),'Gym')]")).getText();
		String exp_amn6 = "gym";
		try {
			Assert.assertEquals(exp_amn6, act_amn6);
			test.log(LogStatus.PASS, "GYM is available in Amenities");

			driver.findElement(By.xpath("//*[@id=\"filter_gym\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "GYM is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Laundry verification
		Thread.sleep(2000);
		String act_amn7 = driver.findElement(By.xpath("//*[contains(text(),'Laundry services')]")).getText();
		String exp_amn7 = "laundry services";
		try {
			Assert.assertEquals(exp_amn7, act_amn7);
			test.log(LogStatus.PASS, "Laundry service is available in Amenities");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='filter_laundry_services__washing_machine']")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Laundry is not available in Amenities");

		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Cafe verification
		Thread.sleep(2000);
		String act_amn8 = driver.findElement(By.xpath("//*[contains(text(),'Café')]")).getText();
		String exp_amn8 = "café";
		try {
			Assert.assertEquals(exp_amn8, act_amn8);

			test.log(LogStatus.PASS, "Café is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_caf_\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Café is not available in Amenities");

		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Pet friendly verification
		Thread.sleep(2000);
		String act_amn9 = driver.findElement(By.xpath("//*[contains(text(),'Pet Friendly')]")).getText();
		String exp_amn9 = "pet friendly";
		try {
			Assert.assertEquals(exp_amn9, act_amn9);
			test.log(LogStatus.PASS, "Pet friendly is available in Amenities");

			driver.findElement(By.xpath("//*[@id=\"filter_pet_friendly\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Pet friendly is not available in Amenities");

		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,800)");

		// In House restaurant verification
		Thread.sleep(2000);
		String act_amn10 = driver.findElement(By.xpath("//*[contains(text(),'In-house Restaurant')]")).getText();
		String exp_amn10 = "in-house restaurant";
		try {
			Assert.assertEquals(exp_amn10, act_amn10);
			System.out.println("In-House restaurant is available in Amenities");
			test.log(LogStatus.PASS, "In-House restaurant is available in Amenities");

			driver.findElement(By.xpath("//*[@id='filter_in_house_restaurant']")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "In-House restaurant is not available in Amenities");

		}

		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,750)");

		// Card Payment verification
		Thread.sleep(2000);
		String act_amn11 = driver.findElement(By.xpath("//*[contains(text(),'Card Payment')]")).getText();
		String exp_amn11 = "card payment";
		try {
			Assert.assertEquals(exp_amn11, act_amn11);
			System.out.println("Card Payment is available in Amenities");
			test.log(LogStatus.PASS, "Card Payment is available in Amenities");

			driver.findElement(By.xpath("//*[@id=\"filter_card_payment\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Card Payment restaurant is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		((JavascriptExecutor) driver).executeScript("scroll(0,750)");

	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
	}
}
