package English_Lang.E2E_Flow;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import multiScreenShot.MultiScreenShot;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class Landing_Page {
public static WebDriver driver = null;


public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\English\\","E2E_Flow");
static ExtentTest test;
static ExtentReports report;
	
@BeforeTest	
public void Launch() throws Exception {
		
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
	driver = new ChromeDriver();
	report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\English\\"+"E2E_Flow.html");
	
    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));

	/*	URL_Launch();
		Title_Verification();
		Header_Verification();
		Home_page_text_Verification();
		Carousel_Verification();
		Why_choose_us_Verification();
		Help_Section_Verification();
		Covid_Measures_Verification();
		Footer_Verification();
		screenshots(); 
		Login();
		System.out.println("Login: PASS"); */
	}
	
@Test (priority = 0, invocationCount = 1)
public void startTest() throws IOException
{
	test = report.startTest("Language");
	try
	{
		test.log(LogStatus.PASS, "Namlatic - English");
	} catch (AssertionError e)
	{
		
	}
}

@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}

@Test (priority = 2, invocationCount = 1)
	public void Title_Verification() throws IOException
	{
	test = report.startTest("Title Verification");
	// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();	
	//Title Verification
				String ExpectedTitle = "Namlatic hotel booking - Hotel booking platform";
				try {
						AssertJUnit.assertEquals(ExpectedTitle, driver.getTitle());
						test.log(LogStatus.PASS, "Title Matching - Pass");
				}
					catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Title mismatches" + ExpectedTitle + " expected :" +driver.getTitle() );
					}
		
		
	}


@Test (priority = 3, invocationCount = 1)
	public void Header_Verification()
	{
	test = report.startTest("Header Verification");
				//Header Verification
			//Logo verification
				WebElement Header_Logo = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/a/img"));
				try
				{
					Header_Logo.isDisplayed();
					test.log(LogStatus.PASS, "Logo available - Pass");
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Logo not available - Fail");
				}
			
			//Login
				//Ensure the availability of Login button
						try {
							driver.findElement(By.xpath("//*[(text()='Login / Signup')]")).isDisplayed();
							driver.findElement(By.xpath("//*[(text()='Login / Signup')]")).isEnabled(); 
							test.log(LogStatus.PASS, "Login Button available - Pass");
							} catch (Exception e)
							{
							test.log(LogStatus.FAIL, "Login Button not available - Fail");
							}
			//Enroll hotel
				// Ensure the availability of Enroll hotel 
						try {
							driver.findElement(By.xpath("//*[(text()='Enroll Hotels')]")).isDisplayed();
							driver.findElement(By.xpath("//*[(text()='Enroll Hotels')]")).isEnabled(); 
							test.log(LogStatus.PASS, "Enroll button available - Pass");
							} catch (Exception e)
							{
							test.log(LogStatus.FAIL, "Enroll button not available - Fail");
							}
			
			//Language Verification
				// Ensure the availability of Language options
						driver.findElement(By.xpath("//div[@class='sc-fzoxKX craeZe']")).click();
						try {
							
							driver.findElement(By.xpath("//*[(text()='English')]")).isDisplayed();
							driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled(); 
							driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
							test.log(LogStatus.PASS, "Language selection option available and selectable - Pass");
							} catch (Exception e)
							{
							test.log(LogStatus.FAIL, "Language selection either not available or not selectable -  Fail");
							}
				// Ensure the availability of Currency options
						try {
							driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]"));
							for (int i = 0; i < 2 ; i++) {
							   // Assert.assertEquals(i, currency.getText());
							    }
							//driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled(); 
							//driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
							test.log(LogStatus.PASS, "Currency selection option is available and selectable - Pass");
						} catch (Exception e)
						{
							test.log(LogStatus.FAIL, "Currency selection either not available or not selectable - Fail");
						} 
					
				// Ensure the availability of Elastic search 
						try {
							driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[1]/div/div[1]/div/input")).isDisplayed();
							test.log(LogStatus.PASS, "Elastic search option is available and allowed to search");
						} catch (Exception e)
						{
							test.log(LogStatus.FAIL, "Elastic search option is not available");
						}
						driver.findElement(By.xpath("//div[@class='sc-fzoxKX craeZe']")).click();

			}
	

@Test (priority = 4, invocationCount = 1)
	public void Home_page_text_Verification() throws IOException, InterruptedException
	{
	test = report.startTest("Home Page Verification");
				// Home page text validation
					String Exptext1 = "Algeria's First Hotel Booking Platform";
					String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[1]")).getText());
					
					String Exptext2 = "Stay with us, feel at home";
					String Acttext2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]")).getText();
					
					String Exptext3 = "Live an unforgettable experience in your dream hotel with Namlatic";
					String Acttext3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[3]")).getText();				
					try
					{
					AssertJUnit.assertEquals(Exptext1, Acttext1);	
					AssertJUnit.assertEquals(Exptext2, Acttext2);
					AssertJUnit.assertEquals(Exptext3, Acttext3);
					test.log(LogStatus.PASS, "Home Page text verified, Pass");
					}
					catch (AssertionError e)
					  {
						  
						test.log(LogStatus.FAIL, "Home page text mismatches with expected ");
					  }
					
					
					
			// Ensure the availability of search button
					try {
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div")).isDisplayed();
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div")).isEnabled();
						test.log(LogStatus.PASS, "Search button is available and allowed to select");
						} catch (Exception e)
						{
							test.log(LogStatus.FAIL, "Search button either not available or not able to selectable");
						}
	}

@Test (priority = 5, invocationCount = 1)
public void Carousel_Verification() throws IOException, InterruptedException
{
	test = report.startTest("Carousel section Verification");
			// Ensure the title text of carousel image
				/*	try {
						WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[1]"));
						String Expected_title = "You have a destination, we'll get you the perfect place to stay!";
						String Actual_title = title.getText();
						System.out.println(title.getText());
						AssertJUnit.assertEquals(Expected_title,Actual_title);
						System.out.println("Carousel title is matching with expected");
						test.log(LogStatus.PASS, "Carousel title is matching with expected");

						} catch (Exception e)
						{
							System.out.println("Carousel title mismatching with expected");
							test.log(LogStatus.FAIL, "Carousel title mismatching with expected");
						}		
*/
		
		// Carousel image1 title verification
	
					WebElement img1_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[5]/div/wrapper/div/div[1]/div"));
					String Act_img1_title = img1_title.getText();
					String Exp_img1_title = "Alger";
					try {
						AssertJUnit.assertEquals(Exp_img1_title, Act_img1_title);			
						System.out.println("Title matching with expected: " + Exp_img1_title);	
						test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img1_title);
						} catch (Exception e)
						{
							System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img1_title + "Expected Title:" +Exp_img1_title);
							test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img1_title + "Expected Title:" +Exp_img1_title);
						}
	
	
	/*	// Carousel image1 text verification
					WebElement Cr_img1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[2]"));
					String Act_img1_text = Cr_img1.getText();
					String Exp_img1_text = "Discover Algiers, nicknamed \"the white one\", \"the beloved one\", this dream city of the Mediterranean.";
					try {
						AssertJUnit.assertEquals(Exp_img1_text, Act_img1_text);
						System.out.println("Carousel image 1 'Alger' text is matching with expected");
						test.log(LogStatus.PASS, "Carousel image 1 'Alger' text is matching with expected");

						} catch (Exception e)
						{
							System.out.println("Not Matching, Expected:" + Exp_img1_text +"Actual:"+ Act_img1_text);
							test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img1_text +"Actual:"+ Act_img1_text);

						}
					multiScreens.multiScreenShot(driver);
		*/
		// Carousel image2 title verification
					WebElement img2_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[6]/div/wrapper/div/div[1]/div"));
					String Act_img2_title = img2_title.getText();
					String Exp_img2_title = "Annaba";
					try {
						AssertJUnit.assertEquals(Exp_img2_title, Act_img2_title);			
						System.out.println("Title matching with expected: " + Exp_img2_title);
						test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img2_title);

					} catch (Exception e)
					{
						System.out.println("Title mismatching with expected: " + "Acual Title:" + Act_img2_title + "Expected Title:" +Exp_img2_title);
						test.log(LogStatus.FAIL, "Title mismatching with expected:" + "Acual Title:" + Act_img2_title + "Expected Title:" +Exp_img2_title);

					}
				
		/* // Carousel image2 text verification
					WebElement Cr_img2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[2]"));
					String Act_img2_text = Cr_img2.getText();
					String Exp_img2_text = "All the beauty of Algeria, Between the corniche, the seafront, and the historic city center, Annaba is a complete destination that should please you";	
					try {
						AssertJUnit.assertEquals(Exp_img2_text, Act_img2_text);
						System.out.println("Carousel image 2 'Annaba' text is matching with expected");
						test.log(LogStatus.PASS, "Carousel image 2 'Annaba' text is matching with expected");

					} catch (Exception e)
					{
						System.out.println("Not Matching, Expected:" + Exp_img2_text +"Actual:"+ Act_img2_text);
						test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img2_text +"Actual:"+ Act_img2_text);

					}
					multiScreens.multiScreenShot(driver); */
		
		// Carousel image3 title verification
					WebElement img3_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[7]/div/wrapper/div/div[1]/div"));
					String Act_img3_title = img3_title.getText();
					String Exp_img3_title = "Bejaia";
					try {
						AssertJUnit.assertEquals(Exp_img3_title, Act_img3_title);			
						System.out.println("Title matching with expected: " + Exp_img3_title);	
						test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img3_title);

						} catch (Exception e)
						{
							System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img3_title + "Expected Title:" +Exp_img3_title);
							test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img3_title + "Expected Title:" +Exp_img3_title);

						}
				
		/* // Carousel image3 text verification
					WebElement Cr_img3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[2]"));
					String Act_img3_text = Cr_img3.getText();
					String Exp_img3_text = "The coastal city of Algeria, the largest city in Kabylia, a superb city, bright as you want, and bordered by the sea.";
					try {
						AssertJUnit.assertEquals(Exp_img3_text, Act_img3_text);
						System.out.println("Carousel image 3 'Bejaia' text is matching with expected");
						test.log(LogStatus.PASS, "Carousel image 3 'Bejaia' text is matching with expected");

						}catch (Exception e)
						{
							System.out.println("Not Matching, Expected:" + Exp_img3_text +"Actual:"+ Act_img3_text);
							test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img3_text +"Actual:"+ Act_img3_text);

						}	
					multiScreens.multiScreenShot(driver); */
	
		// Carousel image4 title verification
					WebElement img4_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[8]/div/wrapper/div/div[1]/div"));
					String Act_img4_title = img4_title.getText();
					String Exp_img4_title = "Constantine";
					try {
						AssertJUnit.assertEquals(Exp_img4_title, Act_img4_title);			
						System.out.println("Title matching with expected: " + Exp_img4_title);
						test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img4_title);

						} catch (Exception e)
						{
							System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img4_title + "Expected Title:" +Exp_img4_title);
							test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img4_title + "Expected Title:" +Exp_img4_title);

						}
				
	/*	// Carousel image4 text verification
					WebElement Cr_img4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[2]"));
					String Act_img4_text = Cr_img4.getText();
					String Exp_img4_text = "The magical city of Algeria, Nicknamed \"the city of suspended bridges\", a must-see in Algeria, it is probably for its unparalleled beauty!";	
					try {
						AssertJUnit.assertEquals(Exp_img4_text, Act_img4_text);
						System.out.println("Carousel image 4 'Constantine' text is matching with expected");
						test.log(LogStatus.PASS, "Carousel image 4 'Constantine' text is matching with expected");

						} catch (Exception e)
						{

							System.out.println("Not Matching, Expected:" + Exp_img4_text +"Actual:"+ Act_img4_text);
							test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img4_text +"Actual:"+ Act_img4_text);

						}
					multiScreens.multiScreenShot(driver); */

			// Carousel image5 title verification
					((JavascriptExecutor)driver).executeScript("scroll(0,401)");
					Thread.sleep(3000);
					WebElement next = driver.findElement(By.xpath ("//*[@rx='17.5'][1]"));
					next.click();
					Thread.sleep(3000);
					WebElement img5_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[18]/div/wrapper/div/div[1]/div"));
					String Act_img5_title = img5_title.getText();
					String Exp_img5_title = "Beni Abbes";
					try {
						AssertJUnit.assertEquals(Exp_img5_title, Act_img5_title);			
						System.out.println("Title matching with expected: " + Exp_img5_title);	
						test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img5_title);
						} catch (AssertionError  e)
						{
							System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img5_title + "Expected Title:" +Exp_img5_title);
							test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img5_title + "Expected Title:" +Exp_img5_title);

						}
				
		/*	// Carousel image5 text verification
				WebElement Cr_img5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[2]"));
				String Act_img5_text = Cr_img5.getText();
				String Exp_img5_text = "Beautiful city of Algeria in the middle of nature, means \"broom collar\". Between sea and mountain. a typical city, discreet, but out of the ordinary!";
				
				try {
				AssertJUnit.assertEquals(Exp_img5_text, Act_img5_text);
				System.out.println("Carousel image 5 'Tizi ouzou' text is matching with expected");
				test.log(LogStatus.PASS, "Carousel image 5 'Tizi ouzou' text is matching with expected");
				
				}
				catch (Exception e)
				{
					System.out.println("Not Matching, Expected:" + Exp_img5_text +"Actual:"+ Act_img5_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img5_text +"Actual:"+ Act_img5_text);

				} 			
				multiScreens.multiScreenShot(driver);*/

			// Carousel image6 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img6_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[17]/div/wrapper/div/div[1]/div"));
				String Act_img6_title = img6_title.getText();
				String Exp_img6_title = "Tamanrasset";
				try {
					AssertJUnit.assertEquals(Exp_img6_title, Act_img6_title);			
					System.out.println("Title matching with expected: " + Exp_img6_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img6_title);	
				} catch (AssertionError  e)
					{
						System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img6_title + "Expected Title:" +Exp_img6_title);
						test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img6_title + "Expected Title:" +Exp_img6_title);

					}
			
		/*	// Carousel image6 text verification
				WebElement Cr_img6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[2]"));
				String Act_img6_text = Cr_img6.getText();
				String Exp_img6_text = "An Algerian city treasure, a privileged destination for tourists, between sea and mountains, we find ourselves around an admirable landscape that pleases the eyes!";
				
				try {
				AssertJUnit.assertEquals(Exp_img6_text, Act_img6_text);
				System.out.println("Carousel image 6" + "'" +Exp_img6_title + "'" + "text is matching with expected");
				test.log(LogStatus.PASS, "Carousel image 6" + "'" +Exp_img6_title + "'" + "text is matching with expected");	

				}
				catch (Exception e)
				{
					System.out.println("Not Matching, Expected:" + Exp_img6_text +"Actual:"+ Act_img6_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img6_text +"Actual:"+ Act_img6_text);

				} 
				multiScreens.multiScreenShot(driver); */
				
			// Carousel image7 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img7_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[16]/div/wrapper/div/div[1]/div"));
				String Act_img7_title = img7_title.getText();
				String Exp_img7_title = "Ghardaia";
				try {
					AssertJUnit.assertEquals(Exp_img7_title, Act_img7_title);			
					System.out.println("Title matching with expected: " + Exp_img7_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img7_title);	

				} catch (AssertionError  e)
					{
						System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img7_title + "Expected Title:" +Exp_img7_title);
						test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img7_title + "Expected Title:" +Exp_img7_title);

					}
			
			/* // Carousel image7 text verification
				WebElement Cr_img7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[2]"));
				String Act_img7_text = Cr_img7.getText();
				String Exp_img7_text = "Discover the pearl of the West Coast, and enjoy visiting the many historical attractions and beaches that decorate the long coastline.";
					
				try {
					AssertJUnit.assertEquals(Exp_img7_text, Act_img7_text);
					System.out.println("Carousel image 7" +"'"+Exp_img7_title+"'"+ "text is matching with expected");
					test.log(LogStatus.PASS, "Carousel image 7" +"'"+Exp_img7_title+"'"+ "text is matching with expected");	
		
				}
					catch (Exception e)
					{
						System.out.println("Not Matching, Expected:" + Exp_img7_text +"Actual:"+ Act_img7_text);
						test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img7_text +"Actual:"+ Act_img7_text);
						
					} 
					multiScreens.multiScreenShot(driver); */
						
			// Carousel image8 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img8_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[15]/div/wrapper/div/div[1]/div"));
				String Act_img8_title = img8_title.getText();
				String Exp_img8_title = "Tizi ouzou";
				try {
					AssertJUnit.assertEquals(Exp_img8_title, Act_img8_title);			
					System.out.println("Title matching with expected: " + Exp_img8_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img8_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img8_title + "Expected Title:" +Exp_img8_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img8_title + "Expected Title:" +Exp_img8_title);

					}
				
			/*// Carousel image8 text verification
				WebElement Cr_img8 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[2]"));
				String Act_img8_text = Cr_img8.getText();
				String Exp_img8_text = "The city of strawberries, formerly known as Philippeville, a port city, in the east of Algeria, with a vast waterfront, and a lively city center";
					
				try {
					AssertJUnit.assertEquals(Exp_img8_text, Act_img8_text);
					System.out.println("Carousel image 8" +"'"+Exp_img8_title+"'"+ "text is matching with expected");
					test.log(LogStatus.PASS, "Carousel image 8" +"'"+Exp_img8_title+"'"+ "text is matching with expected");	
				
				}catch (Exception e)
					{
					System.out.println("Not Matching, Expected:" + Exp_img8_text +"Actual:"+ Act_img8_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img8_text +"Actual:"+ Act_img8_text);

					} 
				multiScreens.multiScreenShot(driver);*/
			
			// Carousel image9 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img9_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[14]/div/wrapper/div/div[1]/div"));
				String Act_img9_title = img9_title.getText();
				String Exp_img9_title = "Tipaza";
				try {
					AssertJUnit.assertEquals(Exp_img9_title, Act_img9_title);			
					System.out.println("Title matching with expected: " + Exp_img9_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img9_title);	

					} catch (AssertionError  e)
					{
						test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img9_title + "Expected Title:" +Exp_img9_title);

						System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img9_title + "Expected Title:" +Exp_img9_title);
					}
					
		/*	// Carousel image9 text verification
				WebElement Cr_img9 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[2]"));
				String Act_img9_text = Cr_img9.getText();
				String Exp_img9_text = "Wahran or El Bahia, one of the most beautiful cities in Algeria. Visit its sumptuous monuments and especially its breathtaking view of the Mediterranean";
							
				try {
					AssertJUnit.assertEquals(Exp_img9_text, Act_img9_text);
					System.out.println("Carousel image 9" +"'"+Exp_img9_title+"'"+ "text is matching with expected");
					test.log(LogStatus.PASS, "Carousel image 9" +"'"+Exp_img9_title+"'"+ "text is matching with expected");	

				}
					catch (Exception e)
					{
					System.out.println("Not Matching, Expected:" + Exp_img9_text +"Actual:"+ Act_img9_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img9_text +"Actual:"+ Act_img9_text);

					} 
				multiScreens.multiScreenShot(driver); */
						
			// Carousel image10 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img10_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[13]/div/wrapper/div/div[1]/div"));
				String Act_img10_title = img10_title.getText();
				String Exp_img10_title = "Ain Temouchent";
				try {
					AssertJUnit.assertEquals(Exp_img10_title, Act_img10_title);			
					System.out.println("Title matching with expected: " + Exp_img10_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img10_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img10_title + "Expected Title:" +Exp_img10_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img10_title + "Expected Title:" +Exp_img10_title);

					}
						
		/*	// Carousel image10 text verification
				WebElement Cr_img10 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[2]"));
				String Act_img10_text = Cr_img10.getText();
				String Exp_img10_text = "One of the most beautiful coastal towns in Algeria, the perfect place to cut yourself off from the world and appreciate the sea and the simple pleasures of the city.";
									
				try {
					AssertJUnit.assertEquals(Exp_img10_text, Act_img10_text);
					System.out.println("Carousel image 10" +"'"+Exp_img10_title+"'"+ "text is matching with expected");
					test.log(LogStatus.PASS, "Carousel image 10" +"'"+Exp_img10_title+"'"+ "text is matching with expected");	
					
				}
				catch (Exception e)
					{
					System.out.println("Not Matching, Expected:" + Exp_img10_text +"Actual:"+ Act_img10_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img10_text +"Actual:"+ Act_img10_text);

					} 
				multiScreens.multiScreenShot(driver); */
							
			// Carousel image11 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img11_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[12]/div/wrapper/div/div[1]/div"));
				String Act_img11_title = img11_title.getText();
				String Exp_img11_title = "Skikda";
				try {
					AssertJUnit.assertEquals(Exp_img11_title, Act_img11_title);			
					System.out.println("Title matching with expected: " + Exp_img11_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img11_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img11_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img11_title);

					}
						
		/*	// Carousel image11 text verification
				WebElement Cr_img11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[2]"));
				String Act_img11_text = Cr_img11.getText();
				String Exp_img11_text = "Sétif, an ancient and Roman city located between the Atlas Tellien and the Desert chain. Obviously, the site of Djemila, a jewel of cultural heritage, will reveal unexpected treasures!";
										
				try {
					AssertJUnit.assertEquals(Exp_img11_text, Act_img11_text);
					System.out.println("Carousel image 11" +"'"+Exp_img11_title+"'"+ "text is matching with expected");
					test.log(LogStatus.PASS, "Carousel image 11" +"'"+Exp_img11_title+"'"+ "text is matching with expected");	
			
				}
					catch (Exception e)
					{
					System.out.println("Not Matching, Expected:" + Exp_img11_text +"Actual:"+ Act_img11_text);
					test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img11_text +"Actual:"+ Act_img11_text);
					
					}*/
				
				// Carousel image12 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img12_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[11]/div/wrapper/div/div[1]/div"));
				String Act_img12_title = img12_title.getText();
				String Exp_img12_title = "Oran";
				try {
					AssertJUnit.assertEquals(Exp_img12_title, Act_img12_title);			
					System.out.println("Title matching with expected: " + Exp_img12_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img12_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img12_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img12_title);

					}
				
				// Carousel image13 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img13_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[10]/div/wrapper/div/div[1]/div"));
				String Act_img13_title = img13_title.getText();
				String Exp_img13_title = "Mostaganem";
				try {
					AssertJUnit.assertEquals(Exp_img13_title, Act_img13_title);			
					System.out.println("Title matching with expected: " + Exp_img13_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img13_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img13_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img13_title);

					}
				
				// Carousel image14 title verification
				((JavascriptExecutor)driver).executeScript("scroll(0,401)");
				driver.findElement(By.xpath ("//*[@rx='17.5'][1]")).click();
				Thread.sleep(3000);
				WebElement img14_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[9]/div/wrapper/div/div[1]/div"));
				String Act_img14_title = img14_title.getText();
				String Exp_img14_title = "Setif";
				try {
					AssertJUnit.assertEquals(Exp_img14_title, Act_img14_title);			
					System.out.println("Title matching with expected: " + Exp_img14_title);	
					test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img14_title);	

				} catch (AssertionError  e)
					{
					System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img14_title);
					test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img14_title);

					}
				multiScreens.multiScreenShot(driver);
				
		
}


@Test (priority = 6, invocationCount = 1)
public void Why_choose_us_Verification()
	{
	test = report.startTest("Why Choose us section verification");
				// Why choose us!
						// Title Verification
							String exp_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[1]")).getText();
							String act_title = "Why choose us!";
				
							try {
								AssertJUnit.assertEquals(exp_title, act_title);
								System.out.println("Title matching");
								test.log(LogStatus.PASS, "Title matching");	

							} catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Title Mismatching");

								} 
				
						// Easy Booking
							String exp_sec1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[1]")).getText();
							String act_sec1 = "Easy Booking";
				
							try {
								AssertJUnit.assertEquals(exp_sec1, act_sec1);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Easy Booking - Matching");	
								
								} catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Easy Booking - Not Matching");

								}
							String exp_bdy1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[2]")).getText();
							String act_bdy1 = "The booking process should include minimal steps.";
				
							try {
								AssertJUnit.assertEquals(exp_bdy1, act_bdy1);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Easy Booking Content - Matching");	

								}catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Easy Booking Content - Not Matching");

								}
				
				// Friendly Interfaces
							String exp_sec2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[1]")).getText();
							String act_sec2 = "Friendly Interfaces";
				
							try {
								AssertJUnit.assertEquals(exp_sec2, act_sec2);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Friendly Interface Title - Matching");	

								}catch (Exception e)
								{
									test.log(LogStatus.FAIL, "Friendly Interface Title - Not Matching");

									System.out.println("Not Matching");
								}
							String exp_bdy2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[2]")).getText();
							String act_bdy2 = "A hotel booking engine with a good user-friendly.";
				
							try {
								AssertJUnit.assertEquals(exp_bdy2, act_bdy2);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Friendly Interface content - Matching");	

							} catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Friendly Interface content - Not Matching");

								}
				
				// Wide Payment options
							String exp_sec3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[1]")).getText();
							String act_sec3 = "Wide payment options";
				
							try {
								AssertJUnit.assertEquals(exp_sec3, act_sec3);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Wide Payment Title - Matching");	

							}catch (Exception e)
								{
								test.log(LogStatus.FAIL, "Wide Payment Title - Not Matching");

								System.out.println("Not Matching");
								}
							String exp_bdy3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[2]")).getText();
							String act_bdy3 = "Domestic and international payments accepted";
				
							try {
								AssertJUnit.assertEquals(exp_bdy3, act_bdy3);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Wide Payment content - Matching");	
								
								} catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Wide Payment content - Not Matching");

								}
				
				// Customized cancellation
							String exp_sec4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[1]")).getText();
							String act_sec4 = "Customized Cancellation";
				
							try {
								AssertJUnit.assertEquals(exp_sec4, act_sec4);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Customized Cancellation - Matching");	

								} catch (Exception e)
								{
									System.out.println("Not Matching");
									test.log(LogStatus.FAIL, "Customized Cancellation - Not Matching");

								}
							String exp_bdy4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[2]")).getText();
							String act_bdy4 = "You can cancel at free of cost according to policies";
				
							try {
								AssertJUnit.assertEquals(exp_bdy4, act_bdy4);
								System.out.println("Matching");
								test.log(LogStatus.PASS, "Customized Cancellation content - Matching");	
								
								} catch (Exception e)
								{
									test.log(LogStatus.FAIL, "Customized Cancellation content - Not Matching");

									System.out.println("Not Matching");
								}
		
				// Free cancellation
						String exp_sec5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[1]")).getText();
						String act_sec5 = "Algeria's First Platform for Hotel Booking with local payment option and Free cancellation";
				
						try {
							AssertJUnit.assertEquals(exp_sec5, act_sec5);
							System.out.println("Matching");
							test.log(LogStatus.PASS, "Free Cancellation Title - Matching");	

						} catch (Exception e)
							{
								System.out.println("Not Matching");
								test.log(LogStatus.FAIL, "Free Cancellation Title - Not Matching");

							}
						String exp_bdy5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[2]")).getText();
						String act_bdy5 = "*Terms and Condition applied on Cancellation Policies";
				
						try {
							AssertJUnit.assertEquals(exp_bdy5, act_bdy5);
							System.out.println("Matching");
							test.log(LogStatus.PASS, "Free Cancellation content - Matching");	

							}catch (Exception e)
							{
								test.log(LogStatus.FAIL, "Free Cancellation content - Not Matching");

								System.out.println("Not Matching");
							}
					
	}

@Test (priority = 7, invocationCount = 1)
public void Help_Section_Verification()
	{
	test = report.startTest("Help section verification");
			// Help Section
				String exp_help_title= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[1]")).getText();
				String act_help_title = "We are ready to help you and ease your booking process.\n"
						+ " Just follow our steps and get everything easily.";
				
				try {
					AssertJUnit.assertEquals(exp_help_title, act_help_title);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section Title - Matching");	

				}catch (Exception e)
					{
						System.out.println("Not Matching");
						test.log(LogStatus.FAIL, "Help section Title - Not Matching");

					}
		
			// Location Section
				String exp_help1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[2]")).getText();
				String act_help1 = "Choose the location you want to stay in";
									
				try {
					AssertJUnit.assertEquals(exp_help1, act_help1);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section1 Title - Matching");	
					
					}catch (Exception e)
					{
						System.out.println("Not Matching");
						test.log(LogStatus.FAIL, "Help section1 Title - Not Matching");

					}
				String exp_help1_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[3]")).getText();
				String act_help1_con ="Choose a hotel from the selected city";
				try {
					AssertJUnit.assertEquals(exp_help1_con, act_help1_con);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section1 content - Matching");	

					}catch (Exception e)
					{
						System.out.println("Not Matching");
						test.log(LogStatus.FAIL, "Help section1 content - Not Matching");

					}
				
			//Choice
				String exp_help2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[2]")).getText();
				String act_help2 = "Book your choice";
									
				try {
					AssertJUnit.assertEquals(exp_help2, act_help2);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section2 title - Matching");	

					} catch (Exception e)
					{
						test.log(LogStatus.FAIL, "Help section2 title - Not Matching");
						System.out.println("Not Matching");
					}
				String exp_help2_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[3]")).getText();
				String act_help2_con ="Only by making a transaction you have managed to get all the facilities.";
				try {
					AssertJUnit.assertEquals(exp_help2_con, act_help2_con);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section2 content - Matching");	

					}catch (Exception e)
					{
						System.out.println("Not Matching");
						test.log(LogStatus.FAIL, "Help section2 content - Not Matching");

					}
				
			//Checkin
				String exp_help3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[2]")).getText();
				String act_help3 ="Check-in";
				
				try {
					AssertJUnit.assertEquals(exp_help3, act_help3);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section3 Title - Matching");	

				} catch (Exception e)
				{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "Help section3 Title - Not Matching");

				}
				String exp_help3_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[3]")).getText();
				String act_help3_con ="Come to hotel with ID and proof of payment and enjoy your stay.";
				try {
					AssertJUnit.assertEquals(exp_help3_con, act_help3_con);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "Help section3 content - Matching");	

					}catch (Exception e)
					{
						test.log(LogStatus.FAIL, "Help section3 content - Not Matching");

						System.out.println("Not Matching");
					}
				
	}
	
@Test (priority = 8, invocationCount = 1)
public void Covid_Measures_Verification()
	{
	test = report.startTest("Covid Measure section verification");
		//Covid Measures
			//Title verififcation
				String exp_COVID_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[1]")).getText();
				String act_COVID_title ="COVID-19 measures on Namlatic hotels";
				
				try {
					AssertJUnit.assertEquals(exp_COVID_title, act_COVID_title);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure Title - Matching");	

				}catch (Exception e)
					{
					test.log(LogStatus.FAIL, "COVID Measure Title - Not Matching");

					System.out.println("Not Matching");
					}
				
			//Sub Title 
				String exp_COVID_subtitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[1]")).getText();
				String act_COVID_subtitle ="Namlatic hotels are under COVID19 preventive measures";
				try {
					AssertJUnit.assertEquals(exp_COVID_subtitle, act_COVID_subtitle);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure Sub_Title - Matching");	

					}catch (Exception e)
					{
						System.out.println("Not Matching");
						test.log(LogStatus.FAIL, "COVID Measure Sub_Title - Not Matching");

					}
		
				//Measure 1
				String exp_COVID_mes1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[2]")).getText();
				String act_COVID_mes1 ="100% Safe & Disinfected";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes1, act_COVID_mes1);
					test.log(LogStatus.PASS, "COVID Measure_1 - Matching");	

					System.out.println("Matching");
					}
					catch (Exception e)
					{
						test.log(LogStatus.FAIL, "COVID Measure_1 -Not Matching");

					}
				
				//Measure 2
				String exp_COVID_mes2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[2]/wrapper/div[2]")).getText();
				String act_COVID_mes2 ="Mask, latex, gloves for all staffs";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes2, act_COVID_mes2);
					test.log(LogStatus.PASS, "COVID Measure_2 - Matching");	
					System.out.println("Matching");
					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_2 -Not Matching");

					
					}
				
				
				//Measure 3
				String exp_COVID_mes3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[3]/wrapper/div[2]")).getText();
				String act_COVID_mes3 ="Mask, latex, gloves for all staffs";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes3, act_COVID_mes3);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure_3 - Matching");	

					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_3 -Not Matching");

					}
			
				//Measure 4
				String exp_COVID_mes4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[4]/wrapper/div[2]")).getText();
				String act_COVID_mes4 ="Sanitization every 2 hours";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes4, act_COVID_mes4);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure_4 - Matching");	

					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_4 -Not Matching");

					}
		
				//Measure 5
				String exp_COVID_mes5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[5]/wrapper/div[2]")).getText();
				String act_COVID_mes5 ="Temperature check mandatory for all guest as well as staffs";
				System.out.println(exp_COVID_mes5);
				try {
					AssertJUnit.assertEquals(exp_COVID_mes5, act_COVID_mes5);
					test.log(LogStatus.PASS, "COVID Measure_5 - Matching");	

					System.out.println("Matching");
					}
					catch (Exception e)
					{
						test.log(LogStatus.FAIL, "COVID Measure_5 -Not Matching");

						System.out.println("Not Matching");
					}
				
				//Measure 6
				String exp_COVID_mes6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[6]/wrapper/div[2]")).getText();
				String act_COVID_mes6 ="Linen/ Utensils washed in 70 c + water";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes6, act_COVID_mes6);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure_6 - Matching");	

					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_6 -Not Matching");
					}
				
				//Measure 7 
				String exp_COVID_mes7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[7]/wrapper/div[2]")).getText();
				String act_COVID_mes7 ="Hospital grade cleaning chemicals used at the property";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes7, act_COVID_mes7);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure_7 - Matching");	

					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_7 -Not Matching");
					}
				
				//Measure 8 
				String exp_COVID_mes8 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[8]/wrapper/div[2]")).getText();
				String act_COVID_mes8 ="Social distancing at reception,lift, common areas & gardens";
				try {
					AssertJUnit.assertEquals(exp_COVID_mes8, act_COVID_mes8);
					System.out.println("Matching");
					test.log(LogStatus.PASS, "COVID Measure_8 - Matching");	

					}
					catch (Exception e)
					{
					System.out.println("Not Matching");
					test.log(LogStatus.FAIL, "COVID Measure_8 -Not Matching");
					}
				
	}			
	
@Test (priority = 9)	
public void Footer_Verification() throws InterruptedException, IOException
	{
	test = report.startTest("Footer Verification");
			//Footer Verification
				//Namlatic logo verification
				WebElement footer1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/a/img"));
				try {
					footer1.isDisplayed();
					System.out.println("Namlatic logo available in Footer");
					test.log(LogStatus.PASS, "Namlatic logo available in Footer");	
					} catch (Exception e)
					{
						test.log(LogStatus.FAIL, "Namlatic logo missing in Footer");
						System.out.println("Logo missing in Footer");
					}
				
				//About us
				String exp_footer2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]")).getText();
				String act_footer2 = "About us";
				try {
					AssertJUnit.assertEquals(exp_footer2, act_footer2);
					test.log(LogStatus.PASS, "About us is available in footer");	
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "About us is not available in footer");
						System.out.println("About us is not available in footer");
					}
				
				//Terms & conditions
				String exp_footer3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[2]")).getText();
				String act_footer3 = "Terms and Conditions";
				try {
					AssertJUnit.assertEquals(exp_footer3, act_footer3);
					test.log(LogStatus.PASS, "Terms and Conditions is available in footer");	
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Terms and Conditions is not available in footer");	
				}
	
				//Privacy Policy
				String exp_footer4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[3]")).getText();
				String act_footer4 = "Privacy Policy";
				try {
					AssertJUnit.assertEquals(exp_footer4, act_footer4);
					test.log(LogStatus.PASS, "Privacy Policy is available in footer");	
				}
				catch (AssertionError e)
				{
					System.out.println("Privacy Policy is not available in footer");
					test.log(LogStatus.FAIL, "Privacy Policy is not available in footer");	
				}
	
				//Namlatic help center
				String exp_footer5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[4]")).getText();
				String act_footer5 = "Namlatic Help Center";
				try {
					AssertJUnit.assertEquals(exp_footer5, act_footer5);
					test.log(LogStatus.PASS, "Namlatic Help Center is available in footer");	
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Namlatic Help Center is available in footer");	
				}
				
				//FAQ
				String exp_footer_FAQ = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[5]")).getText();
				String act_footer_FAQ = "FAQ";
				try {
					AssertJUnit.assertEquals(exp_footer_FAQ, act_footer_FAQ);
					test.log(LogStatus.PASS, "FAQ is available in footer");	
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "FAQ is available in footer");	
				}
	
				//Follow Us
				String exp_footer6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[4]/div/div[1]")).getText();
				String act_footer6 = "Follow us";
				try {
					AssertJUnit.assertEquals(exp_footer6, act_footer6);
					test.log(LogStatus.PASS, "Follow us is available in footer");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Follow us is not available in footer");
				}
			
			//Social media logo verification
				//FB
				WebElement FB = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][1]"));
				try {
					FB.isDisplayed();
					test.log(LogStatus.PASS, "FB icon available");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "FB is not available");
				}
				
				//Twitter
				WebElement Twitter = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][2]"));
				try {
					Twitter.isDisplayed();
					test.log(LogStatus.PASS, "Twitter icon available");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Twitter is not available");
				}
				
				//Linkedin
				WebElement Linkedin = driver.findElement(By.xpath("//*[name()='svg'][3]"));
				try {
					Linkedin.isDisplayed();
					test.log(LogStatus.PASS, "Linkedin icon available");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Linkedin is not available");
				}
				
				//Instagram
				WebElement Instagram = driver.findElement(By.xpath("//*[name()='svg'][4]"));
				try {
					Instagram.isDisplayed();
					test.log(LogStatus.PASS, "Instagram icon available");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Instagram is not available");
				}
				
				//Youtube
				WebElement Youtube = driver.findElement(By.xpath("//*[name()='svg'][5]"));
				try {
					Youtube.isDisplayed();
					test.log(LogStatus.PASS, "Youtube icon available");
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Youtube is not available");
				}
			
			//Removing hubspot
				
				((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
				Thread.sleep(2000);
				driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
		 		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		 		Thread.sleep(2000);
		 		
		 		//driver.findElement(By.xpath("//button[@class='InitialMessageBubble__CloseButton-mbyu1g-2 gQJDPL']")).click();
		 		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		 		Thread.sleep(2000);
		 		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		 		System.out.println("Within iFrame");
		 		driver.switchTo().parentFrame(); // Parent Frame
		 		((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
				Thread.sleep(2000);
				
		 	//Facebook
			//Facebook launch verification
		 		
		 	
				
				WebElement FB_logo = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][1]"));
				FB_logo.click();
							
			//Facebook URL verification
			//Get handles of the windows
				String MainWindow4 = driver.getWindowHandle();
				String mainWindowHandle4 = driver.getWindowHandle();
				Set<String> allWindowHandles4 = driver.getWindowHandles();
				Iterator<String> iterator4 = allWindowHandles4.iterator();
				try
		 		{	
			// Here we will check if child window has other child windows and will fetch the heading of the child window
				while (iterator4.hasNext()) {
				String ChildWindow4 = iterator4.next();
				    if (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
				    driver.switchTo().window(ChildWindow4);
				      	}}
					multiScreens.multiScreenShot(driver);
					String exp_FB_title1 = "https://www.facebook.com/Namlatic";							 
					String exp_FB_title2 ="https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FNamlatic";
					try
					{
						AssertJUnit.assertEquals(driver.getCurrentUrl(),exp_FB_title1);
						test.log(LogStatus.PASS,"Navigating to Namlatic Facebook page");
					}
					  catch (AssertionError e)
					  	{
						  Assert.assertEquals(driver.getCurrentUrl(),exp_FB_title2);
						  test.log(LogStatus.PASS,"Navigating to Namlatic Facebook page");
						}
					  catch (Error e)
						{
						test.log(LogStatus.FAIL,"Namlatic Facebook page is not loaded or loading to improper URL");
						} 
		 		
					 
					Thread.sleep(3000);
										
					driver.switchTo().window(MainWindow4);	
					test.log(LogStatus.PASS, "Facebook URL launched successfully");
		 			}
		 			catch (AssertionError e)
		 			{
		 			test.log(LogStatus.FAIL, "Facebook URL not launched successfully");
		 			}
		 		
		 	//Twitter launch verification
		 	
				Thread.sleep(2000);
				//WebElement TW = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[4]/div/div[2]/svg[2]/path"));
				WebElement TW = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][2]"));
				TW.click();
					
				//Twitter URL verification
					//Get handles of the windows
					String MainWindow3 = driver.getWindowHandle();
					String mainWindowHandle3 = driver.getWindowHandle();
					Set<String> allWindowHandles3 = driver.getWindowHandles();
					Iterator<String> iterator3 = allWindowHandles3.iterator();
					try
				 	{
					// Here we will check if child window has other child windows and will fetch the heading of the child window
						while (iterator3.hasNext()) {
							String ChildWindow3 = iterator3.next();
			            		if (!mainWindowHandle3.equalsIgnoreCase(ChildWindow3)) {
			            		driver.switchTo().window(ChildWindow3);
			            		}}
					multiScreens.multiScreenShot(driver);
					 String exp_TW_title = "https://twitter.com/Namla_officiel";							 
					 try
						{
						AssertJUnit.assertEquals(exp_TW_title, driver.getCurrentUrl());
						test.log(LogStatus.PASS, "Navigating to Namlatic Twitter page");
						} catch(AssertionError e)
						{
							test.log(LogStatus.FAIL,"Namlatic Twitter page is not loaded or loading to improper URL");
						} 
					driver.switchTo().window(MainWindow3);	
					test.log(LogStatus.PASS, "Twitter URL launched successfully");
		 	}
		 	catch (AssertionError e)
		 	{
		 		test.log(LogStatus.FAIL, "Twitter URL launched successfully");
		 	}
					
		//LinkedIN launch verification
			
			((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
			Thread.sleep(2000);
			WebElement IN = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][3]"));
			IN.click();
								
		//LinkedIN URL verification
			//Get handles of the windows
				String MainWindow2 = driver.getWindowHandle();
				String mainWindowHandle2 = driver.getWindowHandle();
				Set<String> allWindowHandles2 = driver.getWindowHandles();
				Iterator<String> iterator2 = allWindowHandles2.iterator();
				try
				{	
			// Here we will check if child window has other child windows and will fetch the heading of the child window
				while (iterator2.hasNext()) {
				String ChildWindow2 = iterator2.next();
				 if (!mainWindowHandle2.equalsIgnoreCase(ChildWindow2)) {
			         driver.switchTo().window(ChildWindow2);
			         }}
				multiScreens.multiScreenShot(driver);
				String exp_IN_title1 = "NAMLATIC | LinkedIn"; 
				String exp_IN_title2 = "Sign In | LinkedIn";
				String exp_IN_title3 ="Security Verification | LinkedIn";
				try
				{
					AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title1); 
					test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page");
				} catch (AssertionError e)
					{
					AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title2);
					test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page");
					} catch (Error e)
						{
						AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title3);
						test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page");
						} catch (Exception e)
							{
							test.log(LogStatus.FAIL,"Not Navigating to Namlatic LinkedIN page");
							}
				test.log(LogStatus.PASS,"LinkedIN URL launched successfully");
				driver.switchTo().window(MainWindow2);
					}
				catch (AssertionError e)
					{
					test.log(LogStatus.FAIL,"Namlatic LinkedIN page is not loaded or loading to improper URL");
					}
				Thread.sleep(3000);	
				
		 //Instagram launch verification
			
		 	WebElement insta = driver.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][4]"));
			insta.click();
				
		//Instagram URL verification
		//Get handles of the windows
			String MainWindow6 = driver.getWindowHandle();
			String mainWindowHandle = driver.getWindowHandle();
			Set<String> allWindowHandles = driver.getWindowHandles();
			Iterator<String> iterator = allWindowHandles.iterator();
			
			try
			{
				// Here we will check if child window has other child windows and will fetch the heading of the child window
				while (iterator.hasNext()) {
					String ChildWindow = iterator.next();
					if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
						driver.switchTo().window(ChildWindow);
					}}
				multiScreens.multiScreenShot(driver);
						
			String exp_insta_title1 = "Login • Instagram";
			String exp_insta_title2 = "https://www.instagram.com/accounts/login/";
				
			try 
				{
				AssertJUnit.assertEquals(driver.getTitle(),exp_insta_title1);
				test.log(LogStatus.PASS,"Navigating to Namlatic Instagram page");
				} catch(Error e)
						{
						test.log(LogStatus.FAIL,"Namlatic Instagram page is not loaded or loading to improper URL");
						}
				test.log(LogStatus.PASS, "Instagram URL launched successfully");
				driver.switchTo().window(MainWindow6);
				} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Instagram URL not launched successfully");
					}
			
		//Youtube launch verification
			
			((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
			Thread.sleep(2000);
			WebElement YT = driver.findElement(By.xpath("//*[name()='svg'][5]"));
			YT.click();
					
			//Youtube URL verification
			//Get handles of the windows
				String MainWindow = driver.getWindowHandle();
				String mainWindowHandle1 = driver.getWindowHandle();
				Set<String> allWindowHandles1 = driver.getWindowHandles();
				Iterator<String> iterator1 = allWindowHandles1.iterator();
				try
				{
				// Here we will check if child window has other child windows and will fetch the heading of the child window
					while (iterator1.hasNext()) {
					String ChildWindow1 = iterator1.next();
			           if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
			            driver.switchTo().window(ChildWindow1);
			            }}
				multiScreens.multiScreenShot(driver);
				String exp_YT_title = "https://www.youtube.com/channel/UCqHvyjnCAW6FyOmmcr78fRw";
					
				try
				{
					AssertJUnit.assertEquals(exp_YT_title, driver.getCurrentUrl());
					test.log(LogStatus.PASS,"Navigating to Namlatic Youtube page");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Namlatic Youtube page is not loaded or loading to improper URL");
					}
				test.log(LogStatus.FAIL, "Youtube URL launched successfully");
				driver.switchTo().window(MainWindow);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Youtube URL not launched successfully");
			}
					
			//About us 
			   // Launch verifiation
					((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
					Thread.sleep(3000);
					driver.findElement(By.xpath("//*[contains(text(),'About us')]")).click(); 
			
			  // URL verification
					//Get handles of the windows
					String MainWindow7 = driver.getWindowHandle();
					String mainWindowHandle6 = driver.getWindowHandle();
					Set<String> allWindowHandles6 = driver.getWindowHandles();
					Iterator<String> iterator6 = allWindowHandles6.iterator();
			
				// Here we will check if child window has other child windows and will fetch the heading of the child window
						while (iterator6.hasNext()) {
							String ChildWindow6 = iterator6.next();
		            			if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
		            				driver.switchTo().window(ChildWindow6);
		            				}}
						multiScreens.multiScreenShot(driver);
						String exp_about_title = "https://business.namlatic.com/";
						try
						{
							AssertJUnit.assertEquals(exp_about_title, driver.getCurrentUrl());
							test.log(LogStatus.PASS,"Navigating to Namlatic About us page");
						} catch(AssertionError e)
						{
							test.log(LogStatus.FAIL,"Namlatic About us page is not loaded or loading to improper URL");
						}
						((JavascriptExecutor)driver).executeScript("scroll(0,0)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,400)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,800)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						((JavascriptExecutor)driver).executeScript("scroll(0,3600)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,4000)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,4400)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,4800)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,5200)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,5600)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,6000)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

						((JavascriptExecutor)driver).executeScript("scroll(0,6400)");
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);
						
						driver.switchTo().window(MainWindow7);
						
		
			//Terms & Conditions 
				// Launch verifiation
						((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[contains(text(),'Terms and Conditions')]")).click(); 	
						String exp_TC_title = driver.findElement(By.xpath("//*[contains(text(),'Terms & Conditions')]")).getText();
						try
						{
							Assert.assertNotNull(exp_TC_title);
							test.log(LogStatus.PASS,"Navigating to Namlatic Terms & Condition page");
						}
							catch(AssertionError e)
								{
									test.log(LogStatus.FAIL,"Namlatic Terms & Conditions page is not loaded or loading to improper URL");
								}
							((JavascriptExecutor)driver).executeScript("scroll(0,0)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,400)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,800)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
						
							((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
						
							((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
						
							((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
						
							driver.navigate().back();	
	
		//Privacy Policy 
			// Launch verifiation
						((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[contains(text(),'Privacy Policy')]")).click(); 
						
						String exp_PP_title = driver.findElement(By.xpath("//*[contains(text(),'Privacy Policy')]")).getText();
						try
						{
							Assert.assertNotNull(exp_PP_title.isEmpty());
								test.log(LogStatus.PASS,"Navigating to Namlatic Privacy Policy page");
						}
						catch(AssertionError e)
							{
								test.log(LogStatus.FAIL,"Namlatic Privacy Policy page is not loaded or loading to improper URL");
							}
							((JavascriptExecutor)driver).executeScript("scroll(0,0)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,400)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,800)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							
							driver.navigate().back();				
					
				//Help Center 
					// Launch verifiation
						((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[contains(text(),'Help Center')]")).click(); 
									
					// URL verification
						//Get handles of the windows
						String MainWindow9 = driver.getWindowHandle();
						String mainWindowHandle9 = driver.getWindowHandle();
						Set<String> allWindowHandles9 = driver.getWindowHandles();
						Iterator<String> iterator9 = allWindowHandles9.iterator();
											
						// Here we will check if child window has other child windows and will fetch the heading of the child window
							while (iterator9.hasNext()) {
								String ChildWindow9 = iterator9.next();
						         		if (!mainWindowHandle9.equalsIgnoreCase(ChildWindow9)) {
						           		driver.switchTo().window(ChildWindow9);
						           		}}
							multiScreens.multiScreenShot(driver);
							String exp_help_title1 = "Login - Jira Service Management";
						
							try
								{
								AssertJUnit.assertEquals(exp_help_title1, driver.getTitle());
								test.log(LogStatus.PASS,"Navigating to Namlatic Help Center page");
								} catch(AssertionError e)
								{
									test.log(LogStatus.FAIL,"Namlatic Help Center page is not loaded or loading to improper URL");
								}
							((JavascriptExecutor)driver).executeScript("scroll(0,0)");
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);
							driver.switchTo().window(MainWindow9);
							
					//FAQ
						((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
						Thread.sleep(2000);
						driver.findElement(By.xpath("//*[contains(text(),'FAQ')]")).click(); 
						Thread.sleep(2000);
							String exp_FAQ_title = driver.findElement(By.xpath("//*[contains(text(),'FAQs')]")).getText();
							System.out.println(exp_FAQ_title);
							System.out.println(exp_FAQ_title);
								try
								{
									Assert.assertNotNull(exp_FAQ_title);
										test.log(LogStatus.PASS,"Navigating to FAQ section");
								} catch(AssertionError e)
								{
									test.log(LogStatus.FAIL,"FAQ section is not loaded or loading to improper URL");
								}
					
						driver.navigate().back();	
						
			//Card verification
					WebElement exp_footer7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div[1]"));
						try {
							exp_footer7.isDisplayed();
							test.log(LogStatus.PASS,"We accept Visa, American express, Master card, CIB, Wire Transfer");
							}catch (Exception e)
							{
								test.log(LogStatus.FAIL,"Card section missing");
							} 


	} 		

public void screenshots() throws IOException, InterruptedException
	{
			//Screenshots
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				multiScreens.multiScreenShot(driver);
				Thread.sleep(2000);
				
				((JavascriptExecutor)driver).executeScript("scroll(0,900)");
				multiScreens.multiScreenShot(driver);
				Thread.sleep(2000);
				
				((JavascriptExecutor)driver).executeScript("scroll(0,1000)");
				multiScreens.multiScreenShot(driver);
				Thread.sleep(2000);
				
				((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
				multiScreens.multiScreenShot(driver);
				Thread.sleep(2000);
				
				((JavascriptExecutor)driver).executeScript("scroll(0,1900)");
				multiScreens.multiScreenShot(driver);
				Thread.sleep(2000);
	}			
	
@Test (priority = 10, invocationCount = 1)
public void Login() throws IOException, InterruptedException
	{
	// Login 		
	test = report.startTest("Login Functionality and Elastic search verification");
	try
	{
				WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login')]"));
				nam_Login.click();
				WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
				uname.sendKeys("9047232893");
				WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
				pwd.sendKeys("Test@123");
				multiScreens.multiScreenShot(driver);
				pwd.sendKeys(Keys.ENTER);
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				Thread.sleep(2000);
				test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Login Failed");
	}
	
	//Hotel search
	try
		{
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				WebElement Hotel = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
				Hotel.sendKeys("Annaba");
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ENTER);
				Thread.sleep(2000);	 
				//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				WebElement btnclick1 = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
				btnclick1.click();
				multiScreens.multiScreenShot(driver);
				test.log(LogStatus.PASS, "Hotel search working fine");
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

//--- Hotel Search screen validation---


@Test (priority = 11)
public void filter() throws IOException, InterruptedException
{
 // Price filter verification
	//Title Verification
	test = report.startTest("Filter verification");
Thread.sleep(3000);
		
		String act_Price_title =driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div")).getText();
		String exp_Price_title = "price";
		try {
			Assert.assertEquals(exp_Price_title, act_Price_title);	
			test.log(LogStatus.PASS, "Price filter menu available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Price filter menu not available");
			}
		multiScreens.multiScreenShot(driver);

	//Lowest price verification
		WebElement lowprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[5]"));
		
		try {
			test.log(LogStatus.PASS, "Lowest price is enabled: " +lowprice.isDisplayed());

			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Lowest price is not enabled");
			}
		multiScreens.multiScreenShot(driver);
		
	//Highest price verifcation
		WebElement highprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[7]"));
		
		try {
			test.log(LogStatus.PASS, "Highest price is enabled: " +highprice.isDisplayed());
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Lowest price is not enabled");
			}
		multiScreens.multiScreenShot(driver);
		
	//Slider verification
		WebElement slider = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div"));
		//WebElement slider2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[6]"));
		try {
			test.log(LogStatus.PASS, "Price slider1 is enabled: " +slider.isDisplayed());
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Price slider is not enabled");
			}
		multiScreens.multiScreenShot(driver);
	
	// Slider navigation
		Actions act = new Actions(driver);
		act.dragAndDropBy(slider, 0, 80).build().perform();
		//act.dragAndDropBy(slider, -20, 0).build().perform();
		multiScreens.multiScreenShot(driver);
		
	//Price low to high
		String act_lowtohigh = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/label")).getText();
		String exp_lowtohigh = "price low to high";
		WebElement lowtohigh = driver.findElement(By.id("filter_price_low_to_high"));
	
		try {
			Assert.assertEquals(exp_lowtohigh, act_lowtohigh);
			System.out.println("Low to high option button is displayed:" +lowtohigh.isDisplayed());
			System.out.println("Low to high option button is selected:" +lowtohigh.isSelected());				
			driver.findElement(By.xpath("//*[@id=\"filter_price_low_to_high\"]")).click();	
			test.log(LogStatus.PASS, "Price - low to high is displaying and enabled");

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Price - low to high is missing or not enabled to select");
			}
		multiScreens.multiScreenShot(driver);
		
	//Price high to low
		String act_hightolow = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[3]/label")).getText();
		String exp_hightolow = "price high to low";
		WebElement hightolow = driver.findElement(By.id("filter_price_high_to_low"));
		
		try {
			Assert.assertEquals(exp_hightolow, act_hightolow);
			System.out.println("High to Low option button is displayed:" +hightolow.isDisplayed());
			System.out.println("High to Low option button is selected:" +hightolow.isSelected());				
			driver.findElement(By.xpath("//*[@id=\"filter_price_high_to_low\"]")).click();
			test.log(LogStatus.PASS, "Price - high to low is displaying and enabled");

		} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Price - high to low is missing or not enabled to select");

			}
		multiScreens.multiScreenShot(driver);

	//Price filter enable and disable
		//Deselect
			WebElement pricefilter = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div"));
			pricefilter.click();
			Thread.sleep(3000);
		//Select
			pricefilter.click();
			multiScreens.multiScreenShot(driver);
			

}
@Test (priority = 12)
public void popular_sr() throws InterruptedException, IOException
{
	test = report.startTest("Popular search verification");
	//Popular search popular filter enable and disable
		  //Deselect
			WebElement popularfilter = driver.findElement(By.xpath("//*[contains(text(),'Popular Searches')]"));
			popularfilter.click();
			Thread.sleep(3000);
		  //Select
			popularfilter.click();
			multiScreens.multiScreenShot(driver);
			
		//Title verification
			String act_pop_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[1]/div")).getText();
			String exp_pop_title = "popular searches";
			try {
				Assert.assertEquals(exp_pop_title, act_pop_title);
				System.out.println("Popular search title is matching");
				test.log(LogStatus.PASS, "Popular search title is matching");
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Popular search title is not matching");
				}
			multiScreens.multiScreenShot(driver);
			
		//Couple Friendly verification
			String act_pop1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[1]/label")).getText();
			String exp_pop1 = "couple friendly";
			try {
				Assert.assertEquals(exp_pop1, act_pop1);
				test.log(LogStatus.PASS, "Couple friendly is available in Popular search");
				driver.findElement(By.xpath("//*[@id=\"filter_couple_friendly\"]")).click();
				} catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Couple friendly is not available in Popular search");
				}
			multiScreens.multiScreenShot(driver);
		
		//Air Conditioning verification
			String act_pop2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[2]/label")).getText();
			String exp_pop2 = "air conditioning";
			try {
				Assert.assertEquals(exp_pop2, act_pop2);
				test.log(LogStatus.PASS, "Air Conditioning is available in Popular search");
				driver.findElement(By.xpath("//*[@id=\"filter_air_conditioning\"]")).click();				
				} catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Air Conditioning is not available in Popular search");
				}
			multiScreens.multiScreenShot(driver);
		
		//Free Breakfast verification
			String act_pop3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[3]/label")).getText();
			String exp_pop3 = "free breakfast";
			try {
				Assert.assertEquals(exp_pop3, act_pop3);
				test.log(LogStatus.PASS, "Free Breakfast is available in Popular search");
				driver.findElement(By.xpath("//*[@id=\"filter_free_breakfast\"]")).click();	
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Free Breakfast is available in Popular search");
				}		
			multiScreens.multiScreenShot(driver);
			((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		
		//Free Cancellation Policy verification
			String act_pop4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[4]/label")).getText();
			String exp_pop4 = "free cancellation policy";
			try {
				Assert.assertEquals(exp_pop4, act_pop4);
				test.log(LogStatus.PASS, "Free Cancellation is available in Popular search");
				driver.findElement(By.xpath("//*[@id=\"filter_free_cancellation_policy\"]")).click();
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Free Cancellation is available in Popular search");
				}
			multiScreens.multiScreenShot(driver);
			((JavascriptExecutor)driver).executeScript("scroll(0,500)");

		//Pay at hotel verification
			String act_pop5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[5]/label")).getText();
			String exp_pop5 = "pay at hotel";
			try {
				Assert.assertEquals(exp_pop5, act_pop5);
				test.log(LogStatus.PASS, "Pay at hotel is available in Popular search");
				driver.findElement(By.xpath("//*[@id=\"filter_pay_at_hotel\"]")).click();
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Pay at hotel is not available in Popular search");
				}		
			multiScreens.multiScreenShot(driver);
			
			//Deselect
				popularfilter.click();
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
				Thread.sleep(2000);
}

@Test (priority = 13)
public void Amenities() throws InterruptedException, IOException
{
	test = report.startTest("Amenities section verification");
	//Amenities filter enable and disable
		//Deselect
			WebElement amenitiesfilter = driver.findElement(By.xpath("//*[contains(text(),'Amenities')]"));
			amenitiesfilter.click();
			Thread.sleep(3000);
		//Select
			amenitiesfilter.click();
			multiScreens.multiScreenShot(driver);
			((JavascriptExecutor)driver).executeScript("scroll(0,500)");
				
		//Title verification
			String act_amn_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[1]/div")).getText();
				String exp_amn_title = "amenities";
				try {
					Assert.assertEquals(exp_amn_title, act_amn_title);
					test.log(LogStatus.PASS, "Amenities title is matching");
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Amenities title is mismatching");
					}
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");

		//Free Wi-fi verification
				String act_amn1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[1]/label")).getText();
				String exp_amn1 = "free wi-fi";
				try {
					Assert.assertEquals(exp_amn1, act_amn1);
					test.log(LogStatus.PASS, "Free Wifi is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_free_wi_fi\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Free wifi is not available in Amenities");
					}
							
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
				
		//Power Backup verification
				Thread.sleep(2000);
				String act_amn2 = driver.findElement(By.xpath("//*[contains(text(),'Power Backup')]")).getText();
				//html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[2]/label")).getText();
				String exp_amn2 = "power backup";
				System.out.println(act_amn2);
				try {
					Assert.assertEquals(exp_amn2, act_amn2);
					
					test.log(LogStatus.PASS, "Power Backup is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_power_backup\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Power Backup is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
				
			//Gardern verification
				Thread.sleep(2000);
				String act_amn3 = driver.findElement(By.xpath("//*[contains(text(),'Garden/ Backyard')]")).getText();
				String exp_amn3 = "garden/ backyard";
				System.out.println(act_amn3);

				try {
					Assert.assertEquals(exp_amn3, act_amn3);
					test.log(LogStatus.PASS, "Gardern is available in Amenities");
					
					driver.findElement(By.xpath("//*[@id=\"filter_garden__backyard\"]")).click();
					} catch (AssertionError e)
					{
					
					test.log(LogStatus.FAIL, "Gardern is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		
			//SPA verification
				Thread.sleep(2000);
				String act_amn4 = driver.findElement(By.xpath("//*[contains(text(),'Spa')]")).getText();
				String exp_amn4 = "spa";
				try {
					Assert.assertEquals(exp_amn4, act_amn4);
				
					test.log(LogStatus.PASS, "SPA is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_spa\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "SPA is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

			//Swimming pool verification
				Thread.sleep(2000);
				String act_amn5 = driver.findElement(By.xpath("//*[contains(text(),'Swimming Pool')]")).getText();
				String exp_amn5 = "swimming pool";
				try {
					Assert.assertEquals(exp_amn5, act_amn5);
					test.log(LogStatus.PASS, "Swimming pool is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_swimming_pool\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Swmimming pool is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	


			//Gym verification
				Thread.sleep(2000);
				String act_amn6 = driver.findElement(By.xpath("//*[contains(text(),'Gym')]")).getText();
				String exp_amn6 = "gym";
				try {
					Assert.assertEquals(exp_amn6, act_amn6);
					test.log(LogStatus.PASS, "GYM is available in Amenities");
				
					driver.findElement(By.xpath("//*[@id=\"filter_gym\"]")).click();
					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "GYM is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

			//Laundry verification
				Thread.sleep(2000);
				String act_amn7 = driver.findElement(By.xpath("//*[contains(text(),'Laundry services')]")).getText();
				String exp_amn7 = "laundry services";
				try {
					Assert.assertEquals(exp_amn7, act_amn7);
					test.log(LogStatus.PASS, "Laundry service is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_laundry_services__washing_machine\"]")).click();
					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Laundry is not available in Amenities");

					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

			//Cafe verification
				Thread.sleep(2000);
				String act_amn8 = driver.findElement(By.xpath("//*[contains(text(),'Café')]")).getText();
				String exp_amn8 = "café";
				try {
					Assert.assertEquals(exp_amn8, act_amn8);
					
					test.log(LogStatus.PASS, "Café is available in Amenities");
					driver.findElement(By.xpath("//*[@id=\"filter_caf_\"]")).click();
					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Café is not available in Amenities");

					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	


			//Pet friendly verification
				Thread.sleep(2000);
				String act_amn9 = driver.findElement(By.xpath("//*[contains(text(),'Pet Friendly')]")).getText();
				String exp_amn9 = "pet friendly";
				try {
					Assert.assertEquals(exp_amn9, act_amn9);
					test.log(LogStatus.PASS, "Pet friendly is available in Amenities");

					driver.findElement(By.xpath("//*[@id=\"filter_pet_friendly\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Pet friendly is not available in Amenities");

					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

			//In House restaurant verification
				Thread.sleep(2000);
				String act_amn10 = driver.findElement(By.xpath("//*[contains(text(),'In-house Restaurant')]")).getText();
				String exp_amn10 = "in-house restaurant";
				try {
					Assert.assertEquals(exp_amn10, act_amn10);
					System.out.println("In-House restaurant is available in Amenities");
					test.log(LogStatus.PASS, "In-House restaurant is available in Amenities");

					driver.findElement(By.xpath("//*[@id=\"filter_in_house_restaurant\"]")).click();
					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "In-House restaurant is not available in Amenities");

					}
				
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,750)");	

			//Card Payment verification
				Thread.sleep(2000);
				String act_amn11 = driver.findElement(By.xpath("//*[contains(text(),'Card Payment')]")).getText();
				String exp_amn11 = "card payment";
				try {
					Assert.assertEquals(exp_amn11, act_amn11);
					System.out.println("Card Payment is available in Amenities");
					test.log(LogStatus.PASS, "Card Payment is available in Amenities");

					driver.findElement(By.xpath("//*[@id=\"filter_card_payment\"]")).click();
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Card Payment restaurant is not available in Amenities");
					}		
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				((JavascriptExecutor)driver).executeScript("scroll(0,750)");
			
}

@Test (priority = 14)
public void Hotel_search() throws InterruptedException				
{	
	test = report.startTest("Hotel search verification");
	//Hotel Search
	try
		{
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));
			Hotel.sendKeys("The Coffee, Pondicherry");
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");

			String Hotel_name = driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).getText();
			System.out.println(Hotel_name);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);	 
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div")).click();
			Thread.sleep(2000);	
			driver.findElement(By.xpath("//div[contains(text(),'The Coffee, Pondicherry')]/following::div[text()='View Details']"));
			test.log(LogStatus.PASS, "Able to search hotel successful");
			
}catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Unable to search hotel with given keyword");
	}
	}
	

//--- Hotel details page verification
@Test(priority = 16, invocationCount = 1)
void booking() throws IOException
{
	test = report.startTest("Hotel details page laoding and Hotel name verification");
	///*driver.navigate().to("https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD");
	//driver.manage().window().maximize();
	//multiScreens.multiScreenShot(driver); 
	
		WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div"));	
		Hotel_sel.click(); 
		String exp_hot_name = Hotel_sel.getText();
		multiScreens.multiScreenShot(driver);
	
	//Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();
	
	// Here we will check if child window has other child windows and will fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
            		driver.switchTo().window(ChildWindow);
        }}
		multiScreens.multiScreenShot(driver);
	//Scroll bar navigation
	//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
	
	// Hotel name verification
		//Hotel_Filter hot = new Hotel_Filter();
		String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
		
		try
		{
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
		
		} catch (AssertionError e)
		{
			
			test.log(LogStatus.FAIL, "Hotel name mismatching");
			
		}
		
}

@Test(priority = 17, invocationCount = 1)

public void image()
{
	test = report.startTest("Image section verification");
	try
	{
	//Image verification
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
	test.log(LogStatus.PASS, "Image loading properly");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Images are not loading properly");
	}
}

@Test(priority = 18, invocationCount = 1)
void description()
{
	// Description verification
	test = report.startTest("Hotel Description section verification");

	try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]")).isDisplayed();
			test.log(LogStatus.PASS,"Hotel description is available");
			
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Hotel description is not available");
		}
		
}

@Test(priority = 19, invocationCount = 1)

void roomtype()
{
	test = report.startTest("Room type section verification");		
	//Type of rooms section verification
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")).isDisplayed();
			test.log(LogStatus.PASS,"Type of rooms section is available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Types of rooms section missing");
		}
		
		
	  //Room type images verification	
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img")).isDisplayed();
			test.log(LogStatus.PASS,"Room type images are available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Room type images are missing");
		}
		
	 //Room type sharing details verification
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div")).isDisplayed();
			test.log(LogStatus.PASS,"Sharing details available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Sharing details are not available");
		}	
		
	// Amenities section verification
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]")).isDisplayed();
			test.log(LogStatus.PASS,"Amenities section available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Amenities section not available");
		}
		
	// Rate section verification
		try
		{
			String rate = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).getText();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).isDisplayed();
			//driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
			System.out.println("Selected currency" + rate);
			test.log(LogStatus.PASS,"Rate section available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Rate section not available");
		}
	
	// Add Room section verification
		try
		{
			driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
			test.log(LogStatus.PASS,"Add room section available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Add room section not available");
		
		}	
		
		((JavascriptExecutor)driver).executeScript("scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
}		


// 	Verification of Cancellation policies

@Test(priority = 20, invocationCount = 1)

public void cancellation_policy()
{
	test = report.startTest("Cancellation Policy section verification");
String cancel_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div")).getText();
System.out.println(cancel_type);
if (cancel_type.equals("Free Cancellation")) {
	// Card Payment verification
		String exp_title1 ="Cancellation Policy";
		String exp_title2 ="Card payment";
		String exp_text1 = "Free cancellation before 06:00:00 PM ( a day prior), service charges may apply.\n\n" 

		+ "If Cancelled after 06:00:00 PM (a day prior), the first night stay amount will be chargeable.\n\n" 

		+ "No amount will be refunded if cancelled after check in time.\n\n"

		+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n"

		+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.";

		String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
		String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
		String act_text1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]")).getText();
		try
		{
			Assert.assertEquals(exp_title1, act_title1);
			Assert.assertEquals(exp_title2, act_title2);
			Assert.assertEquals(exp_text1, act_text1);
			test.log(LogStatus.PASS, "Cancellation policy for Card payment content and title is matching");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Cancellation policy for Card payment content is not matching");
		}


	// Wire Transfer verification
		String exp_text11 = "Wire Transfer";
		String exp_text12 = "Free cancellation before 06:00:00 PM ( a day prior), service charges may apply\n\n" 
		
		+ "If Cancelled after 06:00:00 PM (a day prior), the first night stay amount will be chargeable.\n\n" 

		+ "No amount will be refunded if cancelled after check in time.\n\n"

		+ "if the guest does not show up on their confirmed booking we treat it as cancellation and no amount will be refunded.\n\n"

		+ "Late check-in hours will not be subjected to Re-fund\n\n"
				
		+ "Reserved booking will be auto cancelled incase of a failed transaction.\n\n"
				
		+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n"
				
		+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.\n\n"

		+ "Refunds are applicable only when a cancellation request has been formally submitted through Namlatic platform.\n\n"
				
		+ "Applicable Refunds will take 5 to 7 working days to process.";
				
		String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
		String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
		try
			{
				Assert.assertEquals(exp_text11, act_text11);
				Assert.assertEquals(exp_text12, act_text12);
				test.log(LogStatus.PASS, "Cancellation policy for Wire Transfer content is matching");
				} catch (Exception e)
				{
					test.log(LogStatus.FAIL,"Cancellation policy for Wire Transfer content is not matching");
				}

		// Pay at hotel
		((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
		String exp_text21 = "Pay at hotel";
		String exp_text22 = "When a booking made with \"Pay at hotel\" option, the concerned hotel will collect the entire payment against the booking at the time of check-in.\n\n" 
		
		+ "Reservation should be confirmed 48 hours before checkin time of the journey date by email or a call with Namlatic.Reservation shall be considered as confirmed only on personal verfication, else will be subjected to cancellation under Cancellation Policy Clause.\n\n" 

		+ "Following are the contact details:\n"
				
		+ "Email: confirmation@namlatic.com\n"
				
		+ "Namlatic DZ Hotline: + 213 982 414 415\n\n"
				
		+ "For international tourists, the payment will be charged in local currency or in any other currency, as decided by the hotel.\n\n"
				
		+ "For security purposes, the User must provide the 4 digit pin received with their Booking Confirmation E-mail.Namlatic or the Hotel may cancel the booking Incase If the pin found is incorrect.\n\n"

		+ "Namlatic allows Partial cancellation and Full cancellation of Booked rooms before check-in.\n\n"
				
		+ "Any booking cancelled after/during check-in time will be considered as cancellation with \"No refund\".\n\n"
				
		+ "Late Check-in hours will not subject to any partial refund.\n\n"
				
		+ "You can cancel a booking any time from Namlatic Online Booking website.\n\n"

		+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n"
				
		+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.";
				
		String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
		String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
		try
			{
			Assert.assertEquals(exp_text21, act_text21);
			Assert.assertEquals(exp_text22, act_text22);
			test.log(LogStatus.PASS, "Cancellation policy for Pay at hotel content is matching");			
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL,"Cancellation policy for Pay at hotel content is not matching");
			}
		
		// CIB
		((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
		String exp_text31 = "CIB";
		String exp_text32 = "You can cancel a booking any time from Namlatic Online Booking website.\n\n" 
		
		+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n" 

		+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.\n\n"

		+ "Refund Policy\n\n"
				
		+ "Any bookings cancelled before 6:00:00 PM through Namlatic platform will get full refund of their amount paid through CIB card (Cancellation fee may apply).\n\n"
				
		+ "Cancellation fee & service charges applied may vary according to the booking and property choosen.\n\n"
				
		+ "Applicable refund process might take 3-5 working days.\n\n"
				
		+ "No-Show\n\n"
				
		+ "No refund will be provided if a person didn't show up to the respective hotel on their confirmed booking.\n\n"
				
		+ "Namlatic does not accept any liability or responsibility for the consequences of your delayed arrival or any cancellation or charged no-show fee by the Property Provider.\n\n"
				
		+ "Late Check-in hours will not subject to any refund.";

		String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
		String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
		try
			{
			Assert.assertEquals(exp_text31, act_text31);
			Assert.assertEquals(exp_text32, act_text32);
			test.log(LogStatus.PASS, "Cancellation policy for CIB payment method content is matching");
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL, "Cancellation policy for CIB payment method content is not matching");
			}
		}
	else if (cancel_type.equals("Non-refundable"))
		{
	// Card Payment verification
		String exp_title1 ="Cancellation Policy";
		String exp_title2 ="Card payment";
		String exp_text1 = "No amount will be refunded during any time of Cancellation.\n\n" 
		
		+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n" 

		+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.";

		String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
		String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
		String act_text1 = driver.findElement(By.xpath("//div[contains(text(),'No amount will be refunded during any time of Cancellation')]")).getText();
		try
			{
			Assert.assertEquals(exp_title1, act_title1);
			Assert.assertEquals(exp_title2, act_title2);
			Assert.assertEquals(exp_text1, act_text1);
			test.log(LogStatus.PASS, "Cancellation policy for Card payment content and title is matching");
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL, "Cancellation policy for Card payment content is not matching");
			}

	// Wire Transfer verification
		String exp_text11 = "Wire Transfer";
		String exp_text12 = "No refund will be provided at any time of Cancellation.\n\n" 
				
		+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n" 

		+ "Reserved booking will be auto cancelled incase of a failed transaction.";

		String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
		String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
		try
			{
			Assert.assertEquals(exp_text11, act_text11);
			Assert.assertEquals(exp_text12, act_text12);
			test.log(LogStatus.PASS,"Cancellation policy for Wire Transfer content is matching");
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL,"Cancellation policy for Wire Transfer content is not matching");
			}

	// Pay at hotel
		((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
		String exp_text21 = "Pay at hotel";
		String exp_text22 = "When a booking made with \"Pay at hotel\" option, the concerned hotel will collect the entire payment against the booking at the time of check-in.\n\n" 
				
		+ "Reservation should be confirmed 48 hours before checkin time of the journey date by email or a call with Namlatic.Reservation shall be considered as confirmed only on personal verfication, else will be subjected to cancellation under Cancellation Policy Clause.\n\n" 

		+ "Following are the contact details:\n"
						
		+ "Email: confirmation@namlatic.com\n"
						
		+ "Namlatic DZ Hotline: + 213 982 414 415\n\n"
						
		+ "For international tourists, the payment will be charged in local currency or in any other currency, as decided by the hotel.\n\n"
						
		+ "For security purposes, the User must provide the 4 digit pin received with their Booking Confirmation E-mail.Namlatic or the Hotel may cancel the booking Incase If the pin found is incorrect.\n\n"

		+ "Namlatic allows Partial cancellation and Full cancellation of Booked rooms before check-in.\n\n"
						
		+ "Any booking cancelled after/during check-in time will be considered as cancellation with \"No refund\".\n\n"
						
		+ "Late Check-in hours will not subject to any partial refund.\n\n"
						
		+ "You can cancel a booking any time from Namlatic Online Booking website.\n\n"

		+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n"
						
		+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.";
						
		String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
		String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
		try
			{
				Assert.assertEquals(exp_text21, act_text21);
				Assert.assertEquals(exp_text22, act_text22);
				test.log(LogStatus.PASS,"Cancellation policy for Pay at hotel content is matching");
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL,"Cancellation policy for Pay at hotel content is not matching");
			}
				
	// CIB
		((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
		String exp_text31 = "CIB";
		String exp_text32 = "You can cancel a booking any time from Namlatic Online Booking website.\n\n" 
		
		+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n" 

		+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.\n\n"

		+ "Refund Policy\n\n"
						
		+ "Reservations made with the \"non-refundable\" hotel conditions are not subjected to refund on a cancellation or no show.\n\n"
						
		+ "Any booking cancelled after/during check-in time will be considered as Non-refundable cancellation with \"No refund\".";
						
		String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
		String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
		try
			{
				Assert.assertEquals(exp_text31, act_text31);
				Assert.assertEquals(exp_text32, act_text32);
				test.log(LogStatus.PASS,"Cancellation policy for CIB payment method content is matching");
			} catch (Exception e)
			{
				test.log(LogStatus.FAIL,"Cancellation policy for CIB payment method content is not matching");
			}

		}

	}

@Test(priority = 21, invocationCount = 1)

public void amenities_ver()
{
	test = report.startTest("Amenities section Verification");
	// Amenities section2 verification
				try
				{
					driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div")).isDisplayed();
					test.log(LogStatus.PASS,"Amenities section available");
				} catch (Exception e)
				{
					test.log(LogStatus.FAIL,"Amenities section not available");
				}
}

@Test(priority = 22, invocationCount = 1)

public void policy_ver()
{
	test = report.startTest("Hotel Policy Verification");

	// Hotel Policy verification
				try
				{
					driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div")).isDisplayed();
					test.log(LogStatus.PASS,"Hotel Policy section available");
				} catch (Exception e)
				{
					test.log(LogStatus.FAIL,"Hotel Policy section not available");
				}
}

@Test(priority = 23, invocationCount = 1)

public void booking_confirmation() throws InterruptedException, IOException
{				
	Date_room_selection();
//only one option should be active in script.
	CIB();
	//Card_Payment();
	//Wire_Transfer();
}

public void Date_room_selection() throws InterruptedException, IOException
{
	test = report.startTest("Date and Room selection section verification");
try
{
	//From and To Date Selection
	//	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
	//Thread.sleep(500);

	//WebElement navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
	//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", navigate);
	
	//	driver.findElement(By.xpath("//*[text()='29']")).click();
		//driver.findElement(By.xpath("//*[text()='30']")).click();

		
	//Room selection
		Thread.sleep(2000); 
		((JavascriptExecutor)driver).executeScript("scroll(0,700)");
		WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
		Select room_sel=new Select(element);
		room_sel.selectByVisibleText("1");
		room_sel.getFirstSelectedOption();
		multiScreens.multiScreenShot(driver);

		//Book now button verification
		
		
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
			test.log(LogStatus.PASS,"Book Now button available in booking section");
		 
		
	//Click on book now button
		WebElement book_now = driver.findElement (By.xpath("//*[(text()='Book Now')]"));
				book_now.click();
				multiScreens.multiScreenShot(driver);
}
catch (Exception e)
{
	test.log(LogStatus.FAIL,"Book Now button not available in Booking section");
}
report.endTest(test);	

}	

//---Payment method selection.
//Select payment methods ' CIB '  using radio butt


@Test(priority = 25, invocationCount = 1)
public void CIB() throws IOException, InterruptedException
				{
	test = report.startTest("Payment Method verification");
			try
			{
					if (driver.findElement(By.id("cib")).isEnabled())
					{
						WebElement radio_cib=driver.findElement(By.id("cib"));
						radio_cib.click();
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

			//Select Terms and Conditions - check box
				WebElement checkbox_terms=driver.findElement(By.id("terms"));
				checkbox_terms.click();
				multiScreens.multiScreenShot(driver);
					} else
					{
					//Currency selection
						WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
						Thread.sleep(500);
						cur.click();
						WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
						cur_sel.click();
					
					//CIB selection and proceed
						Date_room_selection();									
						WebElement radio_cib=driver.findElement(By.id("cib"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
						Thread.sleep(500);
						radio_cib.click();
						multiScreens.multiScreenShot(driver);
						Thread.sleep(3000);

					//Select Terms and Conditions - check box
						WebElement checkbox_terms=driver.findElement(By.id("terms"));
						checkbox_terms.click();
						multiScreens.multiScreenShot(driver);
						test.log(LogStatus.PASS,"CIB Card selected");
					}
			} catch(AssertionError e)
			{
				test.log(LogStatus.FAIL,"Unable to select CIB card");
			}
			report.endTest(test);	
					}


public void Card_Payment() throws IOException, InterruptedException
		{
			WebElement radio_card=driver.findElement(By.id("card"));
			radio_card.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			System.out.println("Card Payment selected");
		}
		
		public void Wire_Transfer() throws IOException, InterruptedException
		{
			WebElement radio_wt=driver.findElement(By.id("wt"));
			radio_wt.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			System.out.println("Wire Transfer selected");
		}

@Test(priority = 26, invocationCount = 1)
public void various_section_Validation() throws InterruptedException
{
	// Booking section verification
	test = report.startTest("Verification of other section in Room details page");
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div")).isDisplayed();
			test.log(LogStatus.PASS,"Booking section available");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Booking section not available");
		}
		
		//Pricing
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]")).isDisplayed();
			test.log(LogStatus.PASS,"Pricing details available in booking section");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Pricing details not available in Booking section");
		}
		
		// Calender
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).isDisplayed();
			test.log(LogStatus.PASS,"Calender details available in booking section");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Calender details not available in Booking section");
		}
		
	/*	//Guest
		try
		{
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
			test.log(LogStatus.PASS,"Guest details available in booking section");
		} catch (Exception e)
		{
			test.log(LogStatus.FAIL,"Guest details not available in Booking section");
		}
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,2000)");*/

	//Navigating to various section
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
		Thread.sleep(1000);
		//((JavascriptExecutor)driver).executeScript("scroll(0,400)");

		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
		Thread.sleep(2000);
		
	/* //Select reCAPTCHA Check box
		driver.switchTo().defaultContent().findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"));
		System.out.println("test");
		WebElement checkbox_recaptcha=driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
		checkbox_recaptcha.click();
		System.out.println("reCAPTCHA check box is selected"); 
	

	//Click on Proceed To Pay button
		//WebElement Proceed_element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"));
		//Proceed_element.click(); */
}

//---My Booking Page Validation


public void pg_launch() throws InterruptedException, IOException
{
	
Thread.sleep(3000);
((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
	menu.click();
	WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
	menuoption.click();
	multiScreens.multiScreenShot(driver);
}

@Test(priority = 28, invocationCount = 1)
public void view_booking() throws IOException, InterruptedException
{		
//View Booking
	test = report.startTest("My Booking Page - View Booking option verification");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();
		WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
	try
	{
	WebElement view_book_id = driver.findElement(By.xpath("//div[contains(text(),'NS78P4QYDI5')]/following::div[text()='View booking']")); 
	view_book_id.click();
	test.log(LogStatus.PASS, "View booking section working fine");	
	multiScreens.multiScreenShot(driver);
	
	

driver.getWindowHandle();			
		String mainWindowHandle1 = driver.getWindowHandle();
		Set<String> allWindowHandles1 = driver.getWindowHandles();
		Iterator<String> iterator1 = allWindowHandles1.iterator();
	//Here we will check if child window has other child windows and will fetch the heading of the child window
		 while (iterator1.hasNext()) {
		     String ChildWindow1 = iterator1.next();
		           if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
				            driver.switchTo().window(ChildWindow1);
				      
						        }}
		//Scroll Down in View Booking Details page
		 ((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	//WebDriverWait wait=new WebDriverWait(driver,50);
	Thread.sleep(5000);
	multiScreens.multiScreenShot(driver);
						
//Click on Download Voucher link

	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	//WebElement download_Vou =driver.findElement(By.xpath("//*[(@class, 'sc-plWPA hwKzKa']"));
	Thread.sleep(5000); 
	WebElement download_Vou =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
	download_Vou.click();
	
	Thread.sleep(5000); 
	test.log(LogStatus.PASS, "Download voucher working fine");
	
//Click on Email Voucher link
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	Thread.sleep(5000); 
	WebElement send_email=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
	send_email.click();
		
	Thread.sleep(5000);
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS, "Email voucher working fine");				
	
/* //Click on Print Voucher link	
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	Thread.sleep(5000); 
	WebElement print_Vou=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
	print_Vou.click(); 
		
	Thread.sleep(5000);
	Robot esc_print_window = new Robot();
	esc_print_window.keyPress(KeyEvent.VK_ESCAPE);
	esc_print_window.keyRelease(KeyEvent.VK_ESCAPE);
	test.log(LogStatus.PASS, "Print voucher working fine");	
		*/
	} catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Facing problem in View booking section");
	}


}

@Test(priority = 27)
void space()
{
	//
}
//Cancellation
@Test(priority = 29, invocationCount = 1)
void cancellation() throws AWTException, InterruptedException, IOException
{
	test = report.startTest("My Booking Page - Cancellation option verification");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();
		WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);


	Thread.sleep(3000);
	try
		{
			WebElement cancel = driver.findElement(By.xpath("//div[contains(text(),'NS78P4QYDI5')]/following::div[text()='Cancel booking']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
			String Price = driver.findElement(By.xpath("//div[contains(text(),'NS78P4QYDI5')]/following::div[@class='sc-pCOPB jZjRAo']")).getText().substring(1);
			float exp_price = Float.parseFloat(Price);
			double exp_p1 = Math.round(exp_price*100.0)/100;
			cancel.click();
			test.log(LogStatus.PASS, "Able to select 'Cancel button' successfully");
			driver.findElement(By.id("reason-1")).click();
					
	// Validation:
			//Title verification
					String exp_title = "Cancel your booking";
					String act_title = driver.findElement(By.xpath("//div[contains(text(),'Cancel your booking')]")).getText();
					
					try
					{
						Assert.assertEquals(exp_title, act_title);
						test.log(LogStatus.PASS, "Cancellation Popup Title matching");
					} catch (Exception e)
					{
						test.log(LogStatus.FAIL, "Cancellation Popup Title is mismatching");
					}
					
			// cancell info verification
					String exp_info ="Select the room you wanted to cancel";
					String act_info = driver.findElement(By.xpath("//div[contains(text(),'Select the room you wanted to cancel')]")).getText();
					
					try
					{
						Assert.assertEquals(exp_info, act_info);
						test.log(LogStatus.PASS,"Cancellation Info. matching");
					} catch (Exception e)
					{
						test.log(LogStatus.FAIL,"Cancellation Info. mismatching");
					}
					
			// Price verification
					String act_price1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[2]")).getText().substring(1);
				/*	if ( driver.findElement(By.xpath("//*[@class='sc-pcHDm cOnSkc']")).isDisplayed())
						{ 
							String act_price2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[6]/div/div[3]/div[2]/div[2]/div[2]")).getText().substring(1);
							
							float actprice1f =Float.parseFloat(act_price1);
							double p1 = Math.round(actprice1f*100.0)/100;
							float actprice2f = Float.parseFloat(act_price2);
							double p2 = Math.round(actprice2f*100.0)/100;
							double act_price3= p1 + p2;
							System.out.println(act_price3);
							try
							{
								Assert.assertEquals(exp_p1, act_price3);
								System.out.println("Price matching");
							} catch(AssertionError e)
							{
								System.out.println("Price not matching");
							}
						} else
						{	*/
						try
							{
							float actprice1f =Float.parseFloat(act_price1);
							double p1 = Math.round(actprice1f*100.0)/100;
								Assert.assertEquals(exp_p1, p1);
								test.log(LogStatus.PASS,"Cancellation Price matching");
							} catch(AssertionError e)
							{
								test.log(LogStatus.FAIL,"Cancellation Price not matching");
							}
						
							
					
			// Verification of cancallation title
					String exp_title1 = "Tell us your reason for cancelling booking";
					String act_title1 = driver.findElement(By.xpath("//div[contains(text(),'Tell us your reason for cancelling booking')]")).getText();

					try
					{
						Assert.assertEquals(exp_title1, act_title1);
						test.log(LogStatus.PASS,"Cancellation Reason title matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation Reason title is not matching");
					}
					
			// Verification of cancallation title
					String exp_opt1 = "Change of dates / destination";
					String act_opt1 = driver.findElement(By.xpath("//*[contains(text(),'Change of dates / destination')]")).getText();

					try
					{
						Assert.assertEquals(exp_opt1, act_opt1);
						test.log(LogStatus.PASS,"Cancellation option 1 matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation option 1 not matching");
					}
					
			// Verification of cancallation title
					String exp_opt2 = "Personal reason";
					String act_opt2 = driver.findElement(By.xpath("//*[contains(text(),'Personal reason')]")).getText();

					try
					{
						Assert.assertEquals(exp_opt2, act_opt2);
						test.log(LogStatus.PASS,"Cancellation option 2 matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation option 2 not matching");
					}
				

			// Verification of cancallation title
					String exp_opt3 = "Found alternative accomodation";
					String act_opt3 = driver.findElement(By.xpath("//*[contains(text(),'Found alternative accomodation')]")).getText();

					try
					{
						Assert.assertEquals(exp_opt3, act_opt3);
						test.log(LogStatus.PASS,"Cancellation option 3 matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation option 3 not matching");
					}
		
			// Verification of cancallation title
					String exp_opt4 = "Change in number of travellers";
					String act_opt4 = driver.findElement(By.xpath("//*[contains(text(),'Change in number of travellers')]")).getText();

					try
					{
						Assert.assertEquals(exp_opt4, act_opt4);
						test.log(LogStatus.PASS,"Cancellation option 4 matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation option 4 not matching");
					}

			// Verification of cancallation title
					String exp_opt5 = "Other reason";
					String act_opt5 = driver.findElement(By.xpath("//*[contains(text(),'Other reason')]")).getText();

					try
					{
						Assert.assertEquals(exp_opt5, act_opt5);
						test.log(LogStatus.PASS,"Cancellation option 5 matching");
					} catch(AssertionError e)
					{
						test.log(LogStatus.FAIL,"Cancellation option 5 not matching");
					}
					
					Robot esc = new Robot();
					esc.keyPress(KeyEvent.VK_ESCAPE);
		}catch (AssertionError e)
		{
			test.log(LogStatus.FAIL,"Cancellation section is not working properly");
		}
}

@Test(priority = 30, invocationCount = 1)
public void book_now() throws InterruptedException, IOException
{
	Thread.sleep(3000);	
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		test = report.startTest("My Booking Page - Booknow option verification");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();

		driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]")).click();

	
	try
	{
	/*	WebElement Hotel_sel1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[2]/div/div[1]"));	
		String exp_hot_name1 = Hotel_sel1.getText();
		multiScreens.multiScreenShot(driver);
		*/WebElement Bk_now = driver.findElement(By.xpath("//div[contains(text(),'N7NH9JRUJ3O')]/following::div[text()='Book Now']"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
	Thread.sleep(500);
	
	Bk_now.click();


		
		// Hotel name verification
			/*String act_hot_name1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
			
			try
			{
				Assert.assertEquals(exp_hot_name1, act_hot_name1);
				test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name1);
			
			} catch (AssertionError e)
			{
				
				test.log(LogStatus.FAIL, "Hotel name mismatching");				
			}
			report.endTest(test);	
*/

			try {
			//Room selection
			Thread.sleep(2000); 
			((JavascriptExecutor)driver).executeScript("scroll(0,700)");
			WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
			Select room_sel=new Select(element);
			room_sel.selectByVisibleText("1");
			room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);

			//Book now button verification
			
			
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				test.log(LogStatus.PASS,"Book Now button available in booking section");
			} catch (AssertionError e)
			{
				test.log(LogStatus.PASS,"Book Now button not available in booking section");
			}
			report.endTest(test);	
			
		//Click on book now button
			WebElement book_now = driver.findElement (By.xpath("//*[(text()='Book Now')]"));
					book_now.click();
					multiScreens.multiScreenShot(driver);

	test.log(LogStatus.PASS, "Able to proceed booking via 'Book now' from My Booking");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Facing error while proceeding booking via 'Book own' from My Booking");
	}
}
	

// -- Profile Edit
@Test(priority = 31, invocationCount = 1)
	public void Profileedit() throws InterruptedException, NullPointerException, IOException
	{
	test = report.startTest("Edit Profile verification");

	try
	{
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
		Actions builder=new Actions(driver);
		builder.moveToElement(menu).build().perform();
		//wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM wEvDo']")));
		WebElement menuoption=driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
		WebElement fname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/input"));
		fname.clear();
		fname.sendKeys("Test");
	
		WebElement lname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div[2]/input"));
		lname.clear();
		lname.sendKeys("User");	
	
		((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	
		multiScreens.multiScreenShot(driver);
		WebElement prsave =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[6]/div"));
		prsave.click();
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "Edit Profile: PASS");
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	} catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Unable to Edit Profile");	
	}
	}

// Signout
@Test(priority = 32, invocationCount = 1)
public void Signoff() throws IOException, InterruptedException
{
	test = report.startTest("Signout verification");
	try
	{
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
	menu.click();

	WebElement menu_signout=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[3]"));
	menu_signout.click();
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS, "Signout: PASS");
	} catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Facing error while proceeding signout");
	}
 
	driver.quit();

} 

@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
}
}

