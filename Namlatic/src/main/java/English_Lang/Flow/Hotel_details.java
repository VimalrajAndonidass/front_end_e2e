package English_Lang.Flow;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import English_Lang.Flow.Supporting_Files.Booking;
import English_Lang.Flow.Supporting_Files.Declaration;
import English_Lang.Flow.Supporting_Files.Login;
import English_Lang.Flow.Supporting_Files.Search;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

@SuppressWarnings("unused")
public class Hotel_details extends Booking {

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\English\\Booking\\", "Hotel Details");
	public static ExtentTest test;
	public static ExtentReports report;
	public Fillo fillo = new Fillo();
	Booking book = new Booking();
	Search srh = new Search();
	Login lgn = new Login();
	Declaration declare = new Declaration();
	
	@BeforeClass
	public void declaration() throws Exception {
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\English\\Booking\\" + "Hotel_Details.html");
		Declaration.chrome_declare();
		//Declaration.firefox_declare();
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));	
	}

	@Test(priority = 1)
	public void launch() throws IOException, FilloException, InterruptedException {
		
		test = report.startTest("Verification of Elastic search");
		try
		{
			book.URL_Launch();
			English_Lang.Flow.Supporting_Files.Login.Corporate_User_Login();
			Search.Free_cancel_48();
		test.log(LogStatus.PASS,
				"Able to search the hotel");
	}
	catch (Exception e)
	{
		test.log(LogStatus.FAIL,
				"Unable to search the hotel");
	}
}

	@Test(priority = 2)
	public void CP_booking() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		try {
			book.booking();
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
			
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}
		
		
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}