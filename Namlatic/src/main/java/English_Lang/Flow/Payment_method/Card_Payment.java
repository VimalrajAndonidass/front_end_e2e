package English_Lang.Flow.Payment_method;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import English_Lang.Flow.Supporting_Files.Booking;
import English_Lang.Flow.Supporting_Files.Declaration;
import English_Lang.Flow.Supporting_Files.Login;
import English_Lang.Flow.Supporting_Files.Login;
import English_Lang.Flow.Supporting_Files.Search;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

@SuppressWarnings("unused")
public class Card_Payment extends Booking {

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"D:\\Automation\\Screenshots\\Front_End\\English\\Booking\\", "Card Payment");
	public static ExtentTest test;
	public static ExtentReports report;
	public Fillo fillo = new Fillo();
	Booking book = new Booking();
	Search srh = new Search();
	Login lgn = new Login();
	Declaration declare = new Declaration();
	
	@BeforeClass
	public void declaration() throws Exception {
		report = new ExtentReports("D:\\Automation\\Reports\\Front_End\\English\\Booking\\" + "Card_Payment.html");
		Declaration.chrome_declare();
		//Declaration.firefox_declare();
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));	
	}

	@Test(priority = 1)
	public void start() throws InterruptedException, IOException, FilloException, AWTException {
		test = report.startTest("Scenario");
		try {
			test.log(LogStatus.PASS, "Booking via Card Payment(English)");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 2)
	public void launch() throws IOException, FilloException, InterruptedException {
		test = report.startTest("URL Verification");
		try {
			book.URL_Launch();
			test.log(LogStatus.PASS, "URL Verified");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL not matching");
		}
	}

	@Test(priority = 3)
	public void login() throws IOException, InterruptedException, FilloException {
		test = report.startTest("Verification of Login Functionality");
		try {
			English_Lang.Flow.Supporting_Files.Login.Corporate_User_Login();
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}
	
	@Test(priority = 4)
	public void search() {
	test = report.startTest("Verification of Elastic search");
	try
	{
		Thread.sleep(3000);
		Search.Free_cancel_72();
		test.log(LogStatus.PASS,
				"Able to search the hotel");
	}
	catch (Exception e)
	{
		test.log(LogStatus.FAIL,
				"Unable to search the hotel");
	}
}

	@Test(priority = 5)
	public void CP_booking() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		try {
			book.booking();
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
			
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}
		
		try
		{
			book.booking_confirmation();
			test.log(LogStatus.PASS, "Language changed successfully to USD");
			test.log(LogStatus.PASS, "Start and End date selected properly");
			test.log(LogStatus.PASS, "Rooms selected successfully");
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");

			test.log(LogStatus.PASS,
					"Hotel details page loading properly and allowing to proceed booking successfully");
			
		} catch (AssertionError e) {
			test.log(LogStatus.PASS, "Unable to change Language");
			test.log(LogStatus.FAIL, "Unable to select start/ end date");
			test.log(LogStatus.FAIL, "Unable to select Rooms");
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");

			test.log(LogStatus.FAIL, "Hotel details page not loading properly");
		}
			
	}

	@Test(priority = 6)
	public void guest() throws IOException, InterruptedException {
		test = report.startTest("Guest details updation");
		try {
			book.Guest_details();
			test.log(LogStatus.PASS, "Main Guest details are autopopulated successfully");
			test.log(LogStatus.PASS, "Additional Guest details are successfully");

			test.log(LogStatus.PASS, "Able to proceed payment page successfully with guest details");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Main Guest details are not autopopulating");
			test.log(LogStatus.FAIL, "Additional Guest details are unable to add");

			test.log(LogStatus.FAIL, "Unable to proceed payment page");

		}
	}

	@Test(priority = 7)
	public void Paymode_selection() throws InterruptedException {
		test = report.startTest("Card Payment Verification");
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,0)");

		try {
			Thread.sleep(3000);
			WebElement radio_wt = driver.findElement(By.id("card"));
			radio_wt.click();
			Thread.sleep(3000);
			
			WebElement saved_card = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/wrapper/div[1]/div/div[3]/div[3]/div[1]/input"));
			saved_card.click();
			
			WebElement CVV = driver.findElement(By.xpath("/html/body/div[1]/form/span[2]/span/input"));
			CVV.sendKeys("123");
			
			test.log(LogStatus.PASS,
					"Able to select 'Card Payment' option and allowing to proceed further successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select 'Card Payment'option");

		}

		//Click on Book now button
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[contains(@class,'sc-beqWaB cQLiug')]")).click();

			Thread.sleep(4000);
			WebElement card_name = driver
					.findElement(By.xpath("//*[contains(@placeholder,'Enter the name on your card')]"));
			card_name.sendKeys("Test");
			Thread.sleep(3000);

			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Secure card number input frame']")));
			Thread.sleep(2000);
			System.out.println("In frame");
			WebElement card_no = driver.findElement(By.xpath("//*[contains(@name, 'cardnumber')]"));
			card_no.clear();
			card_no.sendKeys("4242 4242 4242 4242");
			driver.switchTo().parentFrame(); // Parent Frame

			driver.switchTo()
					.frame(driver.findElement(By.xpath("//iframe[@title='Secure expiration date input frame']")));
			Thread.sleep(2000);
			System.out.println("In frame");
			WebElement exp_date = driver.findElement(By.xpath("//*[contains(@placeholder,'MM / YY')]"));
			exp_date.sendKeys("0224");
			driver.switchTo().parentFrame(); // Parent Frame

			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Secure CVC input frame']")));
			Thread.sleep(2000);
			System.out.println("In frame");
			WebElement cvv = driver.findElement(By.xpath("//*[contains(@placeholder,'CVC')]"));
			cvv.sendKeys("123");
			driver.switchTo().parentFrame(); // Parent Frame

			driver.findElement(By.xpath("//*[contains(text(),'Proceed to pay')]")).click();

			test.log(LogStatus.PASS, "Payment done successfully via Card Payment");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Payment failed");
		}

	}

	@Test(priority = 8)
	public void voucher() throws IOException, InterruptedException, AWTException {
		{
			test = report.startTest("Voucher Verification");

			try {
				Thread.sleep(3000);
				book.payment();
				test.log(LogStatus.PASS,
						"Payment done successfully via Card Payment and following are the booking details"
								+ "<br/>  Booking ID: " + Booking_ID + "<br/> Transaction Time and date: "
								+ Trans_time_date + "<br/>  Hotel Name: " + Hotel_Name_voucher
								+ "<br/>  Check In(Time and Date): " + Check_in + "<br/>  Check Out(Time and Date): "
								+ Check_out + "<br/>  Guest Count: " + Guest_count + "<br/>  Room Count: " + Room_count
								+ "<br/>  Guest name: " + Guest_Name + "<br/>  Room Type: " + Room_Type
								+ "<br/>  Booking Amount: " + Booking_amt);
				test.log(LogStatus.PASS, "Download voucher working fine");
				test.log(LogStatus.PASS, "Email voucher working fine");
				test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);

			} catch (AssertionError e) {
				test.log(LogStatus.FAIL,
						"Payment done successfully via PAH Payment but voucher not loaded properly and details are mismatching");
				test.log(LogStatus.FAIL, "Unable to Download voucher");
				test.log(LogStatus.FAIL, "Unable to Email voucher");
				test.log(LogStatus.FAIL, "Hotel booking is not done successfully");

			}
		}
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}