package English_Lang.Flow.Payment_method;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import English_Lang.Flow.Supporting_Files.Booking;
import English_Lang.Flow.Supporting_Files.Declaration;
import English_Lang.Flow.Supporting_Files.Login;
import English_Lang.Flow.Supporting_Files.Search;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Namla_Wallet extends Booking {

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"D:\\Automation\\Screenshots\\Front_End\\English\\Booking\\", "Namla Wallet");
	public static ExtentTest test;
	public static ExtentReports report;
	public Fillo fillo = new Fillo();
	Booking book = new Booking();
	Search srh = new Search();
	Login lgn = new Login ();
	Declaration declare = new Declaration();

	@BeforeClass
	public void declaration() throws Exception {
		Declaration.chrome_declare();
		//Declaration.firefox_declare();
		report = new ExtentReports("D:\\Automation\\Reports\\Front_End\\English\\Booking\\" + "Namla_Wallet.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 1)
	public void start() throws InterruptedException, IOException, FilloException, AWTException {
		test = report.startTest("Scenario");
		try {
			test.log(LogStatus.PASS, "Booking via Namla Wallet(English)");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 2)
	public void launch() throws IOException, FilloException, InterruptedException {
		test = report.startTest("URL Verification");
		try {
			book.URL_Launch();
			test.log(LogStatus.PASS, "URL Verified");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL not matching");
		}
	}

	@Test(priority = 3)
	public void login() throws IOException, InterruptedException, FilloException {
		test = report.startTest("Verification of Login Functionality");
		try {
			English_Lang.Flow.Supporting_Files.Login.Corporate_User_Login();
			//Thread.sleep(2);
			//driver.get("https://test.namlatic.com");
			System.out.println("New");
			
			//driver.get("https://test.namlatic.com//");
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}

	@Test(priority = 4)
	public void search() {
		
	test = report.startTest("Verification of Elastic search");
	try
	{
		Thread.sleep(3000);
		//driver.get("https://test.namlatic.com");
		Search.city_search();
		test.log(LogStatus.PASS,
				"Able to search the hotel");
	}
	catch (Exception e)
	{
		test.log(LogStatus.FAIL,
				"Unable to search the hotel");
	}
}
	
	@Test(priority = 5)
	public void NW_booking() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		try {
			book.booking();
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
			
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}
		
		try
		{
			book.booking_confirmation();
			test.log(LogStatus.PASS, "Language changed successfully to USD");
			test.log(LogStatus.PASS, "Start and End date selected properly");
			test.log(LogStatus.PASS, "Rooms selected successfully");
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");

			test.log(LogStatus.PASS,
					"Hotel details page loading properly and allowing to proceed booking successfully");
			
		} catch (AssertionError e) {
			test.log(LogStatus.PASS, "Unable to change Language");
			test.log(LogStatus.FAIL, "Unable to select start/ end date");
			test.log(LogStatus.FAIL, "Unable to select Rooms");
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");

			test.log(LogStatus.FAIL, "Hotel details page not loading properly");
		}
	}

	@Test(priority = 6)
	public void guest() throws IOException, InterruptedException {
		test = report.startTest("Guest details updation");
		try {
			book.Guest_details();
			test.log(LogStatus.PASS, "Main Guest details are autopopulated successfully");
			test.log(LogStatus.PASS, "Additional Guest details are successfully");

			test.log(LogStatus.PASS, "Able to proceed payment page successfully with guest details");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Main Guest details are not autopopulating");
			test.log(LogStatus.FAIL, "Additional Guest details are unable to add");

			test.log(LogStatus.FAIL, "Unable to proceed payment page");

		}
	}

	@Test(priority = 7)
	public void Paymode_selection() throws InterruptedException, FilloException, IOException {
		test = report.startTest("Namla Wallet Payment Verification");
		try {
			Thread.sleep(3000);
			WebElement radio_wt = driver.findElement(By.id("namla"));
			radio_wt.click();
			Thread.sleep(3000);
			test.log(LogStatus.PASS,
					"Able to select 'Namla Wallet' option");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select 'Namla Wallet'option");

		}
		try {
			Thread.sleep(3000);
			WebElement Book_now = driver.findElement(By.xpath("//*[contains(text(),'Book now')]"));
			Book_now.click();
			test.log(LogStatus.PASS,
					"Able to proceed further successfully");
		}
		catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select 'Book now");

		}
		
		try
		{
		String baseURL = driver.getCurrentUrl();  
	    ((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1)); //switches to new tab
	    driver.get("https://yopmail.com");
	    driver.switchTo().window(tabs.get(0)); // switch back to main screen        

		// Get handles of the windows
				String mainWindowHandle = driver.getWindowHandle();
				Set<String> allWindowHandles = driver.getWindowHandles();
				Iterator<String> iterator = allWindowHandles.iterator();

				// Here we will check if child window has other child windows and will fetch the
				// heading of the child window
				while (iterator.hasNext()) {
					String ChildWindow = iterator.next();
					if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
						driver.switchTo().window(ChildWindow);
					}
				}
		
		WebElement userid = driver.findElement(By.xpath("//*[@id=\"login\"]"));
		userid.sendKeys("B2B@yopmail.com");
		
		WebElement proceed = driver.findElement(By.xpath("/html/body/div[1]/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div[1]/div[4]/button/i"));
		proceed.click();
		
		Thread.sleep(15000);
		WebElement refresh = driver.findElement(By.xpath("//*[@id=\"refresh\"]"));
		refresh.click();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"ifmail\"][1]")));
	//	System.out.println("Within frame");
		Thread.sleep(3000);
		
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		Thread.sleep(3000);
		WebElement OTP_Text = driver.findElement (By.xpath("/html/body/main/div/div/div/div/div/div[3]"));
		String OTP =OTP_Text.getText();
		System.out.println(OTP);
		driver.close();

	//Back to main screen
		 driver.switchTo().window(tabs.get(0)); // switch back to main screen  
		 int OTP_i=Integer.parseInt(OTP);
		 int mod1 = OTP_i % 1000;
		 int mod2 = OTP_i % 100;
		 int mod3 = OTP_i % 10;
		 
		 int S1 = OTP_i / 1000;
		 String s1=Integer.toString(S1);
		// System.out.println("S1:" + S1);
		 int S2 = mod1 / 100;
		 String s2=Integer.toString(S2);
		// System.out.println("mod1:" + mod1);
		// System.out.println("S2:" + S2);
		 int S3 = mod2/10;
		 String s3=Integer.toString(S3);
		 // System.out.println("mod2:" + mod2);
		 // System.out.println("S3:" + S3);
		 // System.out.println("mod3:" + mod3);
		 String s4=Integer.toString(mod3);
		 WebElement OTP_1 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/div/div/div[1]/input[1]"));
		 OTP_1.sendKeys(s1);
		 
		 WebElement OTP_2 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/div/div/div[1]/input[2]"));
		 OTP_2.sendKeys(s2);

		 WebElement OTP_3 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/div/div/div[1]/input[3]"));
		 OTP_3.sendKeys(s3);
		 
		 WebElement OTP_4 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/div/div/div[1]/input[4]"));
		 OTP_4.sendKeys(s4);
		 
		 WebElement submit = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div/div/div/div[2]/div"));
		 submit.click();
		 
		 test.log(LogStatus.PASS, "Payment successful");
		}
		catch (Exception e) {
			test.log(LogStatus.FAIL, "Invalid/ Expired OTP");

		}
		
	}
	
	

	@Test(priority = 8)
	public void voucher() throws IOException, InterruptedException, AWTException {
		{
			test = report.startTest("Voucher Verification");

			try {
				book.payment();
				test.log(LogStatus.PASS,
						"Payment done successfully via Namla Wallet Payment and following are the booking details"
								+ "<br/>  Booking ID: " + Booking_ID + "<br/> Transaction Time and date: "
								+ Trans_time_date + "<br/>  Hotel Name: " + Hotel_Name_voucher
								+ "<br/>  Check In(Time and Date): " + Check_in + "<br/>  Check Out(Time and Date): "
								+ Check_out + "<br/>  Guest Count: " + Guest_count + "<br/>  Room Count: " + Room_count
								+ "<br/>  Guest name: " + Guest_Name + "<br/>  Room Type: " + Room_Type
								+ "<br/>  Booking Amount: " + Booking_amt);
				test.log(LogStatus.PASS, "Download voucher working fine");
				test.log(LogStatus.PASS, "Email voucher working fine");
				test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);

			} catch (AssertionError e) {
				test.log(LogStatus.FAIL,
						"Payment done successfully via Namla Wallet Payment but voucher not loaded properly and details are mismatching");
				test.log(LogStatus.FAIL, "Unable to Download voucher");
				test.log(LogStatus.FAIL, "Unable to Email voucher");
				test.log(LogStatus.FAIL, "Hotel booking is not done successfully");

			}
		}
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}