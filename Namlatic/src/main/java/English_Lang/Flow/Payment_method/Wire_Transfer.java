package English_Lang.Flow.Payment_method;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import English_Lang.Flow.Supporting_Files.Booking;
import English_Lang.Flow.Supporting_Files.Declaration;
import English_Lang.Flow.Supporting_Files.Login;
import English_Lang.Flow.Supporting_Files.Search;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Wire_Transfer extends Booking {

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"D:\\Automation\\Screenshots\\Front_End\\English\\Booking\\", "Wire Transfer");
	public static ExtentTest test;
	public static ExtentReports report;
	public Fillo fillo = new Fillo();
	Booking book = new Booking();
	Search srh = new Search();
	Login lgn = new Login();
	Declaration declare = new Declaration();

	@BeforeClass
	public void declaration() throws Exception {
		Declaration.chrome_declare();
		//Declaration.firefox_declare();
		report = new ExtentReports("D:\\Automation\\Reports\\Front_End\\English\\Booking\\" + "Wire Transfer.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 1)
	public void start() throws InterruptedException, IOException, FilloException, AWTException {
		test = report.startTest("Scenario");
		try {
			test.log(LogStatus.PASS, "Booking via Wire Transfer(English)");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 2)
	public void launch() throws IOException, FilloException, InterruptedException {
		test = report.startTest("URL Verification");
		try {
			book.URL_Launch();
			test.log(LogStatus.PASS, "URL Verified");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL not matching");
		}
	}

	@Test(priority = 3)
	public void login() throws IOException, InterruptedException, FilloException {
		test = report.startTest("Verification of Login Functionality");
		try {
			English_Lang.Flow.Supporting_Files.Login.Hotel_Owner_Login();;
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}

	@Test(priority = 4)
	public void search() {
	test = report.startTest("Verification of Elastic search");
	try
	{
		Search.city_search();
		test.log(LogStatus.PASS,
				"Able to search the hotel");
	}
	catch (Exception e)
	{
		test.log(LogStatus.FAIL,
				"Unable to search the hotel");
	}
}
	
	@Test(priority = 5)
	public void WT_booking() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		
		try {
			book.booking();
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
			
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}
		
		//Date selection
		try {
			book.currency_sel();
			book.date_selection();
			test.log(LogStatus.PASS, "Date selected: " );		}
		catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select the date");
		} 
		
		//Room selection
		try
		{
			book.booking_confirmation();
			test.log(LogStatus.PASS, "Language changed successfully to USD");
			test.log(LogStatus.PASS, "Start and End date selected properly");
			test.log(LogStatus.PASS, "Rooms selected successfully");
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");
			test.log(LogStatus.PASS,
					"Hotel details page loading properly and allowing to proceed booking successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.PASS, "Unable to change Language");
			test.log(LogStatus.FAIL, "Unable to select start/ end date");
			test.log(LogStatus.FAIL, "Unable to select Rooms");
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");

			test.log(LogStatus.FAIL, "Hotel details page not loading properly");
		}
	}

	@Test(priority = 6)
	public void guest() throws IOException, InterruptedException {
		test = report.startTest("Guest details updation");
		try {
			book.Guest_details();
			test.log(LogStatus.PASS, "Main Guest details are autopopulated successfully");
			test.log(LogStatus.PASS, "Additional Guest details are successfully");
			test.log(LogStatus.PASS, "Able to proceed payment page successfully with guest details");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Main Guest details are not autopopulating");
			test.log(LogStatus.FAIL, "Additional Guest details are unable to add");
			test.log(LogStatus.FAIL, "Unable to proceed payment page");
		}
	}

	@Test(priority = 7)
	public void Paymode_selection() throws InterruptedException {
		test = report.startTest("Wire Transfer Payment Verification");
		try {
			Thread.sleep(3000);
			WebElement radio_wt = driver.findElement(By.id("wt"));
			radio_wt.click();
			Thread.sleep(3000);
			test.log(LogStatus.PASS,
					"Able to select 'Wire Transfer' option and allowing to proceed further successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select 'Wire Transfer'option");

		}
		
		//Click on Book now button
				try {
					((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[contains(@class,'sc-beqWaB cQLiug')]")).click();
					test.log(LogStatus.PASS, "Payment done successfully via Wire Transfer Payment");
				} catch (AssertionError e) {
					test.log(LogStatus.FAIL, "Payment failed");
				}
	}

	@Test(priority = 8)
	public void voucher() throws IOException, InterruptedException, AWTException {
		{
			test = report.startTest("Voucher Verification");
			try {
				book.payment();
				driver.findElement(By.xpath("//div[contains(@class,'sc-pTStT ejVUNV')]")).getText();
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[1]/div[2]/div[2]")).getText();
				driver.findElement(By.xpath("//div[contains(@class,'sc-qOwrV hysHKf')]")).getText();
				driver.findElement(By.xpath("//div[contains(@class,'sc-qOwrV hysHKf')]")).getText();
				driver.findElement(By.xpath("//div[contains(@class,'sc-pZEQg cDKGcz')]")).getText();
				driver.findElement(By.xpath("//div[contains(@class,'sc-pjVMd iyFleE')]")).getText();
				driver.findElement(By.xpath("//div[contains(@class,'sc-qYfxO jMKVEm')]")).getText();
				driver.findElement(By.xpath("//*[contains(@class,'sc-qYSBj hkhgWR')]")).getText();
				test.log(LogStatus.PASS,
						"Payment done successfully via WT Payment and following are the booking details"
								+ "<br/>  Booking ID: " + Booking_ID + "<br/> Transaction Time and date: "
								+ Trans_time_date + "<br/>  Hotel Name: " + Hotel_Name_voucher
								+ "<br/>  Check In(Time and Date): " + Check_in + "<br/>  Check Out(Time and Date): "
								+ Check_out + "<br/>  Guest Count: " + Guest_count + "<br/>  Room Count: " + Room_count
								+ "<br/>  Guest name: " + Guest_Name + "<br/>  Room Type: " + Room_Type
								+ "<br/>  Booking Amount: " + Booking_amt);
				test.log(LogStatus.PASS, "Download voucher working fine");
				test.log(LogStatus.PASS, "Email voucher working fine");
				test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);

			} catch (AssertionError e) {
				test.log(LogStatus.FAIL,
						"Payment done successfully via WT Payment but voucher not loaded properly and details are mismatching");
				test.log(LogStatus.FAIL, "Unable to Download voucher");
				test.log(LogStatus.FAIL, "Unable to Email voucher");
				test.log(LogStatus.FAIL, "Hotel booking is not done successfully");
			}
		}
		
	}
		@Test(priority = 9)
		public void receipt_upload() throws InterruptedException, IOException, AWTException
		{
			// My Booking verification
			try {
				((JavascriptExecutor) driver).executeScript("scroll(0,0)");
				Thread.sleep(3000);
				WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[1]/div"));
				menu.click();
				WebElement menuoption = driver
						.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[2]/ul/li[2]"));
				menuoption.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				WebElement scroll = driver.findElement(By.xpath(
						"/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div"));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
				String[] Bookingid_Time = scroll.getText().split(" ");
				String Bookingid = Bookingid_Time[0];
				Assert.assertEquals(Booking_ID, Bookingid);
				test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Hotel booking is not done successfully");
			}

			// Upload receipt
			// Navigating to My Booking
			try {
				((JavascriptExecutor) driver).executeScript("scroll(0,10)");
				Thread.sleep(3000);
				WebElement menu = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
				menu.click();
				WebElement menuoption = driver
						.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
				menuoption.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				WebElement scroll = driver.findElement(By.xpath("//*[text()='Upcoming bookings']"));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
				WebElement BKingId = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div"));
				String[] Bookingid_Time = BKingId.getText().split(" ");
				String Bookingid = Bookingid_Time[0];
				Assert.assertEquals(Booking_ID, Bookingid);
				test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Booking details not reflecting in My Booking page");
			}
			try {
				Thread.sleep(3000);
				/*
				 * WebElement scroll1
				 * =driver.findElement(By.xpath("//*[text()='Wire transfer']"));
				 * Thread.sleep(2000); ((JavascriptExecutor)
				 * driver).executeScript("arguments[0].scrollIntoView(true);",scroll1);
				 */

				// Removing hubspot
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
				Thread.sleep(2000);
				driver.findElement(By.xpath(
						"//button[@class='IconLauncher__BaseLauncher-sc-1puz2rt-0 IconLauncher__CircleLauncher-sc-1puz2rt-2 gUUGBf reagan--widget-loaded undefined']"))
						.click();
				Thread.sleep(2000);
				driver.findElement(By.xpath(
						"//button[@class='IconLauncher__BaseLauncher-sc-1puz2rt-0 IconLauncher__CircleLauncher-sc-1puz2rt-2 gUUGBf reagan--widget-loaded undefined']"))
						.click();
				multiScreens.multiScreenShot(driver);
				// driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
				Thread.sleep(2000);
				// driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
				driver.switchTo().parentFrame(); // Parent Frame

				driver.findElement(By.xpath("//*[text()='Upload Receipt']")).click();
				Thread.sleep(6000);

				WebElement WTType = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[1]/div/div[2]/select"));
				WTType.click();
				WTType.sendKeys("Bank");
				WTType.click();

				WebElement WTrcno = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[3]/div/div/div[2]/input"));
				WTrcno.sendKeys("123456");

				WebElement doc_upload = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[2]/div/label/span[2]"));
				doc_upload.click();
				Thread.sleep(3000);
				Robot rb = new Robot();
				// copying File path to Clipboard
				StringSelection str = new StringSelection("C:\\Automation\\Downloads\\NZOWO6X168X.pdf");
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
				// press Contol+V for pasting
				rb.keyPress(KeyEvent.VK_CONTROL);
				rb.keyPress(KeyEvent.VK_V);
				// release Control+V for pasting
				rb.keyRelease(KeyEvent.VK_CONTROL);
				rb.keyRelease(KeyEvent.VK_V);
				// for pressing and releasing Enter
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				multiScreens.multiScreenShot(driver);

				WebElement scroll_sub = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[4]/div[1]/div"));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll_sub);

				WebElement submit = driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[4]/div[1]/div"));
				multiScreens.multiScreenShot(driver);
				submit.click();

				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("scroll(0,10)");
				Thread.sleep(3000);
				;
				WebElement okay = driver
						.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/section/div/div"));
				multiScreens.multiScreenShot(driver);
				okay.click();

				Thread.sleep(3000);
				WebElement scroll_1 = driver.findElement(By.xpath("//*[text()='Please upload the receipt carefully']"));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll_1);
				driver.findElement(By.xpath(
						"/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div"));
				driver.findElement(By.xpath(
						"/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div/span/span"))
						.click();
				multiScreens.multiScreenShot(driver);
				test.log(LogStatus.PASS, "Receipt uploaded successfully");
			} catch (AssertionError e1) {
				test.log(LogStatus.FAIL, "Unable to upload receipt");
			}
		}
	

	

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}