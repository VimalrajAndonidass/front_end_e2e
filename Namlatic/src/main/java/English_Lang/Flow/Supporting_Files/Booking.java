package English_Lang.Flow.Supporting_Files;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Booking {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot("", "");
	public Fillo fillo = new Fillo();
	public String Trans_time_date;
	public String Hotel_Name_voucher;
	public String Check_in;
	public String Check_out;
	public String Guest_count;
	public String Room_count;
	public String Guest_Name;
	public String Room_Type;
	public String Booking_amt;
	public String Booking_ID;
	public String exp_hot_name;
	public String act_hot_name;
	public String Bookingid;

	public void URL_Launch() throws IOException, FilloException, InterruptedException {
		// Connection open for URL launch
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from URL";
		Recordset rs = con.executeQuery(strquery);
		driver.get("chrome://settings/clearBrowserData");
		rs.moveFirst();
		String link = rs.getField("link");
		Thread.sleep(3000);
		driver.navigate().to(link);
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);
		rs.close();
		con.close();

		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/button/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div[2]/div/div[2]/button[1]/div/div"));
		chglang.click();
	}

	public void booking() throws IOException, InterruptedException, AWTException {
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		WebElement Hotel_sel = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div[1]/div/div/div[2]/div[2]/div[1]/div[1]/div[1]"));
		@SuppressWarnings("unused")
		String exp_hot_name = Hotel_sel.getText();
		Hotel_sel.click();
		multiScreens.multiScreenShot(driver);
	
	/*	// Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}*/
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
		act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/h1/h1"))
				.getText();

	}
	
	public void currency_sel() throws InterruptedException {
		// Currency selection
				WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/button/div"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
				Thread.sleep(500);
				cur.click();
				WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='$ USD')]"));
				cur_sel.click();				
	}

	public void date_selection() throws InterruptedException {
		
		// Date validation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,501)");

		// Date validation
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, 4);
		Date futureDateTime = calendar.getTime();
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div/div[2]/div[1]/div/div/div"))
				.click();
		Thread.sleep(1000);
				driver.findElement(
						By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')]"));
				driver.findElement(
						By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')][2]"));

				Thread.sleep(500);
				WebElement start_date = driver.findElement(By.xpath(
						"/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[4]/td[2]/span"));
				start_date.click();

				Thread.sleep(500);
				WebElement end_date = driver.findElement(By.xpath(
						"/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[4]/td[6]/span"));
				end_date.click();
		}
	
	public void booking_confirmation() throws InterruptedException, IOException {


		/*// Removing hubspot
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		System.out.println("Within iFrame");
		driver.switchTo().parentFrame(); // Parent Frame
		*/
		
		// Room selection
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,501)");
		WebElement element = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[2]/div/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[2]/div[2]/select"));
		Select room_sel = new Select(element);
		room_sel.selectByVisibleText("1");
		room_sel.getFirstSelectedOption();
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		Thread.sleep(2000);
		WebElement book_now = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div/div/div/div[4]/div"));
		Thread.sleep(2000);
		book_now.click();
		multiScreens.multiScreenShot(driver);

	}
	
	public void visa_booking() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='visa']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Confirm']")).click();
		
	}
	
	public void visa_guest() throws IOException, InterruptedException {
		// Booking with main guest
				Thread.sleep(3500);
				WebElement Title = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[1]/div[1]/div[2]/select"));
				String GTitle=Title.getText();
				
				WebElement Fname = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[1]/div[2]/div[2]/input"));
				String Fstname = Fname.getText();

				WebElement Lname = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[1]/div[3]/div[2]/input"));
				String Lstname = Lname.getText();

				WebElement DOB = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div/input"));
				DOB.sendKeys("07/11/1985");
				
				WebElement Passport = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[3]/div[2]/div[2]/input"));
				Passport.sendKeys("123");
				
				WebElement mobileno = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[4]/div[2]/div[2]/input"));
				String Mobno = mobileno.getText();

				WebElement email = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[4]/div[1]/div[2]/input"));
				String mail = email.getText();
				
				multiScreens.multiScreenShot(driver);
				try {
					Assert.assertNotNull(Fstname);

					Assert.assertNotNull(Lstname);

					Assert.assertNotNull(Mobno);

					Assert.assertNotNull(mail);
					
					Assert.assertNotNull(DOB);
					
					Assert.assertNotNull(Passport);

				} catch (Exception e) {
				}
				// Adding guest
				((JavascriptExecutor) driver).executeScript("scroll(0,800)");
				driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/button")).click();
				
				WebElement GFname = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[3]/div[1]/div[2]/div[2]/input"));
				GFname.sendKeys("Himaja");
				
				WebElement GLname = driver
						.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[3]/div[1]/div[3]/div[2]/input"));
				GLname.sendKeys("Vimalraj");
				
				WebElement GDOB = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[3]/div[2]/div[1]/div[2]/div[1]/div/input"));
				GDOB.sendKeys("14/11/1992");
				
				WebElement GPassport = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[3]/div[2]/div[2]/div[2]/input"));
				GPassport.sendKeys("1992");
				multiScreens.multiScreenShot(driver);
				
				//Terms and conditions
				WebElement TC = driver.findElement(By.xpath("//*[@id=\"terms\"]"));
				TC.click();

				// Proceeding to Payment method selection page
				Thread.sleep(3000);
				((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
				WebElement Guest = driver.findElement(By.xpath("//*[text()='Proceed to select the mode of payment']"));
				Guest.click();
				multiScreens.multiScreenShot(driver);
				
	}

	public void Guest_details() throws IOException, InterruptedException {

		// Booking with main guest
		Thread.sleep(3500);
		WebElement Title = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[2]/div[1]/div[1]/div[2]/select"));
		String GTitle=Title.getText();
		
		WebElement Fname = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[2]/div[1]/div[2]/div[2]/input"));
		String Fstname = Fname.getText();

		WebElement Lname = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[2]/div[1]/div[3]/div[2]/input"));
		String Lstname = Lname.getText();

		WebElement mobileno = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/input"));
		String Mobno = mobileno.getText();

		WebElement email = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[1]/div[2]/div[2]/div[1]/div[2]/input"));
		String mail = email.getText();
		multiScreens.multiScreenShot(driver);
		try {
			Assert.assertNotNull(Fstname);

			Assert.assertNotNull(Lstname);

			Assert.assertNotNull(Mobno);

			Assert.assertNotNull(mail);

		} catch (Exception e) {
		}

		// Adding guest
		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/button")).click();
		WebElement GFname = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[5]/div/div[2]/div[2]/input"));
		GFname.sendKeys("Louie");
		WebElement GLname = driver
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[3]/div[3]/div[5]/div/div[3]/div[2]/input"));
		GLname.sendKeys("Madhav");
		multiScreens.multiScreenShot(driver);

		// Proceeding to Payment method selection page
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		WebElement Guest = driver.findElement(By.xpath("//*[text()='Proceed to select the mode of payment']"));
		Guest.click();
		multiScreens.multiScreenShot(driver);

	}

	public void payment() throws IOException, InterruptedException, AWTException {

		// Voucher details verification

		Thread.sleep(8000);
		WebElement Booking_ID_web = driver.findElement(By.xpath("//div[contains(@class,'sc-qOhCc bdSQfk')]"));
		String Booking_ID = Booking_ID_web.getText();

		multiScreens.multiScreenShot(driver);
		driver.findElement(By.xpath("//div[contains(text(),'CE')]")).getText();
		WebElement Hot_name_scroll = driver.findElement(By.xpath("//div[contains(@class,'sc-oVexc durqCp')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Hot_name_scroll);
		multiScreens.multiScreenShot(driver);
		Hot_name_scroll.getText();
		driver.findElement(By.xpath("//div[contains(@class,'sc-qWRsQ gCDRoY')]")).getText();
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[1]/div[2]/div[2]")).getText();
		driver.findElement(By.xpath("//div[contains(@class,'sc-oTejv fXnLnM')]")).getText();
		driver.findElement(By.xpath("//div[contains(@class,'sc-oTejv fXnLnM')]")).getText();
		driver.findElement(By.xpath("//div[contains(@class,'sc-qarTB fAGrdd')]")).getText();
		driver.findElement(By.xpath("//div[contains(@class,'sc-pkilo gnrtQK')]")).getText();
	/*	driver.findElement(By.xpath("//div[contains(@class,'sc-qYfxO jMKVEm')]")).getText();
		driver.findElement(By.xpath("//*[contains(@class,'sc-qYSBj hkhgWR')]")).getText(); 

		multiScreens.multiScreenShot(driver);

		// Download Voucher
		((JavascriptExecutor) driver).executeScript("scroll(0,2100)");
		WebElement download_Vou = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]/div"));
		download_Vou.click();
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,1500)");
		multiScreens.multiScreenShot(driver);

		// Email Voucher
		Thread.sleep(5000);
		WebElement send_email = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]/div"));
		send_email.click();
		multiScreens.multiScreenShot(driver);*/

		/*
		 * // Print Voucher try { WebElement print_Vou = driver .findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
		 * print_Vou.click(); Thread.sleep(5000); Robot print_window = new Robot();
		 * print_window.keyPress(KeyEvent.VK_ENTER);
		 * print_window.keyRelease(KeyEvent.VK_ENTER); test.log(LogStatus.PASS,
		 * "Print voucher working fine"); } catch (Exception e) {
		 * test.log(LogStatus.FAIL, "Unable to Print voucher"); }
		 */

		// My Booking verification
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[5]/div[1]/div"));
		menu.click();

		WebElement menuoption = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[5]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);

		Thread.sleep(3000);
		WebElement scroll = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div"));
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
		String[] Bookingid_Time = scroll.getText().split(" ");
		Bookingid = Bookingid_Time[0];
		Assert.assertEquals(Booking_ID, Bookingid);
	}
}

/*
 *
 * private void downloadPdf() { // TODO Auto-generated method stub
 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 * driver.manage().window().maximize();
 *
 * String URL = driver.getCurrentUrl(); System.out.print(URL);
 * //driver.get("https://www.learningcontainer.com/sample-pdf-files-for-testing"
 * );
 *
 * //locator to click the pdf download link // contextClick() method for right
 * click to an element after moving the //mouse to with the moveToElement()
 * Actions a = new Actions(driver);
 * a.contextClick(driver.findElement(By.xpath("*[@id=’plugin’])"))).build().
 * perform(); }
 */
