package English_Lang.Flow.Supporting_Files;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Declaration extends Booking{
	public static Fillo fillo = new Fillo();

	public static void chrome_declare() throws FilloException, InterruptedException, IOException
	{
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		//mention the below chrome option to solve timeout exception issue
		ChromeOptions options = new ChromeOptions();
		//options.setPageLoadStrategy(PageLoadStrategy.NONE);
		
		// Instantiate the chrome driver
		driver = new ChromeDriver(options);
		//driver = new ChromeDriver();
	}
	
	public static void firefox_declare() throws FilloException, InterruptedException, IOException
	{
		System.setProperty("webdriver.gecko.driver",
				System.getProperty("user.dir") + "/Drivers/FireFox/geckodriver.exe");
		driver = new FirefoxDriver();
	}

}
