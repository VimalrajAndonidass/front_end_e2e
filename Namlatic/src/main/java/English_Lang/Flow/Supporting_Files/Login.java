package English_Lang.Flow.Supporting_Files;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Login extends Booking{
	public static Fillo fillo = new Fillo();
	public static String username = null;

	public static void Enroll_corporate_Login() throws FilloException, InterruptedException, IOException
	{
		// Connection open for login
		Connection con = fillo.getConnection("C:\\Users\\Vimal\\eclipse-workspace\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='Enroll corporate'";
		Recordset rs = con.executeQuery(strquery);
		List<String> data = new ArrayList<>();
		new ArrayList<>();
		//List<String> data1 = new ArrayList<>();
		new ArrayList<>();

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			username = data.get(0);
					}
		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
		nam_Login.click();

		Thread.sleep(3000);
		WebElement uname = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter email id')]"));
		uname.sendKeys(username);
		
		WebElement nam_continue = driver.findElement(By.xpath("//*[(text()='Continue with email address')]"));
		nam_continue.click();
		
		Thread.sleep(3000);
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
		yopmail_OTP(username);
	}
	
	public static void Corporate_User_Login() throws FilloException, InterruptedException, IOException
	{
		// Connection open for login
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='Corporate User'";
		Recordset rs = con.executeQuery(strquery);
		List<String> data = new ArrayList<>();
		new ArrayList<>();
	//	List<String> data1 = new ArrayList<>();
		new ArrayList<>();

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			username = data.get(0);
					}

		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
		nam_Login.click();

		Thread.sleep(3000);
		WebElement uname = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter email id')]"));
		uname.sendKeys(username);
	
		WebElement nam_continue = driver.findElement(By.xpath("//*[(text()='Continue with email address')]"));
		nam_continue.click();
		
		multiScreens.multiScreenShot(driver);
		yopmail_OTP(username);
	}
	
	public static void Hotel_Owner_Login() throws FilloException, InterruptedException, IOException
	{
		// Connection open for login
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='Hotel Owner'";
		Recordset rs = con.executeQuery(strquery);
		String username = null;
		List<String> data = new ArrayList<>();
		new ArrayList<>();
		//List<String> data1 = new ArrayList<>();
		new ArrayList<>();

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			username = data.get(0);
					}

		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
		nam_Login.click();

		Thread.sleep(3000);
		WebElement uname = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter email id')]"));
		uname.sendKeys(username);

		WebElement nam_continue = driver.findElement(By.xpath("//*[(text()='Continue with email address')]"));
		nam_continue.click();
		
		yopmail_OTP(username);
	}
	
	public static void Enroll_Hotel_Login() throws FilloException, InterruptedException, IOException
	{
		// Connection open for login
		Connection con = fillo.getConnection("D:\\Users\\Vimal\\eclipse-workspace\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='Enroll Hotel'";
		Recordset rs = con.executeQuery(strquery);
		String username = null;
		List<String> data = new ArrayList<>();
		new ArrayList<>();
		List<String> data1 = new ArrayList<>();
		new ArrayList<>();

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			data1.add(rs.getField("Pwd"));
			username = data.get(0);
					}

		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
		nam_Login.click();

		Thread.sleep(3000);
		WebElement uname = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter email id')]"));
		uname.sendKeys(username);
		
		WebElement nam_continue = driver.findElement(By.xpath("//*[(text()='Continue with email address')]"));
		nam_continue.click();

		multiScreens.multiScreenShot(driver);
	yopmail_OTP(username);
	}
	
	public static void End_User_Login() throws FilloException, InterruptedException, IOException
	{
		// Connection open for login
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='End User'";
		Recordset rs = con.executeQuery(strquery);
		String username = null;
		List<String> data = new ArrayList<>();
		new ArrayList<>();
	

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			username = data.get(0);
					}

		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
		nam_Login.click();

		Thread.sleep(3000);
		WebElement uname = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter email id')]"));
		uname.sendKeys(username);

		WebElement nam_continue = driver.findElement(By.xpath("//*[(text()='Continue with email address')]"));
		nam_continue.click();
		yopmail_OTP(username);
		
	}
	

	public static void yopmail_OTP (String emailID) throws InterruptedException
	{
		//Yopmail/ Gmail
		String baseURL = driver.getCurrentUrl();
	   
	    ((JavascriptExecutor)driver).executeScript("window.open()");

	    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());

	    driver.switchTo().window(tabs.get(1)); //switches to new tab
	    driver.get("https://yopmail.com");
	    
	    driver.switchTo().window(tabs.get(0)); // switch back to main screen        


		// Get handles of the windows
				String mainWindowHandle = driver.getWindowHandle();
				Set<String> allWindowHandles = driver.getWindowHandles();
				Iterator<String> iterator = allWindowHandles.iterator();

				// Here we will check if child window has other child windows and will fetch the
				// heading of the child window
				while (iterator.hasNext()) {
					String ChildWindow = iterator.next();
					if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
						driver.switchTo().window(ChildWindow);
					}
				}
		
		WebElement userid = driver.findElement(By.xpath("//*[@id=\"login\"]"));
		userid.sendKeys(emailID);
		
		WebElement proceed = driver.findElement(By.xpath("/html/body/div[1]/div[2]/main/div[3]/div/div[1]/div[2]/div/div/form/div/div[1]/div[4]/button/i"));
		proceed.click();
		
		Thread.sleep(15000);
		WebElement refresh = driver.findElement(By.xpath("//*[@id=\"refresh\"]"));
		refresh.click();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"ifmail\"][1]")));
	//	System.out.println("Within frame");
		Thread.sleep(3000);
		
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		Thread.sleep(3000);
		WebElement OTP_Text = driver.findElement (By.xpath("/html/body/main/div/div/div/div/div/div[5]"));
		String OTP =OTP_Text.getText();
		System.out.println(OTP);
		driver.close();

	//Back to main screen
		 driver.switchTo().window(tabs.get(0)); // switch back to main screen  
		 int OTP_i=Integer.parseInt(OTP);
		 int mod1 = OTP_i % 1000;
		 int mod2 = OTP_i % 100;
		 int mod3 = OTP_i % 10;
		 
		 int S1 = OTP_i / 1000;
		 String s1=Integer.toString(S1);
		// System.out.println("S1:" + S1);
		 int S2 = mod1 / 100;
		 String s2=Integer.toString(S2);
		// System.out.println("mod1:" + mod1);
		// System.out.println("S2:" + S2);
		 int S3 = mod2/10;
		 String s3=Integer.toString(S3);
		 // System.out.println("mod2:" + mod2);
		 // System.out.println("S3:" + S3);
		 // System.out.println("mod3:" + mod3);
		 String s4=Integer.toString(mod3);
		 WebElement OTP_1 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/input[1]"));
		 OTP_1.sendKeys(s1);
		 
		 WebElement OTP_2 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/input[2]"));
		 OTP_2.sendKeys(s2);

		 WebElement OTP_3 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/input[3]"));
		 OTP_3.sendKeys(s3);
		 
		 WebElement OTP_4 = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/input[4]"));
		 OTP_4.sendKeys(s4);
		 
		 WebElement submit = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/button"));
		 submit.click();	
		 //Thread.sleep(3000);
		 
	/*	 driver.switchTo().window(tabs.get(1)); //switches to new tab
		    driver.get("https://test.namlatic.com");
	System.out.println("New tab");
		 // Get handles of the windows

			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
				if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
					driver.switchTo().window(ChildWindow);
				}
			}
			driver.close();*/
	}
}
