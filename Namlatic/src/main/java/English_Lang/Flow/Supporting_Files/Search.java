package English_Lang.Flow.Supporting_Files;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Search extends Booking{
	public static Fillo fillo = new Fillo();

	public static void hotel_search01() throws FilloException, InterruptedException, IOException
	{
		// Connection open for Search
		Thread.sleep(3000);
		driver.get("https://test.namlatic.com");
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Hotel where Type='Hotel1'";	
		Recordset rs = con.executeQuery(strquery);
		System.out.println(strquery);
		String sr_key =null;
		rs.next();
		System.out.println("search within");
		sr_key = rs.getField("Hotel_name_City_name");
		System.out.println(sr_key);	
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement Hotel = driver
				.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));		
		Hotel.sendKeys(sr_key);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
		Hotel.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/div/div[4]/div"))
				.click();
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
	}
	
	
	
	public static void Free_cancel_48() throws FilloException, InterruptedException, IOException
	{
		// Connection open for Search
		Thread.sleep(4000);
		driver.get("https://test.namlatic.com");
		Thread.sleep(3000);
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Hotel where Type='FC_48hrs'";	
		Recordset rs = con.executeQuery(strquery);
		System.out.println(strquery);
		String sr_key =null;
		rs.next();
		System.out.println("search within");
		sr_key = rs.getField("Hotel_name_City_name");
		System.out.println(sr_key);	
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement Hotel = driver
				.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));		
		Hotel.sendKeys(sr_key);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
		Hotel.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/div/div[4]/div"))
				.click();
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
	}
	
	public static void city_search() throws FilloException, InterruptedException, IOException
	{
		// Connection open for Search
		Thread.sleep(4000);
		Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
		String strquery = "Select * from Hotel where Type='City'";	
		Recordset rs = con.executeQuery(strquery);
		String sr_key =null;
		rs.next();
		sr_key = rs.getField("Hotel_name_City_name");	
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement Hotel = driver
				.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));		
		Hotel.sendKeys(sr_key);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
		Hotel.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/div/div[4]/div"))
				.click();
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
	}

	public static void Free_cancel_24() throws FilloException, IOException, InterruptedException 
		{
			// Connection open for Search
			Thread.sleep(4000);
			
			Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
			String strquery = "Select * from Hotel where Type='FC_24hrs'";	
			Recordset rs = con.executeQuery(strquery);
			System.out.println(strquery);
			String sr_key =null;
			rs.next();
			System.out.println("search within");
			sr_key = rs.getField("Hotel_name_City_name");
			System.out.println(sr_key);	
			Thread.sleep(3000);
			System.out.println("Out of DB");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));		
			Hotel.sendKeys(sr_key);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
		}

	public static void Free_cancel_72() throws FilloException, IOException, InterruptedException 
		{
			// Connection open for Search
			Thread.sleep(4000);
			driver.get("https://test.namlatic.com");
			Thread.sleep(3000);
			Connection con = fillo.getConnection("D:\\02_Automation\\Source_Files\\Read_Excel\\Input.xls");
			String strquery = "Select * from Hotel where Type='FC_72hrs'";	
			Recordset rs = con.executeQuery(strquery);
			System.out.println(strquery);
			String sr_key =null;
			rs.next();
			System.out.println("search within");
			sr_key = rs.getField("Hotel_name_City_name");
			System.out.println(sr_key);	
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));		
			Hotel.sendKeys(sr_key);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
		}
}
