package English_Lang.Java_Files;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import English_Lang.E2E_Flow.Landing_Page;
import junit.framework.Assert;

public class Hotel_Booking extends Landing_Page {
	
	void Hotel_Book() throws InterruptedException, IOException
	{
		booking();
		image();
		description();
		roomtype();
		cancellation_policy();
		amenities_ver();
		policy_ver();
		book_now();
		various_section_Validation();
		
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		System.out.println("Hotel-Bookinng: PASS");
		multiScreens.multiScreenShot(driver);
	}
	
	@Test(priority = 16, invocationCount = 1)
	void booking() throws IOException
	{
		/*driver.navigate().to("https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver); */
		
			WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div"));	
			Hotel_sel.click(); 
			String exp_hot_name = Hotel_sel.getText();
			multiScreens.multiScreenShot(driver);
		
		//Get handles of the windows
			String MainWindow = driver.getWindowHandle();
			String mainWindowHandle = driver.getWindowHandle();
			Set<String> allWindowHandles = driver.getWindowHandles();
			Iterator<String> iterator = allWindowHandles.iterator();
		
		// Here we will check if child window has other child windows and will fetch the heading of the child window
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
	            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	            		driver.switchTo().window(ChildWindow);
	        }}
			multiScreens.multiScreenShot(driver);
		//Scroll bar navigation
		//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
		
		// Hotel name verification
			Hotel_Filter hot = new Hotel_Filter();
			String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
			
			try
			{
				Assert.assertEquals(exp_hot_name, act_hot_name);
				System.out.println("Hotel name matching:" + exp_hot_name);
			} catch (AssertionError e)
			{
				System.out.println("Hotel name mismatching");
				driver.close();
			}
	}

	@Test(priority = 17, invocationCount = 1)

	public void image()
	{
		//Image verification
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
	}

	@Test(priority = 18, invocationCount = 1)

	void description()
	{
		// Description verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]")).isDisplayed();
				System.out.println("Hotel description is available");
			} catch (Exception e)
			{
				System.out.println("Description missing");
				
			}
	}

	@Test(priority = 19, invocationCount = 1)

	void roomtype()
  {
			
		//Type of rooms section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")).isDisplayed();
				System.out.println("Type of rooms section is available");
			} catch (Exception e)
			{
				System.out.println("Types of rooms section missing");
			}
			
		  //Room type images verification	
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img")).isDisplayed();
				System.out.println("Room type images are available");
			} catch (Exception e)
			{
				System.out.println("Images are missing");
			}
			
		 //Room type sharing details verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div")).isDisplayed();
				System.out.println("Sharing details available");
			} catch (Exception e)
			{
				System.out.println("Sharing details are not available");
			}	
			
		// Amenities section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]")).isDisplayed();
				System.out.println("Amenities section available");
			} catch (Exception e)
			{
				System.out.println("Amenities section not available");
			}
			
		// Rate section verification
			try
			{
				String rate = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).getText();
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).isDisplayed();
				//driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
				System.out.println("Selected currency" + rate);
				System.out.println("Rate section available");
			} catch (Exception e)
			{
				System.out.println("Rate section not available");
			}
		
		// Add Room section verification
			try
			{
				driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
				System.out.println("Add room section available");
			} catch (Exception e)
			{
				System.out.println("Add room section not available");
			}	
			
			((JavascriptExecutor)driver).executeScript("scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
	}		
		// 	Verification of Cancellation policies

	@Test(priority = 20, invocationCount = 1)

	public void cancellation_policy()
	{

	String cancel_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div")).getText();
	System.out.println(cancel_type);
	if (cancel_type.equals("Free Cancellation")) {
		// Card Payment verification
			String exp_title1 ="Cancellation Policy";
			String exp_title2 ="Card payment";
			String exp_text1 = "Free cancellation before 06:00:00 PM ( a day prior), service charges may apply.\n\n" 

			+ "If Cancelled after 06:00:00 PM (a day prior), the first night stay amount will be chargeable.\n\n" 

			+ "No amount will be refunded if cancelled after check in time.\n\n"

			+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n"

			+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.";

			String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
			String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
			String act_text1 = driver.findElement(By.xpath("//div[contains(text(),'Free cancellation')]")).getText();
			try
			{
				Assert.assertEquals(exp_title1, act_title1);
				Assert.assertEquals(exp_title2, act_title2);
				Assert.assertEquals(exp_text1, act_text1);
				System.out.println("Cancellation policy for Card payment content and title is matching");
			} catch (Exception e)
			{
				System.out.println("Cancellation policy for Card payment content is not matching");
			}


				// Wire Transfer verification
					String exp_text11 = "Wire Transfer";
					String exp_text12 = "Free cancellation before 06:00:00 PM ( a day prior), service charges may apply\n\n" 
			
					+ "If Cancelled after 06:00:00 PM (a day prior), the first night stay amount will be chargeable.\n\n" 

					+ "No amount will be refunded if cancelled after check in time.\n\n"

					+ "if the guest does not show up on their confirmed booking we treat it as cancellation and no amount will be refunded.\n\n"

					+ "Late check-in hours will not be subjected to Re-fund\n\n"
					
					+ "Reserved booking will be auto cancelled incase of a failed transaction.\n\n"
					
					+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n"
					
					+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.\n\n"

					+ "Refunds are applicable only when a cancellation request has been formally submitted through Namlatic platform.\n\n"
					
					+ "Applicable Refunds will take 5 to 7 working days to process.";
					
					String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
					String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
					try
					{
						Assert.assertEquals(exp_text11, act_text11);
						Assert.assertEquals(exp_text12, act_text12);
						System.out.println("Cancellation policy for Wire Transfer content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Wire Transfer content is not matching");
					}

				// Pay at hotel
					((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
					String exp_text21 = "Pay at hotel";
					String exp_text22 = "When a booking made with \"Pay at hotel\" option, the concerned hotel will collect the entire payment against the booking at the time of check-in.\n\n" 
			
					+ "Reservation should be confirmed 48 hours before checkin time of the journey date by email or a call with Namlatic.Reservation shall be considered as confirmed only on personal verfication, else will be subjected to cancellation under Cancellation Policy Clause.\n\n" 

					+ "Following are the contact details:\n"
					
					+ "Email: confirmation@namlatic.com\n"
					
					+ "Namlatic DZ Hotline: + 213 982 414 415\n\n"
					
					+ "For international tourists, the payment will be charged in local currency or in any other currency, as decided by the hotel.\n\n"
					
					+ "For security purposes, the User must provide the 4 digit pin received with their Booking Confirmation E-mail.Namlatic or the Hotel may cancel the booking Incase If the pin found is incorrect.\n\n"

					+ "Namlatic allows Partial cancellation and Full cancellation of Booked rooms before check-in.\n\n"
					
					+ "Any booking cancelled after/during check-in time will be considered as cancellation with \"No refund\".\n\n"
					
					+ "Late Check-in hours will not subject to any partial refund.\n\n"
					
					+ "You can cancel a booking any time from Namlatic Online Booking website.\n\n"

					+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n"
					
					+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.";
					
					String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
					String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
					try
					{
						Assert.assertEquals(exp_text21, act_text21);
						Assert.assertEquals(exp_text22, act_text22);
						System.out.println("Cancellation policy for Pay at hotel content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Pay at hotel content is not matching");
					}
			
				// CIB
					((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
					String exp_text31 = "CIB";
					String exp_text32 = "You can cancel a booking any time from Namlatic Online Booking website.\n\n" 
			
					+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n" 

					+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.\n\n"

					+ "Refund Policy\n\n"
					
					+ "Any bookings cancelled before 6:00:00 PM through Namlatic platform will get full refund of their amount paid through CIB card (Cancellation fee may apply).\n\n"
					
					+ "Cancellation fee & service charges applied may vary according to the booking and property choosen.\n\n"
					
					+ "Applicable refund process might take 3-5 working days.\n\n"
					
					+ "No-Show\n\n"
					
					+ "No refund will be provided if a person didn't show up to the respective hotel on their confirmed booking.\n\n"
					
					+ "Namlatic does not accept any liability or responsibility for the consequences of your delayed arrival or any cancellation or charged no-show fee by the Property Provider.\n\n"
					
					+ "Late Check-in hours will not subject to any refund.";

					String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
					String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
					try
					{
						Assert.assertEquals(exp_text31, act_text31);
						Assert.assertEquals(exp_text32, act_text32);
						System.out.println("Cancellation policy for CIB payment method content is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for CIB payment method content is not matching");
					}
	}
	else if (cancel_type.equals("Non-refundable"))
	{
		// Card Payment verification
					String exp_title1 ="Cancellation Policy";
					String exp_title2 ="Card payment";
					String exp_text1 = "No amount will be refunded during any time of Cancellation.\n\n" 
			
					+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n" 

					+ "In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.";

					String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
					String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
					String act_text1 = driver.findElement(By.xpath("//div[contains(text(),'No amount will be refunded during any time of Cancellation')]")).getText();
					try
					{
						Assert.assertEquals(exp_title1, act_title1);
						Assert.assertEquals(exp_title2, act_title2);
						Assert.assertEquals(exp_text1, act_text1);
						System.out.println("Cancellation policy for Card payment content and title is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Card payment content is not matching");
					}


						// Wire Transfer verification
							String exp_text11 = "Wire Transfer";
							String exp_text12 = "No refund will be provided at any time of Cancellation.\n\n" 
					
							+ "Namlatic does not allow modification/updation of booking dates through its platform.\n\n" 

							+ "Reserved booking will be auto cancelled incase of a failed transaction.";

							String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
							String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
							try
							{
								Assert.assertEquals(exp_text11, act_text11);
								Assert.assertEquals(exp_text12, act_text12);
								System.out.println("Cancellation policy for Wire Transfer content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for Wire Transfer content is not matching");
							}

						// Pay at hotel
							((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
							String exp_text21 = "Pay at hotel";
							String exp_text22 = "When a booking made with \"Pay at hotel\" option, the concerned hotel will collect the entire payment against the booking at the time of check-in.\n\n" 
					
							+ "Reservation should be confirmed 48 hours before checkin time of the journey date by email or a call with Namlatic.Reservation shall be considered as confirmed only on personal verfication, else will be subjected to cancellation under Cancellation Policy Clause.\n\n" 

							+ "Following are the contact details:\n"
							
							+ "Email: confirmation@namlatic.com\n"
							
							+ "Namlatic DZ Hotline: + 213 982 414 415\n\n"
							
							+ "For international tourists, the payment will be charged in local currency or in any other currency, as decided by the hotel.\n\n"
							
							+ "For security purposes, the User must provide the 4 digit pin received with their Booking Confirmation E-mail.Namlatic or the Hotel may cancel the booking Incase If the pin found is incorrect.\n\n"

							+ "Namlatic allows Partial cancellation and Full cancellation of Booked rooms before check-in.\n\n"
							
							+ "Any booking cancelled after/during check-in time will be considered as cancellation with \"No refund\".\n\n"
							
							+ "Late Check-in hours will not subject to any partial refund.\n\n"
							
							+ "You can cancel a booking any time from Namlatic Online Booking website.\n\n"

							+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n"
							
							+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.";
							
							String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
							String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
							try
							{
								Assert.assertEquals(exp_text21, act_text21);
								Assert.assertEquals(exp_text22, act_text22);
								System.out.println("Cancellation policy for Pay at hotel content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for Pay at hotel content is not matching");
							}
					
						// CIB
							((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
							String exp_text31 = "CIB";
							String exp_text32 = "You can cancel a booking any time from Namlatic Online Booking website.\n\n" 
					
							+ "Once the booking is cancelled you cannot re-initiate the same booking or cannot directly check-in under the same booking ID.\n\n" 

							+ "Incase of any hostile/malicious by the tourist within the property premises with any person found or reported will be subjected to cancellation on the booking spot with no refund of money.\n\n"

							+ "Refund Policy\n\n"
							
							+ "Reservations made with the \"non-refundable\" hotel conditions are not subjected to refund on a cancellation or no show.\n\n"
							
							+ "Any booking cancelled after/during check-in time will be considered as Non-refundable cancellation with \"No refund\".";
							
							String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
							String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
							try
							{
								Assert.assertEquals(exp_text31, act_text31);
								Assert.assertEquals(exp_text32, act_text32);
								System.out.println("Cancellation policy for CIB payment method content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for CIB payment method content is not matching");
							}

	}
	}

	@Test(priority = 21, invocationCount = 1)

	public void amenities_ver()
	{
				// Amenities section2 verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div")).isDisplayed();
						System.out.println("Amenities section available");
					} catch (Exception e)
					{
						System.out.println("Amenities section not available");
					}
	}

	@Test(priority = 22, invocationCount = 1)

	public void policy_ver()
	{
				// Hotel Policy verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div")).isDisplayed();
						System.out.println("Hotel Policy section available");
					} catch (Exception e)
					{
						System.out.println("Hotel Policy section not available");
					}
	}

	@Test(priority = 23, invocationCount = 1)

	public void book_now() throws InterruptedException, IOException
	{				
		Date_room_selection();
	//only one option should be active in script.
		CIB();
		//Card_Payment();
		//Wire_Transfer();
	}

	@Test(priority = 24, invocationCount = 1)
	public void Date_room_selection() throws InterruptedException, IOException
	{
		//From and To Date Selection
		WebElement date=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
		Thread.sleep(500);

		//WebElement navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", navigate);
		
		//	driver.findElement(By.xpath("//*[text()='29']")).click();
			//driver.findElement(By.xpath("//*[text()='30']")).click();

			
		//Room selection
			Thread.sleep(2000); 
			((JavascriptExecutor)driver).executeScript("scroll(0,700)");
			WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
			Select room_sel=new Select(element);
			room_sel.selectByVisibleText("1");
			WebElement firstEle=room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);

			//Book now button verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Book Now button available in booking section");
			} catch (Exception e)
			{
				System.out.println("Book Now button not available in Booking section");
			}
			
		//Click on book now button
			WebElement book_now = driver.findElement (By.xpath("//*[(text()='Book Now')]"));
					book_now.click();
					multiScreens.multiScreenShot(driver);

	}					
		//Select payment methods ' CIB '  using radio button.
	@Test(priority = 25, invocationCount = 1)
	public void CIB() throws IOException, InterruptedException
					{
						if (driver.findElement(By.id("cib")).isEnabled())
						{
							WebElement radio_cib=driver.findElement(By.id("cib"));
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

				//Select Terms and Conditions - check box
					WebElement checkbox_terms=driver.findElement(By.id("terms"));
					checkbox_terms.click();
					multiScreens.multiScreenShot(driver);
					System.out.println("CIB card selected");
						} else
						{
						//Currency selection
							WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
							Thread.sleep(500);
							cur.click();
							WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
							cur_sel.click();
						
						//CIB selection and proceed
							Date_room_selection();									
							WebElement radio_cib=driver.findElement(By.id("cib"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
							Thread.sleep(500);
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

						//Select Terms and Conditions - check box
							WebElement checkbox_terms=driver.findElement(By.id("terms"));
							checkbox_terms.click();
							multiScreens.multiScreenShot(driver);
							System.out.println("CIB card selected");
						}
						}
						
					
			public void Card_Payment() throws IOException, InterruptedException
			{
				WebElement radio_card=driver.findElement(By.id("card"));
				radio_card.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Card Payment selected");
			}
			
			public void Wire_Transfer() throws IOException, InterruptedException
			{
				WebElement radio_wt=driver.findElement(By.id("wt"));
				radio_wt.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Wire Transfer selected");
			}

	@Test(priority = 26, invocationCount = 1)
	public void various_section_Validation() throws InterruptedException
	{
		// Booking section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div")).isDisplayed();
				System.out.println("Booking section available");
			} catch (Exception e)
			{
				System.out.println("Booking section not available");
			}
			
			//Pricing
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]")).isDisplayed();
				System.out.println("Pricing details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Pricing details not available in Booking section");
			}
			
			// Calender
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).isDisplayed();
				System.out.println("Calender details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Calender details not available in Booking section");
			}
			
			//Guest
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Guest details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Guest details not available in Booking section");
			}
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,2000)");

		//Navigating to various section
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
			Thread.sleep(1000);
			//((JavascriptExecutor)driver).executeScript("scroll(0,400)");

			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
			Thread.sleep(2000);
			
		/* //Select reCAPTCHA Check box
			driver.switchTo().defaultContent().findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"));
			System.out.println("test");
			WebElement checkbox_recaptcha=driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
			checkbox_recaptcha.click();
			System.out.println("reCAPTCHA check box is selected"); 
		

		//Click on Proceed To Pay button
			WebElement Proceed_element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"));
			Proceed_element.click(); */
	}			
}