package English_Lang.Java_Files;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class test {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Users\\avimalraj\\eclipse-workspace\\Namlatic\\","English");
	static ExtentTest test;
	static ExtentReports report;

	
	@BeforeTest	
	public void Launch() throws Exception {
		
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
	driver = new ChromeDriver();
	report = new ExtentReports(System.getProperty("user.dir")+"_ExtentReportResults.html");
	
    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	
	@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}

@Test (priority = 2, invocationCount = 1)
	public void Title_Verification() throws IOException
	{
	test = report.startTest("Title Verification");
		//Title Verification
				String ExpectedTitle = "Namlatic";
				try {
						AssertJUnit.assertEquals(ExpectedTitle, driver.getTitle());
						test.log(LogStatus.PASS, "Title Matching - Pass");
				}
					catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Title mismatches with" +e +"Fail");
					}
		
		// Language change
				WebElement language = driver.findElement(By.xpath("//*[(text()='Fran�ais')]"));
				language.click();
				WebElement chglang = language.findElement(By.xpath("//*[(text()='English')]"));
				chglang.click();
	}
@Test (priority = 10, invocationCount = 1)
public void Login() throws IOException, InterruptedException
{
// Login 		
test = report.startTest("Login Functionality and Elastic search verification");
try
{
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
			pwd.sendKeys("Test@123");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
}catch (AssertionError e)
{
	test.log(LogStatus.FAIL, "Login Failed");
}
}
@Test (priority = 11, invocationCount = 1)
	public void book_now() throws InterruptedException, IOException
	{
	Thread.sleep(3000);	
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		test = report.startTest("My Booking Page - Booknow option verification");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();

		driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]")).click();
		try
		{
		//	WebElement Hotel_sel1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[2]/div/div[1]"));	
			//String exp_hot_name1 = Hotel_sel1.getText();
			//multiScreens.multiScreenShot(driver);
			
			WebElement Bk_now = driver.findElement(By.xpath("//div[contains(text(),'N7NH9JRUJ3O')]/following::div[text()='Book Now']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
		Thread.sleep(500);
		
		Bk_now.click();


			
			// Hotel name verification
				/*String act_hot_name1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
				
				try
				{
					Assert.assertEquals(exp_hot_name1, act_hot_name1);
					test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name1);
				
				} catch (AssertionError e)
				{
					
					test.log(LogStatus.FAIL, "Hotel name mismatching");				
				}
				report.endTest(test);	
	
*/
				try {
				//Room selection
				Thread.sleep(2000); 
				((JavascriptExecutor)driver).executeScript("scroll(0,700)");
				WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
				Select room_sel=new Select(element);
				room_sel.selectByVisibleText("1");
				WebElement firstEle=room_sel.getFirstSelectedOption();
				multiScreens.multiScreenShot(driver);

				//Book now button verification
				
				
					driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
					test.log(LogStatus.PASS,"Book Now button available in booking section");
				} catch (AssertionError e)
				{
					test.log(LogStatus.PASS,"Book Now button not available in booking section");
				}
				report.endTest(test);	
				
			//Click on book now button
				WebElement book_now = driver.findElement (By.xpath("//*[(text()='Book Now')]"));
						book_now.click();
						multiScreens.multiScreenShot(driver);

		test.log(LogStatus.PASS, "Able to proceed booking via 'Book now' from My Booking");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Facing error while proceeding booking via 'Book own' from My Booking");
		} 
	}
		
@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
//driver.quit();
}
}



