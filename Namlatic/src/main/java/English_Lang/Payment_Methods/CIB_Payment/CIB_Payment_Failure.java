package English_Lang.Payment_Methods.CIB_Payment;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class CIB_Payment_Failure {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\English\\Booking\\CIB\\", "CIB_Payment_Failure");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void Launch() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports(
				"C:\\Automation\\Reports\\Front_End\\English\\Booking\\CIB\\" + "CIB_Payment_Failure.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException {
		test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);

		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();
	}

	@Test(priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException, AWTException {
		// Login
		test = report.startTest("Login Functionality and Elastic search verification");
		try {
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123456");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
		/*
		 * Robot robot = new Robot(); for (int i = 0; i < 1; i++) {
		 * robot.keyPress(KeyEvent.VK_CONTROL); robot.keyPress(KeyEvent.VK_SUBTRACT);
		 * robot.keyRelease(KeyEvent.VK_SUBTRACT);
		 * robot.keyRelease(KeyEvent.VK_CONTROL); }
		 */
		// Hotel search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));
			Hotel.sendKeys("Annaba");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Search working fine");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

	@Test(priority = 3, invocationCount = 1)
	void booking() throws IOException, InterruptedException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);

		WebElement Hotel_sel = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/wrapper[1]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]"));
		String exp_hot_name = Hotel_sel.getText();
		Hotel_sel.click();
		multiScreens.multiScreenShot(driver);
		// Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
		String act_hot_name = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]"))
				.getText();
		System.out.println(act_hot_name);
		try {
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}

	}

	// @Test(priority = 4, invocationCount = 1)

	/*
	 * public void booking_confirmation() throws InterruptedException, IOException {
	 * Date_room_selection(); CIB(); }
	 */

	@Test(priority = 4, invocationCount = 1)
	public void Date_room_selection() throws InterruptedException, IOException {
		test = report.startTest("Date and Room selection section verification");

		// Removing hubspot

		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		Thread.sleep(2000);

		driver.findElement(By.xpath(
				"//button[@class='IconLauncher__BaseLauncher-yej3u-0 IconLauncher__CircleLauncher-yej3u-2 cNPVGM reagan--widget-loaded undefined']"))
				.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"//button[@class='IconLauncher__BaseLauncher-yej3u-0 IconLauncher__CircleLauncher-yej3u-2 cNPVGM reagan--widget-loaded undefined']"))
				.click();

		multiScreens.multiScreenShot(driver);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		System.out.println("Within iFrame");
		driver.switchTo().parentFrame(); // Parent Frame
		try {

			// From and To Date Selection
			/*
			 * WebElement date = driver.findElement(By.xpath("//*[text()='Description"));
			 * ((JavascriptExecutor)
			 * driver).executeScript("arguments[0].scrollIntoView(true);", date);
			 * Thread.sleep(3000);
			 *
			 * driver.findElement(By.xpath("//*[contains(@class,'sc-pReKu eehIqb')]")).click
			 * (); System.out.println("Date"); Thread.sleep(500);
			 *
			 * WebElement month = driver .findElement(By.xpath(
			 * "//select[contains(@class,'DateRangePicker__MonthHeaderSelect')]")); Select
			 * month_pick = new Select(month); month_pick.selectByVisibleText("May");
			 * month_pick.getFirstSelectedOption();
			 *
			 * Thread.sleep(500); WebElement start_date =
			 * driver.findElement(By.xpath("//*[@text()='1']")); start_date.click();
			 *
			 * Thread.sleep(500); WebElement end_date =
			 * driver.findElement(By.xpath("//*[@text()='10']")); end_date.click();
			 */

			// Currency selection
			WebElement cur = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
			Thread.sleep(500);
			cur.click();
			WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
			cur_sel.click();

			// Room selection
			Thread.sleep(3000);
			WebElement scroll = driver.findElement(By.xpath("//*[text()='+ 9% taxes and charges']"));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
			WebElement element = driver.findElement(By.xpath(
					"//*[contains(@class,'sc-oTpcS hKhvoq') or contains(@class,'sc-pJjas fvjHub') or contains(@class,'sc-qWeMO cNXhsk')]"));
			Select room_sel = new Select(element);
			room_sel.selectByVisibleText("1");
			room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Rooms selected successfully");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select Rooms");
		}

		try {
			// Book now button verification

			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(2000);
			WebElement book_now = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
			Thread.sleep(2000);
			book_now.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");
		}

	}

	@Test(priority = 5, invocationCount = 1)

	public void Guest_details() throws IOException, InterruptedException {
		test = report.startTest("Guest details updation");

		// Booking with main guest
		Thread.sleep(3000);
		WebElement Fname = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[2]/div[2]/div[2]/input"));
		String Fstname = Fname.getText();

		WebElement Lname = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[2]/div[3]/div[2]/input"));
		String Lstname = Lname.getText();

		WebElement mobileno = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[3]/div[1]/div[2]/input"));
		String Mobno = mobileno.getText();

		WebElement email = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[3]/div[2]/div[2]/input"));
		String mail = email.getText();
		try {
			Assert.assertNotNull(Fstname);

			Assert.assertNotNull(Lstname);

			Assert.assertNotNull(Mobno);

			Assert.assertNotNull(mail);

			test.log(LogStatus.PASS, "Main Guest details are autopopulated successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Main Guest details are not autopopulating");
		}

		// Adding guest
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,800)");
			driver.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/button")).click();
			WebElement GFname = driver
					.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[5]/div[2]/div[2]/input"));
			GFname.sendKeys("Louie");
			WebElement GLname = driver
					.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[5]/div[3]/div[2]/input"));
			GLname.sendKeys("Madhav");
			test.log(LogStatus.PASS, "Additional Guest details are successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Additional Guest details are unable to add");
		}

		// Proceeding to Payment method selection page

		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,800)");

			WebElement Guest = driver.findElement(By.xpath("//*[text()='Proceed to select the mode of payment']"));
			Guest.click();
			test.log(LogStatus.PASS, "Able to proceed payment page successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed payment page");
		}
	}

	@Test(priority = 6, invocationCount = 1)
	public void CIB() throws IOException, InterruptedException {
		test = report.startTest("Payment Method verification");
		try {
			WebElement radio_cib = driver.findElement(By.id("cib"));
			radio_cib.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);

//Select Terms and Conditions - check box

			WebElement checkbox_terms = driver.findElement(By.xpath("//*[@class='sc-bTfYFJ hXDpds']"));
			checkbox_terms.click();
			multiScreens.multiScreenShot(driver);

// Book now button verification
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(2000);
			WebElement book_now = driver.findElement(By.xpath("//*[contains(text(),'Proceed to pay')]"));
			Thread.sleep(2000);
			book_now.click();
			test.log(LogStatus.PASS, "Able to select CIB card");

		} catch (

		AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select CIB card");
		}

//Selecting 'Failure' button
		try {

			// Alert validation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/button[2]")).click();
			multiScreens.multiScreenShot(driver);

			test.log(LogStatus.PASS, "Failure button selected successfully");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Failed to select failure button");
		}

	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		report.flush();
		report.close();
//driver.quit();
	}
}
