package French_lang.Cancellation.Direct_Booking_Cancellation.Upcoming;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;


public class Partial_Room_Cancel_Fr {
	public static WebDriver driver = null;


	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\French","French");
	static ExtentTest test;
	static ExtentReports report;
		

	@BeforeTest	
		public void Launch() throws Exception {
			
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\"+"French_DB_Cancel_Partial_ExtentReportResults.html");
		
	    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	@Test (priority = 0, invocationCount = 1)
	public void startTest() throws IOException
	{
		test = report.startTest("Scenario");
		try
		{
			test.log(LogStatus.PASS, "Upcoming Partial cancellation for Direct Booking payment method with Multiple rooms(French)");
		} catch (AssertionError e)
		{
			
		}
	}
	@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}
	@Test (priority = 2, invocationCount = 1)
	public void Lang_Selection() throws IOException
	{
	test = report.startTest("Language Selection");
	// Language change
	try
	{
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div"));
		if (language.getText().equals("English") || language.getText().equals("Arabic"))
		{
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='Fran�ais')]"));
		chglang.click();
		}
		else
		{
		System.out.println("French");

		}
		test.log(LogStatus.PASS, "French language is selected");
				
	}
	catch(AssertionError e)
	{
		test.log(LogStatus.PASS, "French language not changed");
	}
	}

	@Test (priority = 3, invocationCount = 1)
	public void lgn() throws IOException, InterruptedException
	{
	// Login�
	test = report.startTest("Login");
	try
	{
		WebElement nam_Login = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
		uname.sendKeys("tnamlatic@gmail.com");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
		pwd.sendKeys("Test123456");
	multiScreens.multiScreenShot(driver);
	pwd.sendKeys(Keys.ENTER);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	Thread.sleep(2000);
	test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Login Failed");
	}
	}
	@Test (priority = 4, invocationCount = 1)
	public void Myprofile_Menu() throws IOException, InterruptedException
	{
	// Click on my profile menu		
	test = report.startTest("Select my profile menu");
	try
	{
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]"));
		menu.click();
		WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
		/*Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span")).click();
		//Actions builder=new Actions(driver);
		//builder.moveToElement(menu).build().perform();
		WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();*/
		test.log(LogStatus.PASS, "My booking page - Menu option 2 selected");
	}
	catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Unable to select My Profile Menu");	
	}
	}
	//Select Direct Booking tab - Room 1 cancellation
		@Test (priority = 6, invocationCount = 1)
		public void select_DB_r1() throws IOException, InterruptedException
		{
			test = report.startTest("Select Direct Booking tab - Room 1 cancellation");
			try
			{
				Thread.sleep(3000);
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");				
				Thread.sleep(3000);
				WebElement select_DB = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[1]/div[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", select_DB);
				select_DB.click();
				Thread.sleep(1000);
				test.log(LogStatus.PASS, "Direct booking tab is selected - Room 1 Cancellation");
				multiScreens.multiScreenShot(driver);
			}
			catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Direct booking tab is not selected - Room 1 cancellation");	
			}
		}
//Multiple Partial room 1 cancellation for card payment
				@Test (priority = 7, invocationCount = 1)
				public void Cancel_Mul_Partial_Room1_Up() throws IOException, InterruptedException
				{
				// Click on cancel booking on upcoming section
				test = report.startTest("Select multiple Partial room 1 and cancel it - Upcoming section");
				try
				{
					//WebElement scroll = driver.findElement(By.xpath("//div[contains(text(),'NM9Y2SDDTM4')]"));
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);			
					Thread.sleep(3000);
					((JavascriptExecutor)driver).executeScript("scroll(0,0)");				
					Thread.sleep(3000);
					WebElement cancel = driver.findElement(By.xpath("//div[contains(text(),'NDQ4JYO8FCT')]/following::div[text()='Annuler une r�servation']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
					cancel.click();
					Thread.sleep(1000);
					multiScreens.multiScreenShot(driver);

					driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[2]/input")).click(); // Select room 1
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Direct Booking payment on upcoming section - Select multiple partial room 1 and cancel is working");	
					multiScreens.multiScreenShot(driver);

	//Room 1 details
					test.log(LogStatus.PASS," Room 1 Details ");
					//print room types
					String room_type1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[2]/div[1]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print Room type: "+room_type1);
					
					//Print room price value
					String room_price1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[2]/div[2]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print Room Price: "+room_price1);
					
					
					//Print sharing and room count
					String shar_room1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[2]/div[3]/div[3]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print sharing and room count: "+shar_room1);
					/*
					//Print cancellation types
					String cancel_types1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div[2]/div[5]/div")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print cancellation types: "+cancel_types1);*/
					
					driver.findElement(By.id("reason-1")).click();
					JavascriptExecutor m_scroll = (JavascriptExecutor) driver;
					m_scroll.executeScript("window.scrollBy(0,2500)", "");				
					Thread.sleep(3000);
					WebElement scroll1 = driver.findElement(By.xpath("//div[contains(text(),'Oui, annuler cette r�servation')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll1);
					scroll1.click();
					Thread.sleep(1000);
					multiScreens.multiScreenShot(driver);

					
					// Click on OK button - To close a cancel successful pop-up window.
					((JavascriptExecutor)driver).executeScript("scroll(0,600)");				
					Thread.sleep(3000);
					driver.findElement(By.xpath("//div[contains(text(),'accord')]")).click();
					multiScreens.multiScreenShot(driver);

				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Direct Booking payment on upcoming section - Select multiple partial room 1 and cancel not working");	
				}
				}
//select my booking tab
				@Test (priority = 8, invocationCount = 1)
				public void select_MB() throws IOException, InterruptedException
				{
					test = report.startTest("Select My Booking tab");
					try
					{
						Thread.sleep(3000);
						((JavascriptExecutor)driver).executeScript("scroll(0,0)");				
						Thread.sleep(3000);
						WebElement select_DB = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[1]/div[1]"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", select_DB);
						select_DB.click();
						Thread.sleep(1000);
						test.log(LogStatus.PASS, "My booking tab is selected");
						multiScreens.multiScreenShot(driver);
					}
					catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "My booking is not selected");	
					}
				}

				
//Select Direct Booking tab - Room 1 cancellation
				@Test (priority = 9, invocationCount = 1)
				public void select_DB_r2() throws IOException, InterruptedException
				{
					test = report.startTest("Select Direct Booking tab - Room 2 cancellation");
					try
					{
						Thread.sleep(3000);
						((JavascriptExecutor)driver).executeScript("scroll(0,0)");				
						Thread.sleep(3000);
						WebElement select_DB = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[1]/div[2]"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", select_DB);
						select_DB.click();
						Thread.sleep(1000);
						test.log(LogStatus.PASS, "Direct booking tab is selected - Room 2 Cancellation");
						multiScreens.multiScreenShot(driver);
					}
					catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Direct booking tab is not selected - Room 2 cancellation");	
					}
				}

	//Multiple Partial room 2 cancellation for card payment
				@Test (priority = 10, invocationCount = 1)
				public void Cancel_Mul_Partial_Room2_Up() throws IOException, InterruptedException
				{
				// Click on cancel booking on upcoming section
				test = report.startTest("Select multiple Partial room 2 and cancel it - Upcoming section");
				try
				{
					//WebElement scroll = driver.findElement(By.xpath("//div[contains(text(),'NM9Y2SDDTM4')]"));
					//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);			
					Thread.sleep(3000);
					((JavascriptExecutor)driver).executeScript("scroll(0,0)");				
					Thread.sleep(3000);
					WebElement cancel = driver.findElement(By.xpath("//div[contains(text(),'NDQ4JYO8FCT')]/following::div[text()='Annuler une r�servation']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
					cancel.click();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Direct Booking payment on upcoming section - Select multiple partial room 2 and cancel is working");	
					multiScreens.multiScreenShot(driver);
					
					driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[3]/input")).click(); // Select room 2
					Thread.sleep(1000);
					
	//Room 2 details
					test.log(LogStatus.PASS," Room 2 Details ");
					//print room types
					String room_type2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[3]/div[1]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print Room type: "+room_type2);
					
					//Print room price value
					String room_price2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[3]/div[2]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print Room Price: "+room_price2);
					
					
					//Print sharing and room count
					String shar_room2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[4]/div/div[3]/div[2]/div[3]/div[3]/div[3]")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print sharing and room count: "+shar_room2);
					/*
					//Print cancellation types
					String cancel_types2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div[3]/div[5]/div")).getText();
					Thread.sleep(1000);
					test.log(LogStatus.PASS, "Print cancellation types: "+cancel_types2);*/
					multiScreens.multiScreenShot(driver);
					
					driver.findElement(By.id("reason-1")).click();
					JavascriptExecutor m_scroll = (JavascriptExecutor) driver;
					m_scroll.executeScript("window.scrollBy(0,2500)", "");				
					Thread.sleep(3000);
					WebElement scroll = driver.findElement(By.xpath("//div[contains(text(),'Oui, annuler cette r�servation')]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
					scroll.click();
					Thread.sleep(1000);
					
					// Click on OK button - To close a cancel successful pop-up window.
					((JavascriptExecutor)driver).executeScript("scroll(0,600)");				
					Thread.sleep(3000);
					driver.findElement(By.xpath("//div[contains(text(),'accord')]")).click();
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Card payment on upcoming section - Select multiple partial room 2 and cancel not working");	
				}
				}
@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
}
}
