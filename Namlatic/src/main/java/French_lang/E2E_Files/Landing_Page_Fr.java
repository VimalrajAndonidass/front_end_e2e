package French_lang.E2E_Files;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import French_lang.Java_Files.Hotel_Booking_fr;
import French_lang.Java_Files.Hotel_Filter;
import French_lang.Java_Files.My_Booking_fr;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;
public class Landing_Page_Fr{
	public static WebDriver driver = null;

	
	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\","French");
	static ExtentTest test;
	static ExtentReports report;
	
	@BeforeTest 
	public void launch() throws Exception
	{
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
	driver = new ChromeDriver();
	//Report configuration
	report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\"+"_FE_French.html");
    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	@Test (priority = 0, invocationCount = 1)
	public void startTest() throws IOException
	{
	test = report.startTest("Language");
	try
	{
	test.log(LogStatus.PASS, "Namlatic - French");
	} catch (AssertionError e)
	{
	}
	}
	@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
	System.out.println("French");
	System.out.println("***********");
	driver.get("chrome://settings/clearBrowserData");
	driver.navigate().to("https://test.namlatic.com/");
	driver.manage().window().maximize();
	multiScreens.multiScreenShot(driver);
	try
	{
	Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
	test.log(LogStatus.PASS, "URL matching - Pass");
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "URL mismatching - Fail");
	}
	}

	@Test (priority = 2, invocationCount = 1) 
	public void Title_Verification() throws IOException
	{
	test = report.startTest("Title Verification");

	//Title Verification
	String ActualTitle = driver.getTitle();
	String ExpectedTitle = "Namlatic";
	try {
	Assert.assertEquals(ExpectedTitle, driver.getTitle());
	test.log(LogStatus.PASS,"Title Matching - Pass");
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Title mismatches with" +e +"Fail");
	}
	// Language change
	WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div"));
	if (language.getText().equals("English") || language.getText().equals("Arabic"))
	{
	language.click();
	WebElement chglang = language.findElement(By.xpath("//*[(text()='Français')]"));
	chglang.click();
	}
	else
	System.out.println("French");
	}

	@Test (priority = 3, invocationCount = 1)
	public void header()
	{
	test = report.startTest("Header Verification");
	//Header Verification
	//Logo verification
	WebElement Header_Logo = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/a/img"));
	try
	{
	Header_Logo.isDisplayed();
	test.log(LogStatus.PASS,"Header Namlatic logo is available");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Header Namlatic logo is not available");
	}
	//Login
	//Ensure the availability of Login button
	try {
	driver.findElement(By.xpath("//*[(text()=’S'identifier’)]")).isDisplayed();
	driver.findElement(By.xpath("//*[(text()=’S'identifier’)]")).isEnabled();
	test.log(LogStatus.PASS,"Login button available for selection");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Login button is not available to select");
	}
	//Enroll hotel
	// Ensure the availability of Enroll hotel 
	try {
	driver.findElement(By.xpath("//*[(text()='Inscrire des hôtels')]")).isDisplayed();
	driver.findElement(By.xpath("//*[(text()='Inscrire des hôtels')]")).isEnabled();
	test.log(LogStatus.PASS,"Enroll Hotel button available for selection");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Enroll Hotel button is not available to select");
	}
	//Language Verification
	// Ensure the availability of Language options
	try {
	driver.findElement(By.xpath("//*[(text()='French')]")).isDisplayed();
	driver.findElement(By.xpath("//*[(text()='French')]")).isEnabled();
	driver.findElement(By.xpath("//*[(text()='French')]")).isSelected();
	test.log(LogStatus.PASS,"Language French is selected");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Someother language is selected other than French");
	}
	// Ensure the availability of Currency options
	try {
	WebElement currency = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]"));
	for (int i = 0; i < 2 ; i++) {
	System.out.println(currency.getText());
	// Assert.assertEquals(i, currency.getText());
	 }
	//driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled(); 
	//driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
	test.log(LogStatus.PASS,"Currency selection option is available and selectable - Pass");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Currency selection option is available and selectable - Fail");
	}
	// Ensure the availability of Elastic search 
	try {
	driver.findElement(By.xpath("//*[conatins(placeholder(),'Choisissez où vous voulez séjourner.')]")).isDisplayed();
	test.log(LogStatus.PASS,"Elastic search option is available and allowed to search");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Elastic search option is not available");
	}
	}

	@Test (priority = 4, invocationCount = 1)
	public void home_page_text() throws IOException, InterruptedException
	{
	test = report.startTest("Home Page Verification");
	//Home page text validation
	try
	{
	String Exptext1 = "Première plateforme Algérienne \n"
	+ " de réservation d'hôtels";
	String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[1]")).getText());
	Assert.assertEquals(Exptext1, Acttext1);
	String Exptext2 = "Restez avec nous, sentez-vous comme chez vous";
	String Acttext2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[2]")).getText();
	Assert.assertEquals(Exptext2, Acttext2);
	String Exptext3 = "Vivez une expérience inoubliable dans l'hôtel de votre rêve avec Namlatic.";
	String Acttext3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[3]")).getText();
	Assert.assertEquals(Exptext3, Acttext3);

	test.log(LogStatus.PASS,"Home Page text verified, Pass");
	}catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Home page text mismatches with expected");
	}
	// Ensure the availability of search button
	try {
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div")).isDisplayed();
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div")).isEnabled();
	test.log(LogStatus.PASS,"Search button is available and allowed to select");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Search button is not available or not able to selectable");
	}
	// Ensure the title text of carousel image
	try {
	WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[1]"));
	String Expected_title = "Vous avez une destination, nous vous trouverons l'endroit idéal où séjourner !";
	String Actual_title = title.getText();
	System.out.println(title.getText());
	Assert.assertEquals(Expected_title,Actual_title);
	System.out.println("Carousel title is matching with expected");
	} catch (Exception e)
	{
	System.out.println("Carousel title mismatching with expected");
	}
	}
	@Test (priority = 5, invocationCount = 1)
	public void Carousel_Verification() throws IOException, InterruptedException
	{
	test = report.startTest("Carousel section Verification");
	// Carousel image1 title verification
	try {
	WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[1]"));
	String Expected_title = "Vous avez une destination, nous vous trouverons l'endroit idéal où séjourner !";
	String Actual_title = title.getText();
	System.out.println(title.getText());
	AssertJUnit.assertEquals(Expected_title,Actual_title);
	test.log(LogStatus.PASS, "Carousel title is matching with expected");

	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Carousel title mismatching with expected");
	} 
	// Carousel image1 title verification
	WebElement img1_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[1]/div/div"));
	String Act_img1_title = img1_title.getText();
	String Exp_img1_title = "Alger";
	try {
	Assert.assertEquals(Exp_img1_title, Act_img1_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img1_title); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Actual Title:" + Act_img1_title + "Expected Title:" +Exp_img1_title);
	}
	// Carousel image1 text verification
	WebElement Cr_img1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[2]"));
	String Act_img1_text = Cr_img1.getText();
	String Exp_img1_text = "decouvrez Alger, surnommée « la blanche », « la bien-aimée », cette ville rêvée de Méditerranée.";
	try {
	Assert.assertEquals(Exp_img1_text, Act_img1_text);
	test.log(LogStatus.PASS,"Carousel image 1 'Alger' text is matching with expected");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img1_text +"Actual:"+ Act_img1_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image2 title verification
	WebElement img2_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[1]/div/div"));
	String Act_img2_title = img2_title.getText();
	String Exp_img2_title = "Annaba";
	try {
	Assert.assertEquals(Exp_img2_title, Act_img2_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img2_title);
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected: " + "Acual Title:" + Act_img2_title + "Expected Title:" +Exp_img2_title);
	}
	// Carousel image2 text verification
	WebElement Cr_img2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[2]"));
	String Act_img2_text = Cr_img2.getText();
	String Exp_img2_text = "Toute la beauté de l'Algérie, Entre la corniche, le front de mer, et le centre ville historique, Annaba est une destination complète qui devrait vous plaire"; 
	try {
	Assert.assertEquals(Exp_img2_text, Act_img2_text);
	test.log(LogStatus.PASS,"Carousel image 2 'Annaba' text is matching with expected");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img2_text +"Actual:"+ Act_img2_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image3 title verification
	WebElement img3_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[1]/div/div"));
	String Act_img3_title = img3_title.getText();
	String Exp_img3_title = "Béjaïa";
	try {
	Assert.assertEquals(Exp_img3_title, Act_img3_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img3_title); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img3_title + "Expected Title:" +Exp_img3_title);
	}
	// Carousel image3 text verification
	WebElement Cr_img3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[2]"));
	String Act_img3_text = Cr_img3.getText();
	String Exp_img3_text = "La cité cotiere d'Algérie, la plus grande ville de kabylie, une superbe cité, lumineuse à souhait, et bordée par la mer.";
	try {
	Assert.assertEquals(Exp_img3_text, Act_img3_text);
	test.log(LogStatus.PASS,"Carousel image 3 'Bejaia' text is matching with expected");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img3_text +"Actual:"+ Act_img3_text);
	} 
	multiScreens.multiScreenShot(driver);
	// Carousel image4 title verification
	WebElement img4_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[1]/div/div"));
	String Act_img4_title = img4_title.getText();
	String Exp_img4_title = "Constantine";
	try {
	Assert.assertEquals(Exp_img4_title, Act_img4_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img4_title);
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img4_title + "Expected Title:" +Exp_img4_title);
	}
	// Carousel image4 text verification
	WebElement Cr_img4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[2]"));
	String Act_img4_text = Cr_img4.getText();
	String Exp_img4_text = "La ville magique d'Algérie, Surnommée \"la ville des ponts suspensus\", un passage incontournable en Algérie, c'est probablement pour sa beauté sans pareil !"; 
	try {
	Assert.assertEquals(Exp_img4_text, Act_img4_text);
	test.log(LogStatus.PASS,"Carousel image 4 'Constantine' text is matching with expected");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img4_text +"Actual:"+ Act_img4_text);
	}
	multiScreens.multiScreenShot(driver);

	// Carousel image5 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	Thread.sleep(3000);
	driver.findElement(By.xpath ("//*[@class='slick-arrow slick-next']")).click();
	Thread.sleep(3000);
	WebElement img5_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[1]/div/div"));
	System.out.println(img5_title.getText());
	String Act_img5_title = img5_title.getText();
	String Exp_img5_title = "Tizi ouzou";
	try {
	Assert.assertEquals(Exp_img5_title, Act_img5_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img5_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img5_title + "Expected Title:" +Exp_img5_title);
	}
	// Carousel image5 text verification
	WebElement Cr_img5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[2]"));
	String Act_img5_text = Cr_img5.getText();
	String Exp_img5_text = "Belle ville d'algérie en plein nature, signifie \"col des genêts\". Entre mer et montagne. une ville typique, discrète, mais hors du commun !";
	try {
	Assert.assertEquals(Exp_img5_text, Act_img5_text);
	test.log(LogStatus.PASS,"Carousel image 5 'Tizi ouzou' text is matching with expected");
	}
	catch (Exception e)
	{

	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img5_text +"Actual:"+ Act_img5_text);
	} 
	multiScreens.multiScreenShot(driver);

	// Carousel image6 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img6_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[1]/div/div"));
	String Act_img6_title = img6_title.getText();
	String Exp_img6_title = "Tipaza";
	try {
	Assert.assertEquals(Exp_img6_title, Act_img6_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img6_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img6_title + "Expected Title:" +Exp_img6_title);
	}
	// Carousel image6 text verification
	WebElement Cr_img6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[2]"));
	String Act_img6_text = Cr_img6.getText();
	String Exp_img6_text = "un trésor, cette ville algérienne, une destination privilégiée des touristes. Entre mer et montagne, on se retrouve autour d'un paysage admirable qui fait plaisir aux yeux !";
	try {
	Assert.assertEquals(Exp_img6_text, Act_img6_text);
	test.log(LogStatus.PASS,"Carousel image 6" + "'" +Exp_img6_title + "'" + "text is matching with expected");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img6_text +"Actual:"+ Act_img6_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image7 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img7_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[1]/div/div"));
	String Act_img7_title = img7_title.getText();
	String Exp_img7_title = "Ain Témouchent";
	try {
	Assert.assertEquals(Exp_img7_title, Act_img7_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img7_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img7_title + "Expected Title:" +Exp_img7_title);
	}
	// Carousel image7 text verification
	WebElement Cr_img7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[2]"));
	String Act_img7_text = Cr_img7.getText();
	String Exp_img7_text = "Découvrez la perle de la côte ouest et profitez de ses nombreuses attractions historiques et plages qui ornent la longue côte";
	try {
	Assert.assertEquals(Exp_img7_text, Act_img7_text);
	test.log(LogStatus.PASS,"Carousel image 7" +"'"+Exp_img7_title+"'"+ "text is matching with expected");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img7_text +"Actual:"+ Act_img7_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image8 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img8_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[1]/div/div"));
	String Act_img8_title = img8_title.getText();
	String Exp_img8_title = "Skikda";
	try {
	Assert.assertEquals(Exp_img8_title, Act_img8_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img8_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img8_title + "Expected Title:" +Exp_img8_title);
	}
	// Carousel image8 text verification
	WebElement Cr_img8 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[2]"));
	String Act_img8_text = Cr_img8.getText();
	String Exp_img8_text = "La ville des fraises, autrefois baptisée Philippeville, une ville portuaire, à l’est de l’Algérie, avec un vaste front de mer, et un centre-ville animé";
	try {
	Assert.assertEquals(Exp_img8_text, Act_img8_text);
	test.log(LogStatus.PASS,"Carousel image 8" +"'"+Exp_img8_title+"'"+ "text is matching with expected");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img8_text +"Actual:"+ Act_img8_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image9 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img9_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[1]/div/div"));
	System.out.println(img9_title.getText());
	String Act_img9_title = img9_title.getText();
	String Exp_img9_title = "Oran";
	try {
	Assert.assertEquals(Exp_img9_title, Act_img9_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img9_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img9_title + "Expected Title:" +Exp_img9_title);
	}
	// Carousel image9 text verification
	WebElement Cr_img9 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[2]"));
	String Act_img9_text = Cr_img9.getText();
	String Exp_img9_text = "La Radieuse ou encore El Bahia, l'une des plus belles villes d'Algérie. Visitez ses somptueux monuments et surtout sa vue imprenable sur la Méditerranée.";
	try {
	Assert.assertEquals(Exp_img9_text, Act_img9_text);
	test.log(LogStatus.PASS,"Carousel image 9" +"'"+Exp_img9_title+"'"+ "text is matching with expected");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img9_text +"Actual:"+ Act_img9_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image10 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img10_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[1]/div/div"));
	System.out.println(img10_title.getText());
	String Act_img10_title = img10_title.getText();
	String Exp_img10_title = "Mostaganem";
	try {
	Assert.assertEquals(Exp_img10_title, Act_img10_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img10_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img10_title + "Expected Title:" +Exp_img10_title);
	}
	// Carousel image10 text verification
	WebElement Cr_img10 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[2]"));
	String Act_img10_text = Cr_img10.getText();
	String Exp_img10_text = "Une des plus belles villes cotiéres de l'Algerie, le lieu parfait pour se couper du monde et apprécier la mer et les plaisirs simples de la ville.";
	try {
	Assert.assertEquals(Exp_img10_text, Act_img10_text);
	test.log(LogStatus.PASS,"Carousel image 10" +"'"+Exp_img10_title+"'"+ "text is matching with expected");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img10_text +"Actual:"+ Act_img10_text);
	}
	multiScreens.multiScreenShot(driver);
	// Carousel image11 title verification
	((JavascriptExecutor)driver).executeScript("scroll(0,401)");
	driver.findElement(By.xpath ("//*[@id='Path_441']")).click();
	Thread.sleep(3000);
	WebElement img11_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[1]/div/div"));
	System.out.println(img11_title.getText());
	String Act_img11_title = img11_title.getText();
	String Exp_img11_title = "Sétif";
	try {
	Assert.assertEquals(Exp_img11_title, Act_img11_title); 
	test.log(LogStatus.PASS,"Title matching with expected: " + Exp_img11_title); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:" +Exp_img11_title);
	}
	// Carousel image11 text verification
	WebElement Cr_img11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[2]"));
	String Act_img11_text = Cr_img11.getText();
	String Exp_img11_text = "Sétif, ville antique et romaine située entre l’Atlas Téllien et la chaine du Desert. À l’évidence, son site de Djemila, fleuron du patrimoine culturel, vous révélera des richesses inattendues !";
	try {
	Assert.assertEquals(Exp_img11_text, Act_img11_text);
	test.log(LogStatus.PASS,"Carousel image 11" +"'"+Exp_img11_title+"'"+ "text is matching with expected");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Not Matching, Expected:" + Exp_img11_text +"Actual:"+ Act_img11_text);
	}
	multiScreens.multiScreenShot(driver);
	}

	@Test (priority = 6, invocationCount = 1)
	public void why_choose_us()
	{
	test = report.startTest("Why Choose us section verification");
	// Why choose us!
	// Title Verification
	String exp_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[1]")).getText();
	String act_title = "Pourquoi nous choisir ?";
	try {
	Assert.assertEquals(exp_title, act_title);
	test.log(LogStatus.PASS,"Title matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title - Not Matching");
	}
	// Easy Booking
	String exp_sec1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[1]")).getText();
	String act_sec1 = "Réservation facile";
	try {
	Assert.assertEquals(exp_sec1, act_sec1);
	test.log(LogStatus.PASS," Easy Booking - Matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Easy Booking - Not Matching");
	}
	String exp_bdy1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[2]")).getText();
	String act_bdy1 = "Le processus de réservation doit comporter un minimum d'étapes.";
	try {
	Assert.assertEquals(exp_bdy1, act_bdy1);
	test.log(LogStatus.PASS,"Easy Booking content - Matching");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Easy Booking content - Not Matching");
	}
	// Friendly Interfaces
	String exp_sec2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[1]")).getText();
	String act_sec2 = "Interfaces conviviales";
	try {
	Assert.assertEquals(exp_sec2, act_sec2);
	test.log(LogStatus.PASS, "Friendly Interfaces Title - Matching");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Friendly Interfaces Title - Not Matching");
	}
	String exp_bdy2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[2]")).getText();
	String act_bdy2 = "Un moteur de réservation d'hôtel facile et convivial.";
	try {
	Assert.assertEquals(exp_bdy2, act_bdy2);
	test.log(LogStatus.PASS, "Friendly Interfaces content - Matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Friendly Interface content - Not Matching");
	}
	// Wide Payment options
	String exp_sec3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[1]")).getText();
	String act_sec3 = "De nombreuses options de paiement";
	try {
	Assert.assertEquals(exp_sec3, act_sec3);
	test.log(LogStatus.PASS, "Wide Payment Title - Matching"); 

	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Wide Payment Title - Not Matching"); 

	}
	String exp_bdy3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[2]")).getText();
	String act_bdy3 = "Paiements nationaux et internationaux acceptés";
	try {
	Assert.assertEquals(exp_bdy3, act_bdy3);
	test.log(LogStatus.PASS, "Wide Payment content - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Wide Payment content - not Matching"); 
	}
	// Customized cancellation
	String exp_sec4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[1]")).getText();
	String act_sec4 = "Annulation personnalisée";
	try {
	Assert.assertEquals(exp_sec4, act_sec4);
	test.log(LogStatus.PASS, "Customized Cancellation - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Customized Cancellation - Not Matching");
	}
	String exp_bdy4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[2]")).getText();
	String act_bdy4 = "Vous pouvez annuler sans frais selon les politiques en vigueur.";
	try {
	Assert.assertEquals(exp_bdy4, act_bdy4);
	test.log(LogStatus.PASS, "Customized Cancellation content - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Customized Cancellation content - Not Matching"); 
	}
	// Free cancellation
	String exp_sec5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[1]")).getText();
	String act_sec5 = "La première plateforme algérienne de réservation d'hôtels avec option de paiement local et annulation gratuite.";
	try {
	Assert.assertEquals(exp_sec5, act_sec5);
	test.log(LogStatus.PASS, "Free Cancellation Title - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Free Cancellation Title - not Matching"); 
	}
	String exp_bdy5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[2]")).getText();
	String act_bdy5 = "*Conditions générales appliquées aux politiques d'annulation.";
	try {
	Assert.assertEquals(exp_bdy5, act_bdy5);
	test.log(LogStatus.PASS, "Free Cancellation content - Matching"); 
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Free Cancellation content - Not Matching"); 
	}
	}

	@Test (priority = 7, invocationCount = 1)
	public void help()
	{
	test = report.startTest("Help section verification");
	// Help Section
	String exp_help_title= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[1]")).getText();
	String act_help_title = "Nous sommes prêts à vous aider et à faciliter votre processus de réservation.\n"
	+ " Suivez simplement nos étapes et obtenez tout facilement.";
	try {
	Assert.assertEquals(exp_help_title, act_help_title);
	test.log(LogStatus.PASS, "Help section Title - Matching"); 
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section Title - not Matching"); 
	}
	// Location Section
	String exp_help1= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[2]")).getText();
	String act_help1 = "Choisissez l'endroit où vous souhaitez séjourner";
	try {
	Assert.assertEquals(exp_help1, act_help1);
	test.log(LogStatus.PASS, "Help section1 Title - Matching");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section1 Title - not Matching");
	}
	String exp_help1_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[3]")).getText();
	String act_help1_con ="Choisissez un hôtel dans la ville sélectionnée";
	try {
	Assert.assertEquals(exp_help1_con, act_help1_con);
	test.log(LogStatus.PASS, "Help section1 content - Matching"); 
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section1 content - not Matching"); 
	}
	//Choice
	String exp_help2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[2]")).getText();
	String act_help2 = "Réservez votre choix";
	try {
	Assert.assertEquals(exp_help2, act_help2);
	test.log(LogStatus.PASS, "Help section2 title - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section2 title - Not Matching");
	}
	String exp_help2_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[3]")).getText();
	String act_help2_con ="C'est seulement en effectuant une transaction que vous avez réussi à obtenir toutes les facilités.";
	try {
	Assert.assertEquals(exp_help2_con, act_help2_con);
	test.log(LogStatus.PASS, "Help section2 content - Matching");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section2 content - not Matching");
	}
	//Checkin
	String exp_help3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[2]")).getText();
	String act_help3 ="Enregistrement";
	try {
	Assert.assertEquals(exp_help3, act_help3);
	test.log(LogStatus.PASS, "Help section3 Title - Matching"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section3 Title -not Matching"); 
	}
	String exp_help3_con = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[3]")).getText();
	String act_help3_con ="Présentez-vous à l'hôtel avec une pièce d'identité et une preuve de paiement et profitez de votre séjour.";
	try {
	Assert.assertEquals(exp_help3_con, act_help3_con);
	test.log(LogStatus.PASS, "Help section3 content - Matching");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Help section3 content - not Matching");
	}
	}

	@Test (priority = 8, invocationCount = 1)

	public void covid()
	{
	test = report.startTest("Covid Measure section verification");
	//Covid Measures
	//Title verififcation
	String exp_COVID_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[1]")).getText();
	String act_COVID_title ="Mesures COVID-19 sur les hôtels Namlatic";
	try {
	Assert.assertEquals(exp_COVID_title, act_COVID_title);
	test.log(LogStatus.PASS, "COVID Measure Title - Matching"); 
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure Title - Not Matching");
	}
	//Sub Title 
	String exp_COVID_subtitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[1]")).getText();
	String act_COVID_subtitle ="Les hôtels Namlatic appliquent les mesures préventives COVID19";
	try {
	Assert.assertEquals(exp_COVID_subtitle, act_COVID_subtitle);
	test.log(LogStatus.PASS, "COVID Measure Sub_Title - Matching"); 
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure Sub_Title - Not Matching");
	}
	//Measure 1
	String exp_COVID_mes1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[2]")).getText();
	String act_COVID_mes1 ="100% sûr et désinfecté";
	try {
	Assert.assertEquals(exp_COVID_mes1, act_COVID_mes1);
	test.log(LogStatus.PASS, "COVID Measure_1 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_1 -Not Matching");
	}
	//Measure 2
	String exp_COVID_mes2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[2]/wrapper/div[2]")).getText();
	String act_COVID_mes2 ="Masque, latex, gants pour tout le personnel.";
	try {
	Assert.assertEquals(exp_COVID_mes2, act_COVID_mes2);
	test.log(LogStatus.PASS, "COVID Measure_2 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_2 -Not Matching");
	}
	//Measure 3
	String exp_COVID_mes3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[3]/wrapper/div[2]")).getText();
	String act_COVID_mes3 ="Masque, latex, gants pour tout le personnel.";
	try {
	Assert.assertEquals(exp_COVID_mes3, act_COVID_mes3);
	test.log(LogStatus.PASS, "COVID Measure_3 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_3 - Not Matching");
	}
	//Measure 4
	String exp_COVID_mes4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[4]/wrapper/div[2]")).getText();
	String act_COVID_mes4 ="Désinfection toutes les 2 heures";
	try {
	Assert.assertEquals(exp_COVID_mes4, act_COVID_mes4);
	test.log(LogStatus.PASS, "COVID Measure_4 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_4 -Not Matching");
	}
	//Measure 5
	String exp_COVID_mes5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[5]/wrapper/div[2]")).getText();
	String act_COVID_mes5 ="Contrôle de la température obligatoire pour tous les clients et le personnel";
	System.out.println(exp_COVID_mes5);
	try {
	Assert.assertEquals(exp_COVID_mes5, act_COVID_mes5);
	test.log(LogStatus.PASS, "COVID Measure_5 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_5 - Not Matching");
	}
	//Measure 6
	String exp_COVID_mes6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[6]/wrapper/div[2]")).getText();
	String act_COVID_mes6 ="Linge/Ustensiles lavés dans de l'eau à 70 c +.";
	try {
	Assert.assertEquals(exp_COVID_mes6, act_COVID_mes6);
	test.log(LogStatus.PASS, "COVID Measure_6 - Matching"); 
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_6 -Not Matching");
	}
	//Measure 7 
	String exp_COVID_mes7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[7]/wrapper/div[2]")).getText();
	String act_COVID_mes7 ="Produits chimiques de nettoyage de qualité hospitalière utilisés dans la propriété";
	try {
	Assert.assertEquals(exp_COVID_mes7, act_COVID_mes7);
	test.log(LogStatus.PASS, "COVID Measure_7 - Matching");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_7 -Not Matching");
	}
	//Measure 8 
	String exp_COVID_mes8 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[8]/wrapper/div[2]")).getText();
	String act_COVID_mes8 ="Distanciation sociale à la réception, à l'ascenseur, dans les espaces communs et les jardins.";
	try {
	Assert.assertEquals(exp_COVID_mes8, act_COVID_mes8);
	test.log(LogStatus.PASS, "COVID Measure_8 - Matching");
	}
	catch (Exception e)
	{
	test.log(LogStatus.FAIL, "COVID Measure_8 -Not Matching");
	}
	} 
	@Test (priority = 9, invocationCount = 1) 
	public void footer() throws InterruptedException, IOException
	{
	test = report.startTest("Footer Verification");
	//Footer Verification
	//Namlatic logo verification
	WebElement footer1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/a/img"));
	try {
	footer1.isDisplayed();
	test.log(LogStatus.PASS, "Namlatic logo available in Footer"); 
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Namlatic logo missing in Footer");
	}
	//About us
	String exp_footer2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[1]")).getText();
	String act_footer2 = "À propos";
	try {
	Assert.assertEquals(exp_footer2, act_footer2);
	test.log(LogStatus.PASS, "About us is available in footer");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "About us is not available in footer");
	}
	//Terms & conditions
	String exp_footer3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[2]")).getText();
	String act_footer3 = "Termes et conditions";
	try {
	Assert.assertEquals(exp_footer3, act_footer3);
	test.log(LogStatus.PASS, "Terms and Conditions is available in footer"); 
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Terms and Conditions is not available in footer");
	}
	//Privacy Policy
	String exp_footer4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[3]")).getText();
	String act_footer4 = "Politiques de confidentialité";
	try {
	Assert.assertEquals(exp_footer4, act_footer4);
	test.log(LogStatus.PASS, "Privacy Policy is available in footer"); 
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Privacy Policy is not available in footer"); 
	}
	//Namlatic help center
	String exp_footer5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[4]")).getText();
	String act_footer5 = "Namlatic support";
	try
	{
	Assert.assertEquals(exp_footer5, act_footer5);
	test.log(LogStatus.PASS, "Namlatic Help Center is available in footer");
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Help Center is not available in footer"); 
	}
	//Suivez nous
	String exp_footer6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]")).getText();
	String act_footer6 = "Suivez nous";
	try {
	Assert.assertEquals(exp_footer6, act_footer6);
	test.log(LogStatus.PASS, "Follow us is available in footer");
	}
	catch (AssertionError e)
	{
	System.out.println("Suivez nous is not available in footer");
	test.log(LogStatus.FAIL, "Follow us is not available in footer");
	}
	//Instagram launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
	Thread.sleep(2000);
	WebElement insta = driver.findElement(By.xpath("//*[name()='svg'][3]"));
	insta.click();
	//Instagram URL verification
	//Get handles of the windows
	String MainWindow = driver.getWindowHandle();
	String mainWindowHandle = driver.getWindowHandle();
	Set<String> allWindowHandles = driver.getWindowHandles();
	Iterator<String> iterator = allWindowHandles.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator.hasNext()) {
	String ChildWindow = iterator.next();
	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	 driver.switchTo().window(ChildWindow);
	 }}
	multiScreens.multiScreenShot(driver);
	String exp_insta_title = "https://www.instagram.com/namlatic/";
	try
	{
	Assert.assertEquals(exp_insta_title, driver.getCurrentUrl());
	test.log(LogStatus.PASS,"Navigating to Namlatic Instagram page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Namlatic Instagram page is not loaded or loading to improper URL");
	}
	driver.switchTo().window(MainWindow);
	//Youtube launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
	Thread.sleep(2000);
	WebElement YT = driver.findElement(By.xpath("//*[name()='svg'][4]"));
	YT.click();
	//Youtube URL verification
	//Get handles of the windows
	String MainWindow1 = driver.getWindowHandle();
	String mainWindowHandle1 = driver.getWindowHandle();
	Set<String> allWindowHandles1 = driver.getWindowHandles();
	Iterator<String> iterator1 = allWindowHandles1.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator1.hasNext()) {
	String ChildWindow1 = iterator1.next();
	 if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
	 driver.switchTo().window(ChildWindow1);
	 }}
	multiScreens.multiScreenShot(driver);
	String exp_YT_title = "https://www.youtube.com/channel/UCqHvyjnCAW6FyOmmcr78fRw";
	try
	{
	Assert.assertEquals(exp_YT_title, driver.getCurrentUrl());
	test.log(LogStatus.PASS,"Navigating to Namlatic Youtube page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Namlatic Youtube page is not loaded or loading to improper URL");
	}
	driver.switchTo().window(MainWindow);
	//LinkedIN launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
	Thread.sleep(2000);
	WebElement IN = driver.findElement(By.xpath("//*[name()='svg'][5]"));
	IN.click();
	//LinkedIN URL verification
	//Get handles of the windows
	String MainWindow2 = driver.getWindowHandle();
	String mainWindowHandle2 = driver.getWindowHandle();
	Set<String> allWindowHandles2 = driver.getWindowHandles();
	Iterator<String> iterator2 = allWindowHandles2.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator2.hasNext()) {
	String ChildWindow2 = iterator2.next();
	 if (!mainWindowHandle2.equalsIgnoreCase(ChildWindow2)) {
	 driver.switchTo().window(ChildWindow2);
	 }}
	multiScreens.multiScreenShot(driver);
	String exp_IN_title1 = "NAMLATIC | LinkedIn";
	String exp_IN_title2 = "Signup | LinkedIN";
	if (driver.getTitle().equals(exp_IN_title1))
	{
	test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page");
	} else if (driver.getTitle().equals(exp_IN_title2))
	{
	test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page");
	} else
	{
	test.log(LogStatus.FAIL, "Namlatic LinkedIN page is not loaded or loading to improper URL");
	}
	Thread.sleep(3000);
	driver.switchTo().window(MainWindow2);
	//Twitter launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
	Thread.sleep(2000);
	WebElement TW = driver.findElement(By.xpath("//div[contains(text(),'Suivez nous')]/following::div//*[name()='svg'][1]"));
	TW.click();
	//Twitter URL verification
	//Get handles of the windows
	String MainWindow3 = driver.getWindowHandle();
	String mainWindowHandle3 = driver.getWindowHandle();
	Set<String> allWindowHandles3 = driver.getWindowHandles();
	Iterator<String> iterator3 = allWindowHandles3.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator3.hasNext()) {
	String ChildWindow3 = iterator3.next();
	 if (!mainWindowHandle3.equalsIgnoreCase(ChildWindow3)) {
	 driver.switchTo().window(ChildWindow3);
	 }}
	multiScreens.multiScreenShot(driver);
	String exp_TW_title = "https://twitter.com/Namla_officiel"; 
	try
	{
	Assert.assertEquals(exp_TW_title, driver.getCurrentUrl());
	test.log(LogStatus.PASS, "Navigating to Namlatic Twitter page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Twitter page is not loaded or loading to improper URL");
	}
	driver.switchTo().window(MainWindow3);
	//Facebook
	//Twitter launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
	Thread.sleep(2000);
	WebElement FB = driver.findElement(By.xpath("//div[contains(text(),'Suivez nous')]/following::div//*[name()='svg'][2]"));
	FB.click();
	//Twitter URL verification
	//Get handles of the windows
	String MainWindow4 = driver.getWindowHandle();
	String mainWindowHandle4 = driver.getWindowHandle();
	Set<String> allWindowHandles4 = driver.getWindowHandles();
	Iterator<String> iterator4 = allWindowHandles4.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator4.hasNext()) {
	String ChildWindow4 = iterator4.next();
	if (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
	driver.switchTo().window(ChildWindow4);
	}}
	multiScreens.multiScreenShot(driver);
	String exp_FB_title = "https://www.facebook.com/Namlatic"; 
	try
	{
	Assert.assertEquals(exp_FB_title, driver.getCurrentUrl());
	test.log(LogStatus.PASS, "Navigating to Namlatic Facebook page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Facebook page is not loaded or loading to improper URL");
	}
	driver.switchTo().window(MainWindow4);
	//About us 
 // Launch verifiation
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[contains(text(),'À propos')]")).click();
// URL verification
	//Get handles of the windows
	String MainWindow6 = driver.getWindowHandle();
	String mainWindowHandle6 = driver.getWindowHandle();
	Set<String> allWindowHandles6 = driver.getWindowHandles();
	Iterator<String> iterator6 = allWindowHandles6.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator6.hasNext()) {
	String ChildWindow6 = iterator6.next();
	if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
	driver.switchTo().window(ChildWindow6);
	}}
	multiScreens.multiScreenShot(driver);
	String exp_about_title = "https://business.namlatic.com/";
	try
	{
	Assert.assertEquals(exp_about_title, driver.getCurrentUrl());
	test.log(LogStatus.PASS, "Navigating to Namlatic About us page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic About us page is not loaded or loading to improper URL");
	}
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,3600)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,4000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,4400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,4800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,5200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,5600)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,6000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	((JavascriptExecutor)driver).executeScript("scroll(0,6400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	driver.switchTo().window(MainWindow6);
	//Terms & Conditions 
	// Launch verification
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[contains(text(),'Termes et conditions')]")).click(); 
	String exp_TC_title = "Namlatic";
	try
	{
	Assert.assertEquals(exp_TC_title, driver.getTitle());
	test.log(LogStatus.PASS, "Navigating to Namlatic Terms & Condition page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Terms & Conditions page is not loaded or loading to improper URL");
	}
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	driver.navigate().back(); 
	//Privacy Policy 
	// Launch verifiation
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[contains(text(),'Politiques de confidentialité')]")).click();
	String exp_PP_title = "Namlatic";
	try
	{
	Assert.assertEquals(exp_PP_title, driver.getTitle());
	test.log(LogStatus.PASS, "Navigating to Namlatic Privacy Policy page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Privacy Policy page is not loaded or loading to improper URL");
	}
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2400)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2800)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,3200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	driver.navigate().back(); 
	//Help Center 
	// Launch verifiation
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[contains(text(),'Namlatic support')]")).click();
	// URL verification
	//Get handles of the windows
	String MainWindow9 = driver.getWindowHandle();
	String mainWindowHandle9 = driver.getWindowHandle();
	Set<String> allWindowHandles9 = driver.getWindowHandles();
	Iterator<String> iterator9 = allWindowHandles9.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator9.hasNext()) {
	String ChildWindow9 = iterator9.next();
	if (!mainWindowHandle9.equalsIgnoreCase(ChildWindow9)) {
	driver.switchTo().window(ChildWindow9);
	}}
	multiScreens.multiScreenShot(driver);
	String exp_help_title1 = "Login - Jira Service Management";
	try
	{
	Assert.assertEquals(exp_help_title1, driver.getTitle());
	test.log(LogStatus.PASS, "Navigating to Namlatic Help Center page");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Namlatic Help Center page is not loaded or loading to improper URL");
	}
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	driver.switchTo().window(MainWindow9);
	//Card verification
	WebElement exp_footer7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[2]"));
	try {
	exp_footer7.isDisplayed();
	test.log(LogStatus.PASS, "We accept Visa, American express, Master card, CIB, Wire Transfer");
	}catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Card section missing");
	}
	} 
	public void screenshots() throws IOException, InterruptedException
	{
	//Screenshots
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("scroll(0,900)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1000)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("scroll(0,1900)");
	multiScreens.multiScreenShot(driver);
	Thread.sleep(2000);
	}

	//---login--- 
	@Test (priority = 10, invocationCount = 1)
	public void lgn() throws IOException, InterruptedException
	{
	// Login 
	test = report.startTest("Login Functionality and Elastic search verification");
	try
	{
	WebElement nam_Login = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div"));
	nam_Login.click();
	WebElement uname = driver.findElement(By.xpath("/html/body/div[1]/div/div[4]/div/div[3]/div[1]/div[2]/input")); 
	uname.sendKeys("9047232893");
	WebElement pwd = driver.findElement(By.xpath("/html/body/div[1]/div/div[4]/div/div[3]/div[2]/div[2]/input")); 
	pwd.sendKeys("Test@123");
	multiScreens.multiScreenShot(driver);
	pwd.sendKeys(Keys.ENTER);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	Thread.sleep(2000);
	test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Login Failed");
	}
	//Hotel search
	try
	{
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement Hotel = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
	Hotel.sendKeys("Annaba");
	Thread.sleep(2000);
	Hotel.sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(2000);
	Hotel.sendKeys(Keys.ENTER);
	Thread.sleep(2000); 
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement btnclick1 = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
	Thread.sleep(3000);
	btnclick1.click();
	multiScreens.multiScreenShot(driver); 
	test.log(LogStatus.PASS, "Hotel search working fine");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
	}
	}

	//---Hotel Filters---
	@Test (priority = 11)
	public void filter() throws IOException, InterruptedException
	{
	// Price filter verification
	//Title Verification
	test = report.startTest("Filter verification");
	Thread.sleep(3000);
	String act_Price_title =driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div")).getText();
	String exp_Price_title = "prix";
	try {
	Assert.assertEquals(exp_Price_title, act_Price_title); 
	test.log(LogStatus.PASS, "Price filter menu available");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Price filter menu not available");
	}
	multiScreens.multiScreenShot(driver);

	//Lowest price verification
	WebElement lowprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[5]"));
	try {
	test.log(LogStatus.PASS,"Lowest price is enabled: " +lowprice.isDisplayed());
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Lowest price is not enabled"); 
	}
	multiScreens.multiScreenShot(driver);
	//Highest price verification
	WebElement highprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[7]"));
	try {
	test.log(LogStatus.PASS,"Highest price is enabled: " +highprice.isDisplayed());
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Highest price is not enabled"); 
	}
	multiScreens.multiScreenShot(driver);
	//Slider verification
	WebElement slider = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div"));
	//WebElement slider2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[6]"));
	try {
	test.log(LogStatus.PASS,"Price slider1 is enabled: " +slider.isDisplayed());
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Price slider is not enabled"); 
	}
	multiScreens.multiScreenShot(driver);
	// Slider navigation
	Actions act = new Actions(driver);
	act.dragAndDropBy(slider, 0, 80).build().perform();
	//act.dragAndDropBy(slider, -20, 0).build().perform();
	multiScreens.multiScreenShot(driver);
	//Price low to high
	String act_lowtohigh = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/label")).getText();
	String exp_lowtohigh = "prix croissant";
	WebElement lowtohigh = driver.findElement(By.id("filter_price_low_to_high"));
	try {
	Assert.assertEquals(exp_lowtohigh, act_lowtohigh);
	System.out.println("Low to high option button is displayed:" +lowtohigh.isDisplayed());
	System.out.println("Low to high option button is selected:" +lowtohigh.isSelected()); 
	driver.findElement(By.xpath("//*[@id=\"filter_price_low_to_high\"]")).click(); 
	test.log(LogStatus.PASS, "Price - low to high is displaying and enabled");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Price - low to high is missing or not enabled to select"); 
	}
	multiScreens.multiScreenShot(driver);
	//Price high to low
	String act_hightolow = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[3]/label")).getText();
	String exp_hightolow = "Prix décroissant";
	WebElement hightolow = driver.findElement(By.id("filter_price_high_to_low"));
	System.out.println(act_hightolow);
	try {
	Assert.assertEquals(exp_hightolow, act_hightolow);
	System.out.println("High to Low option button is displayed:" +hightolow.isDisplayed());
	System.out.println("High to Low option button is selected:" +hightolow.isSelected()); 
	driver.findElement(By.xpath("//*[@id=\"filter_price_high_to_low\"]")).click();
	test.log(LogStatus.PASS,"Price - high to low is displaying and enabled");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Price - high to low is missing or not enabled to select"); 
	}
	multiScreens.multiScreenShot(driver);

	//Price filter enable and disable
	//Deselect
	WebElement pricefilter = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div"));
	pricefilter.click();
	Thread.sleep(3000);
	//Select
	pricefilter.click();
	multiScreens.multiScreenShot(driver);
	}
	@Test (priority = 12)
	public void popular_sr() throws InterruptedException, IOException
	{
	test = report.startTest("Popular search verification");
	//Popular search popular filter enable and disable
	//Deselect
	WebElement popularfilter = driver.findElement(By.xpath("//*[contains(text(),'Recherches populaires')]"));
	popularfilter.click();
	Thread.sleep(3000);
	//Select
	popularfilter.click();
	multiScreens.multiScreenShot(driver);
	//Title verification
	String act_pop_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[1]/div")).getText();
	String exp_pop_title = "recherches populaires";
	try {
	Assert.assertEquals(exp_pop_title, act_pop_title);
	test.log(LogStatus.PASS, "Popular search title is matching");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Popular search title is mismatching");
	}
	multiScreens.multiScreenShot(driver);
	//Couple Friendly verification
	String act_pop1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[1]/label")).getText();
	String exp_pop1 = "couples";
	try {
	Assert.assertEquals(exp_pop1, act_pop1);
	test.log(LogStatus.PASS,"Couple friendly is available in Popular search");
	driver.findElement(By.xpath("//*[@id=\"filter_couple_friendly\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Couple friendly is not available in Popular search");
	}
	multiScreens.multiScreenShot(driver);
	//Air Conditioning verification
	String act_pop2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[2]/label")).getText();
	String exp_pop2 = "climatisation";
	try {
	Assert.assertEquals(exp_pop2, act_pop2);
	test.log(LogStatus.PASS, "Air Conditioning is available in Popular search");
	driver.findElement(By.xpath("//*[@id=\"filter_air_conditioning\"]")).click(); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Air Conditioning is not available in Popular search");
	}
	multiScreens.multiScreenShot(driver);
	//Free Breakfast verification
	String act_pop3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[3]/label")).getText();
	String exp_pop3 = "petit déjeuner gratuit";
	try {
	Assert.assertEquals(exp_pop3, act_pop3);
	test.log(LogStatus.PASS, "Free Breakfast is available in Popular search");
	driver.findElement(By.xpath("//*[@id=\"filter_free_breakfast\"]")).click(); 
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Free Breakfast is not available in Popular search");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	//Free Cancellation Policy verification
	String act_pop4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[4]/label")).getText();
	String exp_pop4 = "politique d'annulation gratuite";
	try {
	Assert.assertEquals(exp_pop4, act_pop4);
	test.log(LogStatus.PASS, "Free Cancellation is available in Popular search");
	driver.findElement(By.xpath("//*[@id=\"filter_free_cancellation_policy\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Free Cancellation is not available in Popular search");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");

	//Pay at hotel verification
	String act_pop5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[5]/label")).getText();
	String exp_pop5 = "payez à l'hôtel";
	try {
	Assert.assertEquals(exp_pop5, act_pop5);
	test.log(LogStatus.PASS, "Pay at hotel is available in Popular search");
	driver.findElement(By.xpath("//*[@id=\"filter_pay_at_hotel\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Pay at hotel is not available in Popular search");
	} 
	multiScreens.multiScreenShot(driver);
	//Deselect
	popularfilter.click();
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	Thread.sleep(2000);
	}
	@Test (priority = 13)

	public void Amenities() throws InterruptedException, IOException
	{
	test = report.startTest("Amenities section verification");

	//Amenities filter enable and disable
	//Deselect
	WebElement amenitiesfilter = driver.findElement(By.xpath("//*[contains(text(),'Equipements')]"));
	amenitiesfilter.click();
	Thread.sleep(3000);
	//Select
	amenitiesfilter.click();
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	//Title verification
	String act_amn_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[1]/div")).getText();
	String exp_amn_title = "equipements";
	try {
	Assert.assertEquals(exp_amn_title, act_amn_title);
	test.log(LogStatus.PASS,"Amenities title is matching");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Amenities title is mismatching");
	}
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");

	//Free Wi-fi verification
	String act_amn1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[1]/label")).getText();
	String exp_amn1 = "wi-fi gratuit";
	try {
	Assert.assertEquals(exp_amn1, act_amn1);
	test.log(LogStatus.PASS,"Free Wifi is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_free_wi_fi\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Free wifi is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	//Power Backup verification
	Thread.sleep(2000);
	String act_amn2 = driver.findElement(By.xpath("//*[contains(text(),'Sauvegarde d')]")).getText();
	//html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[2]/label")).getText();
	String exp_amn2 = "sauvegarde d'alimentation";
	System.out.println(act_amn2);
	try {
	Assert.assertEquals(exp_amn2, act_amn2);
	test.log(LogStatus.PASS,"Power Backup is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_power_backup\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Power Backup is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	//Gardern verification
	Thread.sleep(2000);
	String act_amn3 = driver.findElement(By.xpath("//*[contains(text(),'Jardin/cour arrière')]")).getText();
	String exp_amn3 = "jardin/cour arrière";
	System.out.println(act_amn3);

	try {
	Assert.assertEquals(exp_amn3, act_amn3);
	test.log(LogStatus.PASS,"Gardern is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_garden__backyard\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Gardern is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)");
	//SPA verification
	Thread.sleep(2000);
	String act_amn4 = driver.findElement(By.xpath("//*[contains(text(),'Spa')]")).getText();
	String exp_amn4 = "spa";
	try {
	Assert.assertEquals(exp_amn4, act_amn4);
	test.log(LogStatus.PASS, "SPA is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_spa\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"SPA is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//Swimming pool verification
	Thread.sleep(2000);
	String act_amn5 = driver.findElement(By.xpath("//*[contains(text(),'Piscine')]")).getText();
	String exp_amn5 = "piscine";
	try {
	Assert.assertEquals(exp_amn5, act_amn5);
	test.log(LogStatus.PASS,"Swimming pool is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_swimming_pool\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Swimming pool+ is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//Gym verification
	Thread.sleep(2000);
	String act_amn6 = driver.findElement(By.xpath("//*[contains(text(),'Gymnase')]")).getText();
	String exp_amn6 = "gymnase";
	try {
	Assert.assertEquals(exp_amn6, act_amn6);
	test.log(LogStatus.PASS,"GYM is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_gym\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "GYM is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//Laundry verification
	Thread.sleep(2000);
	String act_amn7 = driver.findElement(By.xpath("//*[contains(text(),'Service de blanchisserie')]")).getText();
	String exp_amn7 = "service de blanchisserie";
	try {
	Assert.assertEquals(exp_amn7, act_amn7);
	test.log(LogStatus.PASS,"Laundry service is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_laundry_services__washing_machine\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Laundry is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//Cafe verification
	Thread.sleep(2000);
	String act_amn8 = driver.findElement(By.xpath("//*[contains(text(),'Café')]")).getText();
	String exp_amn8 = "café";
	try {
	Assert.assertEquals(exp_amn8, act_amn8);
	test.log(LogStatus.PASS,"Café is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_caf_\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Café is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//Pet friendly verification
	Thread.sleep(2000);
	String act_amn9 = driver.findElement(By.xpath("//*[contains(text(),'Animaux acceptés')]")).getText();
	String exp_amn9 = "animaux acceptés";
	try {
	Assert.assertEquals(exp_amn9, act_amn9);
	test.log(LogStatus.PASS,"Pet friendly is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_pet_friendly\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Pet friendly is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,500)"); 

	//In House restaurant verification
	Thread.sleep(2000);
	String act_amn10 = driver.findElement(By.xpath("//*[contains(text(),'Restaurant sur place')]")).getText();
	String exp_amn10 = "restaurant sur place";
	try {
	Assert.assertEquals(exp_amn10, act_amn10);
	test.log(LogStatus.PASS,"In-House restaurant is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_in_house_restaurant\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"In-House restaurant is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,750)"); 

	//Card Payment verification
	Thread.sleep(2000);
	String act_amn11 = driver.findElement(By.xpath("//*[contains(text(),'Paiement par carte')]")).getText();
	String exp_amn11 = "paiement par carte";
	try {
	Assert.assertEquals(exp_amn11, act_amn11);
	test.log(LogStatus.PASS,"Card Payment is available in Amenities");
	driver.findElement(By.xpath("//*[@id=\"filter_card_payment\"]")).click();
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Card Payment is not available in Amenities");
	} 
	multiScreens.multiScreenShot(driver);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	((JavascriptExecutor)driver).executeScript("scroll(0,750)");
	}

	@Test (priority = 14)
	public void search() throws InterruptedException 
	{ 
	test = report.startTest("Hotel search verification");
	//Hotel Search
	try
	{
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez où vous voulez séjourner.')]")).clear();
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement Hotel = driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez où vous voulez séjourner.')]"));
	Hotel.sendKeys("Le café, Pondichéry");
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");

	String Hotel_name = driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez où vous voulez séjourner.')]")).getText();
	System.out.println(Hotel_name);
	Thread.sleep(2000);
	Hotel.sendKeys(Keys.ARROW_DOWN);
	Thread.sleep(2000);
	Hotel.sendKeys(Keys.ENTER);
	Thread.sleep(2000);
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div[4]/div")).click();
	Thread.sleep(2000); 
	//WebElement book_id = driver.findElement(By.xpath("//div[contains(text(),'Le café, Pondichéry')]/following::div[text()='Voir plus']"));
	test.log(LogStatus.PASS, "Able to search hotel successful");

	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Unable to search hotel with given keyword");
	}
	}
	//--- Hotel details page verification
	@Test(priority = 16, invocationCount = 1)
	void booking_fr() throws IOException
	{
	/*driver.navigate().to("https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD");
	driver.manage().window().maximize();
	multiScreens.multiScreenShot(driver); */
	WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div")); 
	Hotel_sel.click();
	String exp_hot_name = Hotel_sel.getText();
	multiScreens.multiScreenShot(driver);
	//Get handles of the windows
	String MainWindow = driver.getWindowHandle();
	String mainWindowHandle = driver.getWindowHandle();
	Set<String> allWindowHandles = driver.getWindowHandles();
	Iterator<String> iterator = allWindowHandles.iterator();
	// Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator.hasNext()) {
	String ChildWindow = iterator.next();
	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	driver.switchTo().window(ChildWindow);
	}}
	multiScreens.multiScreenShot(driver);
	//Scroll bar navigation
	//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
	// Hotel name verification
	Hotel_Filter hot = new Hotel_Filter();
	String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
	try
	{
	Assert.assertEquals(exp_hot_name, act_hot_name);
	test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Hotel name mismatching");
	driver.close();
	}
	}

	@Test(priority = 17, invocationCount = 1, enabled=false)

	public void image_fr()
	{
	test = report.startTest("Image section verification");
	try
	{
	//Image verification
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
	test.log(LogStatus.PASS, "Image loading properly");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Images are not loading properly");
	}
	}

	@Test(priority = 18, invocationCount = 1)
	void description_fr()
	{
	// Description verification
	test = report.startTest("Hotel Description section verification");
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]")).isDisplayed();
	test.log(LogStatus.PASS,"Hotel description is available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Description missing");
	}
	}
	@Test(priority = 19, invocationCount = 1)
	void roomtype_fr()
	{
	//Type of rooms section verification
	test = report.startTest("Room type section verification"); 
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")).isDisplayed();
	test.log(LogStatus.PASS,"Type of rooms section is available");
	} catch (Exception e)
	{
	test.log(LogStatus.PASS,"Types of rooms section missing");
	}
	//Room type images verification 
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img")).isDisplayed();
	test.log(LogStatus.PASS,"Room type images are available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Images are missing");
	}
	//Room type sharing details verification
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div")).isDisplayed();
	test.log(LogStatus.PASS,"Sharing details available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Sharing details are not available");
	} 
	// Amenities section verification
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]")).isDisplayed();
	test.log(LogStatus.PASS, "Amenities section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Amenities section not available");
	}
	// Rate section verification
	try
	{
	String rate = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).getText();
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).isDisplayed();
	//driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
	System.out.println("Selected currency" + rate);
	test.log(LogStatus.PASS,"Rate section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Rate section not available");
	}
	// Add Room section verification
	try
	{
	driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
	test.log(LogStatus.PASS,"Add room section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Add room section not available");
	} 
	((JavascriptExecutor)driver).executeScript("scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
	} 

	// Verification of Cancellation policies
	@Test(priority = 20, invocationCount = 1)

	public void cancellation_policy_fr()
	{
	test = report.startTest("Cancellation Policy section verification");
	String cancel_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div")).getText();
	System.out.println(cancel_type);
	if (cancel_type.equals("Annulation gratuite")) {
	// Card Payment verification
	String exp_title1 ="Politique d'annulation";
	String exp_title2 ="paiement par carte";
	String exp_text1 = "Annulation gratuite avant 18 h 00 (un jour avant), des frais de service peuvent s'appliquer.\n\n"

	+ "En cas d'annulation après 18h00 (un jour avant), le montant de la première nuit sera facturé.\n\n"

	+ "Aucun montant ne sera remboursé en cas d'annulation après l'enregistrement.\n\n"

	+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"

	+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent conformément aux politiques de l'hôtel.";

	String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
	String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
	String act_text1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]")).getText();
	try
	{
	Assert.assertEquals(exp_title1, act_title1);
	Assert.assertEquals(exp_title2, act_title2);
	Assert.assertEquals(exp_text1, act_text1);
	test.log(LogStatus.PASS,"Cancellation policy for Card payment content and title is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Cancellation policy for Card payment content is not matching");
	}

	// Wire Transfer verification
	String exp_text11 = "Virement bancaire";
	String exp_text12 = "Annulation gratuite avant 18 h 00 (un jour avant), des frais de service peuvent s'appliquer\n\n"
	+ "En cas d'annulation après 18h00 (un jour avant), le montant de la première nuit sera facturé.\n\n"

	+ "Aucun montant ne sera remboursé en cas d'annulation après l'heure d'arrivée.\n\n"

	+ "si le client ne se présente pas sur sa réservation confirmée, nous la traitons comme une annulation et aucun montant ne sera remboursé.\n\n"

	+ "Les heures d'enregistrement tardif ne seront pas soumises à un refinancement.\n\n"
	+ "La réservation réservée sera automatiquement annulée en cas d'échec de la transaction.\n\n"
	+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"
	+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent selon les politiques de l'hôtel.\n\n"

	+ "Les remboursements ne sont applicables que lorsqu'une demande d'annulation a été formellement soumise via la plateforme Namlatic.\n\n"
	+ "Les remboursements applicables prendront 5 à 7 jours ouvrables pour être traités.";
	String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
	String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
	try
	{
	Assert.assertEquals(exp_text11, act_text11);
	Assert.assertEquals(exp_text12, act_text12);
	test.log(LogStatus.PASS,"Cancellation policy for Wire Transfer content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Cancellation policy for Wire Transfer content is not matching");
	}

	// Pay at hotel
	((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')"); 
	String exp_text21 = "Payer à l'hôtel";
	String exp_text22 = "Lors d'une réservation effectuée avec l'option «Payer à l'hôtel», l'hôtel concerné percevra l'intégralité du paiement contre la réservation au moment de l'enregistrement.\n\n"
	+ "La réservation doit être confirmée 48 heures avant l'heure d'enregistrement de la date du voyage par e-mail ou par appel avec Namlatic.La réservation sera considérée comme confirmée uniquement sur vérification personnelle, sinon sera soumise à une annulation en vertu de la clause de politique d'annulation.\n\n"

	+ "Voici les coordonnées:\n"
	+ "Courriel: confirmation@namlatic.com\n"
	+ "Hotline Namlatic DZ: + 213982414415\n\n"
	+ "Pour les touristes internationaux, le paiement sera facturé en devise locale ou dans toute autre devise, comme décidé par l'hôtel.\n\n"
	+ "Pour des raisons de sécurité, l'utilisateur doit fournir le code PIN à 4 chiffres reçu avec son e-mail de confirmation de réservation.Namlatic ou l'hôtel peut annuler la réservation en cas de problème si le code PIN trouvé est incorrect.\n\n"

	+ "Namlatic permet l'annulation partielle et l'annulation complète des chambres réservées avant l'enregistrement.\n\n"
	+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation avec \"Aucun remboursement\".\n\n"
	+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement partiel.\n\n"
	+ "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"

	+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"
	+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.";
	String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
	String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
	try
	{
	Assert.assertEquals(exp_text21, act_text21);
	Assert.assertEquals(exp_text22, act_text22);
	test.log(LogStatus.PASS,"Cancellation policy for Pay at hotel content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Cancellation policy for Pay at hotel content is not matching");
	}
	// CIB
	((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')"); 
	String exp_text31 = "CIB";
	String exp_text32 = "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"
	+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"

	+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.\n\n"

	+ "Politique de remboursement\n\n"
	+ "Toute réservation annulée la veille avant 18h00 via la plateforme Namlatic obtiendra le remboursement intégral du montant payé par carte CIB (des frais d'annulation peuvent s'appliquer).\n\n"
	+ "Les frais d'annulation et les frais de service appliqués peuvent varier en fonction de la réservation et de l'établissement choisi.\n\n"
	+ "Le processus de remboursement applicable peut prendre 3 à 5 jours ouvrables.\n\n"
	+ "Non-présentation\n\n"
	+ "Aucun remboursement ne sera effectué si une personne ne s'est pas présentée à l'hôtel concerné lors de son séjour.\n\n"
	+ "Namlatic n'accepte aucune responsabilité pour les conséquences de votre arrivée retardée ou de toute annulation ou frais de non-présentation facturés par l'hôtel.\n\n"
	+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement.";

	String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
	String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
	try
	{
	Assert.assertEquals(exp_text31, act_text31);
	Assert.assertEquals(exp_text32, act_text32);
	test.log(LogStatus.PASS,"Cancellation policy for CIB payment method content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Cancellation policy for CIB payment method content is not matching");
	}
	}
	else if (cancel_type.equals("Non remboursable"))
	{
	// Card Payment verification
	String exp_title1 ="Politique d'annulation";
	String exp_title2 ="paiement par carte";
	String exp_text1 = "Aucun montant ne sera remboursé à tout moment de l'annulation.\n\n"
	+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"

	+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent conformément aux politiques de l'hôtel.";

	String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
	String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
	String act_text1 = driver.findElement(By.xpath("//div[contains(text(),'Aucun montant ne sera remboursé à tout moment de l'annulation.')]")).getText();
	try
	{
	Assert.assertEquals(exp_title1, act_title1);
	Assert.assertEquals(exp_title2, act_title2);
	Assert.assertEquals(exp_text1, act_text1);
	test.log(LogStatus.PASS,"Cancellation policy for Card payment content and title is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Cancellation policy for Card payment content is not matching");
	}

	// Wire Transfer verification
	String exp_text11 = "Virement bancaire";
	String exp_text12 = "Aucun remboursement ne sera fourni à tout moment de l'annulation.\n\n"
	+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"

	+ "La réservation réservée sera automatiquement annulée en cas d'échec de la transaction.";

	String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
	String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
	try
	{
	Assert.assertEquals(exp_text11, act_text11);
	Assert.assertEquals(exp_text12, act_text12);
	test.log(LogStatus.PASS,"Cancellation policy for Wire Transfer content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Cancellation policy for Wire Transfer content is not matching");
	}

	// Pay at hotel
	((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')"); 
	String exp_text21 = "Payer à l'hôtel";
	String exp_text22 = "Lors d'une réservation effectuée avec l'option «Payer à l'hôtel», l'hôtel concerné percevra l'intégralité du paiement contre la réservation au moment de l'enregistrement.\n\n"
	+ "La réservation doit être confirmée 48 heures avant l'heure d'enregistrement de la date du voyage par e-mail ou par appel avec Namlatic.La réservation sera considérée comme confirmée uniquement sur vérification personnelle, sinon sera soumise à une annulation en vertu de la clause de politique d'annulation.\n\n"

	+ "Voici les coordonnées:\n"
	+ "Courriel: confirmation@namlatic.com\n"
	+ "Hotline Namlatic DZ: + 213982414415\n\n"
	+ "Pour les touristes internationaux, le paiement sera facturé en devise locale ou dans toute autre devise, comme décidé par l'hôtel.\n\n"
	+ "Pour des raisons de sécurité, l'utilisateur doit fournir le code PIN à 4 chiffres reçu avec son e-mail de confirmation de réservation.Namlatic ou l'hôtel peut annuler la réservation en cas de problème si le code PIN trouvé est incorrect.\n\n"

	+ "Namlatic permet l'annulation partielle et l'annulation complète des chambres réservées avant l'enregistrement.\n\n"
	+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation avec \"Aucun remboursement\".\n\n"
	+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement partiel.\n\n"
	+ "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"

	+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"
	+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.";
	String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
	String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
	try
	{
	Assert.assertEquals(exp_text21, act_text21);
	Assert.assertEquals(exp_text22, act_text22);
	test.log(LogStatus.PASS, "Cancellation policy for Pay at hotel content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL, "Cancellation policy for Pay at hotel content is not matching");
	}
	// CIB
	((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')"); 
	String exp_text31 = "CIB";
	String exp_text32 = "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"
	+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"

	+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.\n\n"

	+ "Politique de remboursement\n\n"
	+ "Les réservations faites avec les conditions de l'hôtel \"non remboursable\" ne peuvent pas faire l'objet d'annulation ou de remboursement.\n\n"
	+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation non remboursable avec \"Aucun remboursement\"";
	String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
	String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
	try
	{
	Assert.assertEquals(exp_text31, act_text31);
	Assert.assertEquals(exp_text32, act_text32);
	test.log(LogStatus.PASS,"Cancellation policy for CIB payment method content is matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Cancellation policy for CIB payment method content is not matching");
	}

	}
	}
	@Test(priority = 21, invocationCount = 1)

	public void amenities_ver_fr()
	{
	test = report.startTest("Amenities section Verification");
	// Amenities section2 verification
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div")).isDisplayed();
	test.log(LogStatus.PASS,"Amenities section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Amenities section not available");
	}
	}
	@Test(priority = 22, invocationCount = 1)
	public void policy_ver_fr()
	{
	// Hotel Policy verification
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div")).isDisplayed();
	test.log(LogStatus.PASS,"Hotel Policy section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Hotel Policy section not available");
	}
	}
	@Test(priority = 23, invocationCount = 1)
	public void book_now_fr() throws InterruptedException, IOException
	{ 
	Date_room_selection_fr();
	//only one option should be active in script.
	CIB_fr();
	//Card_Payment_fr();
	//Wire_Transfer_fr();
	}

	public void Date_room_selection_fr() throws InterruptedException, IOException
	{
	test = report.startTest("Date and Room selection section verification");
	//From and To Date Selection
	WebElement date=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
	//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
	Thread.sleep(500);

	//WebElement navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
	//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", navigate);
	// driver.findElement(By.xpath("//*[text()='29']")).click();
	//driver.findElement(By.xpath("//*[text()='30']")).click();

	//Room selection
	Thread.sleep(2000);
	((JavascriptExecutor)driver).executeScript("scroll(0,700)");
	WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
	Select room_sel=new Select(element);
	room_sel.selectByVisibleText("1");
	WebElement firstEle=room_sel.getFirstSelectedOption();
	multiScreens.multiScreenShot(driver);

	//Book now button verification
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
	test.log(LogStatus.PASS,"Book Now button available in booking section");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Book Now button not available in Booking section");
	}
	//Click on book now button
	WebElement book_now = driver.findElement (By.xpath("//*[(text()='Reserve maintenant')]"));
	book_now.click();
	multiScreens.multiScreenShot(driver);
	} 
	//Select payment methods ' CIB '  using radio button.

	@Test(priority = 25, invocationCount = 1)
	public void CIB_fr() throws IOException, InterruptedException
	{
	test = report.startTest("Payment Method verification");
	try
	{
	if (driver.findElement(By.id("cib")).isEnabled())
	{
	WebElement radio_cib=driver.findElement(By.id("cib"));
	radio_cib.click();
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	//Select Terms and Conditions - check box
	WebElement checkbox_terms=driver.findElement(By.id("terms"));
	checkbox_terms.click();
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS,"CIB card selected");
	} else
	{
	//Currency selection
	WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
	Thread.sleep(500);
	cur.click();
	WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
	cur_sel.click();
	//CIB selection and proceed
	Date_room_selection_fr(); 
	WebElement radio_cib=driver.findElement(By.id("cib"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
	Thread.sleep(500);
	radio_cib.click();
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);

	//Select Terms and Conditions - check box
	WebElement checkbox_terms=driver.findElement(By.id("terms"));
	checkbox_terms.click();
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS,"CIB card selected");
	}
	}
	catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Unable to select CIB card");
	}
	}
	public void Card_Payment_fr() throws IOException, InterruptedException
	{
	WebElement radio_card=driver.findElement(By.id("card"));
	radio_card.click();
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	System.out.println("Card Payment selected");
	}
	public void Wire_Transfer_fr() throws IOException, InterruptedException
	{
	WebElement radio_wt=driver.findElement(By.id("wt"));
	radio_wt.click();
	multiScreens.multiScreenShot(driver);
	Thread.sleep(3000);
	System.out.println("Wire Transfer selected");
	}
	@Test(priority = 26, invocationCount = 1)
	public void various_section_Validation_fr() throws InterruptedException
	{
	// Booking section verification
	test = report.startTest("Verification of other section in Room details page");
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div")).isDisplayed();
	test.log(LogStatus.PASS,"Booking section available");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Booking section not available");
	}
	//Pricing
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]")).isDisplayed();
	test.log(LogStatus.PASS,"Pricing details available in booking section");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Pricing details not available in Booking section");
	}
	// Calender
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).isDisplayed();
	test.log(LogStatus.PASS,"Calender details available in booking section");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Calender details not available in Booking section");
	}
	//Guest
	try
	{
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
	test.log(LogStatus.PASS,"Guest details available in booking section");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Guest details not available in Booking section");
	}
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");

	//Navigating to various section
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
	Thread.sleep(1000);
	//((JavascriptExecutor)driver).executeScript("scroll(0,400)");

	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
	Thread.sleep(2000);
	/* //Select reCAPTCHA Check box
	driver.switchTo().defaultContent().findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"));
	System.out.println("test");
	WebElement checkbox_recaptcha=driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
	checkbox_recaptcha.click();
	System.out.println("reCAPTCHA check box is selected"); 

	//Click on Proceed To Pay button
	WebElement Proceed_element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"));
	Proceed_element.click(); */
	}

	//---My Booking Page Validation---

	/*public void Fr_Booking() throws InterruptedException, AWTException, IOException
	{
	//Selecting view booking ad Book now functionalities 
	view_booking();
	book_now();
	//Post execution result section
	System.out.println("My Booking: PASS"); 
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	multiScreens.multiScreenShot(driver); 
	}*/

	public void page_launch() throws InterruptedException, IOException
	{
	//selecting my booking menu from right top menu
	Thread.sleep(3000);
	WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
	menu.click();
	WebElement menuoption=driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
	menuoption.click();
	multiScreens.multiScreenShot(driver);
	}

	@Test(priority = 27, invocationCount = 1)
	public void view_booking() throws IOException, InterruptedException
	{ 
	test = report.startTest("My Booking Page - View Booking option verification");
	page_launch();
	//View Booking
	Thread.sleep(3000);
	try
	{
	WebElement view_book_id = driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[text()='Voir la réservation']"));
	view_book_id.click();
	test.log(LogStatus.PASS, "View booking section working fine"); 
	multiScreens.multiScreenShot(driver);
	//View Voucher details
	//Get handles of the windows
	String MainWindow1 = driver.getWindowHandle(); 
	String mainWindowHandle1 = driver.getWindowHandle();
	Set<String> allWindowHandles1 = driver.getWindowHandles();
	Iterator<String> iterator1 = allWindowHandles1.iterator();
	//Here we will check if child window has other child windows and will fetch the heading of the child window
	while (iterator1.hasNext()) {
	String ChildWindow1 = iterator1.next();
	if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
	driver.switchTo().window(ChildWindow1);
	}}
	//Scroll Down in View Booking Details page
	((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
	//WebDriverWait wait=new WebDriverWait(driver,50);
	Thread.sleep(5000);
	multiScreens.multiScreenShot(driver);
	//Click on Download Voucher link

	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	//WebElement download_Vou =driver.findElement(By.xpath("//*[(@class, 'sc-plWPA hwKzKa']"));
	Thread.sleep(5000);
	WebElement download_Vou =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
	download_Vou.click();
	Thread.sleep(5000);
	test.log(LogStatus.PASS, "Download voucher working fine"); 
	//Click on Email Voucher link
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	Thread.sleep(5000);
	WebElement send_email=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
	send_email.click();
	Thread.sleep(5000);
	test.log(LogStatus.PASS, "Email voucher working fine"); 
	multiScreens.multiScreenShot(driver);
	/* //Click on Print Voucher link 
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
	Thread.sleep(5000); 
	WebElement print_Vou=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
	print_Vou.click(); 
	Thread.sleep(5000);
	Robot esc_print_window = new Robot();
	esc_print_window.keyPress(KeyEvent.VK_ESCAPE);
	esc_print_window.keyRelease(KeyEvent.VK_ESCAPE);
	*/
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Facing problem in View booking section");
	}
	}
	/*
	void Can_Book() throws AWTException, InterruptedException, IOException
	{ 
	cancellation();
	book_now();
	System.out.println("Cancellation page loading properly");
	}*/
	//cancellation
	@Test(priority = 28, invocationCount = 1)
	void cancellation() throws AWTException, InterruptedException, IOException
	{
	test = report.startTest("My Booking Page - Cancellation option verification");
	My_Booking_fr book_fr = new My_Booking_fr();
	book_fr.page_launch();
	Thread.sleep(3000);
	//((JavascriptExecutor)driver).executeScript("scroll(0,400)"); 
	Thread.sleep(3000);
	try
	{
	WebElement cancel =driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[text()='Annuler une réservation']"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
	String Price = driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[@class='sc-pCOPB jZjRAo']")).getText().substring(1);
	float exp_price = Float.parseFloat(Price);
	double exp_p1 = Math.round(exp_price*100.0)/100; 
	cancel.click();
	driver.findElement(By.id("reason-1")).click();
	// Validation:
	//Title verification
	String exp_title = "Annuler votre réservation";
	String act_title = driver.findElement(By.xpath("//div[contains(text(),'Annuler votre réservation')]")).getText();
	try
	{
	Assert.assertEquals(exp_title, act_title);
	test.log(LogStatus.PASS,"Title matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Title mismatching");
	}
	// cancell info verification
	String exp_info ="Sélectionnez la chambre que vous vouliez annuler";
	String act_info = driver.findElement(By.xpath("//div[contains(text(),'Sélectionnez la chambre que vous vouliez annuler')]")).getText();
	try
	{
	Assert.assertEquals(exp_info, act_info);
	test.log(LogStatus.PASS,"Info. matching");
	} catch (Exception e)
	{
	test.log(LogStatus.FAIL,"Info. mismatching");
	}
	// Price verification
	String act_price1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[2]")).getText().substring(1); /* if ( driver.findElement(By.xpath("//*[@class='sc-pcHDm cOnSkc']")).isDisplayed())
	{ 
	String act_price2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[6]/div/div[3]/div[2]/div[2]/div[2]")).getText().substring(1); 
	float actprice1f =Float.parseFloat(act_price1);
	double p1 = Math.round(actprice1f*100.0)/100;
	float actprice2f = Float.parseFloat(act_price2);
	double p2 = Math.round(actprice2f*100.0)/100;
	double act_price3= p1 + p2;
	System.out.println(act_price3);
	try
	{
	Assert.assertEquals(exp_p1, act_price3);
	System.out.println("Price matching");
	} catch(AssertionError e)
	{
	System.out.println("Price not matching");
	}
	} else
	{ */
	try
	{
	float actprice1f =Float.parseFloat(act_price1);
	double p1 = Math.round(actprice1f*100.0)/100;
	Assert.assertEquals(exp_p1, p1);
	test.log(LogStatus.PASS,"Price matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Price not matching");
	}
	//}
	// Verification of cancallation title
	String exp_title1 = "Indiquez-nous la raison de l'annulation de votre réservation";
	String act_title1 = driver.findElement(By.xpath("//div[contains(text(),'Indiquez-nous la raison de')]")).getText();
	try
	{
	Assert.assertEquals(exp_title1, act_title1);
	test.log(LogStatus.PASS,"Cancellation title matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation title not matching");
	}
	// Verification of cancallation title
	String exp_opt1 = "Changement de dates / de destination";
	String act_opt1 = driver.findElement(By.xpath("//*[contains(text(),'Changement de dates / de destination')]")).getText();
	try
	{
	Assert.assertEquals(exp_opt1, act_opt1);
	test.log(LogStatus.PASS,"Cancellation option 1 matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation option 1 not matching");
	}
	// Verification of cancallation title
	String exp_opt2 = "Raison personnelle";
	String act_opt2 = driver.findElement(By.xpath("//*[contains(text(),'Raison personnelle')]")).getText();
	try
	{
	Assert.assertEquals(exp_opt2, act_opt2);
	test.log(LogStatus.PASS,"Cancellation option 2 matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation option 2 not matching");
	}
	// Verification of cancallation title
	String exp_opt3 = "Hébergement alternatif trouvé";
	String act_opt3 = driver.findElement(By.xpath("//*[contains(text(),'Hébergement alternatif trouvé')]")).getText();
	try
	{
	Assert.assertEquals(exp_opt3, act_opt3);
	test.log(LogStatus.PASS,"Cancellation option 3 matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation option 3 not matching");
	}
	// Verification of cancallation title
	String exp_opt4 = "Évolution du nombre de voyageurs";
	String act_opt4 = driver.findElement(By.xpath("//*[contains(text(),'Évolution du nombre de voyageurs')]")).getText();
	try
	{
	Assert.assertEquals(exp_opt4, act_opt4);
	test.log(LogStatus.PASS,"Cancellation option 4 matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL, "Cancellation option 4 not matching");
	}
	// Verification of cancallation title
	String exp_opt5 = "Autre raison";
	String act_opt5 = driver.findElement(By.xpath("//*[contains(text(),'Autre raison')]")).getText();
	try
	{
	Assert.assertEquals(exp_opt5, act_opt5);
	test.log(LogStatus.PASS, "Cancellation option 5 matching");
	} catch(AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation option 5 not matching");
	} 
	Robot esc = new Robot();
	esc.keyPress(KeyEvent.VK_ESCAPE);
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL,"Cancellation section is not working properly");
	}
	}
	@Test(priority = 29, invocationCount = 1)
	public void book_now() throws InterruptedException, IOException
	{
	test = report.startTest("My Booking Page - Booknow option verification");
	try
	{
	WebElement Bk_now = driver.findElement(By.xpath("//div[contains(text(),'N8SREUE8863')]/following::div[text()='Réservez maintenant']"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
	Thread.sleep(500);
	Bk_now.click();
	Hotel_Booking_fr bking = new Hotel_Booking_fr();
	bking.book_now_fr();
	test.log(LogStatus.PASS, "Able to proceed booking via 'Book now' from My Booking");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Facing error while proceeding booking via 'Book own' from My Booking");
	}

	}

	//--- Profile Edit ---
	@Test(priority = 30, invocationCount = 1)
	public static void Profileedit() throws InterruptedException, NullPointerException, IOException
	{
	test = report.startTest("Edit Profile verification");
	try
	{
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
	Actions builder=new Actions(driver);
	builder.moveToElement(menu).build().perform();
	WebDriverWait wait1=new WebDriverWait(driver,5);
	//wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM wEvDo']")));
	WebElement menuoption=driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
	menuoption.click();
	multiScreens.multiScreenShot(driver);
	WebElement fname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/input"));
	fname.clear();
	fname.sendKeys("Louie Madhav");

	WebElement lname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div[2]/input"));
	lname.clear();
	lname.sendKeys("Ogistan"); 
	((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	WebElement prsave =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[6]/div"));
	prsave.click();
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS,"Edit Profile: PASS");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	}
	catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Unable to Edit Profile"); 
	}
	}
	@Test(priority = 31, invocationCount = 1)
	public void Signoff() throws IOException, InterruptedException
	{
	//Signout
	test = report.startTest("Signout verification");
	try
	{
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
	menu.click();
	Thread.sleep(3000);

	WebElement menu_signout=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[3]"));
	menu_signout.click();
	multiScreens.multiScreenShot(driver);
	test.log(LogStatus.PASS,"Signout: PASS");
	} catch (AssertionError e)
	{
	test.log(LogStatus.FAIL, "Facing error while proceeding signout");
	}
	driver.quit();
	}


	
		}

