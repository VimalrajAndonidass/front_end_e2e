package French_lang.Payment_Methods.CIB_Payment;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import multiScreenShot.MultiScreenShot;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;


public class CIB_Payment_Success {
public static WebDriver driver = null;


public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\French\\Booking\\CIB\\","CIB_Payment_Success");
static ExtentTest test;
static ExtentReports report;

@BeforeTest	
	public void Launch() throws Exception {
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
	driver = new ChromeDriver();
	report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\French\\Booking\\CIB\\"+"CIB_Payment_Success.html");
	
    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}
	
@Test (priority = 0, invocationCount = 1)
public void startTest() throws IOException
{
	test = report.startTest("Scenario");
	try
	{
		test.log(LogStatus.PASS, "CIB payment Success validation(French)");
	} catch (AssertionError e)
	{
		
	}
}

@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div"));
		if (language.getText().equals("English") || language.getText().equals("Arabic"))
			{
			language.click();
			WebElement chglang = language.findElement(By.xpath("//*[(text()='Français')]"));
			chglang.click();
			}
			else
			System.out.println("French");
		}
		
@Test (priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException
	{
	// Login 		
	test = report.startTest("Login Functionality and Elastic search verification");
	try
	{
		WebElement nam_Login = driver.findElement(By.xpath("//*[contains(text(),'identifier')]"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
		uname.sendKeys("9047232893");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
		pwd.sendKeys("Test@123");
		multiScreens.multiScreenShot(driver);
		pwd.sendKeys(Keys.ENTER);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Login Failed");
	}
	
	//Hotel search
	try
		{
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				WebElement Hotel = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
				Hotel.sendKeys("Annaba");
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ENTER);
				Thread.sleep(2000);	 
				//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				WebElement btnclick1 = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
				Hotel.click();
				Hotel.sendKeys(Keys.ENTER);
				btnclick1.click();
				multiScreens.multiScreenShot(driver);
				test.log(LogStatus.PASS, "Hotel search working fine");
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

@Test(priority = 3, invocationCount = 1)
    void booking() throws IOException, InterruptedException
	{
	test = report.startTest("Hotel details page laoding and Hotel name verification");
	Thread.sleep(2000);
	WebElement Hotel_sel = driver.findElement(By.xpath("//div[contains(text(),'hôtel - 50')]"));
	Hotel_sel.click();
	String exp_hot_name = Hotel_sel.getText();
	System.out.println(exp_hot_name);
	multiScreens.multiScreenShot(driver);
	//Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();
	
	// Here we will check if child window has other child windows and will fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
            		driver.switchTo().window(ChildWindow);
        }}
		multiScreens.multiScreenShot(driver);
	//Scroll bar navigation
	//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
	
	// Hotel name verification
		//Hotel_Filter hot = new Hotel_Filter();
		Thread.sleep(2000);
		String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
		try
		{
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Hotel name mismatching");	
		}
		
}

 //  @Test(priority = 4, invocationCount = 1)

  /*  public void booking_confirmation() throws InterruptedException, IOException
{				
	Date_room_selection();	
	CIB();
}*/

   @Test(priority = 4, invocationCount = 1)
    public void Date_room_selection() throws InterruptedException, IOException
{
	test = report.startTest("Date and Room selection section verification");
try
{
	/*//From and To Date Selection
	//WebElement date = driver.findElement(By.xpath("//*[text()='Description"));
	//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
	Thread.sleep(3000);
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
	Thread.sleep(500);

	WebElement month = driver.findElement(By.xpath("//select[contains(@class,'DateRangePicker__MonthHeaderSelect')]"));
	Select month_pick=new Select(month);
	month_pick.selectByVisibleText("October");
	month_pick.getFirstSelectedOption();
	
	Thread.sleep(500);
	WebElement start_date = driver.findElement(By.xpath("//*[@text()='1']"));
	start_date.click();
	
	Thread.sleep(500);
	WebElement end_date = driver.findElement(By.xpath("//*[@text()='10']"));
	end_date.click();
		*/
	 //Currency selection
		WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
		Thread.sleep(500);
		cur.click();
		WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
		cur_sel.click();

	//Room selection
	Thread.sleep(3000);
		WebElement scroll =driver.findElement(By.xpath("//*[text()='Types de chambres']"));
		Thread.sleep(2000); 
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll);
		WebElement element =driver.findElement(By.xpath("//*[contains(@class,'sc-oTpcS hKhvoq') or contains(@class,'sc-pJjas fvjHub') or contains(@class,'sc-qQKPx ksBkya')]"));
		Select room_sel=new Select(element);
		room_sel.selectByVisibleText("1");
		room_sel.getFirstSelectedOption();
		
		/*WebElement element1 =driver.findElement(By.xpath("//select[contains(@class,'sc-oTpcS hKhvoq')][2]"));
		Select room_sel1=new Select(element1);
		room_sel1.selectByVisibleText("2");
		room_sel1.getFirstSelectedOption(); */
		
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		test.log(LogStatus.PASS,"Rooms selected successfully");

}
catch (Exception e)
{
	test.log(LogStatus.FAIL,"Unable to select Rooms");
}

try
{
	//Book now button verification
		((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		Thread.sleep(2000);
		//WebElement book_scroll =driver.findElement(By.xpath("//select[contains(@class,'sc-pTUKB hbNNSd')]"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",book_scroll);
		Thread.sleep(2000); 
		WebElement book_now = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
		Thread.sleep(2000); 	
		book_now.click();
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS,"Book Now button available and able to proceed booking");
}
catch (Exception e)
{
	test.log(LogStatus.FAIL,"Book Now button is not available or unable to proceed Booking");
}

}	

   @Test(priority = 5, invocationCount = 1)

    public void CIB() throws IOException, InterruptedException
	{
	   test = report.startTest("Payment Method verification");
	   try
	   {
			
		if (driver.findElement(By.id("cib")).isEnabled())
		{
			WebElement radio_cib=driver.findElement(By.id("cib"));
			radio_cib.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);

//Select Terms and Conditions - check box
	/*WebElement cib_scroll=driver.findElement(By.xpath("//div[contains(text(),'Remaining')]"));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cib_scroll);		
	WebElement checkbox_terms=driver.findElement(By.id("terms"));
	checkbox_terms.click(); */
	multiScreens.multiScreenShot(driver);
		} else
		{
		//Currency selection
			WebElement cur1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur1);
			Thread.sleep(500);
			cur1.click();
			WebElement cur_sel1 = cur1.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
			cur_sel1.click();
	
		//Room selection
			Thread.sleep(3000);
				WebElement scroll =driver.findElement(By.xpath("//*[text()='Types de chambres']"));
				Thread.sleep(2000); 
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll);
				WebElement element =driver.findElement(By.xpath("//select[contains(@class,'sc-oTpcS hKhvoq')]"));
				Select room_sel=new Select(element);
				room_sel.selectByVisibleText("1");
				room_sel.getFirstSelectedOption();

	//Book now button verification
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
				Thread.sleep(2000);
				//WebElement book_scroll =driver.findElement(By.xpath("//select[contains(@class,'sc-pTUKB hbNNSd')]"));
				//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",book_scroll);
				Thread.sleep(2000); 
				WebElement book_now = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
				Thread.sleep(2000); 	
				book_now.click();
		
		//CIB selection and proceed
									
			WebElement cib_scroll=driver.findElement(By.xpath("//div[contains(text(),'restants')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cib_scroll);
			Thread.sleep(500);
			WebElement radio_cib=driver.findElement(By.id("cib"));
			radio_cib.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);

		//Select Terms and Conditions - check box
			WebElement checkbox_terms=driver.findElement(By.id("terms"));
			checkbox_terms.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS,"CIB Card selected");
		}
} catch(AssertionError e)
{
	test.log(LogStatus.FAIL,"Unable to select CIB card");
}

//Selecting 'Success' button
	try
	{
		WebElement book_now_scroll =driver.findElement(By.xpath("//div[contains(text(),'72h avant')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",book_now_scroll);
		Thread.sleep(3000);
		WebElement Proceed_element =driver.findElement(By.xpath("//*[contains(text(),'Procéder au paiement')]"));
		Proceed_element.click(); 
		
		WebElement CC_scroll =driver.findElement(By.xpath("//div[contains(text(),'La description')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",CC_scroll);
		multiScreens.multiScreenShot(driver);
		
		Thread.sleep(3000);
		
	//Alert validation
		driver.switchTo().alert().accept();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/button[1]")).click();		
		multiScreens.multiScreenShot(driver);

		test.log(LogStatus.PASS,"Payment done successfully via CIB Payment");
		multiScreens.multiScreenShot(driver);

	}
	catch(AssertionError e)
	{
		test.log(LogStatus.FAIL,"Payment failed");
	}	
	
	// Voucher details verification

	Thread.sleep(8000);
	WebElement Booking_ID_web = driver.findElement(By.xpath("//div[contains(@class,'sc-pAytO fbvEUZ') or contains(@class,'sc-ptdGt ePHOSB') or contains(@class,'sc-oVeeF NgKHZ')]"));
	String Booking_ID = Booking_ID_web.getText();
	try
	{
		multiScreens.multiScreenShot(driver);
		String Trans_time_date = driver.findElement(By.xpath("//div[contains(text(),'CET')]")).getText();
		WebElement Hot_name_scroll =driver.findElement(By.xpath("//div[contains(@class,'sc-pYQRR coZWsy') or contains(@class,'sc-ptUam QNhqv')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",Hot_name_scroll);
		multiScreens.multiScreenShot(driver);
		String Hotel_Name_voucher = Hot_name_scroll.getText();
		String Check_in = driver.findElement(By.xpath("//div[contains(@class,'sc-pKJXg gnvEA') or contains(@class,'sc-pdNVt bBXhFy')]")).getText();
		String Check_out = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[1]/div[2]/div[2]")).getText();
		String Guest_count = driver.findElement(By.xpath("//div[contains(@class,'sc-plXmT cxLvr') or contains(@class,'sc-pALlw ldfshs')]")).getText();
		String Room_count = driver.findElement(By.xpath("//div[contains(@class,'sc-puHdH XrpUX') or contains(@class,'sc-pJvck jQCsMK')]")).getText();
		String Guest_Name = driver.findElement(By.xpath("//div[contains(@class,'sc-pTSRm cJMMUy') or contains(@class,'sc-pjVoC hjQhoq')]")).getText();
		String Room_Type = driver.findElement(By.xpath("//div[contains(@class,'sc-pjVoC hjQhoq')]")).getText();
		String Booking_amt = driver.findElement(By.xpath("//div[contains(@class,'sc-pRQdf eaSvPy')]")).getText();
		String Pay_Mode = driver.findElement(By.xpath("//*[contains(@class,'sc-pRfvo llXBTr')]")).getText();
		String Total_amt = driver.findElement(By.xpath("//*[contains(text(),'Montant total')]")).getText();
		String Net_amt = driver.findElement(By.xpath("//*[contains(text(),'Montant net payé')]")).getText();
		String Bal_amt = driver.findElement(By.xpath("//*[contains(text(),'Montant du solde à payer à l')]")).getText();
		String Order_ID = driver.findElement(By.xpath("//*[contains(text(),'ID de commande')]")).getText();
		String Cancellation_Type = driver.findElement(By.xpath("//*[contains(@class,'sc-ptEpz dIebWX')]")).getText();
		multiScreens.multiScreenShot(driver);

		test.log(LogStatus.PASS,"Payment done successfully via CIB Payment and following are the booking details" + "<br/>  Booking ID: " +Booking_ID + "<br/> Transaction Time and date: "+Trans_time_date + "<br/>  Hotel Name: "+ Hotel_Name_voucher + "<br/>  Check In(Time and Date): " + Check_in + "<br/>  Check Out(Time and Date): " + Check_out + "<br/>  Guest Count: " +Guest_count + "<br/>  Room Count: " +Room_count + "<br/>  Guest name: "+Guest_Name +"<br/>  Room Type: " +Room_Type + "<br/>  Booking Amount: " +Booking_amt + "<br/>" +Total_amt + "<br/>"+Net_amt +"<br/>" +Bal_amt +"<br/>  Payment Type: "+Pay_Mode +"<br/>"+Order_ID +"<br/>"+Cancellation_Type);
	}
	catch (AssertionError e)
	{
		test.log(LogStatus.FAIL,"Payment done successfully via CIB Payment but voucher not loaded properly and details are mismatching");		
	}

	//Download Voucher
	try {

		WebElement download_Vou =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
		download_Vou.click();
		multiScreens.multiScreenShot(driver);		
		test.log(LogStatus.PASS, "Download voucher working fine");
		
	} catch(Exception e)
	{	
		test.log(LogStatus.FAIL, "Unable to Download voucher");
	}
	
	//Email Voucher
	try {
		Thread.sleep(5000); 
		WebElement send_email=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
		send_email.click();
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "Email voucher working fine");				
	} catch(Exception e)
	{
		test.log(LogStatus.FAIL, "Unable to Email voucher");		
	}
	
	//Print Voucher
	try {
		WebElement print_Vou=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
		print_Vou.click(); 		
		Thread.sleep(5000);
		Robot print_window = new Robot();
		print_window.keyPress(KeyEvent.VK_ENTER);
		print_window.keyRelease(KeyEvent.VK_ENTER);
		test.log(LogStatus.PASS, "Print voucher working fine");	
	} catch(Exception e)
	{
		test.log(LogStatus.FAIL, "Unable to Print voucher");				
	}
	
	//My Booking verification
	try
	{
	((JavascriptExecutor)driver).executeScript("scroll(0,10)");
	Thread.sleep(3000);
	WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
	menu.click();
	   
	WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
	menuoption.click();
	multiScreens.multiScreenShot(driver);
	
	Thread.sleep(3000);
	WebElement scroll =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div"));
	Thread.sleep(2000); 
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll);
	String[] Bookingid_Time = scroll.getText().split(" ");
	String Bookingid = Bookingid_Time[0];
	Assert.assertEquals(Booking_ID, Bookingid);
			System.out.println(Bookingid);
			test.log(LogStatus.PASS, "Hotel booking done successfully and verified "+Bookingid);	

	}
	catch (Exception e)
	{
		test.log(LogStatus.FAIL, "Hotel booking is not done successfully");	
		
	}
	}
 

@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
//driver.quit();
}
}

