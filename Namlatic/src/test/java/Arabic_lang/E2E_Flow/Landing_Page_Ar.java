package Arabic_lang.E2E_Flow;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Arabic_lang.Java_Files.Main;
import English_Lang.Java_Files.Hotel_Filter;
import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Landing_Page_Ar extends Main {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\",
			"Arabic");
	static ExtentTest test;
	static ExtentReports report;
	/*
	 * @Test (invocationCount = 2) public void login_Ar() throws Exception {
	 * 
	 * launch_Ar(); header_Ar(); home_page_text_Ar(); carousel_Ar();
	 * why_choose_us_Ar(); help_Ar(); covid_Ar(); footer_Ar(); screenshots_Ar();
	 * lgn_Ar(); System.out.println("Login: PASS"); }
	 */

	// --- Home page ---
	@BeforeTest
	public void launch_Ar() throws IOException {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		// Report configuration
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\" + "_FE_Arabic.html");

		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic - Arabic");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException {
		test = report.startTest("URL Verification");

		System.out.println("Arabic");
		System.out.println("***********");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}

	@Test(priority = 2, invocationCount = 1)
	public void Title_Verification() throws IOException {
		test = report.startTest("Title Verification");
		driver.getTitle();
		String ExpectedTitle = "Namlatic";
		try {
			Assert.assertEquals(ExpectedTitle, driver.getTitle());
			test.log(LogStatus.PASS, "Assert Title Result - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatches with" + e + "Fail");
		}

		// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]"));
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='عربي')]"));
		chglang.click();

	}

	@Test(priority = 3, invocationCount = 1)
	public void header_Ar() {
		test = report.startTest("Header Verification");
		// Header Verification
		// Logo verification
		WebElement Header_Logo = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/a/img"));
		try {
			Header_Logo.isDisplayed();
			test.log(LogStatus.PASS, "Header Namlatic logo is available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Header Namlatic logo is not available");
		}

		// Login
		// Ensure the availability of Login button
		try {
			driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]")).isEnabled();
			test.log(LogStatus.PASS, "Login button available for selection");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Login button is not available to select");
		}
		// Enroll hotel
		// Ensure the availability of Enroll hotel
		try {
			driver.findElement(By.xpath("//*[(text()='تسجيل الفندق')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='تسجيل الفندق')]")).isEnabled();
			test.log(LogStatus.PASS, "Enroll Hotel button available for selection");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Enroll Hotel button is not available to select");
		}
		// Language Verification
		// Ensure the availability of Language options
		try {
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isEnabled();
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isSelected();
			test.log(LogStatus.PASS, "Language Arabic is selected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Someother language is selected other than Arabic");
		}
		// Ensure the availability of Currency options
		try {
			WebElement currency = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]"));
			for (int i = 0; i < 2; i++) {
				System.out.println(currency.getText());
				// Assert.assertEquals(i, currency.getText());
			}
			// driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled();
			// driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
			test.log(LogStatus.PASS, "Language English is selected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Someother language is selected other than English");
		}

		// Ensure the availability of Enroll hotel
		try {
			driver.findElement(By.xpath("//*[conatins(placeholder(),'اخترأين تريد أن تقيم ؟')]")).isDisplayed();
			test.log(LogStatus.PASS, "Elastic search option is available and allowed to search");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Elastic search option is not available");
		}
	}

	@Test(priority = 4, invocationCount = 1)
	public void home_page_text_Ar() throws IOException, InterruptedException {
		test = report.startTest("Home Page Verification");
		// Home page text validation
		try {
			String Exptext1 = "المنصة الجزائرية \n" + "  الأولى لحجز الفنادق";
			String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[1]"))
					.getText());
			System.out.println(Acttext1);
			Assert.assertEquals(Exptext1, Acttext1);

			String Exptext2 = "ابق معنا ، و اشعر كأنك في بيتك";
			String Acttext2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[2]"))
					.getText();
			Assert.assertEquals(Exptext2, Acttext2);

			String Exptext3 = "عش تجربة لا تُنسى في فندق أحلامك مع Namlatic.";
			String Acttext3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[3]"))
					.getText();
			Assert.assertEquals(Exptext3, Acttext3);

			test.log(LogStatus.PASS, "Home Page text verified, Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Home page text mismatches with expected");
		}

		// Ensure the availability of search button
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"))
					.isDisplayed();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"))
					.isEnabled();
			test.log(LogStatus.PASS, "Search button is available and allowed to select");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Search button is not available or not able to selectable");
		}

	}

	@Test(priority = 5, invocationCount = 1)
	public void carousel_Ar() throws IOException, InterruptedException {
		test = report.startTest("Carousel section Verification");
		// Ensure the title text of carousel image
		try {
			WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[1]"));
			String Expected_title = "لديك وجهة ، سنوفر لك المكان المثالي للإقامة!";
			String Actual_title = title.getText();
			System.out.println(title.getText());
			Assert.assertEquals(Expected_title, Actual_title);
			test.log(LogStatus.PASS, "Carousel title is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Carousel title mismatching with expected");
		}

		// Carousel image1 title verification
		WebElement img1_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[1]/div/div"));
		String Act_img1_title = img1_title.getText();
		String Exp_img1_title = "الجزائر";
		try {
			Assert.assertEquals(Exp_img1_title, Act_img1_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img1_title);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img1_title
					+ "Expected Title:" + Exp_img1_title);
		}

		// Carousel image1 text verification
		WebElement Cr_img1 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[2]"));
		String Act_img1_text = Cr_img1.getText();
		String Exp_img1_text = "إكتشفْ الجزائر العاصمة، \"البيضاء\"، مدينة أحلام البحر الأبيض المتوسط.";
		try {
			Assert.assertEquals(Exp_img1_text, Act_img1_text);
			test.log(LogStatus.PASS, "Carousel image 1 'Alger' text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img1_text + "Actual:" + Act_img1_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image2 title verification
		WebElement img2_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[1]/div/div"));
		String Act_img2_title = img2_title.getText();
		String Exp_img2_title = "عنابة";
		try {
			Assert.assertEquals(Exp_img2_title, Act_img2_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img2_title);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected: " + "Acual Title:" + Act_img2_title
					+ "Expected Title:" + Exp_img2_title);
		}

		// Carousel image2 text verification
		WebElement Cr_img2 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[2]"));
		String Act_img2_text = Cr_img2.getText();
		String Exp_img2_text = "كل جمال الجزائر ، بين الكورنيش, الواجهة البحرية ووسط المدينة التاريخي ، عنابة وجهة كاملة يجب أن ترضيك";
		try {
			Assert.assertEquals(Exp_img2_text, Act_img2_text);
			test.log(LogStatus.PASS, "Carousel image 2 'Annaba' text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img2_text + "Actual:" + Act_img2_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image3 title verification
		WebElement img3_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[1]/div/div"));
		String Act_img3_title = img3_title.getText();
		String Exp_img3_title = "بجاية";
		try {
			Assert.assertEquals(Exp_img3_title, Act_img3_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img3_title);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img3_title
					+ "Expected Title:" + Exp_img3_title);
		}

		// Carousel image3 text verification
		WebElement Cr_img3 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[2]"));
		String Act_img3_text = Cr_img3.getText();
		String Exp_img3_text = "مدينة الجزائر الساحلية ، أكبر مدينة في منطقة القبائل ، مدينة رائعة ، مشرقة كما تريد ، ويحدها البحر.";
		try {
			Assert.assertEquals(Exp_img3_text, Act_img3_text);
			test.log(LogStatus.PASS, "Carousel image 3 'Bejaia' text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img3_text + "Actual:" + Act_img3_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image4 title verification
		WebElement img4_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[1]/div/div"));
		String Act_img4_title = img4_title.getText();
		String Exp_img4_title = "قسنطينة";
		try {
			Assert.assertEquals(Exp_img4_title, Act_img4_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img4_title);
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img4_title
					+ "Expected Title:" + Exp_img4_title);
		}

		// Carousel image4 text verification
		WebElement Cr_img4 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[2]"));
		String Act_img4_text = Cr_img4.getText();
		String Exp_img4_text = "مدينة الجزائر الساحرة ، الملقبة بـ \"مدينة الجسور المعلقة\" ، ممر أساسي في الجزائر ، ربما بسبب جمالها الذي لا مثيل له!";
		try {
			Assert.assertEquals(Exp_img4_text, Act_img4_text);
			test.log(LogStatus.PASS, "Carousel image 4 'Constantine' text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img4_text + "Actual:" + Act_img4_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image5 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img5_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[1]/div/div"));
		System.out.println(img5_title.getText());
		String Act_img5_title = img5_title.getText();
		String Exp_img5_title = "تيزي وزو";
		try {
			Assert.assertEquals(Exp_img5_title, Act_img5_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img5_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img5_title
					+ "Expected Title:" + Exp_img5_title);
		}

		// Carousel image5 text verification
		WebElement Cr_img5 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[2]"));
		String Act_img5_text = Cr_img5.getText();
		String Exp_img5_text = "مدينة الجزائر الجميلة في وسط الطبيعة ، تعني \"طوق مكنسة\". بين البحر والجبل. مدينة نموذجية ، سرية ، لكنها خارجة عن المألوف!";

		try {
			Assert.assertEquals(Exp_img5_text, Act_img5_text);
			test.log(LogStatus.PASS, "Carousel image 5 'Tizi ouzou' text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img5_text + "Actual:" + Act_img5_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image6 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img6_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[1]/div/div"));
		String Act_img6_title = img6_title.getText();
		String Exp_img6_title = "تيبازة";
		try {
			Assert.assertEquals(Exp_img6_title, Act_img6_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img6_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img6_title
					+ "Expected Title:" + Exp_img6_title);
		}

		// Carousel image6 text verification
		WebElement Cr_img6 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[2]"));
		String Act_img6_text = Cr_img6.getText();
		String Exp_img6_text = "كنز مدينة جزائرية ، وجهة مميزة للسياح ، بين البحر والجبال ، نجد أنفسنا حول مناظر طبيعية رائعة ترضي العيون!";

		try {
			Assert.assertEquals(Exp_img6_text, Act_img6_text);
			test.log(LogStatus.PASS,
					"Carousel image 6" + "'" + Exp_img6_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img6_text + "Actual:" + Act_img6_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image7 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img7_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[1]/div/div"));
		String Act_img7_title = img7_title.getText();
		String Exp_img7_title = "عـيـن تـمـوشـنـت";
		try {
			Assert.assertEquals(Exp_img7_title, Act_img7_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img7_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img7_title
					+ "Expected Title:" + Exp_img7_title);
		}

		// Carousel image7 text verification
		WebElement Cr_img7 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[2]"));
		String Act_img7_text = Cr_img7.getText();
		String Exp_img7_text = "اكتشف لؤلؤة الساحل الغربي ، واستمتع بزيارة العديد من المعالم التاريخية والشواطئ التي تزين الساحل الطويل";
		try {
			Assert.assertEquals(Exp_img7_text, Act_img7_text);
			test.log(LogStatus.PASS,
					"Carousel image 7" + "'" + Exp_img7_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img7_text + "Actual:" + Act_img7_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image8 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img8_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[1]/div/div"));
		String Act_img8_title = img8_title.getText();
		String Exp_img8_title = "سكيكدة";
		try {
			Assert.assertEquals(Exp_img8_title, Act_img8_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img8_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img8_title
					+ "Expected Title:" + Exp_img8_title);
		}

		// Carousel image8 text verification
		WebElement Cr_img8 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[2]"));
		String Act_img8_text = Cr_img8.getText();
		String Exp_img8_text = "مدينة الفراولة ، المعروفة سابقًا باسم فيليبفيل، مدينة ساحلية في شرق الجزائر ، تتميز بواجهة بحرية واسعة ووسط مدينة نابض بالحياة";
		System.out.println(Act_img8_text);

		try {
			Assert.assertEquals(Exp_img8_text, Act_img8_text);
			test.log(LogStatus.PASS,
					"Carousel image 8" + "'" + Exp_img8_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img8_text + "Actual:" + Act_img8_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image9 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img9_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[1]/div/div"));
		System.out.println(img9_title.getText());
		String Act_img9_title = img9_title.getText();
		String Exp_img9_title = "وهران";
		try {
			Assert.assertEquals(Exp_img9_title, Act_img9_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img9_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img9_title
					+ "Expected Title:" + Exp_img9_title);
		}

		// Carousel image9 text verification
		WebElement Cr_img9 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[2]"));
		String Act_img9_text = Cr_img9.getText();
		String Exp_img9_text = "وهران أو البهية، من أجمل المدن في الجزائر. زيارة آثارها اللامعة ونظرتها المذهلة على البحر الأبيض المتوسط";

		try {
			Assert.assertEquals(Exp_img9_text, Act_img9_text);
			test.log(LogStatus.PASS,
					"Carousel image 9" + "'" + Exp_img9_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img9_text + "Actual:" + Act_img9_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image10 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img10_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[1]/div/div"));
		System.out.println(img10_title.getText());
		String Act_img10_title = img10_title.getText();
		String Exp_img10_title = "مستغانم";
		try {
			Assert.assertEquals(Exp_img10_title, Act_img10_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img10_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img10_title
					+ "Expected Title:" + Exp_img10_title);
		}

		// Carousel image10 text verification
		WebElement Cr_img10 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[2]"));
		String Act_img10_text = Cr_img10.getText();
		String Exp_img10_text = "واحدة من أجمل المدن الساحلية في الجزائر ، المكان المثالي لعزل نفسك عن العالم والاستمتاع بالبحر ومتعة المدينة البسيطة.";

		try {
			Assert.assertEquals(Exp_img10_text, Act_img10_text);
			test.log(LogStatus.PASS,
					"Carousel image 10" + "'" + Exp_img10_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img10_text + "Actual:" + Act_img10_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image11 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img11_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[1]/div/div"));
		System.out.println(img11_title.getText());
		String Act_img11_title = img11_title.getText();
		String Exp_img11_title = "سطيف";
		try {
			Assert.assertEquals(Exp_img11_title, Act_img11_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img11_title);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title
					+ "Expected Title:" + Exp_img11_title);
		}

		// Carousel image11 text verification
		WebElement Cr_img11 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[2]"));
		String Act_img11_text = Cr_img11.getText();
		String Exp_img11_text = "سطيف ، مدينة قديمة ورومانية تقع بين سلسلة أطلس تيليان والصحراء. ومن الواضح أن موقع \"جميلا\" ، جوهرة من التراث الثقافي ، سوف يكشف عن كنوز غير متوقعة";

		try {
			Assert.assertEquals(Exp_img11_text, Act_img11_text);
			test.log(LogStatus.PASS,
					"Carousel image 11" + "'" + Exp_img11_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching, Expected:" + Exp_img11_text + "Actual:" + Act_img11_text);
		}
		multiScreens.multiScreenShot(driver);
	}

	@Test(priority = 6, invocationCount = 1)
	public void why_choose_us_Ar() {
		test = report.startTest("Why Choose us section verification");

		// Why choose us!
		// Title Verification
		String act_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[1]"))
				.getText();
		String exp_title = "لماذا تختارنا ؟";
		try {
			Assert.assertEquals(exp_title, act_title);
			test.log(LogStatus.PASS, "Title matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not Matching");
		}

		// Easy Booking
		String act_sec1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[1]"))
				.getText();
		String exp_sec1 = "سهولة الحجز";

		try {
			Assert.assertEquals(exp_sec1, act_sec1);
			test.log(LogStatus.PASS, "Easy booking Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Easy booking Not Matching");
		}
		String act_bdy1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[2]"))
				.getText();
		String exp_bdy1 = "يجب أن تتضمن عملية الحجز الحد الأدنى من الخطوات.";

		try {
			Assert.assertEquals(exp_bdy1, act_bdy1);
			test.log(LogStatus.PASS, "Easy booking content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Easy booking content - Not Matching");
		}

		// Friendly Interfaces
		String act_sec2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[1]"))
				.getText();
		String exp_sec2 = "واجهات ودية";

		try {
			Assert.assertEquals(exp_sec2, act_sec2);
			test.log(LogStatus.PASS, "Friendly interface title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Friendly interface title - Not Matching");
		}
		String act_bdy2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[2]"))
				.getText();
		String exp_bdy2 = "محرك حجز الفنادق سهل الاستخدام.";

		try {
			Assert.assertEquals(exp_bdy2, act_bdy2);
			test.log(LogStatus.PASS, "Friendly interface content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Friendly interface - Not Matching");
		}

		// Wide Payment options
		String act_sec3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[1]"))
				.getText();
		String exp_sec3 = "خيارات دفع واسعة";

		try {
			Assert.assertEquals(exp_sec3, act_sec3);
			test.log(LogStatus.PASS, "Wide Payment title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, " Wide Payment title - Not Matching");
		}
		String act_bdy3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[2]"))
				.getText();
		String exp_bdy3 = "الدفع المحلي و العالمي مقبولين";

		try {
			Assert.assertEquals(exp_bdy3, act_bdy3);
			test.log(LogStatus.PASS, "Wide Payment content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Wide Payment content - Not Matching");
		}

		// Customized cancellation
		String act_sec4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[1]"))
				.getText();
		String exp_sec4 = "الإلغاء المخصص";

		try {
			Assert.assertEquals(exp_sec4, act_sec4);
			test.log(LogStatus.PASS, "Customized Cancellation - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Customized Cancellation - not Matching");
		}
		String act_bdy4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[2]"))
				.getText();
		String exp_bdy4 = "يمكنك الإلغاء مجانًا وفقًا للسياسات المعمول بيها";

		try {
			Assert.assertEquals(exp_bdy4, act_bdy4);
			test.log(LogStatus.PASS, "Customized Cancellation content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Customized Cancellation content - not Matching");
		}

		// Free cancellation
		String act_sec5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[1]"))
				.getText();
		String exp_sec5 = "أول منصة جزائرية لحجز الفنادق مع خيار الدفع المحلي والإلغاء المجاني";

		try {
			Assert.assertEquals(exp_sec5, act_sec5);
			test.log(LogStatus.PASS, "Free Cancellation Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Free Cancellation Title -  Not Matching");
		}
		String act_bdy5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[2]"))
				.getText();
		String exp_bdy5 = "* تطبق الشروط والأحكام على سياسات الإلغاء";

		try {
			Assert.assertEquals(exp_bdy5, act_bdy5);
			test.log(LogStatus.PASS, "Free Cancellation content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Free Cancellation content - Not Matching");
		}
	}

	@Test(priority = 7, invocationCount = 1)
	public void help_Ar() {
		test = report.startTest("Help section verification");

		// Help Section
		String act_help_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[1]"))
				.getText();
		String exp_help_title = "نحن على استعداد لمساعدتك وتسهيل عملية الحجز الخاصة بك.\n"
				+ " ما عليك سوى اتباع خطواتنا والحصول على كل شيء بسهولة.";

		try {
			Assert.assertEquals(exp_help_title, act_help_title);
			test.log(LogStatus.PASS, "Help section Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section Title - not Matching");
		}

		// Location Section
		String act_help1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[2]"))
				.getText();
		System.out.println(act_help1);
		String exp_help1 = "اختر الموقع الذي تريد البقاء فيه";

		try {
			Assert.assertEquals(exp_help1, act_help1);
			test.log(LogStatus.PASS, "Help section1 Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section1 Title - not Matching");
		}
		String exp_help1_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[3]")).getText();
		String act_help1_con = "احجز اختيارك";

		try {
			Assert.assertEquals(exp_help1_con, act_help1_con);
			test.log(LogStatus.PASS, "Help section1 content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section1 content - not Matching");
		}

		// Choice
		String exp_help2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[2]"))
				.getText();
		String act_help2 = "احجز اختيارك";

		try {
			Assert.assertEquals(exp_help2, act_help2);
			test.log(LogStatus.PASS, "Help section2 title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section2 title - not Matching");
		}
		String exp_help2_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[3]")).getText();
		String act_help2_con = "من خلال اجراء صفقة فقط يمكنك الحصول على جميع التسهيلات";
		try {
			Assert.assertEquals(exp_help2_con, act_help2_con);
			test.log(LogStatus.PASS, "Help section2 content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section2 content - not Matching");
		}

		// Checkin
		String exp_help3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[2]"))
				.getText();
		String act_help3 = "تحقق في";

		try {
			Assert.assertEquals(exp_help3, act_help3);
			test.log(LogStatus.PASS, "Help section3 Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section3 Title - not Matching");
		}
		String exp_help3_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[3]")).getText();
		String act_help3_con = "تعال إلى الفندق مع بطاقة الهوية وإثبات الدفع واستمتع بإقامتك.";
		try {
			Assert.assertEquals(exp_help3_con, act_help3_con);
			test.log(LogStatus.PASS, "Help section3 Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section3 Title - not Matching");
		}
	}

	@Test(priority = 8, invocationCount = 1)

	public void covid_Ar() {
		test = report.startTest("Covid Measure section verification");
		// Covid Measures
		// Title verififcation
		String exp_COVID_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[1]")).getText();
		String act_COVID_title = "تدابير COVID-19 على فنادق Namlatic";

		try {
			Assert.assertEquals(exp_COVID_title, act_COVID_title);
			test.log(LogStatus.PASS, "COVID Measure Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure Title - Not Matching");
		}

		// Sub Title
		String exp_COVID_subtitle = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[1]")).getText();
		String act_COVID_subtitle = "فنادق Namlatic تحت التدابير الوقائية لـ COVID19";
		try {
			Assert.assertEquals(exp_COVID_subtitle, act_COVID_subtitle);
			test.log(LogStatus.PASS, "COVID Measure Sub_Title - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure Sub_Title - Not Matching");
		}

		// Measure 1
		String exp_COVID_mes1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[2]")).getText();
		String act_COVID_mes1 = "100٪ آمن ومعقم";
		try {
			Assert.assertEquals(exp_COVID_mes1, act_COVID_mes1);
			test.log(LogStatus.PASS, "COVID Measure_1 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_1 -Not Matching");
		}

		// Measure 2
		String exp_COVID_mes2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[2]/wrapper/div[2]")).getText();
		String act_COVID_mes2 = "قناع ، لاتكس ، قفازات لجميع العاملين";
		try {
			Assert.assertEquals(exp_COVID_mes2, act_COVID_mes2);
			test.log(LogStatus.PASS, "COVID Measure_2 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_2 -Not Matching");
		}

		// Measure 3
		String exp_COVID_mes3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[3]/wrapper/div[2]")).getText();
		String act_COVID_mes3 = "قناع ، لاتكس ، قفازات لجميع العاملين";
		try {
			Assert.assertEquals(exp_COVID_mes3, act_COVID_mes3);
			test.log(LogStatus.PASS, "COVID Measure_3 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_3 - Not Matching");
		}

		// Measure 4
		String exp_COVID_mes4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[4]/wrapper/div[2]")).getText();
		String act_COVID_mes4 = "تطهير كل ساعتين";
		try {
			Assert.assertEquals(exp_COVID_mes4, act_COVID_mes4);
			test.log(LogStatus.PASS, "COVID Measure_4 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_4 -Not Matching");
		}

		// Measure 5
		String exp_COVID_mes5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[5]/wrapper/div[2]")).getText();
		String act_COVID_mes5 = "التحقق من درجة الحرارة إلزامي لجميع الضيوف وكذلك الموظفين";
		System.out.println(exp_COVID_mes5);
		try {
			Assert.assertEquals(exp_COVID_mes5, act_COVID_mes5);
			test.log(LogStatus.PASS, "COVID Measure_5 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_5 - Not Matching");
		}

		// Measure 6
		String exp_COVID_mes6 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[6]/wrapper/div[2]")).getText();
		String act_COVID_mes6 = "الكتان / الأواني مغسولة في 70 درجة مئوية + ماء";
		try {
			Assert.assertEquals(exp_COVID_mes6, act_COVID_mes6);
			test.log(LogStatus.PASS, "COVID Measure_6 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_6 -Not Matching");
		}

		// Measure 7
		String exp_COVID_mes7 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[7]/wrapper/div[2]")).getText();
		String act_COVID_mes7 = "كيماويات تنظيف المستشفيات مستخدمة في العقار";
		try {
			Assert.assertEquals(exp_COVID_mes7, act_COVID_mes7);
			test.log(LogStatus.PASS, "COVID Measure_7 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_7 -Not Matching");
		}

		// Measure 8
		String exp_COVID_mes8 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[8]/wrapper/div[2]")).getText();
		String act_COVID_mes8 = "التباعد الاجتماعي في الاستقبال والمصعد والمناطق العامة والحدائق";
		try {
			Assert.assertEquals(exp_COVID_mes8, act_COVID_mes8);
			test.log(LogStatus.PASS, "COVID Measure_8 - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_8 - not Matching");
		}
	}

	@Test(priority = 9, invocationCount = 1)
	public void footer_Ar() throws InterruptedException, IOException {
		test = report.startTest("Footer Verification");
		// Footer Verification
		// Namlatic logo verification
		WebElement footer1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/a/img"));
		try {
			footer1.isDisplayed();
			test.log(LogStatus.PASS, "Namlatic logo available in Footer");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Logo missing in Footer");
		}

		// About us
		String exp_footer2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[1]"))
				.getText();
		String act_footer2 = "معلومات عنا";
		try {
			Assert.assertEquals(exp_footer2, act_footer2);
			test.log(LogStatus.PASS, "About us is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "About us is not available in footer");

		}

		// Terms & conditions
		String exp_footer3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[2]"))
				.getText();
		String act_footer3 = "الأحكام والشروط";
		try {
			Assert.assertEquals(exp_footer3, act_footer3);
			test.log(LogStatus.PASS, "Terms and Conditions is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Terms and Conditions is not available in footer");

		}

		// Privacy Policy
		String exp_footer4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[3]"))
				.getText();
		String act_footer4 = "سياسة الخصوصية";
		try {
			Assert.assertEquals(exp_footer4, act_footer4);
			test.log(LogStatus.PASS, "Privacy Policy is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Privacy Policy is not available in footer");

		}

		// Namlatic help center
		String exp_footer5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[4]"))
				.getText();
		String act_footer5 = "مركز مساعدة Namlatic";
		try {
			Assert.assertEquals(exp_footer5, act_footer5);
			test.log(LogStatus.PASS, "Namlatic Help Center is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Help Center is not available in footer");
		}

		// Follow Us
		String exp_footer6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]")).getText();
		String act_footer6 = "تابعنا";
		try {
			Assert.assertEquals(exp_footer6, act_footer6);
			test.log(LogStatus.PASS, "Follow us is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Follow us is not available in footer");
		}

		// Instagram launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement insta = driver.findElement(By.xpath("//*[name()='svg'][3]"));
		insta.click();

		// Instagram URL verification
		// Get handles of the windows
		String MainWindow = driver.getWindowHandle();
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_insta_title = "https://www.instagram.com/namlatic/";
		String exp_insta_title2 = "https://www.instagram.com/accounts/login/";
		if (driver.getTitle().equals(exp_insta_title)) {

			test.log(LogStatus.PASS, "Navigating to Namlatic LinkedIN page");
		} else if (driver.getTitle().equals(exp_insta_title2)) {

			test.log(LogStatus.PASS, "Navigating to Namlatic Instagram page");
		} else {

			test.log(LogStatus.FAIL, "Namlatic Instagram page is not loaded or loading to improper URL");
		}

		driver.switchTo().window(MainWindow);
		/*
		 * try { Assert.assertEquals(exp_insta_title, driver.getCurrentUrl());
		 * System.out.println("Navigating to Namlatic Instagram page"); }
		 * catch(AssertionError e) { System.out.
		 * println("Namlatic Instagram page is not loaded or loading to improper URL");
		 * } driver.switchTo().window(MainWindow);
		 */

		// Youtube launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement YT = driver.findElement(By.xpath("//*[name()='svg'][4]"));
		YT.click();

		driver.getWindowHandle();
		String mainWindowHandle1 = driver.getWindowHandle();
		Set<String> allWindowHandles1 = driver.getWindowHandles();
		Iterator<String> iterator1 = allWindowHandles1.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator1.hasNext()) {
			String ChildWindow1 = iterator1.next();
			if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
				driver.switchTo().window(ChildWindow1);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_YT_title = "https://www.youtube.com/channel/UCqHvyjnCAW6FyOmmcr78fRw";

		try {
			Assert.assertEquals(exp_YT_title, driver.getCurrentUrl());
			test.log(LogStatus.PASS, "Navigating to Namlatic Youtube page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Youtube page is not loaded or loading to improper URL");
		}
		driver.switchTo().window(MainWindow);

		// LinkedIN launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement IN = driver.findElement(By.xpath("//*[name()='svg'][5]"));
		IN.click();

		// LinkedIN URL verification
		// Get handles of the windows
		String MainWindow2 = driver.getWindowHandle();
		String mainWindowHandle2 = driver.getWindowHandle();
		Set<String> allWindowHandles2 = driver.getWindowHandles();
		Iterator<String> iterator2 = allWindowHandles2.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator2.hasNext()) {
			String ChildWindow2 = iterator2.next();
			if (!mainWindowHandle2.equalsIgnoreCase(ChildWindow2)) {
				driver.switchTo().window(ChildWindow2);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_IN_title1 = "NAMLATIC | LinkedIn";
		String exp_IN_title2 = "Signup | LinkedIN";
		if (driver.getTitle().equals(exp_IN_title1)) {
			test.log(LogStatus.PASS, "Navigating to Namlatic LinkedIN page");
		} else if (driver.getTitle().equals(exp_IN_title2)) {
			test.log(LogStatus.PASS, "Navigating to Namlatic LinkedIN page");
		} else {
			test.log(LogStatus.FAIL, "Namlatic LinkedIN page is not loaded or loading to improper URL");
		}
		Thread.sleep(3000);
		driver.switchTo().window(MainWindow2);

		// Twitter launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement TW = driver
				.findElement(By.xpath("//div[contains(text(),'تابعنا')]/following::div//*[name()='svg'][1]"));
		TW.click();

		// Twitter URL verification
		// Get handles of the windows
		String MainWindow3 = driver.getWindowHandle();
		String mainWindowHandle3 = driver.getWindowHandle();
		Set<String> allWindowHandles3 = driver.getWindowHandles();
		Iterator<String> iterator3 = allWindowHandles3.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator3.hasNext()) {
			String ChildWindow3 = iterator3.next();
			if (!mainWindowHandle3.equalsIgnoreCase(ChildWindow3)) {
				driver.switchTo().window(ChildWindow3);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_TW_title = "https://twitter.com/Namla_officiel";
		try {
			Assert.assertEquals(exp_TW_title, driver.getCurrentUrl());
			test.log(LogStatus.PASS, "Navigating to Namlatic Twitter page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Twitter page is not loaded or loading to improper URL");
		}
		driver.switchTo().window(MainWindow3);

		// Facebook
		/*
		 * //Facebook launch verification
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
		 * Thread.sleep(2000); WebElement FB = driver.findElement(By.xpath(
		 * "//div[contains(text(),'تابعنا')]/following::div//*[name()='svg'][2]"));
		 * FB.click();
		 * 
		 * //Facebook URL verification //Get handles of the windows String MainWindow4 =
		 * driver.getWindowHandle(); String mainWindowHandle4 =
		 * driver.getWindowHandle(); Set<String> allWindowHandles4 =
		 * driver.getWindowHandles(); Iterator<String> iterator4 =
		 * allWindowHandles4.iterator();
		 * 
		 * // Here we will check if child window has other child windows and will fetch
		 * the heading of the child window while (iterator4.hasNext()) { String
		 * ChildWindow4 = iterator4.next(); if
		 * (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
		 * driver.switchTo().window(ChildWindow4); }}
		 * multiScreens.multiScreenShot(driver); String exp_FB_title =
		 * "https://www.facebook.com/Namlatic"; try { Assert.assertEquals(exp_FB_title,
		 * driver.getCurrentUrl());
		 * test.log(LogStatus.PASS,"Navigating to Namlatic Facebook page"); }
		 * catch(AssertionError e) { test.log(LogStatus.
		 * FAIL,"Namlatic Facebook page is not loaded or loading to improper URL"); }
		 * driver.switchTo().window(MainWindow4);
		 */
		// About us
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'معلومات عنا')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow6 = driver.getWindowHandle();
		String mainWindowHandle6 = driver.getWindowHandle();
		Set<String> allWindowHandles6 = driver.getWindowHandles();
		Iterator<String> iterator6 = allWindowHandles6.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator6.hasNext()) {
			String ChildWindow6 = iterator6.next();
			if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
				driver.switchTo().window(ChildWindow6);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_about_title = "https://business.namlatic.com/";
		try {
			Assert.assertEquals(exp_about_title, driver.getCurrentUrl());
			test.log(LogStatus.PASS, "Navigating to Namlatic About us page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic About us page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,5200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,5600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,6000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,6400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.switchTo().window(MainWindow6);

		// Terms & Conditions
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'الأحكام والشروط')]")).click();
		String exp_TC_title = "Namlatic";
		try {
			Assert.assertEquals(exp_TC_title, driver.getTitle());
			test.log(LogStatus.PASS, "Navigating to Namlatic Terms & Condition page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Terms & Conditions page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Privacy Policy
		// Launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'سياسة الخصوصية')]")).click();

		String exp_PP_title = "Namlatic";
		try {
			Assert.assertEquals(exp_PP_title, driver.getTitle());
			test.log(LogStatus.PASS, "Navigating to Namlatic Privacy Policy page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Privacy Policy page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Help Center
		// Launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'مركز مساعدة Namlatic')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow9 = driver.getWindowHandle();
		String mainWindowHandle9 = driver.getWindowHandle();
		Set<String> allWindowHandles9 = driver.getWindowHandles();
		Iterator<String> iterator9 = allWindowHandles9.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator9.hasNext()) {
			String ChildWindow9 = iterator9.next();
			if (!mainWindowHandle9.equalsIgnoreCase(ChildWindow9)) {
				driver.switchTo().window(ChildWindow9);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_help_title1 = "Login - Jira Service Management";

		try {
			Assert.assertEquals(exp_help_title1, driver.getTitle());
			test.log(LogStatus.PASS, "Navigating to Namlatic Help Center page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Help Center page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		driver.switchTo().window(MainWindow9);

		// Card verification
		WebElement exp_footer7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[2]"));
		try {
			exp_footer7.isDisplayed();
			test.log(LogStatus.PASS, "We accept Visa, American express, Master card, CIB, Wire Transfer");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Card section missing");
		}

	}

	public void screenshots_Ar() throws IOException, InterruptedException {
		// Screenshots
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
	}

	// ---login---
	@Test(priority = 10, invocationCount = 1)
	public void lgn_Ar() throws IOException, InterruptedException {
		// Login
		test = report.startTest("Login Functionality and Elastic search verification");
		try {
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
		// Hotel search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
			Hotel.sendKeys("عنابة");
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement btnclick1 = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
			btnclick1.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Hotel search working fine");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

//---Hotel Filters---
	@Test(priority = 11)
	public void filter_Ar() throws IOException, InterruptedException {
		// Price filter verification
		// Title Verification
		test = report.startTest("Filter verification");
		String act_Price_title = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div"))
				.getText();
		String exp_Price_title = "السعر";
		try {
			Assert.assertEquals(exp_Price_title, act_Price_title);
			test.log(LogStatus.PASS, "Price filter menu available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price filter menu not available");
		}
		multiScreens.multiScreenShot(driver);

		// Lowest price verification
		WebElement lowprice = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[5]"));

		try {
			test.log(LogStatus.PASS, "Lowest price is enabled: " + lowprice.isDisplayed());
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Lowest price is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Highest price verifcation
		WebElement highprice = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[7]"));

		try {
			test.log(LogStatus.PASS, "Highest price is enabled: " + highprice.isDisplayed());
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Highest price is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Slider verification
		WebElement slider = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div"));
		// WebElement slider2 =
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[6]"));
		try {
			test.log(LogStatus.PASS, "Price slider1 is enabled: " + slider.isDisplayed());
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price slider is not enabled");
		}
		multiScreens.multiScreenShot(driver);

		// Slider navigation
		Actions act = new Actions(driver);
		act.dragAndDropBy(slider, 0, 80).build().perform();
		// act.dragAndDropBy(slider, -20, 0).build().perform();
		multiScreens.multiScreenShot(driver);

		// Price low to high
		String act_lowtohigh = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
				.getText();
		String exp_lowtohigh = "السعر من الارخص للاعلى";
		WebElement lowtohigh = driver.findElement(By.id("filter_price_low_to_high"));

		try {
			Assert.assertEquals(exp_lowtohigh, act_lowtohigh);
			System.out.println("Low to high option button is displayed:" + lowtohigh.isDisplayed());
			System.out.println("Low to high option button is selected:" + lowtohigh.isSelected());
			driver.findElement(By.xpath("//*[@id=\"filter_price_low_to_high\"]")).click();
			test.log(LogStatus.PASS, "Price - low to high is displaying and enabled");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price - low to high is missing or not enabled to select");
		}
		multiScreens.multiScreenShot(driver);

		// Price high to low
		String act_hightolow = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[3]/label"))
				.getText();
		String exp_hightolow = "السعر الاعلى الى الادنى";
		WebElement hightolow = driver.findElement(By.id("filter_price_high_to_low"));

		try {
			Assert.assertEquals(exp_hightolow, act_hightolow);
			System.out.println("High to Low option button is displayed:" + hightolow.isDisplayed());
			System.out.println("High to Low option button is selected:" + hightolow.isSelected());
			driver.findElement(By.xpath("//*[@id=\"filter_price_high_to_low\"]")).click();
			test.log(LogStatus.PASS, "Price - high to low is displaying and enabled");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Price - high to low is missing or not enabled to select");
		}
		multiScreens.multiScreenShot(driver);

		// Price filter enable and disable
		// Deselect
		WebElement pricefilter = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div"));
		pricefilter.click();
		Thread.sleep(3000);
		// Select
		pricefilter.click();
		multiScreens.multiScreenShot(driver);
	}

	@Test(priority = 12)
	public void popular_sr_Ar() throws InterruptedException, IOException {
		test = report.startTest("Popular search verification");
		// Popular search popular filter enable and disable
		// Deselect
		WebElement popularfilter = driver.findElement(By.xpath("//*[contains(text(),'عمليات البحث الشعبية')]"));
		popularfilter.click();
		Thread.sleep(3000);
		// Select
		popularfilter.click();
		multiScreens.multiScreenShot(driver);

		// Title verification
		String act_pop_title = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[1]/div"))
				.getText();
		String exp_pop_title = "عمليات البحث الشعبية";
		try {
			Assert.assertEquals(exp_pop_title, act_pop_title);
			test.log(LogStatus.PASS, "Popular search title is matching");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Popular search title is mismatching");
		}
		multiScreens.multiScreenShot(driver);

		// Couple Friendly verification
		String act_pop1 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[1]/label"))
				.getText();
		String exp_pop1 = "ودية للزوجين";
		try {
			Assert.assertEquals(exp_pop1, act_pop1);
			driver.findElement(By.xpath("//*[@id=\"filter_couple_friendly\"]")).click();
			test.log(LogStatus.PASS, "Couple friendly is available in Popular search");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Couple friendly is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Air Conditioning verification
		String act_pop2 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[2]/label"))
				.getText();
		String exp_pop2 = "تكييف";
		try {
			Assert.assertEquals(exp_pop2, act_pop2);
			test.log(LogStatus.PASS, "Air Conditioning is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_air_conditioning\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Air Conditioning is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Free Breakfast verification
		String act_pop3 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[3]/label"))
				.getText();
		String exp_pop3 = "إفطار مجاني";
		try {
			Assert.assertEquals(exp_pop3, act_pop3);
			test.log(LogStatus.PASS, "Free Breakfast is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_free_breakfast\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free Breakfast is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Free Cancellation Policy verification
		String act_pop4 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[4]/label"))
				.getText();
		String exp_pop4 = "سياسة الإلغاء المجاني";
		try {
			Assert.assertEquals(exp_pop4, act_pop4);
			test.log(LogStatus.PASS, "Free Cancellation is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_free_cancellation_policy\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free Cancellation is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Pay at hotel verification
		String act_pop5 = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[5]/label"))
				.getText();
		String exp_pop5 = "ادفع في الفندق";
		try {
			Assert.assertEquals(exp_pop5, act_pop5);
			test.log(LogStatus.PASS, "Pay at hotel is available in Popular search");
			driver.findElement(By.xpath("//*[@id=\"filter_pay_at_hotel\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Pay at hotel is not available in Popular search");
		}
		multiScreens.multiScreenShot(driver);

		// Deselect
		popularfilter.click();
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");
	}

	@Test(priority = 13)
	public void Amenities_Ar() throws InterruptedException, IOException {
		test = report.startTest("Amenities section verification");
		/*
		 * //Amenities filter enable and disable //Deselect WebElement amenitiesfilter =
		 * driver.findElement(By.xpath("//*[contains(text(),'Amenities')]"));
		 * amenitiesfilter.click(); Thread.sleep(3000); //Select
		 * amenitiesfilter.click(); multiScreens.multiScreenShot(driver);
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		 * 
		 * //Title verification String act_amn_title = driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[1]/div")).
		 * getText(); String exp_amn_title = "amenities"; try {
		 * Assert.assertEquals(exp_amn_title, act_amn_title);
		 * test.log(LogStatus.PASS,"Amenities title is matching"); } catch
		 * (AssertionError e) {
		 * test.log(LogStatus.FAIL,"Amenities title is mismatching"); }
		 * multiScreens.multiScreenShot(driver);
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		 */

		// Free Wi-fi verification
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");
		String act_amn1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div/div[1]/label"))
				.getText();
		String exp_amn1 = "خدمة الواي فاي المجانية";
		try {
			Assert.assertEquals(exp_amn1, act_amn1);
			test.log(LogStatus.PASS, "Free Wifi is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_free_wi_fi\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Free wifi is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Power Backup verification
		Thread.sleep(2000);
		String act_amn2 = driver.findElement(By.xpath("//*[contains(text(),'احتياطي الطاقة')]")).getText();
		// html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[2]/label")).getText();
		String exp_amn2 = "احتياطي الطاقة";
		System.out.println(act_amn2);
		try {
			Assert.assertEquals(exp_amn2, act_amn2);
			test.log(LogStatus.PASS, "Power Backup is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_power_backup\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Power Backup is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Gardern verification
		Thread.sleep(2000);
		String act_amn3 = driver.findElement(By.xpath("//*[contains(text(),'حديقة/فناء خلفي')]")).getText();
		String exp_amn3 = "حديقة/فناء خلفي";
		System.out.println(act_amn3);

		try {
			Assert.assertEquals(exp_amn3, act_amn3);
			test.log(LogStatus.PASS, "Gardern is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_garden__backyard\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Gardern is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// SPA verification
		Thread.sleep(2000);
		String act_amn4 = driver.findElement(By.xpath("//*[contains(text(),'منتجع')]")).getText();
		String exp_amn4 = "منتجع";
		try {
			Assert.assertEquals(exp_amn4, act_amn4);
			test.log(LogStatus.PASS, "SPA is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_spa\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "SPA is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Swimming pool verification
		Thread.sleep(2000);
		String act_amn5 = driver.findElement(By.xpath("//*[contains(text(),'حمام سباحة')]")).getText();
		String exp_amn5 = "حمام سباحة";
		try {
			Assert.assertEquals(exp_amn5, act_amn5);
			test.log(LogStatus.PASS, "Swimming pool is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_swimming_pool\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Swimming pool+ is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Gym verification
		Thread.sleep(2000);
		String act_amn6 = driver.findElement(By.xpath("//*[contains(text(),'الصالة الرياضية')]")).getText();
		String exp_amn6 = "الصالة الرياضية";
		try {
			Assert.assertEquals(exp_amn6, act_amn6);
			test.log(LogStatus.PASS, "GYM is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_gym\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "GYM is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Laundry verification
		Thread.sleep(2000);
		String act_amn7 = driver.findElement(By.xpath("//*[contains(text(),'خدمات الغسيل')]")).getText();
		String exp_amn7 = "خدمات الغسيل";
		try {
			Assert.assertEquals(exp_amn7, act_amn7);
			test.log(LogStatus.PASS, "Laundry service is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_laundry_services__washing_machine\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Laundry is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Cafe verification
		Thread.sleep(2000);
		String act_amn8 = driver.findElement(By.xpath("//*[contains(text(),'مقهى')]")).getText();
		String exp_amn8 = "مقهى";
		try {
			Assert.assertEquals(exp_amn8, act_amn8);
			test.log(LogStatus.PASS, "Café is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_caf_\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Café is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// Pet friendly verification
		Thread.sleep(2000);
		String act_amn9 = driver.findElement(By.xpath("//*[contains(text(),'صديقة للحيوانات الأليفة')]")).getText();
		String exp_amn9 = "صديقة للحيوانات الأليفة";
		try {
			Assert.assertEquals(exp_amn9, act_amn9);
			test.log(LogStatus.PASS, "Pet friendly is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_pet_friendly\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Pet friendly is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,500)");

		// In House restaurant verification
		Thread.sleep(2000);
		String act_amn10 = driver.findElement(By.xpath("//*[contains(text(),'مطعم في الموقع')]")).getText();
		String exp_amn10 = "مطعم في الموقع";
		try {
			Assert.assertEquals(exp_amn10, act_amn10);
			test.log(LogStatus.PASS, "In-House restaurant is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_in_house_restaurant\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "In-House restaurant is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,750)");

		// Card Payment verification
		Thread.sleep(2000);
		String act_amn11 = driver.findElement(By.xpath("//*[contains(text(),'الدفع بالبطاقة')]")).getText();
		String exp_amn11 = "الدفع بالبطاقة";
		try {
			Assert.assertEquals(exp_amn11, act_amn11);
			test.log(LogStatus.PASS, "Card Payment is available in Amenities");
			driver.findElement(By.xpath("//*[@id=\"filter_card_payment\"]")).click();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Card Payment is not available in Amenities");
		}
		multiScreens.multiScreenShot(driver);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		((JavascriptExecutor) driver).executeScript("scroll(0,750)");
	}

	@Test(priority = 14)
	public void search_Ar() throws InterruptedException {
		test = report.startTest("Hotel search verification");
		// Hotel Search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'اخترأين تريد أن تقيم ؟')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver.findElement(By.xpath("//input[contains(@placeholder,'اخترأين تريد أن تقيم ؟')]"));
			Hotel.sendKeys("القهوة، بونديشيري");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");

			String Hotel_name = driver.findElement(By.xpath("//input[contains(@placeholder,'اخترأين تريد أن تقيم ؟')]"))
					.getText();
			System.out.println(Hotel_name);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div[4]/div")).click();
			Thread.sleep(2000);
			driver.findElement(
					By.xpath("//div[contains(text(),'القهوة، بونديشيري')]/following::div[text()='عرض التفاصيل']"));
			test.log(LogStatus.PASS, "Able to search hotel successful");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to search hotel with given keyword");
		}
	}
//--- Hotel details page verification

	@Test(priority = 16, invocationCount = 1)
	void booking_Ar() throws IOException {
		test = report.startTest("Hotel Details verification");
		/*
		 * driver.navigate().to(
		 * "https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD"
		 * ); driver.manage().window().maximize(); multiScreens.multiScreenShot(driver);
		 */
		WebElement Hotel_sel = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div"));
		Hotel_sel.click();
		String exp_hot_name = Hotel_sel.getText();
		multiScreens.multiScreenShot(driver);

		driver.getWindowHandle();
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		// Scroll bar navigation
		// ((JavascriptExecutor)driver).executeScript("scroll(0,1601)");

		new Hotel_Filter();
		String act_hot_name = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]"))
				.getText();

		try {
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
			driver.close();
		}
	}

	@Test(priority = 17, invocationCount = 1, enabled = false)

	public void image_Ar() {
		// Image verification
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
		test = report.startTest("Image section verification");
		try {
			test.log(LogStatus.PASS, "Image loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Images are not loading properly");
		}
	}

	@Test(priority = 18, invocationCount = 1)
	void description_Ar() {
		// Description verification
		test = report.startTest("Hotel Description section verification");
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Hotel description is available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Description missing");
		}
	}

	@Test(priority = 19, invocationCount = 1)

	void roomtype_Ar() {
		test = report.startTest("Room type section verification");
		// Type of rooms section verification
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Type of rooms section is available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Types of rooms section missing");
		}

		// Room type images verification
		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Room type images are available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Images are missing");
		}

		// Room type sharing details verification
		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Sharing details available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Sharing details are not available");
		}

		// Amenities section verification
		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Amenities section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Amenities section not available");
		}

		// Rate section verification
		try {
			String rate = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]"))
					.getText();
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]"))
					.isDisplayed();
			// driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
			System.out.println("Selected currency" + rate);
			test.log(LogStatus.PASS, "Rate section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Rate section not available");
		}

		// Add Room section verification
		try {
			driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
			test.log(LogStatus.PASS, "Add room section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Add room section not available");
		}

		((JavascriptExecutor) driver).executeScript(
				"scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
	}

	// Verification of Cancellation policies
	@Test(priority = 20, invocationCount = 1)
	public void cancellation_policy_Ar() {
		test = report.startTest("Cancellation Policy section verification");
		String cancel_type = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div"))
				.getText();
		System.out.println(cancel_type);
		if (cancel_type.equals("إلغاء مجاني")) {
			// Card Payment verification
			String exp_title1 = "سياسة الإلغاء";
			String exp_title2 = "بطاقه ائتمان";
			String exp_text1 = "إلغاء مجاني قبل الساعة 06:00:00 مساءً (قبل يوم واحد) ، قد يتم تطبيق رسوم الخدمة.\n\n"

					+ "إذا تم الإلغاء بعد الساعة 06:00:00 مساءً (قبل يوم واحد) ، فسيتم تحصيل مبلغ إقامة الليلة الأولى.\n\n"

					+ "لن يتم رد أي مبلغ إذا تم إلغاؤه بعد وقت تسجيل الوصول.\n\n"

					+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"

					+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.";

			String act_title1 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
			String act_title2 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]"))
					.getText();
			String act_text1 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_title1, act_title1);
				Assert.assertEquals(exp_title2, act_title2);
				Assert.assertEquals(exp_text1, act_text1);
				test.log(LogStatus.PASS, "Cancellation policy for Card payment content and title is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Card payment content is not matching");
			}

			// Wire Transfer verification
			String exp_text11 = "تحويل بنكي";
			String exp_text12_ar = "إلغاء مجاني قبل الساعة 06:00:00 مساءً (قبل يوم واحد) ، قد يتم تطبيق رسوم الخدمة\n\n"

					+ "في حالة الإلغاء بعد الساعة 06:00:00 مساءً (قبل يوم واحد) ، سيتم تحصيل مبلغ إقامة الليلة الأولى.\n\n"

					+ "لن يتم رد أي مبلغ إذا تم إلغاؤه بعد وقت تسجيل الوصول.\n\n"

					+ "إذا لم يظهر الضيف في حجزه المؤكد ، فإننا نتعامل معه على أنه إلغاء ولن يتم رد أي مبلغ.\n\n"

					+ "لن تخضع ساعات تسجيل الوصول المتأخر لإعادة التمويل\n\n"

					+ "سيتم إلغاء الحجز المحجوز تلقائيًا في حالة فشل المعاملة.\n\n"

					+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"

					+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني الملكية مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.\n\n"

					+ "تسري المبالغ المستردة فقط عندما يتم تقديم طلب الإلغاء رسميًا من خلال منصة Namlatic.\n\n"

					+ "سوف تستغرق معالجة المبالغ المستردة القابلة للتطبيق من 5 إلى 7 أيام عمل.";

			String act_text11 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]"))
					.getText();
			String act_text12_ar = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_text11, act_text11);
				Assert.assertEquals(exp_text12_ar, act_text12_ar);
				test.log(LogStatus.PASS, "Cancellation policy for Wire Transfer content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Wire Transfer content is not matching");
			}

			// Pay at hotel
			((JavascriptExecutor) driver).executeScript("scroll(0,'الدفع في الفندق')");
			String exp_text21 = "الدفع في الفندق";
			String exp_text22 = "عند إجراء حجز باستخدام خيار \"الدفع في الفندق\" ، سيقوم الفندق المعني بتحصيل المبلغ بالكامل مقابل الحجز في وقت تسجيل الوصول.\n\n"

					+ "يجب تأكيد الحجز قبل 48 ساعة من وقت تسجيل الوصول لتاريخ الرحلة عن طريق البريد الإلكتروني أو مكالمة مع Namlatic ، ويعتبر الحجز مؤكدًا فقط عند التحقق الشخصي ، وإلا فسيكون عرضة للإلغاء بموجب بند سياسة الإلغاء.\n\n"

					+ "فيما يلي تفاصيل الاتصال:\n"

					+ "البريد الإلكتروني: Confirm@namlatic.com\n"

					+ "الخط الساخن Namlatic DZ: +213982414415\n\n"

					+ "بالنسبة للسائحين الدوليين ، سيتم تحصيل المبلغ بالعملة المحلية أو بأي عملة أخرى ، وفقًا لما يقرره الفندق.\n\n"

					+ "لأغراض أمنية ، يجب على المستخدم تقديم رقم التعريف الشخصي المكون من 4 أرقام الذي تم استلامه مع البريد الإلكتروني لتأكيد الحجز الخاص به ، ويجوز لـ Namlatic أو الفندق إلغاء الحجز في حالة إذا كان الرمز الذي تم العثور عليه غير صحيح.\n\n"

					+ "يسمح Namlatic بالإلغاء الجزئي والإلغاء الكامل للغرف المحجوزة قبل تسجيل الوصول.\n\n"

					+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء مع \"لا استرداد\"\n\n"

					+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي رد جزئي.\n\n"

					+ "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic Online Booking.\n\n"

					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"

					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.";

			String act_text21 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]"))
					.getText();
			String act_text22 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]"))
					.getText();
			System.out.println(act_text22);

			try {
				Assert.assertEquals(exp_text21, act_text21);
				Assert.assertEquals(exp_text22, act_text22);
				test.log(LogStatus.PASS, "Cancellation policy for Pay at hotel content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Pay at hotel content is not matching");
			}

			// CIB
			((JavascriptExecutor) driver).executeScript("scroll(0,'CIB')");
			String exp_text31 = "CIB";
			String exp_text32 = "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic اون لاين للحجز.\n\n"

					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"

					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.\n\n"

					+ "سياسة الاسترجاع\n\n"

					+ "أي حجوزات يتم إلغاؤها بعد الساعة 6:00 مساءً من خلال منصة Namlatic ، سيتم استرداد جزء من المبلغ المدفوع من خلال بطاقة CIB (قد يتم تطبيق رسوم الإلغاء).\n\n"

					+ "قد تختلف رسوم الإلغاء ورسوم الخدمة المطبقة وفقًا للحجز ومكان الإقامة المختار.\n\n"

					+ "قد تستغرق عملية استرداد الأموال المطبقة 3-5 أيام عمل.\n\n"

					+ "عدم الحضور\n\n"

					+ "لن يتم رد أي أموال إذا لم يحضر الشخص إلى الفندق المعني في حجزه المؤكد.\n\n"

					+ "لا تقبل Namlatic أي التزام أو مسؤولية عن عواقب وصولك المتأخر أو أي إلغاء أو فرض رسوم عدم حضور من قبل مزود الملكية.\n\n"

					+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي استرداد.";

			String act_text31 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]"))
					.getText();
			String act_text32 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_text31, act_text31);
				Assert.assertEquals(exp_text32, act_text32);
				test.log(LogStatus.PASS, "Cancellation policy for CIB payment method content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for CIB payment method content is not matching");
			}
		} else if (cancel_type.equals("غير قابل للاسترجاع")) {
			// Card Payment verification
			String exp_title1 = "سياسة الإلغاء";
			String exp_title2 = "بطاقه ائتمان";
			String exp_text1 = "لن يتم رد أي مبلغ خلال أي وقت من الإلغاء.\n\n"

					+ "Namlatic لا يسمح بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"

					+ "في حالة وجود أي عمل خبيث / عدائي من قبل العميل النهائي داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، فسيكون عرضة لإلغاء الحجز على الفور ، مع عدم استرداد الأموال وفقًا لسياسات الفندق.";

			String act_title1 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
			String act_title2 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]"))
					.getText();
			String act_text1 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_title1, act_title1);
				Assert.assertEquals(exp_title2, act_title2);
				Assert.assertEquals(exp_text1, act_text1);
				test.log(LogStatus.PASS, "Cancellation policy for Card payment content and title is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Card payment content is not matching");
			}

			// Wire Transfer verification
			String exp_text11 = "تحويل بنكي";
			String exp_text12 = "لن يتم استرداد أي مبلغ في أي وقت من الإلغاء.\n\n"

					+ "لا يسمح Namlatic بتعديل / تحديث مواعيد الحجز من خلال منصته.\n\n"

					+ "3. سيتم إلغاء الحجز المحجوز تلقائيًا في حالة فشل المعاملة.";

			String act_text11 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]"))
					.getText();
			String act_text12 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_text11, act_text11);
				Assert.assertEquals(exp_text12, act_text12);
				test.log(LogStatus.PASS, "Cancellation policy for Wire Transfer content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Wire Transfer content is not matching");
			}

			// Pay at hotel
			((JavascriptExecutor) driver).executeScript("scroll(0,'الدفع في الفندق')");
			String exp_text21 = "الدفع في الفندق";
			String exp_text22 = "عند إجراء حجز باستخدام خيار \"الدفع في الفندق\" ، سيقوم الفندق المعني بتحصيل المبلغ بالكامل مقابل الحجز في وقت تسجيل الوصول.\n\n"

					+ "يجب تأكيد الحجز قبل 48 ساعة من وقت تسجيل الوصول لتاريخ الرحلة عن طريق البريد الإلكتروني أو مكالمة مع Namlatic ، ويعتبر الحجز مؤكدًا فقط عند التحقق الشخصي ، وإلا فسيكون عرضة للإلغاء بموجب بند سياسة الإلغاء.\n\n"

					+ "فيما يلي تفاصيل الاتصال:\n"

					+ "البريد الإلكتروني: Confirm@namlatic.com\n"

					+ "الخط الساخن Namlatic DZ: +213982414415\n\n"

					+ "بالنسبة للسائحين الدوليين ، سيتم تحصيل المبلغ بالعملة المحلية أو بأي عملة أخرى ، وفقًا لما يقرره الفندق.\n\n"

					+ "لأغراض أمنية ، يجب على المستخدم تقديم رقم التعريف الشخصي المكون من 4 أرقام الذي تم استلامه مع البريد الإلكتروني لتأكيد الحجز الخاص به ، ويجوز لـ Namlatic أو الفندق إلغاء الحجز في حالة إذا كان الرمز الذي تم العثور عليه غير صحيح.\n\n"

					+ "يسمح Namlatic بالإلغاء الجزئي والإلغاء الكامل للغرف المحجوزة قبل تسجيل الوصول.\n\n"

					+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء مع \"لا استرداد\"\n\n"

					+ "لن تخضع ساعات تسجيل الوصول المتأخر لأي رد جزئي.\n\n"

					+ "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic Online Booking.\n\n"

					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"

					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.";

			String act_text21 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]"))
					.getText();
			String act_text22 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]"))
					.getText();
			try {
				Assert.assertEquals(exp_text21, act_text21);
				Assert.assertEquals(exp_text22, act_text22);
				test.log(LogStatus.PASS, "Cancellation policy for Pay at hotel content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for Pay at hotel content is not matching");
			}

			// CIB
			((JavascriptExecutor) driver).executeScript("scroll(0,'CIB')");
			String exp_text31 = "CIB";
			String exp_text32 = "يمكنك إلغاء الحجز في أي وقت من موقع Namlatic اون لاين للحجز.\n\n"

					+ "بمجرد إلغاء الحجز ، لا يمكنك إعادة بدء نفس الحجز أو لا يمكنك تسجيل الوصول مباشرةً باستخدام نفس معرف الحجز.\n\n"

					+ "في حالة وجود أي عدائية / ضارة من قبل السائح داخل مباني العقار مع أي شخص يتم العثور عليه أو الإبلاغ عنه ، سيتم الإلغاء في مكان الحجز دون استرداد الأموال.\n\n"

					+ "سياسة الاسترجاع\n\n"

					+ "لا يمكن إلغاء أو رد الحجوزات التي تمت بشروط الفندق \"غير القابلة للاسترداد\".\n\n"

					+ "سيتم اعتبار أي حجز يتم إلغاؤه بعد / أثناء وقت تسجيل الوصول على أنه إلغاء غير قابل للاسترداد مع \"عدم استرداد الأموال\"";

			String act_text31 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]"))
					.getText();
			String act_text32 = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]"))
					.getText();
			try {
				Assert.assertEquals(exp_text31, act_text31);
				Assert.assertEquals(exp_text32, act_text32);
				test.log(LogStatus.PASS, "Cancellation policy for CIB payment method content is matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation policy for CIB payment method content is not matching");
			}
		}
	}

	@Test(priority = 21, invocationCount = 1)
	public void amenities_ver_Ar() {
		test = report.startTest("Amenities section Verification");
		// Amenities section2 verification
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Amenities section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Amenities section not available");
		}
	}

	@Test(priority = 22, invocationCount = 1)
	public void policy_ver_Ar() {
		test = report.startTest("Hotel policy Verification");

		// Hotel Policy verification
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Hotel Policy section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Hotel Policy section not available");
		}
	}

	@Test(priority = 23, invocationCount = 1)

	public void book_now_Ar() throws InterruptedException, IOException {
		Date_room_selection_Ar();
//only one option should be active in script.
		CIB_Ar();
		// Card_Payment();
		// Wire_Transfer();
	}

	public void Date_room_selection_Ar() throws InterruptedException, IOException {
		test = report.startTest("Date and Room selection section verification");

		// From and To Date Selection

		// WebElement
		// date=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"));
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);",date);
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
		Thread.sleep(500);

		// WebElement
		// navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);", navigate);

		// driver.findElement(By.xpath("//*[text()='29']")).click();
		// driver.findElement(By.xpath("//*[text()='30']")).click();

		// Room selection
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		WebElement element = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
		Select room_sel = new Select(element);
		room_sel.selectByVisibleText("1");
		room_sel.getFirstSelectedOption();
		multiScreens.multiScreenShot(driver);

		// Book now button verification
		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Book Now button available in booking section");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Book Now button not available in Booking section");
		}

		// Click on book now button
		WebElement book_now = driver.findElement(By.xpath("//*[(text()='احجز الآن')]"));
		book_now.click();
		multiScreens.multiScreenShot(driver);

	}

	// Select payment methods ' CIB ' using radio button.
	@Test(priority = 25, invocationCount = 1)

	public void CIB_Ar() throws IOException, InterruptedException {
		test = report.startTest("Payment Method verification");
		try {
			if (driver.findElement(By.id("cib")).isEnabled()) {
				WebElement radio_cib = driver.findElement(By.id("cib"));
				radio_cib.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);

				// Select Terms and Conditions - check box
				WebElement checkbox_terms = driver.findElement(By.id("terms"));
				checkbox_terms.click();
				multiScreens.multiScreenShot(driver);
				System.out.println("CIB card selected");
			} else {
				// Currency selection
				WebElement cur = driver
						.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
				Thread.sleep(500);
				cur.click();
				WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج دج')]"));
				cur_sel.click();

				// CIB selection and proceed
				Date_room_selection_Ar();
				WebElement radio_cib = driver.findElement(By.id("cib"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
				Thread.sleep(500);
				radio_cib.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);

				// Select Terms and Conditions - check box
				WebElement checkbox_terms = driver.findElement(By.id("terms"));
				checkbox_terms.click();
				multiScreens.multiScreenShot(driver);
				test.log(LogStatus.PASS, "CIB card selected");
			}
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select CIB card");
		}
	}

	public void Card_Payment_Ar() throws IOException, InterruptedException {
		WebElement radio_card = driver.findElement(By.id("card"));
		radio_card.click();
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		System.out.println("Card Payment selected");
	}

	public void Wire_Transfer_Ar() throws IOException, InterruptedException {
		WebElement radio_wt = driver.findElement(By.id("wt"));
		radio_wt.click();
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		System.out.println("Wire Transfer selected");
	}

	@Test(priority = 26, invocationCount = 1)

	public void various_section_Validation_Ar() throws InterruptedException {
		test = report.startTest("Verification of other section in Room details page");
		// Booking section verification
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Booking section available");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Booking section not available");
		}

		// Pricing
		try {
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Pricing details available in booking section");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Pricing details not available in Booking section");
		}

		// Calender
		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Calender details available in booking section");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Calender details not available in Booking section");
		}

		/*
		 * //Guest try { driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"
		 * )).isDisplayed();
		 * test.log(LogStatus.PASS,"Guest details available in booking section"); }
		 * catch (Exception e) {
		 * test.log(LogStatus.FAIL,"Guest details not available in Booking section"); }
		 * Thread.sleep(3000);
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
		 */

		// Navigating to various section
		driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("scroll(0,400)");

		driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
		Thread.sleep(2000);
		driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
		Thread.sleep(2000);
		driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
		Thread.sleep(2000);

		/*
		 * //Select reCAPTCHA Check box
		 * driver.switchTo().defaultContent().findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"
		 * )); System.out.println("test"); WebElement
		 * checkbox_recaptcha=driver.findElement(By.xpath(
		 * "/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
		 * checkbox_recaptcha.click();
		 * System.out.println("reCAPTCHA check box is selected");
		 * 
		 * 
		 * //Click on Proceed To Pay button WebElement Proceed_element
		 * =driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"
		 * )); Proceed_element.click();
		 */
	}

//---My Booking Page Validation---

	public void pg_launch_Ar() throws InterruptedException, IOException {
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement menu = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();
		WebElement menuoption = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
	}

	@Test(priority = 27, invocationCount = 1)

	public void view_booking_Ar() throws IOException, InterruptedException {
		test = report.startTest("My Booking Page - View Booking option verification");

//View Booking
		pg_launch_Ar();
		try {
			WebElement view_book_id = driver
					.findElement(By.xpath("//div[contains(text(),'N2HZLUYZ37G')]/following::div[text()='انظر الحجز']"));
			view_book_id.click();
			test.log(LogStatus.PASS, "View booking section working fine");
			multiScreens.multiScreenShot(driver);

			driver.getWindowHandle();
			String mainWindowHandle1 = driver.getWindowHandle();
			Set<String> allWindowHandles1 = driver.getWindowHandles();
			Iterator<String> iterator1 = allWindowHandles1.iterator();
			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator1.hasNext()) {
				String ChildWindow1 = iterator1.next();
				if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
					driver.switchTo().window(ChildWindow1);

				}
			}
			// Scroll Down in View Booking Details page
			((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
			// WebDriverWait wait=new WebDriverWait(driver,50);
			Thread.sleep(5000);
			multiScreens.multiScreenShot(driver);

//Click on Download Voucher link

			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,2000)", "");
			// WebElement download_Vou =driver.findElement(By.xpath("//*[(@class, 'sc-plWPA
			// hwKzKa']"));
			Thread.sleep(5000);
			WebElement download_Vou = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
			download_Vou.click();

			Thread.sleep(5000);
			test.log(LogStatus.PASS, "Download voucher working fine");

//Click on Email Voucher link
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,2000)", "");
			Thread.sleep(5000);
			WebElement send_email = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
			send_email.click();

			Thread.sleep(5000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Email voucher working fine");

			/*
			 * //Click on Print Voucher link
			 * ((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
			 * Thread.sleep(5000); WebElement print_Vou=driver.findElement(By.xpath(
			 * "/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
			 * print_Vou.click();
			 * 
			 * Thread.sleep(5000); Robot esc_print_window = new Robot();
			 * esc_print_window.keyPress(KeyEvent.VK_ESCAPE);
			 * esc_print_window.keyRelease(KeyEvent.VK_ESCAPE);
			 */
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Facing problem in View booking section");
		}
	}

//Cancellation
	void cancellation_Ar() throws AWTException, InterruptedException, IOException {
		test = report.startTest("My Booking Page - Cancellation option verification");
		pg_launch_Ar();
		Thread.sleep(3000);
		// ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		Thread.sleep(3000);
		try {
			WebElement cancel = driver
					.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[text()='إلغاء حجز']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
			String Price = driver
					.findElement(
							By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[@class='sc-pCOPB dfwnsf']"))
					.getText().substring(1);
			float exp_price = Float.parseFloat(Price);
			double exp_p1 = Math.round(exp_price * 100.0) / 100;
			cancel.click();
			test.log(LogStatus.PASS, "Able to select 'Cancel button' successfully");

			driver.findElement(By.id("reason-1")).click();

// Validation:
			// Title verification
			String exp_title = "إلغاء حجزك";
			String act_title = driver.findElement(By.xpath("//div[contains(text(),'إلغاء حجزك')]")).getText();

			try {
				Assert.assertEquals(exp_title, act_title);
				test.log(LogStatus.PASS, "Cancellation Popup Title matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Cancellation Popup Title not matching");
			}

			// Cancel info verification
			String exp_info = "حدد الغرفة التي تريد إلغاؤها";
			String act_info = driver.findElement(By.xpath("//div[contains(text(),'حدد الغرفة التي تريد إلغاؤها')]"))
					.getText();

			try {
				Assert.assertEquals(exp_info, act_info);
				test.log(LogStatus.PASS, "Info. matching");
			} catch (Exception e) {
				test.log(LogStatus.FAIL, "Info. not mismatching");
			}

			// Price verification
			String act_price1 = driver
					.findElement(By.xpath(
							"/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[2]"))
					.getText().substring(1);
			/*
			 * if (
			 * driver.findElement(By.xpath("//*[@class='sc-pcHDm cOnSkc']")).isDisplayed())
			 * { String act_price2 = driver.findElement(By.xpath(
			 * "/html/body/div[1]/div/div[2]/div/div/div[2]/div[6]/div/div[3]/div[2]/div[2]/div[2]"
			 * )).getText().substring(1);
			 * 
			 * float actprice1f =Float.parseFloat(act_price1); double p1 =
			 * Math.round(actprice1f*100.0)/100; float actprice2f =
			 * Float.parseFloat(act_price2); double p2 = Math.round(actprice2f*100.0)/100;
			 * double act_price3= p1 + p2; System.out.println(act_price3);
			 * 
			 * try { Assert.assertEquals(exp_p1, act_price3);
			 * System.out.println("Price matching"); } catch(AssertionError e) {
			 * System.out.println("Price not matching"); } } else {
			 */
			try {
				float actprice1f = Float.parseFloat(act_price1);
				double p1 = Math.round(actprice1f * 100.0) / 100;
				Assert.assertEquals(exp_p1, p1);
				test.log(LogStatus.PASS, "Price matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Price not matching");
			}
			// }

// Verification of cancellation title
			String exp_title1 = "أخبرنا عن سبب إلغاء حجزك";
			String act_title1 = driver.findElement(By.xpath("//div[contains(text(),'أخبرنا عن سبب إلغاء حجزك')]"))
					.getText();

			try {
				Assert.assertEquals(exp_title1, act_title1);
				test.log(LogStatus.PASS, "Cancellation title matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation title not matching");
			}

// Verification of cancellation title
			String exp_opt1 = "تغيير التواريخ / الوجهة";
			String act_opt1 = driver.findElement(By.xpath("//*[contains(text(),'تغيير التواريخ / الوجهة')]")).getText();

			try {
				Assert.assertEquals(exp_opt1, act_opt1);
				test.log(LogStatus.PASS, "Cancellation option 1 matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation option 1 not matching");
			}

// Verification of cancellation title
			String exp_opt2 = "سبب شخصي";
			String act_opt2 = driver.findElement(By.xpath("//*[contains(text(),'سبب شخصي')]")).getText();
			try {
				Assert.assertEquals(exp_opt2, act_opt2);
				test.log(LogStatus.PASS, "Cancellation option 2 matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation option 2 not matching");
			}

// Verification of cancellation title
			String exp_opt3 = "إيجاد إقامة بديلة";
			String act_opt3 = driver.findElement(By.xpath("//*[contains(text(),'إيجاد إقامة بديلة')]")).getText();

			try {
				Assert.assertEquals(exp_opt3, act_opt3);
				test.log(LogStatus.PASS, "Cancellation option 3 matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation option 3 not matching");
			}

// Verification of cancellation title
			String exp_opt4 = "تغيير عدد المسافرين";
			String act_opt4 = driver.findElement(By.xpath("//*[contains(text(),'تغيير عدد المسافرين')]")).getText();

			try {
				Assert.assertEquals(exp_opt4, act_opt4);
				test.log(LogStatus.PASS, "Cancellation option 4 matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation option 4 not matching");
			}

// Verification of cancellation title
			String exp_opt5 = "سبب أخر";
			String act_opt5 = driver.findElement(By.xpath("//*[contains(text(),'سبب أخر')]")).getText();
			try {
				Assert.assertEquals(exp_opt5, act_opt5);
				test.log(LogStatus.PASS, "Cancellation option 5 matching");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Cancellation option 5 not matching");
			}

			Robot esc = new Robot();
			esc.keyPress(KeyEvent.VK_ESCAPE);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Cancellation section is not working properly");
		}
	}

	@Test(priority = 29, invocationCount = 1)
	public void book_now() throws InterruptedException, IOException {
		test = report.startTest("My Booking Page - Booknow option verification");
		try {
			WebElement Bk_now = driver
					.findElement(By.xpath("//div[contains(text(),'N7NH9JRUJ3O')]/following::div[text()='احجز الآن']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
			Thread.sleep(500);
			Bk_now.click();
			book_now_Ar();
			test.log(LogStatus.PASS, "Able to proceed booking via 'Book now' from My Booking");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Facing error while proceeding booking via 'Book own' from My Booking");
		}
	}

//--- Profile Edit ---
	@Test(priority = 30, invocationCount = 1)
	public void Profileedit_Ar() throws InterruptedException, NullPointerException, IOException {
		test = report.startTest("Edit Profile verification");

		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
			Actions builder = new Actions(driver);
			builder.moveToElement(menu).build().perform();
			new WebDriverWait(driver, 5);
			// wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM
			// wEvDo']")));
			WebElement menuoption = driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
			menuoption.click();
			multiScreens.multiScreenShot(driver);

			WebElement fname = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/input"));
			fname.clear();
			fname.sendKeys("Test");

			WebElement lname = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div[2]/input"));
			lname.clear();
			lname.sendKeys("User");

			((JavascriptExecutor) driver).executeScript("scroll(0,400)");

			multiScreens.multiScreenShot(driver);
			WebElement prsave = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[6]/div"));
			prsave.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Edit Profile: PASS");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		}

		catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to Edit Profile");
		}
	}

	@Test(priority = 31, invocationCount = 1)

	public void Signoff() throws InterruptedException, IOException {
		test = report.startTest("Signout");
//Signout
		try {
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu1 = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
			menu1.click();
			// Actions builder1=new Actions(driver);
			// builder1.moveToElement(menu1).build().perform();
			// WebDriverWait wait2=new WebDriverWait(driver,5);
			// wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM
			// wEvDo']")));
			WebElement menu_signout = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[3]"));
			menu_signout.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Signout: PASS");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Facing error while proceeding signout");
		}

		driver.quit();
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		// report.close();
	}
}