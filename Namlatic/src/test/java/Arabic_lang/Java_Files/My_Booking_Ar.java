package Arabic_lang.Java_Files;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class My_Booking_Ar extends Namlatec_Login_Ar_old {

	public void Booking_Ar() throws InterruptedException, AWTException, IOException {
		// selecting my booking menu from right top menu

		// Selecting view booking ad Book now functionalities
		view_booking_Ar();
		book_now_Ar();

		// Post execution result section
		System.out.println("My Booking: PASS");
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
	}

	public void pg_launch_Ar() throws InterruptedException, IOException {
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement menu = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
		menu.click();
		WebElement menuoption = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
	}

	public void view_booking_Ar() throws IOException, InterruptedException {
		// View Booking
		pg_launch_Ar();
		WebElement view_book_id = driver
				.findElement(By.xpath("//div[contains(text(),'NAPK6YBH8R7')]/following::div[text()='انظر الحجز']"));
		view_book_id.click();
		multiScreens.multiScreenShot(driver);

		driver.getWindowHandle();
		String mainWindowHandle1 = driver.getWindowHandle();
		Set<String> allWindowHandles1 = driver.getWindowHandles();
		Iterator<String> iterator1 = allWindowHandles1.iterator();
		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator1.hasNext()) {
			String ChildWindow1 = iterator1.next();
			if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
				driver.switchTo().window(ChildWindow1);

			}
		}
		// Scroll Down in View Booking Details page
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		// WebDriverWait wait=new WebDriverWait(driver,50);
		Thread.sleep(5000);
		multiScreens.multiScreenShot(driver);

		// Click on Download Voucher link

		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,2000)", "");
		// WebElement download_Vou =driver.findElement(By.xpath("//*[(@class, 'sc-plWPA
		// hwKzKa']"));
		Thread.sleep(5000);
		WebElement download_Vou = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
		download_Vou.click();

		Thread.sleep(5000);

		// Click on Email Voucher link
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,2000)", "");
		Thread.sleep(5000);
		WebElement send_email = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
		send_email.click();

		Thread.sleep(5000);
		multiScreens.multiScreenShot(driver);

		/*
		 * //Click on Print Voucher link
		 * ((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
		 * Thread.sleep(5000); WebElement print_Vou=driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
		 * print_Vou.click();
		 * 
		 * Thread.sleep(5000); Robot esc_print_window = new Robot();
		 * esc_print_window.keyPress(KeyEvent.VK_ESCAPE);
		 * esc_print_window.keyRelease(KeyEvent.VK_ESCAPE);
		 * 
		 * 
		 * 
		 */

	}

	public void book_now_Ar() throws InterruptedException, IOException {
		pg_launch_Ar();
		WebElement Bk_now = driver
				.findElement(By.xpath("//div[contains(text(),'N8SREUE8863')]/following::div[text()='احجز الآن']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
		Thread.sleep(500);
		Bk_now.click();
		Hotel_Booking_Ar bking_Ar = new Hotel_Booking_Ar();
		bking_Ar.book_now_Ar();
	}

}
