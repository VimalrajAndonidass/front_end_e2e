package Arabic_lang.Java_Files;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Namlatec_Login_Ar_old extends Main {
	public static WebDriver driver = new ChromeDriver();
	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Users\\avimalraj\\eclipse-workspace\\Namlatic\\", "Arabic");

	public void login_Ar() throws Exception {

		launch_Ar();
		header_Ar();
		home_page_text_Ar();
		carousel_Ar();
		why_choose_us_Ar();
		help_Ar();
		covid_Ar();
		footer_Ar();
		screenshots_Ar();
		lgn_Ar();
		System.out.println("Login: PASS");
	}

	public void launch_Ar() throws IOException {
		System.out.println("Arabic");
		System.out.println("***********");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);

		driver.getTitle();
		String ExpectedTitle = "Namlatic";
		try {
			Assert.assertEquals(ExpectedTitle, driver.getTitle());
			System.out.println("Assert Title Result - Pass");
		} catch (AssertionError e) {
			System.out.println("Title mismatches with" + e);
		}

		// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]"));
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='عربي')]"));
		chglang.click();

	}

	public void header_Ar() {
		// Header Verification
		// Logo verification
		WebElement Header_Logo = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/a/img"));
		try {
			Header_Logo.isDisplayed();
			System.out.println("Header Namlatic logo is available");
		} catch (AssertionError e) {
			System.out.println("Header Namlatic logo is not available");
		}

		// Login
		// Ensure the availability of Login button
		try {
			driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]")).isEnabled();
			System.out.println("Login button available for selection");
		} catch (Exception e) {
			System.out.println("Login button is not available to select");
		}
		// Enroll hotel
		// Ensure the availability of Enroll hotel
		try {
			driver.findElement(By.xpath("//*[(text()='تسجيل الفندق')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='تسجيل الفندق')]")).isEnabled();
			System.out.println("Enroll Hotel button available for selection");
		} catch (Exception e) {
			System.out.println("Enroll Hotel button is not available to select");
		}
		// Language Verification
		// Ensure the availability of Language options
		try {
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isDisplayed();
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isEnabled();
			driver.findElement(By.xpath("//*[(text()='عربي')]")).isSelected();
			System.out.println("Language Arabic is selected");
		} catch (Exception e) {
			System.out.println("Someother language is selected other than Arabic");
		}
		// Ensure the availability of Currency options
		try {
			WebElement currency = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]"));
			for (int i = 0; i < 2; i++) {
				System.out.println(currency.getText());
				// Assert.assertEquals(i, currency.getText());
			}
			// driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled();
			// driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
			System.out.println("Language English is selected");
		} catch (Exception e) {
			System.out.println("Someother language is selected other than English");
		}

		// Ensure the availability of Enroll hotel
		try {
			driver.findElement(By.xpath("//*[conatins(placeholder(),'اخترأين تريد أن تقيم ؟')]")).isDisplayed();
			System.out.println("Elastic search option is available and allowed to search");
		} catch (Exception e) {
			System.out.println("Elastic search option is not available");
		}
	}

	public void home_page_text_Ar() throws IOException, InterruptedException {
		// Home page text validation
		try {
			String Exptext1 = "المنصة الجزائرية \n" + "  الأولى لحجز الفنادق";
			String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[1]"))
					.getText());
			System.out.println(Acttext1);
			Assert.assertEquals(Exptext1, Acttext1);

			String Exptext2 = "ابق معنا ، و اشعر كأنك في بيتك";
			String Acttext2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[2]"))
					.getText();
			Assert.assertEquals(Exptext2, Acttext2);

			String Exptext3 = "عش تجربة لا تُنسى في فندق أحلامك مع Namlatic.";
			String Acttext3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[3]"))
					.getText();
			Assert.assertEquals(Exptext3, Acttext3);

			System.out.println("Home Page text verified, Pass");
		} catch (AssertionError e) {
			System.out.println("Home page text mismatches with expected");
		}

		// Ensure the availability of search button
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"))
					.isDisplayed();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"))
					.isEnabled();
			System.out.println("Search button is available and allowed to select");
		} catch (Exception e) {
			System.out.println("Search button is not available or not able to selectable");
		}

	}

	public void carousel_Ar() throws IOException, InterruptedException {
		// Ensure the title text of carousel image
		try {
			WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[1]"));
			String Expected_title = "لديك وجهة ، سنوفر لك المكان المثالي للإقامة!";
			String Actual_title = title.getText();
			System.out.println(title.getText());
			Assert.assertEquals(Expected_title, Actual_title);
			System.out.println("Carousel title is matching with expected");
		} catch (Exception e) {
			System.out.println("Carousel title mismatching with expected");
		}

		// Carousel image1 title verification
		WebElement img1_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[1]/div/div"));
		String Act_img1_title = img1_title.getText();
		String Exp_img1_title = "الجزائر";
		try {
			Assert.assertEquals(Exp_img1_title, Act_img1_title);
			System.out.println("Title matching with expected: " + Exp_img1_title);
		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img1_title + "Expected Title:"
					+ Exp_img1_title);
		}

		// Carousel image1 text verification
		WebElement Cr_img1 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[5]/div/wrapper/div[2]"));
		String Act_img1_text = Cr_img1.getText();
		String Exp_img1_text = "إكتشفْ الجزائر العاصمة، \"البيضاء\"، مدينة أحلام البحر الأبيض المتوسط.";
		try {
			Assert.assertEquals(Exp_img1_text, Act_img1_text);
			System.out.println("Carousel image 1 'Alger' text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img1_text + "Actual:" + Act_img1_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image2 title verification
		WebElement img2_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[1]/div/div"));
		String Act_img2_title = img2_title.getText();
		String Exp_img2_title = "عنابة";
		try {
			Assert.assertEquals(Exp_img2_title, Act_img2_title);
			System.out.println("Title matching with expected: " + Exp_img2_title);
		} catch (Exception e) {
			System.out.println("Title mismatching with expected: " + "Acual Title:" + Act_img2_title + "Expected Title:"
					+ Exp_img2_title);
		}

		// Carousel image2 text verification
		WebElement Cr_img2 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[6]/div/wrapper/div[2]"));
		String Act_img2_text = Cr_img2.getText();
		String Exp_img2_text = "كل جمال الجزائر ، بين الكورنيش, الواجهة البحرية ووسط المدينة التاريخي ، عنابة وجهة كاملة يجب أن ترضيك";
		try {
			Assert.assertEquals(Exp_img2_text, Act_img2_text);
			System.out.println("Carousel image 2 'Annaba' text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img2_text + "Actual:" + Act_img2_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image3 title verification
		WebElement img3_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[1]/div/div"));
		String Act_img3_title = img3_title.getText();
		String Exp_img3_title = "بجاية";
		try {
			Assert.assertEquals(Exp_img3_title, Act_img3_title);
			System.out.println("Title matching with expected: " + Exp_img3_title);
		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img3_title + "Expected Title:"
					+ Exp_img3_title);
		}

		// Carousel image3 text verification
		WebElement Cr_img3 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[7]/div/wrapper/div[2]"));
		String Act_img3_text = Cr_img3.getText();
		String Exp_img3_text = "مدينة الجزائر الساحلية ، أكبر مدينة في منطقة القبائل ، مدينة رائعة ، مشرقة كما تريد ، ويحدها البحر.";
		try {
			Assert.assertEquals(Exp_img3_text, Act_img3_text);
			System.out.println("Carousel image 3 'Bejaia' text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img3_text + "Actual:" + Act_img3_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image4 title verification
		WebElement img4_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[1]/div/div"));
		String Act_img4_title = img4_title.getText();
		String Exp_img4_title = "قسنطينة";
		try {
			Assert.assertEquals(Exp_img4_title, Act_img4_title);
			System.out.println("Title matching with expected: " + Exp_img4_title);
		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img4_title + "Expected Title:"
					+ Exp_img4_title);
		}

		// Carousel image4 text verification
		WebElement Cr_img4 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[8]/div/wrapper/div[2]"));
		String Act_img4_text = Cr_img4.getText();
		String Exp_img4_text = "مدينة الجزائر الساحرة ، الملقبة بـ \"مدينة الجسور المعلقة\" ، ممر أساسي في الجزائر ، ربما بسبب جمالها الذي لا مثيل له!";
		try {
			Assert.assertEquals(Exp_img4_text, Act_img4_text);
			System.out.println("Carousel image 4 'Constantine' text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img4_text + "Actual:" + Act_img4_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image5 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img5_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[1]/div/div"));
		System.out.println(img5_title.getText());
		String Act_img5_title = img5_title.getText();
		String Exp_img5_title = "تيزي وزو";
		try {
			Assert.assertEquals(Exp_img5_title, Act_img5_title);
			System.out.println("Title matching with expected: " + Exp_img5_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img5_title + "Expected Title:"
					+ Exp_img5_title);
		}

		// Carousel image5 text verification
		WebElement Cr_img5 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[15]/div/wrapper/div[2]"));
		String Act_img5_text = Cr_img5.getText();
		String Exp_img5_text = "مدينة الجزائر الجميلة في وسط الطبيعة ، تعني \"طوق مكنسة\". بين البحر والجبل. مدينة نموذجية ، سرية ، لكنها خارجة عن المألوف!";

		try {
			Assert.assertEquals(Exp_img5_text, Act_img5_text);
			System.out.println("Carousel image 5 'Tizi ouzou' text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img5_text + "Actual:" + Act_img5_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image6 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img6_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[1]/div/div"));
		String Act_img6_title = img6_title.getText();
		String Exp_img6_title = "تيبازة";
		try {
			Assert.assertEquals(Exp_img6_title, Act_img6_title);
			System.out.println("Title matching with expected: " + Exp_img6_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img6_title + "Expected Title:"
					+ Exp_img6_title);
		}

		// Carousel image6 text verification
		WebElement Cr_img6 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[14]/div/wrapper/div[2]"));
		String Act_img6_text = Cr_img6.getText();
		String Exp_img6_text = "كنز مدينة جزائرية ، وجهة مميزة للسياح ، بين البحر والجبال ، نجد أنفسنا حول مناظر طبيعية رائعة ترضي العيون!";

		try {
			Assert.assertEquals(Exp_img6_text, Act_img6_text);
			System.out.println("Carousel image 6" + "'" + Exp_img6_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img6_text + "Actual:" + Act_img6_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image7 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img7_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[1]/div/div"));
		String Act_img7_title = img7_title.getText();
		String Exp_img7_title = "عـيـن تـمـوشـنـت";
		try {
			Assert.assertEquals(Exp_img7_title, Act_img7_title);
			System.out.println("Title matching with expected: " + Exp_img7_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img7_title + "Expected Title:"
					+ Exp_img7_title);
		}

		// Carousel image7 text verification
		WebElement Cr_img7 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[13]/div/wrapper/div[2]"));
		String Act_img7_text = Cr_img7.getText();
		String Exp_img7_text = "اكتشف لؤلؤة الساحل الغربي ، واستمتع بزيارة العديد من المعالم التاريخية والشواطئ التي تزين الساحل الطويل";
		try {
			Assert.assertEquals(Exp_img7_text, Act_img7_text);
			System.out.println("Carousel image 7" + "'" + Exp_img7_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img7_text + "Actual:" + Act_img7_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image8 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img8_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[1]/div/div"));
		String Act_img8_title = img8_title.getText();
		String Exp_img8_title = "سكيكدة";
		try {
			Assert.assertEquals(Exp_img8_title, Act_img8_title);
			System.out.println("Title matching with expected: " + Exp_img8_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img8_title + "Expected Title:"
					+ Exp_img8_title);
		}

		// Carousel image8 text verification
		WebElement Cr_img8 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[12]/div/wrapper/div[2]"));
		String Act_img8_text = Cr_img8.getText();
		String Exp_img8_text = "مدينة الفراولة ، المعروفة سابقًا باسم فيليبفيل، مدينة ساحلية في شرق الجزائر ، تتميز بواجهة بحرية واسعة ووسط مدينة نابض بالحياة";
		System.out.println(Act_img8_text);

		try {
			Assert.assertEquals(Exp_img8_text, Act_img8_text);
			System.out.println("Carousel image 8" + "'" + Exp_img8_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img8_text + "Actual:" + Act_img8_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image9 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img9_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[1]/div/div"));
		System.out.println(img9_title.getText());
		String Act_img9_title = img9_title.getText();
		String Exp_img9_title = "وهران";
		try {
			Assert.assertEquals(Exp_img9_title, Act_img9_title);
			System.out.println("Title matching with expected: " + Exp_img9_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img9_title + "Expected Title:"
					+ Exp_img9_title);
		}

		// Carousel image9 text verification
		WebElement Cr_img9 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[11]/div/wrapper/div[2]"));
		String Act_img9_text = Cr_img9.getText();
		String Exp_img9_text = "وهران أو البهية، من أجمل المدن في الجزائر. زيارة آثارها اللامعة ونظرتها المذهلة على البحر الأبيض المتوسط";

		try {
			Assert.assertEquals(Exp_img9_text, Act_img9_text);
			System.out.println("Carousel image 9" + "'" + Exp_img9_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img9_text + "Actual:" + Act_img9_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image10 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img10_title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[1]/div/div"));
		System.out.println(img10_title.getText());
		String Act_img10_title = img10_title.getText();
		String Exp_img10_title = "مستغانم";
		try {
			Assert.assertEquals(Exp_img10_title, Act_img10_title);
			System.out.println("Title matching with expected: " + Exp_img10_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img10_title + "Expected Title:"
					+ Exp_img10_title);
		}

		// Carousel image10 text verification
		WebElement Cr_img10 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[10]/div/wrapper/div[2]"));
		String Act_img10_text = Cr_img10.getText();
		String Exp_img10_text = "واحدة من أجمل المدن الساحلية في الجزائر ، المكان المثالي لعزل نفسك عن العالم والاستمتاع بالبحر ومتعة المدينة البسيطة.";

		try {
			Assert.assertEquals(Exp_img10_text, Act_img10_text);
			System.out.println("Carousel image 10" + "'" + Exp_img10_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img10_text + "Actual:" + Act_img10_text);
		}
		multiScreens.multiScreenShot(driver);

		// Carousel image11 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@id='Path_441']")).click();
		Thread.sleep(3000);
		WebElement img11_title = driver.findElement(By
				.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[1]/div/div"));
		System.out.println(img11_title.getText());
		String Act_img11_title = img11_title.getText();
		String Exp_img11_title = "سطيف";
		try {
			Assert.assertEquals(Exp_img11_title, Act_img11_title);
			System.out.println("Title matching with expected: " + Exp_img11_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:"
					+ Exp_img11_title);
		}

		// Carousel image11 text verification
		WebElement Cr_img11 = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[3]/div/div[2]/div/div/div/div[9]/div/wrapper/div[2]"));
		String Act_img11_text = Cr_img11.getText();
		String Exp_img11_text = "سطيف ، مدينة قديمة ورومانية تقع بين سلسلة أطلس تيليان والصحراء. ومن الواضح أن موقع \"جميلا\" ، جوهرة من التراث الثقافي ، سوف يكشف عن كنوز غير متوقعة";

		try {
			Assert.assertEquals(Exp_img11_text, Act_img11_text);
			System.out.println("Carousel image 11" + "'" + Exp_img11_title + "'" + "text is matching with expected");
		} catch (Exception e) {
			System.out.println("Not Matching, Expected:" + Exp_img11_text + "Actual:" + Act_img11_text);
		}
		multiScreens.multiScreenShot(driver);
	}

	public void why_choose_us_Ar() {
		// Why choose us!
		// Title Verification
		String act_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[1]"))
				.getText();
		String exp_title = "لماذا تختارنا ؟";
		try {
			Assert.assertEquals(exp_title, act_title);
			System.out.println("Title matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Easy Booking
		String act_sec1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[1]"))
				.getText();
		String exp_sec1 = "سهولة الحجز";

		try {
			Assert.assertEquals(exp_sec1, act_sec1);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String act_bdy1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[2]"))
				.getText();
		String exp_bdy1 = "يجب أن تتضمن عملية الحجز الحد الأدنى من الخطوات.";

		try {
			Assert.assertEquals(exp_bdy1, act_bdy1);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Friendly Interfaces
		String act_sec2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[1]"))
				.getText();
		String exp_sec2 = "واجهات ودية";

		try {
			Assert.assertEquals(exp_sec2, act_sec2);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String act_bdy2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[2]"))
				.getText();
		String exp_bdy2 = "محرك حجز الفنادق سهل الاستخدام.";

		try {
			Assert.assertEquals(exp_bdy2, act_bdy2);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Wide Payment options
		String act_sec3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[1]"))
				.getText();
		String exp_sec3 = "خيارات دفع واسعة";

		try {
			Assert.assertEquals(exp_sec3, act_sec3);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String act_bdy3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[2]"))
				.getText();
		String exp_bdy3 = "الدفع المحلي و العالمي مقبولين";

		try {
			Assert.assertEquals(exp_bdy3, act_bdy3);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Customized cancellation
		String act_sec4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[1]"))
				.getText();
		String exp_sec4 = "الإلغاء المخصص";

		try {
			Assert.assertEquals(exp_sec4, act_sec4);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String act_bdy4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[2]"))
				.getText();
		String exp_bdy4 = "يمكنك الإلغاء مجانًا وفقًا للسياسات المعمول بيها";

		try {
			Assert.assertEquals(exp_bdy4, act_bdy4);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Free cancellation
		String act_sec5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[1]"))
				.getText();
		String exp_sec5 = "أول منصة جزائرية لحجز الفنادق مع خيار الدفع المحلي والإلغاء المجاني";

		try {
			Assert.assertEquals(exp_sec5, act_sec5);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String act_bdy5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[2]"))
				.getText();
		String exp_bdy5 = "* تطبق الشروط والأحكام على سياسات الإلغاء";

		try {
			Assert.assertEquals(exp_bdy5, act_bdy5);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
	}

	public void help_Ar() {
		// Help Section
		String act_help_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[1]"))
				.getText();
		String exp_help_title = "نحن على استعداد لمساعدتك وتسهيل عملية الحجز الخاصة بك.\n"
				+ " ما عليك سوى اتباع خطواتنا والحصول على كل شيء بسهولة.";

		try {
			Assert.assertEquals(exp_help_title, act_help_title);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Location Section
		String act_help1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[2]"))
				.getText();
		System.out.println(act_help1);
		String exp_help1 = "اختر الموقع الذي تريد البقاء فيه";

		try {
			Assert.assertEquals(exp_help1, act_help1);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String exp_help1_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[3]")).getText();
		String act_help1_con = "احجز اختيارك";

		try {
			Assert.assertEquals(exp_help1_con, act_help1_con);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Choice
		String exp_help2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[2]"))
				.getText();
		String act_help2 = "احجز اختيارك";

		try {
			Assert.assertEquals(exp_help2, act_help2);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String exp_help2_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[3]")).getText();
		String act_help2_con = "من خلال اجراء صفقة فقط يمكنك الحصول على جميع التسهيلات";
		try {
			Assert.assertEquals(exp_help2_con, act_help2_con);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Checkin
		String exp_help3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[2]"))
				.getText();
		String act_help3 = "تحقق في";

		try {
			Assert.assertEquals(exp_help3, act_help3);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
		String exp_help3_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[3]")).getText();
		String act_help3_con = "تعال إلى الفندق مع بطاقة الهوية وإثبات الدفع واستمتع بإقامتك.";
		try {
			Assert.assertEquals(exp_help3_con, act_help3_con);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
	}

	public void covid_Ar() {
		// Covid Measures
		// Title verififcation
		String exp_COVID_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[1]")).getText();
		String act_COVID_title = "تدابير COVID-19 على فنادق Namlatic";

		try {
			Assert.assertEquals(exp_COVID_title, act_COVID_title);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Sub Title
		String exp_COVID_subtitle = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[1]")).getText();
		String act_COVID_subtitle = "فنادق Namlatic تحت التدابير الوقائية لـ COVID19";
		try {
			Assert.assertEquals(exp_COVID_subtitle, act_COVID_subtitle);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 1
		String exp_COVID_mes1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[1]/wrapper/div[2]")).getText();
		String act_COVID_mes1 = "100٪ آمن ومعقم";
		try {
			Assert.assertEquals(exp_COVID_mes1, act_COVID_mes1);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 2
		String exp_COVID_mes2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[2]/wrapper/div[2]")).getText();
		String act_COVID_mes2 = "قناع ، لاتكس ، قفازات لجميع العاملين";
		try {
			Assert.assertEquals(exp_COVID_mes2, act_COVID_mes2);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 3
		String exp_COVID_mes3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[3]/wrapper/div[2]")).getText();
		String act_COVID_mes3 = "قناع ، لاتكس ، قفازات لجميع العاملين";
		try {
			Assert.assertEquals(exp_COVID_mes3, act_COVID_mes3);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 4
		String exp_COVID_mes4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[4]/wrapper/div[2]")).getText();
		String act_COVID_mes4 = "تطهير كل ساعتين";
		try {
			Assert.assertEquals(exp_COVID_mes4, act_COVID_mes4);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 5
		String exp_COVID_mes5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[5]/wrapper/div[2]")).getText();
		String act_COVID_mes5 = "التحقق من درجة الحرارة إلزامي لجميع الضيوف وكذلك الموظفين";
		System.out.println(exp_COVID_mes5);
		try {
			Assert.assertEquals(exp_COVID_mes5, act_COVID_mes5);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 6
		String exp_COVID_mes6 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[6]/wrapper/div[2]")).getText();
		String act_COVID_mes6 = "الكتان / الأواني مغسولة في 70 درجة مئوية + ماء";
		try {
			Assert.assertEquals(exp_COVID_mes6, act_COVID_mes6);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 7
		String exp_COVID_mes7 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[7]/wrapper/div[2]")).getText();
		String act_COVID_mes7 = "كيماويات تنظيف المستشفيات مستخدمة في العقار";
		try {
			Assert.assertEquals(exp_COVID_mes7, act_COVID_mes7);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}

		// Measure 8
		String exp_COVID_mes8 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div[2]/div[8]/wrapper/div[2]")).getText();
		String act_COVID_mes8 = "التباعد الاجتماعي في الاستقبال والمصعد والمناطق العامة والحدائق";
		try {
			Assert.assertEquals(exp_COVID_mes8, act_COVID_mes8);
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
		}
	}

	public void footer_Ar() throws InterruptedException, IOException {
		// Footer Verification
		// Namlatic logo verification
		WebElement footer1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/a/img"));
		try {
			footer1.isDisplayed();
			System.out.println("Namlatic logo available in Footer");
		} catch (Exception e) {
			System.out.println("Logo missing in Footer");
		}

		// About us
		String exp_footer2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[1]"))
				.getText();
		String act_footer2 = "معلومات عنا";
		try {
			Assert.assertEquals(exp_footer2, act_footer2);
			System.out.println("About us is available in footer");
		} catch (AssertionError e) {
			System.out.println("About us is not available in footer");

		}

		// Terms & conditions
		String exp_footer3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[2]"))
				.getText();
		String act_footer3 = "الأحكام والشروط";
		try {
			Assert.assertEquals(exp_footer3, act_footer3);
			System.out.println("Terms and Conditions is available in footer");
		} catch (AssertionError e) {
			System.out.println("Terms and Conditions is not available in footer");

		}

		// Privacy Policy
		String exp_footer4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[3]"))
				.getText();
		String act_footer4 = "سياسة الخصوصية";
		try {
			Assert.assertEquals(exp_footer4, act_footer4);
			System.out.println("Privacy Policy is available in footer");
		} catch (AssertionError e) {
			System.out.println("Privacy Policy is not available in footer");

		}

		// Namlatic help center
		String exp_footer5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[1]/div[4]"))
				.getText();
		String act_footer5 = "مركز مساعدة Namlatic";
		try {
			Assert.assertEquals(exp_footer5, act_footer5);
			System.out.println("Namlatic Help Center is available in footer");
		} catch (AssertionError e) {
			System.out.println("Namlatic Help Center is not available in footer");
		}

		// Follow Us
		String exp_footer6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]")).getText();
		String act_footer6 = "تابعنا";
		try {
			Assert.assertEquals(exp_footer6, act_footer6);
			System.out.println("Follow us is available in footer");
		} catch (AssertionError e) {
			System.out.println("Follow us is not available in footer");
		}

		// Instagram launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement insta = driver.findElement(By.xpath("//*[name()='svg'][3]"));
		insta.click();

		// Instagram URL verification
		// Get handles of the windows
		String MainWindow = driver.getWindowHandle();
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_insta_title = "https://www.instagram.com/namlatic/";

		try {
			Assert.assertEquals(exp_insta_title, driver.getCurrentUrl());
			System.out.println("Navigating to Namlatic Instagram page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Instagram page is not loaded or loading to improper URL");
		}
		driver.switchTo().window(MainWindow);

		// Youtube launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement YT = driver.findElement(By.xpath("//*[name()='svg'][4]"));
		YT.click();

		driver.getWindowHandle();
		String mainWindowHandle1 = driver.getWindowHandle();
		Set<String> allWindowHandles1 = driver.getWindowHandles();
		Iterator<String> iterator1 = allWindowHandles1.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator1.hasNext()) {
			String ChildWindow1 = iterator1.next();
			if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
				driver.switchTo().window(ChildWindow1);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_YT_title = "https://www.youtube.com/channel/UCqHvyjnCAW6FyOmmcr78fRw";

		try {
			Assert.assertEquals(exp_YT_title, driver.getCurrentUrl());
			System.out.println("Navigating to Namlatic Youtube page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Youtube page is not loaded or loading to improper URL");
		}
		driver.switchTo().window(MainWindow);

		// LinkedIN launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement IN = driver.findElement(By.xpath("//*[name()='svg'][5]"));
		IN.click();

		// LinkedIN URL verification
		// Get handles of the windows
		String MainWindow2 = driver.getWindowHandle();
		String mainWindowHandle2 = driver.getWindowHandle();
		Set<String> allWindowHandles2 = driver.getWindowHandles();
		Iterator<String> iterator2 = allWindowHandles2.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator2.hasNext()) {
			String ChildWindow2 = iterator2.next();
			if (!mainWindowHandle2.equalsIgnoreCase(ChildWindow2)) {
				driver.switchTo().window(ChildWindow2);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_IN_title1 = "NAMLATIC | LinkedIn";
		String exp_IN_title2 = "Signup | LinkedIN";
		if (driver.getTitle().equals(exp_IN_title1)) {
			System.out.println("Navigating to Namlatic LinkedIN page");
		} else if (driver.getTitle().equals(exp_IN_title2)) {
			System.out.println("Navigating to Namlatic LinkedIN page");
		} else {
			System.out.println("Namlatic LinkedIN page is not loaded or loading to improper URL");
		}
		Thread.sleep(3000);
		driver.switchTo().window(MainWindow2);

		// Twitter launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement TW = driver
				.findElement(By.xpath("//div[contains(text(),'تابعنا')]/following::div//*[name()='svg'][1]"));
		TW.click();

		// Twitter URL verification
		// Get handles of the windows
		String MainWindow3 = driver.getWindowHandle();
		String mainWindowHandle3 = driver.getWindowHandle();
		Set<String> allWindowHandles3 = driver.getWindowHandles();
		Iterator<String> iterator3 = allWindowHandles3.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator3.hasNext()) {
			String ChildWindow3 = iterator3.next();
			if (!mainWindowHandle3.equalsIgnoreCase(ChildWindow3)) {
				driver.switchTo().window(ChildWindow3);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_TW_title = "https://twitter.com/Namla_officiel";
		try {
			Assert.assertEquals(exp_TW_title, driver.getCurrentUrl());
			System.out.println("Navigating to Namlatic Twitter page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Twitter page is not loaded or loading to improper URL");
		}
		driver.switchTo().window(MainWindow3);

		// Facebook
		// Facebook launch verification
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement FB = driver
				.findElement(By.xpath("//div[contains(text(),'تابعنا')]/following::div//*[name()='svg'][2]"));
		FB.click();

		// Facebook URL verification
		// Get handles of the windows
		String MainWindow4 = driver.getWindowHandle();
		String mainWindowHandle4 = driver.getWindowHandle();
		Set<String> allWindowHandles4 = driver.getWindowHandles();
		Iterator<String> iterator4 = allWindowHandles4.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator4.hasNext()) {
			String ChildWindow4 = iterator4.next();
			if (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
				driver.switchTo().window(ChildWindow4);
			}
		}
		multiScreens.multiScreenShot(driver);
		/*
		 * try { // Assert.assertEquals(exp_FB_title, driver.getCurrentUrl());
		 * System.out.println("Navigating to Namlatic Facebook page"); }
		 * catch(AssertionError e) { System.out.
		 * println("Namlatic Facebook page is not loaded or loading to improper URL"); }
		 */
		driver.switchTo().window(MainWindow4);

		// About us
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'معلومات عنا')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow6 = driver.getWindowHandle();
		String mainWindowHandle6 = driver.getWindowHandle();
		Set<String> allWindowHandles6 = driver.getWindowHandles();
		Iterator<String> iterator6 = allWindowHandles6.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator6.hasNext()) {
			String ChildWindow6 = iterator6.next();
			if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
				driver.switchTo().window(ChildWindow6);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_about_title = "https://business.namlatic.com/";
		try {
			Assert.assertEquals(exp_about_title, driver.getCurrentUrl());
			System.out.println("Navigating to Namlatic About us page");
		} catch (AssertionError e) {
			System.out.println("Namlatic About us page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,5200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,5600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,6000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,6400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.switchTo().window(MainWindow6);

		// Terms & Conditions
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'الأحكام والشروط')]")).click();
		String exp_TC_title = "Namlatic";
		try {
			Assert.assertEquals(exp_TC_title, driver.getTitle());
			System.out.println("Navigating to Namlatic Terms & Condition page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Terms & Conditions page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Privacy Policy
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'سياسة الخصوصية')]")).click();

		String exp_PP_title = "Namlatic";
		try {
			Assert.assertEquals(exp_PP_title, driver.getTitle());
			System.out.println("Navigating to Namlatic Privacy Policy page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Privacy Policy page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Help Center
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'مركز مساعدة Namlatic')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow9 = driver.getWindowHandle();
		String mainWindowHandle9 = driver.getWindowHandle();
		Set<String> allWindowHandles9 = driver.getWindowHandles();
		Iterator<String> iterator9 = allWindowHandles9.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator9.hasNext()) {
			String ChildWindow9 = iterator9.next();
			if (!mainWindowHandle9.equalsIgnoreCase(ChildWindow9)) {
				driver.switchTo().window(ChildWindow9);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_help_title1 = "Login - Jira Service Management";

		try {
			Assert.assertEquals(exp_help_title1, driver.getTitle());
			System.out.println("Navigating to Namlatic Help Center page");
		} catch (AssertionError e) {
			System.out.println("Namlatic Help Center page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		driver.switchTo().window(MainWindow9);

		// Card verification
		WebElement exp_footer7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/div[2]"));
		try {
			exp_footer7.isDisplayed();
			System.out.println("We accept Visa, American express, Master card, CIB, Wire Transfer");
		} catch (Exception e) {
			System.out.println("Card section missing");
		}

	}

	public void screenshots_Ar() throws IOException, InterruptedException {
		// Screenshots
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
	}

	public void lgn_Ar() throws IOException, InterruptedException {
		// Login
		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='تسجيل الدخول')]"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
		uname.sendKeys("9047232893");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
		pwd.sendKeys("Test@123");
		multiScreens.multiScreenShot(driver);
		pwd.sendKeys(Keys.ENTER);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);

		// Hotel search
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		WebElement Hotel = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
		Hotel.sendKeys("عنابة");
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement btnclick1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
		btnclick1.click();
		multiScreens.multiScreenShot(driver);

	}
}
