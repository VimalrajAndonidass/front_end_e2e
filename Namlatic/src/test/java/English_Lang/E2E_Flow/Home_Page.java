package English_Lang.E2E_Flow;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class Home_Page {
	public static WebDriver driver = null;

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\English\\", "E2E_Flow");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void Launch() throws Exception {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\English\\" + "E2E_Flow.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException {
		test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);

		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();
	}


	@Test(priority = 2, invocationCount = 1)
	public void Header_Verification() {
		test = report.startTest("Header Verification");
		// Header Verification
		// Logo verification
		WebElement Header_Logo = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/a/img"));
		try {
			Header_Logo.isDisplayed();
			test.log(LogStatus.PASS, "Logo available - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Logo not available - Fail");
		}

		// Language Verification
		// Ensure the availability of Language options
		//driver.findElement(By.xpath("//div[@class='sc-fzoYkl hfPoTr']")).click();
		try {

			Thread.sleep(3000);
			WebElement language = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div"));
			language.click();
			WebElement chglang = language.findElement(By.xpath(
					"/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div"));
			chglang.click();
			test.log(LogStatus.PASS, "Language selection option available and selectable - Pass");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Language selection either not available or not selectable -  Fail");
		}
		
		// Ensure the availability of Currency options
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]"));
			for (int i = 0; i < 2; i++) {
				// Assert.assertEquals(i, currency.getText());
			}
			// driver.findElement(By.xpath("//*[(text()='English')]")).isEnabled();
			// driver.findElement(By.xpath("//*[(text()='English')]")).isSelected();
			test.log(LogStatus.PASS, "Currency selection option is available and selectable - Pass");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Currency selection either not available or not selectable - Fail");
		}

		// Ensure the availability of Elastic search
		try {
			driver.findElement(By
					.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[1]/div/div[1]/div/input"))
					.isDisplayed();
			test.log(LogStatus.PASS, "Elastic search option is available and allowed to search");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Elastic search option is not available");
		}
		//driver.findElement(By.xpath("//div[@class='sc-fznNvL gSYtBG']")).click();
		
		// Login
				// Ensure the availability of Login button
				try {
					driver.findElement(By.xpath("//*[(text()='Login / Signup')]")).isDisplayed();
					driver.findElement(By.xpath("//*[(text()='Login / Signup')]")).isEnabled();
					test.log(LogStatus.PASS, "Login Button available - Pass");
				} catch (Exception e) {
					test.log(LogStatus.FAIL, "Login Button not available - Fail");
				}
				
				// Enroll hotel
				// Ensure the availability of Enroll hotel
				try {
					driver.findElement(By.xpath("//*[(text()='Enroll Hotels')]")).isDisplayed();
					driver.findElement(By.xpath("//*[(text()='Enroll Hotels')]")).isEnabled();
					test.log(LogStatus.PASS, "Enroll Hotel button available - Pass");
				} catch (Exception e) {
					test.log(LogStatus.FAIL, "Enroll Hotel button not available - Fail");
				}
				
				// Enroll Corporate
				// Ensure the availability of Enroll Corporate
				try {
					driver.findElement(By.xpath("//*[(text()='Enroll Corporate')]")).isDisplayed();
					driver.findElement(By.xpath("//*[(text()='Enroll Corporate')]")).isEnabled();
					test.log(LogStatus.PASS, "Enroll Corporate button available - Pass");
				} catch (Exception e) {
					test.log(LogStatus.FAIL, "Enroll Corporate button not available - Fail");
				}
	}

	@Test(priority = 3, invocationCount = 1)
	public void Title_Verification() throws IOException, InterruptedException {
		test = report.startTest("Title Verification");
		
		// Title Verification
		Thread.sleep(3000);
		String ExpectedTitle = "Namlatic hotel booking - Hotel booking platform";
		try {
			AssertJUnit.assertEquals(ExpectedTitle, driver.getTitle());
			test.log(LogStatus.PASS, "Title Matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatches" + ExpectedTitle + " expected :" + driver.getTitle());
		}
	}
	
	@Test(priority = 4, invocationCount = 1)
	public void Home_page_text_Verification() throws IOException, InterruptedException {
		test = report.startTest("Home Page Verification");
		// Home page text validation
		String Exptext1 = "Algeria's First Hotel Booking Platform";
		String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[1]"))
				.getText());

		String Exptext2 = "Stay with us, feel at home";
		String Acttext2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[2]"))
				.getText();

		String Exptext3 = "Live an unforgettable experience in your dream hotel with Namlatic";
		String Acttext3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[3]"))
				.getText();
		try {
			AssertJUnit.assertEquals(Exptext1, Acttext1);
			AssertJUnit.assertEquals(Exptext2, Acttext2);
			AssertJUnit.assertEquals(Exptext3, Acttext3);
			test.log(LogStatus.PASS, "Home Page text verified, Pass");
		} catch (AssertionError e) {

			test.log(LogStatus.FAIL, "Home page text mismatches with expected ");
		}

		// Ensure the availability of search button
		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.isDisplayed();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.isEnabled();
			test.log(LogStatus.PASS, "Search button is available and allowed to select");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Search button either not available or not able to selectable");
		}
		// Removing hubspot
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
				Thread.sleep(2000);
				multiScreens.multiScreenShot(driver);
				driver.findElement(By.xpath("/html/body/div[2]/div[1]/span/div/button")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("/html/body/div[2]/div[1]/span/div/button")).click();
				System.out.println("Within iFrame");
				driver.switchTo().parentFrame(); // Parent Frame
	}

	@Test(priority = 5, invocationCount = 1)
	public void Carousel_Verification() throws IOException, InterruptedException {
		test = report.startTest("Carousel section Verification");
		// Ensure the title text of carousel image

		// Carousel image1 title verification

		WebElement img1_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[5]/div/wrapper/div/div[1]/div"));
		String Act_img1_title = img1_title.getText();
		String Exp_img1_title = "Alger";
		try {
			AssertJUnit.assertEquals(Exp_img1_title, Act_img1_title);
			System.out.println("Title matching with expected: " + Exp_img1_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img1_title);
		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img1_title + "Expected Title:"
					+ Exp_img1_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img1_title
					+ "Expected Title:" + Exp_img1_title);
		}

		// Carousel image2 title verification
		WebElement img2_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[6]/div/wrapper/div/div[1]/div"));
		String Act_img2_title = img2_title.getText();
		String Exp_img2_title = "Annaba";
		try {
			AssertJUnit.assertEquals(Exp_img2_title, Act_img2_title);
			System.out.println("Title matching with expected: " + Exp_img2_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img2_title);

		} catch (Exception e) {
			System.out.println("Title mismatching with expected: " + "Acual Title:" + Act_img2_title + "Expected Title:"
					+ Exp_img2_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected:" + "Acual Title:" + Act_img2_title
					+ "Expected Title:" + Exp_img2_title);

		}

		// Carousel image3 title verification
		WebElement img3_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[7]/div/wrapper/div/div[1]/div"));
		String Act_img3_title = img3_title.getText();
		String Exp_img3_title = "Bejaia";
		try {
			AssertJUnit.assertEquals(Exp_img3_title, Act_img3_title);
			System.out.println("Title matching with expected: " + Exp_img3_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img3_title);

		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img3_title + "Expected Title:"
					+ Exp_img3_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img3_title
					+ "Expected Title:" + Exp_img3_title);

		}

		// Carousel image4 title verification
		WebElement img4_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[8]/div/wrapper/div/div[1]/div"));
		String Act_img4_title = img4_title.getText();
		String Exp_img4_title = "Constantine";
		System.out.println(Act_img4_title);
		try {
			AssertJUnit.assertEquals(Exp_img4_title, Act_img4_title);
			System.out.println("Title matching with expected: " + Exp_img4_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img4_title);

		} catch (Exception e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img4_title + "Expected Title:"
					+ Exp_img4_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img4_title
					+ "Expected Title:" + Exp_img4_title);
		}

		// Carousel image5 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		Thread.sleep(3000);
		WebElement next = driver.findElement(By.xpath("//*[@rx='17.5'][1]"));
		next.click();
		Thread.sleep(3000);
		WebElement img5_title = driver.findElement(
				By.xpath("//*[text()='Setif']"));
		String Act_img5_title = img5_title.getText();
		String Exp_img5_title = "Setif";
		System.out.println(Act_img5_title);
		try {
			AssertJUnit.assertEquals(Exp_img5_title, Act_img5_title);
			System.out.println("Title matching with expected: " + Exp_img5_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img5_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img5_title + " Expected Title:"
					+ Exp_img5_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img5_title
					+ " Expected Title:" + Exp_img5_title);

		}

		// Carousel image6 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img6_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[10]/div/wrapper/div/div[1]/div"));
		String Act_img6_title = img6_title.getText();
		String Exp_img6_title = "Mostaganem";
		try {
			AssertJUnit.assertEquals(Exp_img6_title, Act_img6_title);
			System.out.println("Title matching with expected: " + Exp_img6_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img6_title);
		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img6_title + "Expected Title:"
					+ Exp_img6_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img6_title
					+ "Expected Title:" + Exp_img6_title);
		}

		// Carousel image7 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img7_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[11]/div/wrapper/div/div[1]/div"));
		String Act_img7_title = img7_title.getText();
		String Exp_img7_title = "Oran";
		try {
			AssertJUnit.assertEquals(Exp_img7_title, Act_img7_title);
			System.out.println("Title matching with expected: " + Exp_img7_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img7_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img7_title + "Expected Title:"
					+ Exp_img7_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img7_title
					+ "Expected Title:" + Exp_img7_title);
		}

		// Carousel image8 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");

		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img8_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[12]/div/wrapper/div/div[1]/div"));
		String Act_img8_title = img8_title.getText();
		String Exp_img8_title = "Skikda";
		try {
			AssertJUnit.assertEquals(Exp_img8_title, Act_img8_title);
			System.out.println("Title matching with expected: " + Exp_img8_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img8_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img8_title + "Expected Title:"
					+ Exp_img8_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img8_title
					+ "Expected Title:" + Exp_img8_title);
		}

		// Carousel image9 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img9_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[13]/div/wrapper/div/div[1]/div"));
		String Act_img9_title = img9_title.getText();
		String Exp_img9_title = "Ain Temouchent";
		try {
			AssertJUnit.assertEquals(Exp_img9_title, Act_img9_title);
			System.out.println("Title matching with expected: " + Exp_img9_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img9_title);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img9_title
					+ "Expected Title:" + Exp_img9_title);

			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img9_title + "Expected Title:"
					+ Exp_img9_title);
		}

		// Carousel image10 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img10_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[14]/div/wrapper/div/div[1]/div"));
		String Act_img10_title = img10_title.getText();
		String Exp_img10_title = "Tipaza";
		try {
			AssertJUnit.assertEquals(Exp_img10_title, Act_img10_title);
			System.out.println("Title matching with expected: " + Exp_img10_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img10_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img10_title + "Expected Title:"
					+ Exp_img10_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img10_title
					+ "Expected Title:" + Exp_img10_title);

		}

		// Carousel image11 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img11_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[15]/div/wrapper/div/div[1]/div"));
		String Act_img11_title = img11_title.getText();
		String Exp_img11_title = "Tizi ouzou";
		try {
			AssertJUnit.assertEquals(Exp_img11_title, Act_img11_title);
			System.out.println("Title matching with expected: " + Exp_img11_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img11_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:"
					+ Exp_img11_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title
					+ "Expected Title:" + Exp_img11_title);

		}

		// Carousel image12 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img12_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[16]/div/wrapper/div/div[1]/div"));
		String Act_img12_title = img12_title.getText();
		String Exp_img12_title = "Ghardaia";
		try {
			AssertJUnit.assertEquals(Exp_img12_title, Act_img12_title);
			System.out.println("Title matching with expected: " + Exp_img12_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img12_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:"
					+ Exp_img12_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title
					+ "Expected Title:" + Exp_img12_title);

		}

		// Carousel image13 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img13_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[17]/div/wrapper/div/div[1]/div"));
		String Act_img13_title = img13_title.getText();
		String Exp_img13_title = "Tamanrasset";
		try {
			AssertJUnit.assertEquals(Exp_img13_title, Act_img13_title);
			System.out.println("Title matching with expected: " + Exp_img13_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img13_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:"
					+ Exp_img13_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title
					+ "Expected Title:" + Exp_img13_title);

		}

		// Carousel image14 title verification
		((JavascriptExecutor) driver).executeScript("scroll(0,401)");
		driver.findElement(By.xpath("//*[@rx='17.5'][1]")).click();
		Thread.sleep(3000);
		WebElement img14_title = driver.findElement(
				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div/div[18]/div/wrapper/div/div[1]/div"));
		String Act_img14_title = img14_title.getText();
		String Exp_img14_title = "Beni Abbes";
		try {
			AssertJUnit.assertEquals(Exp_img14_title, Act_img14_title);
			System.out.println("Title matching with expected: " + Exp_img14_title);
			test.log(LogStatus.PASS, "Title matching with expected: " + Exp_img14_title);

		} catch (AssertionError e) {
			System.out.println("Title mismatching with expected" + "Acual Title:" + Act_img11_title + "Expected Title:"
					+ Exp_img14_title);
			test.log(LogStatus.FAIL, "Title mismatching with expected" + "Acual Title:" + Act_img11_title
					+ "Expected Title:" + Exp_img14_title);

		}
		multiScreens.multiScreenShot(driver);
	}

	@Test(priority = 6, invocationCount = 1, enabled = false)
	public void Why_choose_us_Verification() throws InterruptedException, IOException {
		test = report.startTest("Why Choose us section verification");
		// Why choose us!
		// Title Verification
		String exp_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[1]"))
				.getText();
		String act_title = "Why choose us!";

		try {
			AssertJUnit.assertEquals(exp_title, act_title);
			System.out.println("Title matching");
			test.log(LogStatus.PASS, "Title matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Title Mismatching");
		}

		// Easy Booking
		String exp_sec1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[1]"))
				.getText();
		String act_sec1 = "Easy Booking";

		try {
			AssertJUnit.assertEquals(exp_sec1, act_sec1);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Easy Booking - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Easy Booking - Not Matching");
		}
		String exp_bdy1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[1]/wrapper/div[2]"))
				.getText();
		String act_bdy1 = "The booking process should include minimal steps.";

		try {
			AssertJUnit.assertEquals(exp_bdy1, act_bdy1);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Easy Booking Content - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Easy Booking Content - Not Matching");
		}

		// Friendly Interfaces
		String exp_sec2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[1]"))
				.getText();
		String act_sec2 = "Friendly Interfaces";

		try {
			AssertJUnit.assertEquals(exp_sec2, act_sec2);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Friendly Interface Title - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Friendly Interface Title - Not Matching");

			System.out.println("Not Matching");
		}
		String exp_bdy2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[2]/wrapper/div[2]"))
				.getText();
		String act_bdy2 = "A hotel booking engine with a good user-friendly.";

		try {
			AssertJUnit.assertEquals(exp_bdy2, act_bdy2);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Friendly Interface content - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Friendly Interface content - Not Matching");
		}

		// Wide Payment options
		String exp_sec3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[1]"))
				.getText();
		String act_sec3 = "Wide payment options";

		try {
			AssertJUnit.assertEquals(exp_sec3, act_sec3);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Wide Payment Title - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Wide Payment Title - Not Matching");

			System.out.println("Not Matching");
		}
		String exp_bdy3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[3]/wrapper/div[2]"))
				.getText();
		String act_bdy3 = "Domestic and international payments accepted";

		try {
			AssertJUnit.assertEquals(exp_bdy3, act_bdy3);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Wide Payment content - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Wide Payment content - Not Matching");
		}

		// Customized cancellation
		String exp_sec4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[1]"))
				.getText();
		String act_sec4 = "Customized Cancellation";

		try {
			AssertJUnit.assertEquals(exp_sec4, act_sec4);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Customized Cancellation - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Customized Cancellation - Not Matching");
		}
		String exp_bdy4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[4]/wrapper/div[2]"))
				.getText();
		String act_bdy4 = "You can cancel at free of cost according to policies";

		try {
			AssertJUnit.assertEquals(exp_bdy4, act_bdy4);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Customized Cancellation content - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Customized Cancellation content - Not Matching");
		}

		// Free cancellation
		String exp_sec5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[1]"))
				.getText();
		String act_sec5 = "Algeria's First Platform for Hotel Booking with local payment option and Free cancellation";

		try {
			AssertJUnit.assertEquals(exp_sec5, act_sec5);
			test.log(LogStatus.PASS, "Free Cancellation Title - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Free Cancellation Title - Not Matching");

		}
		String exp_bdy5 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div[5]/wrapper/div[2]"))
				.getText();
		String act_bdy5 = "*Terms and Condition applied on Cancellation Policies";

		try {
			AssertJUnit.assertEquals(exp_bdy5, act_bdy5);
			test.log(LogStatus.PASS, "Free Cancellation content - Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Free Cancellation content - Not Matching");
		}
		


	}

	@Test(priority = 7, invocationCount = 1, enabled=false)
	public void Explore_section() throws IOException, InterruptedException {
		// Removing hubspot
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		Thread.sleep(2000);
		
		  driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
		  driver.switchTo().frame(driver.findElement(By.
		  xpath("//iframe[@title='chat widget']"))); Thread.sleep(2000);
		  driver.switchTo().parentFrame(); // Parent Frame
		 

		test = report.startTest("Blogs verification");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div[2]/div[1]/div/div/div/a/div")).click();
		String MainWindow4 = driver.getWindowHandle();
		String mainWindowHandle4 = driver.getWindowHandle();
		Set<String> allWindowHandles4 = driver.getWindowHandles();
		Iterator<String> iterator4 = allWindowHandles4.iterator();
		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		try {
			while (iterator4.hasNext()) {
				String ChildWindow4 = iterator4.next();
				if (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
					driver.switchTo().window(ChildWindow4);
				}
			}
			multiScreens.multiScreenShot(driver);
			String ExpExplore1 = "https://blog.namlatic.com/pourquoi-il-faut-envisager-le-tourisme-en-algerie/";
			AssertJUnit.assertEquals(driver.getCurrentUrl(), ExpExplore1);
			driver.switchTo().window(MainWindow4);
			test.log(LogStatus.PASS, "Blog 1 launching successfully");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Blog 1 URL is incorrect");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div[2]/div[2]/div[1]/div/div/div/a/div"))
				.click();
		String MainWindow5 = driver.getWindowHandle();
		String mainWindowHandle5 = driver.getWindowHandle();
		Set<String> allWindowHandles5 = driver.getWindowHandles();
		Iterator<String> iterator5 = allWindowHandles5.iterator();

		try {
			while (iterator5.hasNext()) {
				String ChildWindow5 = iterator5.next();
				if (!mainWindowHandle5.equalsIgnoreCase(ChildWindow5)) {
					driver.switchTo().window(ChildWindow5);
				}
			}
			multiScreens.multiScreenShot(driver);
			String ExpExplore2 = "https://blog.namlatic.com/comment-faire-du-tourisme-a-constantine-avec-un-petit-budget/";
			Thread.sleep(2000);

			AssertJUnit.assertEquals(driver.getCurrentUrl(), ExpExplore2);
			Thread.sleep(2000);

			driver.switchTo().window(MainWindow5);
			test.log(LogStatus.PASS, "Blog 2 launching successfully");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Blog 2 URL is incorrect");
		}

		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div[2]/div[2]/div[2]/div/div/div/a/div")).click();
		String MainWindow6 = driver.getWindowHandle();
		String mainWindowHandle6 = driver.getWindowHandle();
		Set<String> allWindowHandles6 = driver.getWindowHandles();
		Iterator<String> iterator6 = allWindowHandles6.iterator();

		try {
			while (iterator6.hasNext()) {
				String ChildWindow6 = iterator6.next();
				if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
					driver.switchTo().window(ChildWindow6);
				}
			}
			multiScreens.multiScreenShot(driver);
			String ExpExplore3 = "https://blog.namlatic.com/pourquoi-faut-il-explorer-le-sahara-algerien-et-ses-merveilles/";
			AssertJUnit.assertEquals(driver.getCurrentUrl(), ExpExplore3);
			driver.switchTo().window(MainWindow6);
			test.log(LogStatus.PASS, "Blog 3 launching successfully");
			driver.switchTo().window(MainWindow6);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Blog 3 URL is incorrect");
		}
	}

	@Test(priority = 8, invocationCount = 1, enabled=false)
	public void Help_Section_Verification() throws InterruptedException, IOException {
		test = report.startTest("Help section verification");
		// Help Section
		String exp_help_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[1]"))
				.getText();
		String act_help_title = "We are ready to help you and ease your booking process.\n"
				+ " Just follow our steps and get everything easily.";

		try {
			AssertJUnit.assertEquals(exp_help_title, act_help_title);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section Title - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Help section Title - Not Matching");

		}

		// Location Section
		String exp_help1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[2]"))
				.getText();
		String act_help1 = "Choose the location you want to stay in";

		try {
			AssertJUnit.assertEquals(exp_help1, act_help1);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section1 Title - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Help section1 Title - Not Matching");

		}
		String exp_help1_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[1]/div[3]")).getText();
		String act_help1_con = "Choose a hotel from the selected city";
		try {
			AssertJUnit.assertEquals(exp_help1_con, act_help1_con);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section1 content - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Help section1 content - Not Matching");

		}

		// Choice
		String exp_help2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[2]"))
				.getText();
		String act_help2 = "Book your choice";

		try {
			AssertJUnit.assertEquals(exp_help2, act_help2);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section2 title - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section2 title - Not Matching");
			System.out.println("Not Matching");
		}
		String exp_help2_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[2]/div[3]")).getText();
		String act_help2_con = "Only by making a transaction you have managed to get all the facilities.";
		try {
			AssertJUnit.assertEquals(exp_help2_con, act_help2_con);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section2 content - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Help section2 content - Not Matching");

		}

		// Checkin
		String exp_help3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[2]"))
				.getText();
		String act_help3 = "Check-in";

		try {
			AssertJUnit.assertEquals(exp_help3, act_help3);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section3 Title - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "Help section3 Title - Not Matching");

		}
		String exp_help3_con = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[5]/div/div[2]/div[3]/div[3]")).getText();
		String act_help3_con = "Come to hotel with ID and proof of payment and enjoy your stay.";
		try {
			AssertJUnit.assertEquals(exp_help3_con, act_help3_con);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "Help section3 content - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Help section3 content - Not Matching");

			System.out.println("Not Matching");
		}
		
	}

	@Test(priority = 9, invocationCount = 1)
	public void Covid_Measures_Verification() {
		test = report.startTest("Covid Measure section verification");
		// Covid Measures
		// Title verififcation
		String exp_COVID_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[1]")).getText();
		String act_COVID_title = "Namlatic hotels are under COVID19 preventive measures";

		try {
			AssertJUnit.assertEquals(exp_COVID_title, act_COVID_title);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "COVID Measure Title - Matching");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure Title - Not Matching");

			System.out.println("Not Matching");
		}

		// Sub Title
		String exp_COVID_subtitle = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[2]")).getText();
		String act_COVID_subtitle = "100% Safe & Disinfected";
		try {
			AssertJUnit.assertEquals(exp_COVID_subtitle, act_COVID_subtitle);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "COVID Measure Sub_Title - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "COVID Measure Sub_Title - Not Matching");

		}

		// Measure 1
		String exp_COVID_mes1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[3]/div[1]/div[2]")).getText();
		String act_COVID_mes1 = "Mask, latex, gloves for all staffs";
		try {
			AssertJUnit.assertEquals(exp_COVID_mes1, act_COVID_mes1);
			test.log(LogStatus.PASS, "COVID Measure_1 - Matching");

			System.out.println("Matching");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "COVID Measure_1 -Not Matching");
		}

		// Measure 2
		String exp_COVID_mes2 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[3]/div[2]/div[2]")).getText();
		String act_COVID_mes2 = "Temperature check mandatory for all guest as well as staffs";
		try {
			AssertJUnit.assertEquals(exp_COVID_mes2, act_COVID_mes2);
			test.log(LogStatus.PASS, "COVID Measure_2 - Matching");
			System.out.println("Matching");
		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "COVID Measure_2 -Not Matching");

		}

		// Measure 3
		String exp_COVID_mes3 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[3]/div[3]/div[2]")).getText();
		String act_COVID_mes3 = "Linen/ Utensils washed in 70 c + water";
		try {
			AssertJUnit.assertEquals(exp_COVID_mes3, act_COVID_mes3);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "COVID Measure_3 - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "COVID Measure_3 -Not Matching");

		}

		// Measure 4
		String exp_COVID_mes4 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[6]/div/div[3]/div[4]/div[2]")).getText();
		String act_COVID_mes4 = "Social distancing at reception,lift, common areas & gardens";
		try {
			AssertJUnit.assertEquals(exp_COVID_mes4, act_COVID_mes4);
			System.out.println("Matching");
			test.log(LogStatus.PASS, "COVID Measure_4 - Matching");

		} catch (Exception e) {
			System.out.println("Not Matching");
			test.log(LogStatus.FAIL, "COVID Measure_4 -Not Matching");

		}


	}

	@Test(priority = 10)
	public void Footer_Verification() throws InterruptedException, IOException {
		test = report.startTest("Footer Verification");
		// Footer Verification
		// Namlatic logo verification
		WebElement footer1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[1]/a/img"));
		try {
			footer1.isDisplayed();
			System.out.println("Namlatic logo available in Footer");
			test.log(LogStatus.PASS, "Namlatic logo available in Footer");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Namlatic logo missing in Footer");
			System.out.println("Logo missing in Footer");
		}

		// About us
		String exp_footer2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[1]")).getText();
		String act_footer2 = "About us";
		try {
			AssertJUnit.assertEquals(exp_footer2, act_footer2);
			test.log(LogStatus.PASS, "About us is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "About us is not available in footer");
			System.out.println("About us is not available in footer");
		}

		// Terms & conditions
		String exp_footer3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[2]")).getText();
		String act_footer3 = "Terms and Conditions";
		try {
			AssertJUnit.assertEquals(exp_footer3, act_footer3);
			test.log(LogStatus.PASS, "Terms and Conditions is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Terms and Conditions is not available in footer");
		}

		// Privacy Policy
		String exp_footer4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[3]")).getText();
		String act_footer4 = "Privacy Policy";
		try {
			AssertJUnit.assertEquals(exp_footer4, act_footer4);
			test.log(LogStatus.PASS, "Privacy Policy is available in footer");
		} catch (AssertionError e) {
			System.out.println("Privacy Policy is not available in footer");
			test.log(LogStatus.FAIL, "Privacy Policy is not available in footer");
		}

		// Namlatic help center
		String exp_footer5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[4]")).getText();
		String act_footer5 = "Namlatic Help Center";
		try {
			AssertJUnit.assertEquals(exp_footer5, act_footer5);
			test.log(LogStatus.PASS, "Namlatic Help Center is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Help Center is available in footer");
		}

		// FAQ
		String exp_footer_FAQ = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[2]/div[5]"))
				.getText();
		String act_footer_FAQ = "FAQ";
		try {
			AssertJUnit.assertEquals(exp_footer_FAQ, act_footer_FAQ);
			test.log(LogStatus.PASS, "FAQ is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "FAQ is available in footer");
		}

		// Follow Us
		String exp_footer6 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[4]/div/div[1]"))
				.getText();
		String act_footer6 = "Follow us";
		try {
			AssertJUnit.assertEquals(exp_footer6, act_footer6);
			test.log(LogStatus.PASS, "Follow us is available in footer");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Follow us is not available in footer");
		}

		// Social media logo verification
		// FB
		WebElement FB = driver
				.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][1]"));
		try {
			FB.isDisplayed();
			test.log(LogStatus.PASS, "FB icon available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "FB is not available");
		}

		// Twitter
		WebElement Twitter = driver
				.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][2]"));
		try {
			Twitter.isDisplayed();
			test.log(LogStatus.PASS, "Twitter icon available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Twitter is not available");
		}

		// Linkedin
		WebElement Linkedin = driver.findElement(By.xpath("//*[name()='svg'][3]"));
		try {
			Linkedin.isDisplayed();
			test.log(LogStatus.PASS, "Linkedin icon available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Linkedin is not available");
		}

		// Instagram
		WebElement Instagram = driver.findElement(By.xpath("//*[name()='svg'][4]"));
		try {
			Instagram.isDisplayed();
			test.log(LogStatus.PASS, "Instagram icon available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Instagram is not available");
		}

		// Youtube
		WebElement Youtube = driver.findElement(By.xpath("//*[name()='svg'][5]"));
		try {
			Youtube.isDisplayed();
			test.log(LogStatus.PASS, "Youtube icon available");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Youtube is not available");
		}

		
		 
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000); 
		//Instant booking
		 driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
		/*  driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click(); 
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).
		  click(); System.out.println("Within iFrame");
		 driver.switchTo().parentFrame(); // Parent Frame*/

		// Facebook
		// Facebook launch verification

		WebElement FB_logo = driver
				.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][1]"));
		FB_logo.click();

		// Facebook URL verification
		// Get handles of the windows
		String MainWindow4 = driver.getWindowHandle();
		String mainWindowHandle4 = driver.getWindowHandle();
		Set<String> allWindowHandles4 = driver.getWindowHandles();
		Iterator<String> iterator4 = allWindowHandles4.iterator();
		try {
			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator4.hasNext()) {
				String ChildWindow4 = iterator4.next();
				if (!mainWindowHandle4.equalsIgnoreCase(ChildWindow4)) {
					driver.switchTo().window(ChildWindow4);
				}
			}
			multiScreens.multiScreenShot(driver);
			String exp_FB_title1 = "https://www.facebook.com/Namlatic";
			String exp_FB_title2 = "https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FNamlatic";
			try {
				AssertJUnit.assertEquals(driver.getCurrentUrl(), exp_FB_title1);
				test.log(LogStatus.PASS, "Navigating to Namlatic Facebook page");
			} catch (AssertionError e) {
				Assert.assertEquals(driver.getCurrentUrl(), exp_FB_title2);
				test.log(LogStatus.PASS, "Navigating to Namlatic Facebook page");
			} catch (Error e) {
				test.log(LogStatus.FAIL, "Namlatic Facebook page is not loaded or loading to improper URL");
			}

			Thread.sleep(3000);

			driver.switchTo().window(MainWindow4);
			test.log(LogStatus.PASS, "Facebook URL launched successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Facebook URL not launched successfully");
		}

		// Twitter launch verification

		Thread.sleep(2000);
		// WebElement TW =
		// driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[4]/div/div[2]/svg[2]/path"));
		WebElement TW = driver
				.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][2]"));
		TW.click();

		// Twitter URL verification
		// Get handles of the windows
		String MainWindow3 = driver.getWindowHandle();
		String mainWindowHandle3 = driver.getWindowHandle();
		Set<String> allWindowHandles3 = driver.getWindowHandles();
		Iterator<String> iterator3 = allWindowHandles3.iterator();
		try {
			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator3.hasNext()) {
				String ChildWindow3 = iterator3.next();
				if (!mainWindowHandle3.equalsIgnoreCase(ChildWindow3)) {
					driver.switchTo().window(ChildWindow3);
				}
			}
			multiScreens.multiScreenShot(driver);
			String exp_TW_title = "https://twitter.com/Namla_officiel";
			try {
				AssertJUnit.assertEquals(exp_TW_title, driver.getCurrentUrl());
				test.log(LogStatus.PASS, "Navigating to Namlatic Twitter page");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Namlatic Twitter page is not loaded or loading to improper URL");
			}
			driver.switchTo().window(MainWindow3);
			test.log(LogStatus.PASS, "Twitter URL launched successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Twitter URL launched successfully");
		}

		/*
		 * //LinkedIN launch verification
		 *
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
		 * Thread.sleep(2000); WebElement IN = driver.findElement(By.
		 * xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][3]"
		 * )); IN.click();
		 *
		 * //LinkedIN URL verification //Get handles of the windows String MainWindow2 =
		 * driver.getWindowHandle(); String mainWindowHandle2 =
		 * driver.getWindowHandle(); Set<String> allWindowHandles2 =
		 * driver.getWindowHandles(); Iterator<String> iterator2 =
		 * allWindowHandles2.iterator(); try { // Here we will check if child window has
		 * other child windows and will fetch the heading of the child window while
		 * (iterator2.hasNext()) { String ChildWindow2 = iterator2.next(); if
		 * (!mainWindowHandle2.equalsIgnoreCase(ChildWindow2)) {
		 * driver.switchTo().window(ChildWindow2); }}
		 * multiScreens.multiScreenShot(driver); String exp_IN_title1 =
		 * "NAMLATIC | LinkedIn"; String exp_IN_title2 = "Sign In | LinkedIn"; String
		 * exp_IN_title3 ="Security Verification | LinkedIn"; try {
		 * AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title1);
		 * test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page"); } catch
		 * (AssertionError e) {
		 * AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title2);
		 * test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page"); } catch
		 * (Error e) { AssertJUnit.assertEquals(driver.getTitle(),exp_IN_title3);
		 * test.log(LogStatus.PASS,"Navigating to Namlatic LinkedIN page"); } catch
		 * (Exception e) {
		 * test.log(LogStatus.FAIL,"Not Navigating to Namlatic LinkedIN page"); }
		 * test.log(LogStatus.PASS,"LinkedIN URL launched successfully");
		 * driver.switchTo().window(MainWindow2); } catch (AssertionError e) {
		 * test.log(LogStatus.
		 * FAIL,"Namlatic LinkedIN page is not loaded or loading to improper URL"); }
		 * Thread.sleep(3000);
		 */
		// Instagram launch verification

		WebElement insta = driver
				.findElement(By.xpath("//div[contains(text(),'Follow us')]/following::div//*[name()='svg'][4]"));
		insta.click();

		// Instagram URL verification
		// Get handles of the windows
		String MainWindow6 = driver.getWindowHandle();
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		try {
			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
				if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
					driver.switchTo().window(ChildWindow);
				}
			}
			multiScreens.multiScreenShot(driver);

			String exp_insta_url = "https://www.instagram.com/namlatic/";
			try {
				AssertJUnit.assertEquals(driver.getCurrentUrl(), exp_insta_url);
				test.log(LogStatus.PASS, "Navigating to Namlatic Instagram page");
			} catch (Error e) {
				test.log(LogStatus.FAIL, "Namlatic Instagram page is not loaded or loading to improper URL");
			}
			test.log(LogStatus.PASS, "Instagram URL launched successfully");
			driver.switchTo().window(MainWindow6);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Instagram URL not launched successfully");
		}

		// Youtube launch verification

		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		WebElement YT = driver.findElement(By.xpath("//*[name()='svg'][5]"));
		YT.click();

		// Youtube URL verification
		// Get handles of the windows
		String MainWindow = driver.getWindowHandle();
		String mainWindowHandle1 = driver.getWindowHandle();
		Set<String> allWindowHandles1 = driver.getWindowHandles();
		Iterator<String> iterator1 = allWindowHandles1.iterator();
		try {
			// Here we will check if child window has other child windows and will fetch the
			// heading of the child window
			while (iterator1.hasNext()) {
				String ChildWindow1 = iterator1.next();
				if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
					driver.switchTo().window(ChildWindow1);
				}
			}
			multiScreens.multiScreenShot(driver);
			String exp_YT_title = "https://www.youtube.com/channel/UCqHvyjnCAW6FyOmmcr78fRw";

			try {
				AssertJUnit.assertEquals(exp_YT_title, driver.getCurrentUrl());
				System.out.println(driver.getCurrentUrl());
				test.log(LogStatus.PASS, "Navigating to Namlatic Youtube page");
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Namlatic Youtube page is not loaded or loading to improper URL");
			}
			test.log(LogStatus.PASS, "Youtube URL launched successfully");
			driver.switchTo().window(MainWindow);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Youtube URL not launched successfully");
		}

		// About us
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[contains(text(),'About us')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow7 = driver.getWindowHandle();
		String mainWindowHandle6 = driver.getWindowHandle();
		Set<String> allWindowHandles6 = driver.getWindowHandles();
		Iterator<String> iterator6 = allWindowHandles6.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator6.hasNext()) {
			String ChildWindow6 = iterator6.next();
			if (!mainWindowHandle6.equalsIgnoreCase(ChildWindow6)) {
				driver.switchTo().window(ChildWindow6);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_about_title = "https://business.namlatic.com/";
		try {
			AssertJUnit.assertEquals(exp_about_title, driver.getCurrentUrl());
			test.log(LogStatus.PASS, "Navigating to Namlatic About us page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic About us page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,2800)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,4400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,6400)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.switchTo().window(MainWindow7);

		// Terms & Conditions
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'Terms and Conditions')]")).click();
		String exp_TC_title = driver.findElement(By.xpath("//*[contains(text(),'Terms & Conditions')]")).getText();
		try {
			Assert.assertNotNull(exp_TC_title);
			test.log(LogStatus.PASS, "Navigating to Namlatic Terms & Condition page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Terms & Conditions page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Privacy Policy
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'Privacy Policy')]")).click();

		String exp_PP_title = driver.findElement(By.xpath("//*[contains(text(),'Privacy Policy')]")).getText();
		try {
			Assert.assertNotNull(exp_PP_title.isEmpty());
			test.log(LogStatus.PASS, "Navigating to Namlatic Privacy Policy page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Privacy Policy page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		((JavascriptExecutor) driver).executeScript("scroll(0,3200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);

		driver.navigate().back();

		// Help Center
		// Launch verifiation
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'Help Center')]")).click();

		// URL verification
		// Get handles of the windows
		String MainWindow9 = driver.getWindowHandle();
		String mainWindowHandle9 = driver.getWindowHandle();
		Set<String> allWindowHandles9 = driver.getWindowHandles();
		Iterator<String> iterator9 = allWindowHandles9.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator9.hasNext()) {
			String ChildWindow9 = iterator9.next();
			if (!mainWindowHandle9.equalsIgnoreCase(ChildWindow9)) {
				driver.switchTo().window(ChildWindow9);
			}
		}
		multiScreens.multiScreenShot(driver);
		String exp_help_title1 = "Login - Jira Service Management";

		try {
			AssertJUnit.assertEquals(exp_help_title1, driver.getTitle());
			test.log(LogStatus.PASS, "Navigating to Namlatic Help Center page");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Namlatic Help Center page is not loaded or loading to improper URL");
		}
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		driver.switchTo().window(MainWindow9);

		// FAQ
		((JavascriptExecutor) driver).executeScript("scroll(0,3000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[contains(text(),'FAQ')]")).click();
		Thread.sleep(2000);
		String exp_FAQ_title = driver.findElement(By.xpath("//*[contains(text(),'FAQs')]")).getText();
		System.out.println(exp_FAQ_title);
		System.out.println(exp_FAQ_title);
		try {
			Assert.assertNotNull(exp_FAQ_title);
			test.log(LogStatus.PASS, "Navigating to FAQ section");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "FAQ section is not loaded or loading to improper URL");
		}

		driver.navigate().back();

		// Card verification
		WebElement exp_footer7 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div[3]/div[1]"));
		try {
			exp_footer7.isDisplayed();
			test.log(LogStatus.PASS, "We accept Visa, American express, Master card, CIB, Wire Transfer");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Card section missing");
		}

	}

	public void screenshots() throws IOException, InterruptedException {
		// Screenshots
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("scroll(0,1900)");
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
	}

	@Test(priority = 11, invocationCount = 1)
	public void Login() throws IOException, InterruptedException {
		// Login
		test = report.startTest("Login Functionality");
		try {
			Thread.sleep(3000);
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("9047232893");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123456");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}

	}

	@Test(priority = 12)
	public void Hotel_search() throws InterruptedException {
		test = report.startTest("Elastic search verification");
		// Hotel Search
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));
			Hotel.sendKeys("Louie Palace Pondicherry");
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");

			String Hotel_name = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"))
					.getText();
			System.out.println(Hotel_name);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Search working fine");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to search hotel with given keyword");
		}
		report.endTest(test);
		//driver.quit();
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		report.flush();
		report.close();
	}
}
