package English_Lang.Java_Files;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import English_Lang.E2E_Flow.Landing_Page;
import junit.framework.Assert;


public class Cancel_Booking extends Landing_Page {
	
	public void Can_Book() throws AWTException, InterruptedException, IOException
	{

		
		cancellation();
		book_now();
		System.out.println("Cancellation page loading properly");
	}

@Test
	void cancellation() throws AWTException, InterruptedException, IOException
	{
	
		My_Booking book = new My_Booking();
		book.pg_launch();
	
		Thread.sleep(3000);
				//((JavascriptExecutor)driver).executeScript("scroll(0,400)");				
				Thread.sleep(3000);
				WebElement cancel = driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[text()='Cancel booking']"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
				String Price = driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[@class='sc-pCOPB jZjRAo']")).getText().substring(1);
				float exp_price = Float.parseFloat(Price);
				double exp_p1 = Math.round(exp_price*100.0)/100;
				cancel.click();
				driver.findElement(By.id("reason-1")).click();
						
		// Validation:
				//Title verification
						String exp_title = "Cancel your booking";
						String act_title = driver.findElement(By.xpath("//div[contains(text(),'Cancel your booking')]")).getText();
						
						try
						{
							Assert.assertEquals(exp_title, act_title);
							System.out.println("Title matching");
						} catch (Exception e)
						{
							System.out.println("Title mismatching");
						}
						
				// cancell info verification
						String exp_info ="Select the room you wanted to cancel";
						String act_info = driver.findElement(By.xpath("//div[contains(text(),'Select the room you wanted to cancel')]")).getText();
						
						try
						{
							Assert.assertEquals(exp_info, act_info);
							System.out.println("Info. matching");
						} catch (Exception e)
						{
							System.out.println("Info. mismatching");
						}
						
				// Price verification
						String act_price1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[5]/div/div[3]/div[2]/div/div[2]")).getText().substring(1);
					/*	if ( driver.findElement(By.xpath("//*[@class='sc-pcHDm cOnSkc']")).isDisplayed())
							{ 
								String act_price2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[6]/div/div[3]/div[2]/div[2]/div[2]")).getText().substring(1);
								
								float actprice1f =Float.parseFloat(act_price1);
								double p1 = Math.round(actprice1f*100.0)/100;
								float actprice2f = Float.parseFloat(act_price2);
								double p2 = Math.round(actprice2f*100.0)/100;
								double act_price3= p1 + p2;
								System.out.println(act_price3);
								try
								{
									Assert.assertEquals(exp_p1, act_price3);
									System.out.println("Price matching");
								} catch(AssertionError e)
								{
									System.out.println("Price not matching");
								}
							} else
							{	*/
							try
								{
								float actprice1f =Float.parseFloat(act_price1);
								double p1 = Math.round(actprice1f*100.0)/100;
									Assert.assertEquals(exp_p1, p1);
									System.out.println("Price matching");
								} catch(AssertionError e)
								{
									System.out.println("Price not matching");
								}
							//}
								
						
						
				// Verification of cancallation title
						String exp_title1 = "Tell us your reason for cancelling booking";
						String act_title1 = driver.findElement(By.xpath("//div[contains(text(),'Tell us your reason for cancelling booking')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_title1, act_title1);
							System.out.println("Cancellation title matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation title not matching");
						}
						
				// Verification of cancallation title
						String exp_opt1 = "Change of dates / destination";
						String act_opt1 = driver.findElement(By.xpath("//*[contains(text(),'Change of dates / destination')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_opt1, act_opt1);
							System.out.println("Cancellation option 1 matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation option 1 not matching");
						}
						
				// Verification of cancallation title
						String exp_opt2 = "Personal reason";
						String act_opt2 = driver.findElement(By.xpath("//*[contains(text(),'Personal reason')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_opt2, act_opt2);
							System.out.println("Cancellation option 2 matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation option 2 not matching");
						}
					
	
				// Verification of cancallation title
						String exp_opt3 = "Found alternative accomodation";
						String act_opt3 = driver.findElement(By.xpath("//*[contains(text(),'Found alternative accomodation')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_opt3, act_opt3);
							System.out.println("Cancellation option 3 matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation option 3 not matching");
						}
			
				// Verification of cancallation title
						String exp_opt4 = "Change in number of travellers";
						String act_opt4 = driver.findElement(By.xpath("//*[contains(text(),'Change in number of travellers')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_opt4, act_opt4);
							System.out.println("Cancellation option 4 matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation option 4 not matching");
						}
	
				// Verification of cancallation title
						String exp_opt5 = "Other reason";
						String act_opt5 = driver.findElement(By.xpath("//*[contains(text(),'Other reason')]")).getText();
	
						try
						{
							Assert.assertEquals(exp_opt5, act_opt5);
							System.out.println("Cancellation option 5 matching");
						} catch(AssertionError e)
						{
							System.out.println("Cancellation option 5 not matching");
						}
						
						Robot esc = new Robot();
						esc.keyPress(KeyEvent.VK_ESCAPE);
	}

@Test
	public void book_now() throws InterruptedException, IOException
	{
		WebElement Bk_now = driver.findElement(By.xpath("//div[contains(text(),'N8SREUE8863')]/following::div[text()='Book Now']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
		Thread.sleep(500);
		Bk_now.click();
		Hotel_Booking bking = new Hotel_Booking();
		bking.booking();
	}
}
