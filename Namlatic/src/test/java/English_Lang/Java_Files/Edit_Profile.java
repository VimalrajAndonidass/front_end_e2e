package English_Lang.Java_Files;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import English_Lang.E2E_Flow.Landing_Page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebElement;

public class Edit_Profile extends Landing_Page{
	
	@Test
	public void Profileedit() throws InterruptedException, NullPointerException, IOException
	{
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
		Actions builder=new Actions(driver);
		builder.moveToElement(menu).build().perform();
		WebDriverWait wait1=new WebDriverWait(driver,5);
		//wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='sc-fzoaKM wEvDo']")));
		WebElement menuoption=driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
	
		
		
		WebElement fname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/input"));
		fname.clear();
		fname.sendKeys("Test");
	
		WebElement lname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div[2]/input"));
		lname.clear();
		lname.sendKeys("User");	
	
		((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	
		multiScreens.multiScreenShot(driver);
		WebElement prsave =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[6]/div"));
		prsave.click();
		multiScreens.multiScreenShot(driver);
		System.out.println("Edit Profile: PASS");
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		}
}
