package English_Lang.Payment_Methods;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;
import multiScreenShot.MultiScreenShot;

public class PAH {
	public static WebDriver driver = null;

	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Front_End\\English\\Booking\\", "Pay_at_Hotel");
	static ExtentTest test;
	static ExtentReports report;
	Fillo fillo = new Fillo();

	@BeforeTest
	public void Launch() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\English\\Booking\\" + "Pay_at_Hotel.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Scenario");
		try {
			test.log(LogStatus.PASS, "Booking via Pay at Hotel(English)");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException, FilloException, InterruptedException {
		test = report.startTest("URL Verification");
		// Connection open for URL launch
		Connection con = fillo.getConnection("C:\\Users\\Vimal\\eclipse-workspace\\Read_Excel\\Input.xls");
		String strquery = "Select * from URL";
		Recordset rs = con.executeQuery(strquery);
		try {
			driver.get("chrome://settings/clearBrowserData");
			rs.moveFirst();
			String link = rs.getField("link");
			System.out.println(link);
			Thread.sleep(3000);
			driver.navigate().to(link);
			driver.manage().window().maximize();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
		rs.close();
		con.close();

		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();
	}

	@Test(priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException, FilloException {
		// Login
		test = report.startTest("Login Functionality and Elastic search verification");
		// Connection open for login
		Connection con = fillo.getConnection("C:\\Users\\Vimal\\eclipse-workspace\\Read_Excel\\Input.xls");
		String strquery = "Select * from Login where User_Type='Hotel Owner'";
		Recordset rs = con.executeQuery(strquery);
		String username = null;
		String Password = null;
		List<String> data = new ArrayList<>();
		new ArrayList<>();
		List<String> data1 = new ArrayList<>();
		new ArrayList<>();

		while (rs.next()) {
			data.add(rs.getField("Uname"));
			data1.add(rs.getField("Pwd"));
			username = data.get(0);
			Password = data1.get(0);
		}

		try {
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));
			nam_Login.click();

			Thread.sleep(3000);
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys(username);

			data.add(rs.getField("Pwd"));
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys(Password);
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			Thread.sleep(2000);

			// pwd.sendKeys(Keys.ENTER);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
		rs.close();
		con.close();

		// Hotel search
		// Connection for hotel search
		Connection hotel_con = fillo.getConnection("C:\\Users\\Vimal\\eclipse-workspace\\Read_Excel\\Input.xls");
		String hotel_strquery = "Select * from Hotel where Type='Hotel2'";
		Recordset hotel_rs = hotel_con.executeQuery(hotel_strquery);
		try {
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]")).clear();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement Hotel = driver
					.findElement(By.xpath("//input[contains(@placeholder,'Choose where do you want to stay?')]"));
			// search_data.add(hotel_rs.getField("Hotel_name_City_name"));
			hotel_rs.moveFirst();
			String search = hotel_rs.getField("Hotel_name_City_name");
			Hotel.sendKeys(search);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
			Hotel.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(2000);
			Hotel.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div[4]/div/div/div[4]/div"))
					.click();
			Thread.sleep(2000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Search working fine");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}
		hotel_rs.close();
		hotel_con.close();
	}

	@Test(priority = 3, invocationCount = 1)
	void booking() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Hotel details page laoding and Hotel name verification");
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		WebElement Hotel_sel = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/wrapper[1]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]"));
		String exp_hot_name = Hotel_sel.getText();
		Hotel_sel.click();
		multiScreens.multiScreenShot(driver);
		// Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();

		// Here we will check if child window has other child windows and will fetch the
		// heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
			if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);
		String act_hot_name = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]"))
				.getText();
		try {
			Assert.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching: " + exp_hot_name);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Hotel name mismatching");
		}

	}

	@Test(priority = 4, invocationCount = 1)

	public void booking_confirmation() throws InterruptedException, IOException {
		test = report.startTest("Verification of Language, Booking date, an Room selection");

		// Currency selection
		try {
			WebElement cur = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
			Thread.sleep(500);
			cur.click();
			WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
			cur_sel.click();
			test.log(LogStatus.PASS, "Language changed successfully to DZD");
		} catch (AssertionError e) {
			test.log(LogStatus.PASS, "Unable to change Language");
		}

		// Removing hubspot
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).click();
		System.out.println("Within iFrame");
		driver.switchTo().parentFrame(); // Parent Frame
		// Date Selection
		try {
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,501)");

			// Date validation
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_YEAR, 4);
			Date futureDateTime = calendar.getTime();
			System.out.println(futureDateTime);

			Thread.sleep(2000);
			driver.findElement(By.xpath(
					"//html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div"))
					.click();
			Thread.sleep(1000);

			driver.findElement(
					By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')]"));
			driver.findElement(
					By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')][2]"));

			System.out.println("Calender selected");
			/*
			 * System.out.println("Start date:" + sel_start_date.getText());
			 * System.out.println("End date:" + sel_end_date.getText());
			 *
			 * int set_start_date = Integer.parseInt(sel_start_date.getText()) + 4;
			 * System.out.println("New start date: " + set_start_date);
			 *
			 * int set_end_date = Integer.parseInt(sel_end_date.getText()) + 4;
			 * System.out.println("New end date: " + set_end_date);
			 *
			 *
			 * driver.findElement(By.xpath(
			 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[2]/td[4]/div[2]"
			 * )) .click(); driver.findElement(By.xpath(
			 * "/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[2]/td[4]/div[2]"
			 * )) .click();
			 *
			 * /* WebElement month =
			 * driver.findElement(By.xpath("//*[contains(@class,'sc-fzoOEf dfzouv')]"));
			 * Select month_pick=new Select(month);
			 * month_pick.selectByVisibleText("October");
			 * month_pick.getFirstSelectedOption();
			 */
			Thread.sleep(500);
			WebElement start_date = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[2]/td[4]/span"));
			start_date.click();
			System.out.println("Start date selected");
			Thread.sleep(500);
			WebElement end_date = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[3]/table/tbody/tr[3]/td[5]/span"));
			end_date.click();
			System.out.println("End date selected");
			test.log(LogStatus.PASS, "Start and End date selected properly");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select start/ end date");
			// driver.close();
		}

		// Room selection
		try {
			Thread.sleep(3000);
			WebElement scroll = driver.findElement(By.xpath("//*[text()='+ 9% taxes and charges']"));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
			WebElement element = driver.findElement(By.xpath(
					"//*[contains(@class,'sc-oTpcS hKhvoq') or contains(@class,'sc-pJjas fvjHub') or contains(@class,'sc-qWeMO cNXhsk')]"));
			Select room_sel = new Select(element);
			room_sel.selectByVisibleText("1");
			room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Rooms selected successfully");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to select Rooms");
		}

		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(2000);
			WebElement book_now = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
			Thread.sleep(2000);
			book_now.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Book Now button available and able to proceed booking");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Book Now button is not available or unable to proceed Booking");
		}

	}

	@Test(priority = 5, invocationCount = 1)

	public void Guest_details() throws IOException, InterruptedException {
		test = report.startTest("Guest details updation");

		// Booking with main guest
		Thread.sleep(3000);
		WebElement Fname = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[2]/div[2]/div[2]/input"));
		String Fstname = Fname.getText();

		WebElement Lname = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[2]/div[3]/div[2]/input"));
		String Lstname = Lname.getText();

		WebElement mobileno = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[3]/div[1]/div[2]/input"));
		String Mobno = mobileno.getText();

		WebElement email = driver
				.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[1]/div[3]/div[2]/div[2]/input"));
		String mail = email.getText();
		multiScreens.multiScreenShot(driver);
		try {
			Assert.assertNotNull(Fstname);

			Assert.assertNotNull(Lstname);

			Assert.assertNotNull(Mobno);

			Assert.assertNotNull(mail);

			test.log(LogStatus.PASS, "Main Guest details are autopopulated successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Main Guest details are not autopopulating");
		}

		// Adding guest
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,800)");
			driver.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/button")).click();
			WebElement GFname = driver
					.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[5]/div[2]/div[2]/input"));
			GFname.sendKeys("Louie");
			WebElement GLname = driver
					.findElement(By.xpath("/html/body/div[1]/wrapper/div/div/div[5]/div[3]/div[2]/input"));
			GLname.sendKeys("Madhav");
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Additional Guest details are successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Additional Guest details are unable to add");
		}

		// Proceeding to Payment method selection page

		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,800)");
			WebElement Guest = driver.findElement(By.xpath("//*[text()='Proceed to select the mode of payment']"));
			Guest.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Able to proceed payment page successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to proceed payment page");
		}
	}

	@Test(priority = 5, invocationCount = 1)

	public void PAH() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Payment Method verification");
		try {
			WebElement radio_wt = driver.findElement(By.id("payhotel"));
			radio_wt.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "Pay at Hotel Payment selected");

			// actions.contextClick(elementLocator).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();

			System.out.println("Test");

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select Pay at Hotel");
		}

//Click on Book now button
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[contains(@class,'sc-bdvvtL dSbLfH')]")).click();
			test.log(LogStatus.PASS, "Payment done successfully via Pay at Hotel option");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Payment failed");
		}

		// Voucher details verification

		Thread.sleep(8000);
		WebElement Booking_ID_web = driver.findElement(By.xpath("//div[contains(@class,'sc-pKJEJ emwQLc')]"));
		String Booking_ID = Booking_ID_web.getText();
		try {
			multiScreens.multiScreenShot(driver);
			String Trans_time_date = driver.findElement(By.xpath("//div[contains(text(),'CE')]")).getText();
			WebElement Hot_name_scroll = driver.findElement(By.xpath("//div[contains(@class,'sc-oVexc durqCp')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Hot_name_scroll);
			multiScreens.multiScreenShot(driver);
			String Hotel_Name_voucher = Hot_name_scroll.getText();
			String Check_in = driver.findElement(By.xpath("//div[contains(@class,'sc-pTStT ejVUNV')]")).getText();
			String Check_out = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[1]/div[2]/div[2]"))
					.getText();
			String Guest_count = driver.findElement(By.xpath("//div[contains(@class,'sc-qOwrV hysHKf')]")).getText();
			String Room_count = driver.findElement(By.xpath("//div[contains(@class,'sc-qOwrV hysHKf')]")).getText();
			String Guest_Name = driver.findElement(By.xpath("//div[contains(@class,'sc-pZEQg cDKGcz')]")).getText();
			String Room_Type = driver.findElement(By.xpath("//div[contains(@class,'sc-pjVMd iyFleE')]")).getText();
			String Booking_amt = driver.findElement(By.xpath("//div[contains(@class,'sc-qYfxO jMKVEm')]")).getText();
			driver.findElement(By.xpath("//*[contains(@class,'sc-qYSBj hkhgWR')]")).getText();

			multiScreens.multiScreenShot(driver);

			test.log(LogStatus.PASS,
					"Payment done successfully via PAH Payment and following are the booking details"
							+ "<br/>  Booking ID: " + Booking_ID + "<br/> Transaction Time and date: " + Trans_time_date
							+ "<br/>  Hotel Name: " + Hotel_Name_voucher + "<br/>  Check In(Time and Date): " + Check_in
							+ "<br/>  Check Out(Time and Date): " + Check_out + "<br/>  Guest Count: " + Guest_count
							+ "<br/>  Room Count: " + Room_count + "<br/>  Guest name: " + Guest_Name
							+ "<br/>  Room Type: " + Room_Type + "<br/>  Booking Amount: " + Booking_amt);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Payment done successfully via PAH Payment but voucher not loaded properly and details are mismatching");
		}

		// Download Voucher
		try {

			WebElement download_Vou = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
			download_Vou.click();
			multiScreens.multiScreenShot(driver);
			((JavascriptExecutor) driver).executeScript("scroll(0,700)");
			multiScreens.multiScreenShot(driver);

			test.log(LogStatus.PASS, "Download voucher working fine");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to Download voucher");
		}

		// Email Voucher
		try {
			Thread.sleep(5000);
			WebElement send_email = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
			send_email.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Email voucher working fine");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to Email voucher");
		}

		/*
		 * // Print Voucher try { WebElement print_Vou = driver .findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
		 * print_Vou.click(); Thread.sleep(5000); Robot print_window = new Robot();
		 * print_window.keyPress(KeyEvent.VK_ENTER);
		 * print_window.keyRelease(KeyEvent.VK_ENTER); test.log(LogStatus.PASS,
		 * "Print voucher working fine"); } catch (Exception e) {
		 * test.log(LogStatus.FAIL, "Unable to Print voucher"); }
		 */

		// My Booking verification
		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(3000);
			WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[1]/div"));
			menu.click();

			WebElement menuoption = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[2]/ul/li[2]"));
			menuoption.click();
			multiScreens.multiScreenShot(driver);

			Thread.sleep(3000);
			WebElement scroll = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div"));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
			String[] Bookingid_Time = scroll.getText().split(" ");
			String Bookingid = Bookingid_Time[0];
			System.out.println(Bookingid);
			System.out.println(Booking_ID);
			System.out.println(scroll.getText());
			Assert.assertEquals(Booking_ID, Bookingid);

			test.log(LogStatus.PASS, "Hotel booking done successfully and verified " + Bookingid);

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Hotel booking is not done successfully");
		}

	}

	/*
	 *
	 * private void downloadPdf() { // TODO Auto-generated method stub
	 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 * driver.manage().window().maximize();
	 *
	 * String URL = driver.getCurrentUrl(); System.out.print(URL);
	 * //driver.get("https://www.learningcontainer.com/sample-pdf-files-for-testing"
	 * );
	 *
	 * //locator to click the pdf download link // contextClick() method for right
	 * click to an element after moving the //mouse to with the moveToElement()
	 * Actions a = new Actions(driver);
	 * a.contextClick(driver.findElement(By.xpath("*[@id=’plugin’])"))).build().
	 * perform(); }
	 */
	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
//driver.quit();
	}
}
