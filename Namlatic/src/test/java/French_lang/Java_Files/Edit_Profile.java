package French_lang.Java_Files;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import French_lang.E2E_Files.Landing_Page_Fr;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebElement;

public class Edit_Profile extends Landing_Page_Fr{
	
	public static void Profileedit() throws InterruptedException, NullPointerException, IOException
	{
		
		My_Booking_fr book= new My_Booking_fr();
		book.page_launch();
		
		WebElement fname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div[2]/input"));
		fname.clear();
		fname.sendKeys("Louie Madhav");
	
		WebElement lname =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div[2]/input"));
		lname.clear();
		lname.sendKeys("Ogistan");	
	
		((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	
		


		WebElement prsave =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div[2]/div[6]/div"));
		prsave.click();
		multiScreens.multiScreenShot(driver);
		System.out.println("Edit Profile: PASS");
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	
		}
}
