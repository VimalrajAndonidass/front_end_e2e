package French_lang.Java_Files;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import English_Lang.Java_Files.Hotel_Filter;
import French_lang.E2E_Files.Landing_Page_Fr;
import junit.framework.Assert;

public class Hotel_Booking_fr extends Landing_Page_Fr{

	public void Hotel_Book_fr() throws InterruptedException, IOException
	{
		booking_fr();
		image_fr();
		description_fr();
		roomtype_fr();
		cancellation_policy_fr();
		amenities_ver_fr();
		policy_ver_fr();
		book_now_fr();
		various_section_Validation_fr();
		
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		System.out.println("Hotel-Bookinng: PASS");
		multiScreens.multiScreenShot(driver);
	}
	
	void booking_fr() throws IOException
	{
		/*driver.navigate().to("https://test.namlatic.com/hotel-alger-holiday-inn-algiers---cheraga-tower?searchKey=Holiday%20Inn%20Algiers%20-%20Cheraga%20Tower&start=28_06_2021&end=29_06_2021&room=1&guest=1&page=0&searchId=c81473e4-83de-49d5-9e6f-3d2a1f3e5ea7&city=Alger&showSeachbar=true&translateUrl=true&language=english&currency=USD");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver); */
		
			WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div/div[2]/div[2]/div[1]/div"));	
			Hotel_sel.click(); 
			String exp_hot_name = Hotel_sel.getText();
			multiScreens.multiScreenShot(driver);
		
		//Get handles of the windows
			String MainWindow = driver.getWindowHandle();
			String mainWindowHandle = driver.getWindowHandle();
			Set<String> allWindowHandles = driver.getWindowHandles();
			Iterator<String> iterator = allWindowHandles.iterator();
		
		// Here we will check if child window has other child windows and will fetch the heading of the child window
			while (iterator.hasNext()) {
				String ChildWindow = iterator.next();
	            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
	            		driver.switchTo().window(ChildWindow);
	        }}
			multiScreens.multiScreenShot(driver);
		//Scroll bar navigation
		//((JavascriptExecutor)driver).executeScript("scroll(0,1601)");
		
		// Hotel name verification
			Hotel_Filter hot = new Hotel_Filter();
			String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
			
			try
			{
				Assert.assertEquals(exp_hot_name, act_hot_name);
				System.out.println("Hotel name matching:" + exp_hot_name);
			} catch (AssertionError e)
			{
				System.out.println("Hotel name mismatching");
				driver.close();
			}
	}
	
	public void image_fr()
	{
		//Image verification
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[3]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[1]/div[2]/div")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[3]/div/svg[2]")).click();
			//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/svg")).click();
	}
	

	void description_fr()
	{
		// Description verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[3]")).isDisplayed();
				System.out.println("Hotel description is available");
			} catch (Exception e)
			{
				System.out.println("Description missing");
				
			}
	}
	
	void roomtype_fr()
	{
			
		//Type of rooms section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]")).isDisplayed();
				System.out.println("Type of rooms section is available");
			} catch (Exception e)
			{
				System.out.println("Types of rooms section missing");
			}
			
		  //Room type images verification	
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/div/img")).isDisplayed();
				System.out.println("Room type images are available");
			} catch (Exception e)
			{
				System.out.println("Images are missing");
			}
			
		 //Room type sharing details verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[1]/div[1]/div")).isDisplayed();
				System.out.println("Sharing details available");
			} catch (Exception e)
			{
				System.out.println("Sharing details are not available");
			}	
			
		// Amenities section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]")).isDisplayed();
				System.out.println("Amenities section available");
			} catch (Exception e)
			{
				System.out.println("Amenities section not available");
			}
			
		// Rate section verification
			try
			{
				String rate = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).getText();
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]")).isDisplayed();
				//driver.findElement(By.xpath("//div [contians(text(),'د٠ج')]")).isDisplayed();
				System.out.println("Selected currency" + rate);
				System.out.println("Rate section available");
			} catch (Exception e)
			{
				System.out.println("Rate section not available");
			}
		
		// Add Room section verification
			try
			{
				driver.findElement(By.xpath("//div[contains(text(),'Add room')]")).isDisplayed();
				System.out.println("Add room section available");
			} catch (Exception e)
			{
				System.out.println("Add room section not available");
			}	
			
			((JavascriptExecutor)driver).executeScript("scroll(0,'In case of any malicious/hostile act by the end client within the property premises with any person found or reported will be subjected to cancellation of booking on spot, with no refund of money as per hotel policies.')");
	}		
		// 	Verification of Cancellation policies
	
public void cancellation_policy_fr()
{
	
	String cancel_type = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div")).getText();
	System.out.println(cancel_type);
	if (cancel_type.equals("Annulation gratuite")) {
		// Card Payment verification
			String exp_title1 ="Politique d'annulation";
			String exp_title2 ="paiement par carte";
			String exp_text1 = "Annulation gratuite avant 18 h 00 (un jour avant), des frais de service peuvent s'appliquer.\n\n" 
	
			+ "En cas d'annulation après 18h00 (un jour avant), le montant de la première nuit sera facturé.\n\n" 

			+ "Aucun montant ne sera remboursé en cas d'annulation après l'enregistrement.\n\n"

			+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"

			+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent conformément aux politiques de l'hôtel.";

			String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
			String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
			String act_text1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]")).getText();
			try
			{
				Assert.assertEquals(exp_title1, act_title1);
				Assert.assertEquals(exp_title2, act_title2);
				Assert.assertEquals(exp_text1, act_text1);
				System.out.println("Cancellation policy for Card payment content and title is matching");
			} catch (Exception e)
			{
				System.out.println("Cancellation policy for Card payment content is not matching");
			}


	// Wire Transfer verification
			String exp_text11 = "Virement bancaire";
			String exp_text12 = "Annulation gratuite avant 18 h 00 (un jour avant), des frais de service peuvent s'appliquer\n\n" 
			
			+ "En cas d'annulation après 18h00 (un jour avant), le montant de la première nuit sera facturé.\n\n" 

			+ "Aucun montant ne sera remboursé en cas d'annulation après l'heure d'arrivée.\n\n"

			+ "si le client ne se présente pas sur sa réservation confirmée, nous la traitons comme une annulation et aucun montant ne sera remboursé.\n\n"

			+ "Les heures d'enregistrement tardif ne seront pas soumises à un refinancement.\n\n"
					
			+ "La réservation réservée sera automatiquement annulée en cas d'échec de la transaction.\n\n"
					
			+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n"
					
			+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent selon les politiques de l'hôtel.\n\n"

			+ "Les remboursements ne sont applicables que lorsqu'une demande d'annulation a été formellement soumise via la plateforme Namlatic.\n\n"
					
			+ "Les remboursements applicables prendront 5 à 7 jours ouvrables pour être traités.";
					
			String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
			String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
			try
				{
					Assert.assertEquals(exp_text11, act_text11);
					Assert.assertEquals(exp_text12, act_text12);
					System.out.println("Cancellation policy for Wire Transfer content is matching");
				} catch (Exception e)
				{
					System.out.println("Cancellation policy for Wire Transfer content is not matching");
				}

		// Pay at hotel
			((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
			String exp_text21 = "Payer à l'hôtel";
			String exp_text22 = "Lors d'une réservation effectuée avec l'option «Payer à l'hôtel», l'hôtel concerné percevra l'intégralité du paiement contre la réservation au moment de l'enregistrement.\n\n" 
		
			+ "La réservation doit être confirmée 48 heures avant l'heure d'enregistrement de la date du voyage par e-mail ou par appel avec Namlatic.La réservation sera considérée comme confirmée uniquement sur vérification personnelle, sinon sera soumise à une annulation en vertu de la clause de politique d'annulation.\n\n" 

			+ "Voici les coordonnées:\n"
					
			+ "Courriel: confirmation@namlatic.com\n"
					
			+ "Hotline Namlatic DZ: + 213982414415\n\n"
					
			+ "Pour les touristes internationaux, le paiement sera facturé en devise locale ou dans toute autre devise, comme décidé par l'hôtel.\n\n"
					
			+ "Pour des raisons de sécurité, l'utilisateur doit fournir le code PIN à 4 chiffres reçu avec son e-mail de confirmation de réservation.Namlatic ou l'hôtel peut annuler la réservation en cas de problème si le code PIN trouvé est incorrect.\n\n"

			+ "Namlatic permet l'annulation partielle et l'annulation complète des chambres réservées avant l'enregistrement.\n\n"
					
			+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation avec \"Aucun remboursement\".\n\n"
					
			+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement partiel.\n\n"
					
			+ "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"

			+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"
					
			+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.";
				
			String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
			String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
			try
				{
					Assert.assertEquals(exp_text21, act_text21);
					Assert.assertEquals(exp_text22, act_text22);
					System.out.println("Cancellation policy for Pay at hotel content is matching");
				} catch (Exception e)
				{
					System.out.println("Cancellation policy for Pay at hotel content is not matching");
				}
			
		// CIB
			((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
			String exp_text31 = "CIB";
			String exp_text32 = "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n" 
			
			+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n" 

			+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.\n\n"

			+ "Politique de remboursement\n\n"
					
			+ "Toute réservation annulée la veille avant 18h00 via la plateforme Namlatic obtiendra le remboursement intégral du montant payé par carte CIB (des frais d'annulation peuvent s'appliquer).\n\n"
					
			+ "Les frais d'annulation et les frais de service appliqués peuvent varier en fonction de la réservation et de l'établissement choisi.\n\n"
					
			+ "Le processus de remboursement applicable peut prendre 3 à 5 jours ouvrables.\n\n"
					
			+ "Non-présentation\n\n"
					
			+ "Aucun remboursement ne sera effectué si une personne ne s'est pas présentée à l'hôtel concerné lors de son séjour.\n\n"
					
			+ "Namlatic n'accepte aucune responsabilité pour les conséquences de votre arrivée retardée ou de toute annulation ou frais de non-présentation facturés par l'hôtel.\n\n"
					
			+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement.";

			String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
			String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
			try
				{
				Assert.assertEquals(exp_text31, act_text31);
				Assert.assertEquals(exp_text32, act_text32);
				System.out.println("Cancellation policy for CIB payment method content is matching");
				} catch (Exception e)
				{
					System.out.println("Cancellation policy for CIB payment method content is not matching");
				}
		}
		else if (cancel_type.equals("Non remboursable"))
			{
			// Card Payment verification
				String exp_title1 ="Politique d'annulation";
				String exp_title2 ="paiement par carte";
				String exp_text1 = "Aucun montant ne sera remboursé à tout moment de l'annulation.\n\n" 
			
				+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n" 

				+ "En cas d'acte malveillant / hostile par le client final dans les locaux de la propriété avec toute personne trouvée ou signalée sera soumise à l'annulation de la réservation sur place, sans remboursement d'argent conformément aux politiques de l'hôtel.";

				String act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/h3")).getText();
				String act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]")).getText();
				String act_text1 = driver.findElement(By.xpath("//div[contains(text(),'Aucun montant ne sera remboursé à tout moment de l'annulation.')]")).getText();
				try
					{
						Assert.assertEquals(exp_title1, act_title1);
						Assert.assertEquals(exp_title2, act_title2);
						Assert.assertEquals(exp_text1, act_text1);
						System.out.println("Cancellation policy for Card payment content and title is matching");
					} catch (Exception e)
					{
						System.out.println("Cancellation policy for Card payment content is not matching");
					}


		// Wire Transfer verification
			String exp_text11 = "Virement bancaire";
			String exp_text12 = "Aucun remboursement ne sera fourni à tout moment de l'annulation.\n\n" 
					
			+ "Namlatic n'autorise pas la modification / mise à jour des dates de réservation via sa plateforme.\n\n" 

			+ "La réservation réservée sera automatiquement annulée en cas d'échec de la transaction.";

			String act_text11 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]")).getText();
			String act_text12 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[2]/div[3]")).getText();
			try
				{
				Assert.assertEquals(exp_text11, act_text11);
				Assert.assertEquals(exp_text12, act_text12);
				System.out.println("Cancellation policy for Wire Transfer content is matching");
				} catch (Exception e)
				{
				System.out.println("Cancellation policy for Wire Transfer content is not matching");
				}

		// Pay at hotel
			((JavascriptExecutor)driver).executeScript("scroll(0,'Pay at hotel')");					
			String exp_text21 = "Payer à l'hôtel";
			String exp_text22 = "Lors d'une réservation effectuée avec l'option «Payer à l'hôtel», l'hôtel concerné percevra l'intégralité du paiement contre la réservation au moment de l'enregistrement.\n\n" 
					
			+ "La réservation doit être confirmée 48 heures avant l'heure d'enregistrement de la date du voyage par e-mail ou par appel avec Namlatic.La réservation sera considérée comme confirmée uniquement sur vérification personnelle, sinon sera soumise à une annulation en vertu de la clause de politique d'annulation.\n\n" 

			+ "Voici les coordonnées:\n"
							
			+ "Courriel: confirmation@namlatic.com\n"
							
			+ "Hotline Namlatic DZ: + 213982414415\n\n"
							
			+ "Pour les touristes internationaux, le paiement sera facturé en devise locale ou dans toute autre devise, comme décidé par l'hôtel.\n\n"
							
			+ "Pour des raisons de sécurité, l'utilisateur doit fournir le code PIN à 4 chiffres reçu avec son e-mail de confirmation de réservation.Namlatic ou l'hôtel peut annuler la réservation en cas de problème si le code PIN trouvé est incorrect.\n\n"

			+ "Namlatic permet l'annulation partielle et l'annulation complète des chambres réservées avant l'enregistrement.\n\n"
							
			+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation avec \"Aucun remboursement\".\n\n"
							
			+ "Les heures d'enregistrement tardif ne feront l'objet d'aucun remboursement partiel.\n\n"
							
			+ "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n"

			+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n"
							
			+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.";
							
			String act_text21 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[1]")).getText();
			String act_text22 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[3]/div[2]")).getText();
			try
				{
					Assert.assertEquals(exp_text21, act_text21);
					Assert.assertEquals(exp_text22, act_text22);
					System.out.println("Cancellation policy for Pay at hotel content is matching");
					} catch (Exception e)
					{
					System.out.println("Cancellation policy for Pay at hotel content is not matching");
					}
					
						// CIB
							((JavascriptExecutor)driver).executeScript("scroll(0,'CIB')");					
							String exp_text31 = "CIB";
							String exp_text32 = "Vous pouvez annuler une réservation à tout moment sur le site de réservation en ligne Namlatic.\n\n" 
					
							+ "Une fois la réservation annulée, vous ne pouvez pas relancer la même réservation ni vous enregistrer directement sous le même numéro de réservation.\n\n" 

							+ "Tout acte hostile / malveillant par le touriste dans les locaux de l'hôtel, avec toute personne trouvée ou signalée, sera soumis à une annulation de réservation sans remboursement d'argent.\n\n"

							+ "Politique de remboursement\n\n"
							
							+ "Les réservations faites avec les conditions de l'hôtel \"non remboursable\" ne peuvent pas faire l'objet d'annulation ou de remboursement.\n\n"
							
							+ "Toute réservation annulée après / pendant l'heure d'enregistrement sera considérée comme une annulation non remboursable avec \"Aucun remboursement\"";
							
							String act_text31 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]")).getText();
							String act_text32 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[4]/div[3]")).getText();
							try
							{
								Assert.assertEquals(exp_text31, act_text31);
								Assert.assertEquals(exp_text32, act_text32);
								System.out.println("Cancellation policy for CIB payment method content is matching");
							} catch (Exception e)
							{
								System.out.println("Cancellation policy for CIB payment method content is not matching");
							}

	}
	}

public void amenities_ver_fr()
{
				// Amenities section2 verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div")).isDisplayed();
						System.out.println("Amenities section available");
					} catch (Exception e)
					{
						System.out.println("Amenities section not available");
					}
}

public void policy_ver_fr()
{
				// Hotel Policy verification
					try
					{
						driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[5]/div")).isDisplayed();
						System.out.println("Hotel Policy section available");
					} catch (Exception e)
					{
						System.out.println("Hotel Policy section not available");
					}
}

public void book_now_fr() throws InterruptedException, IOException
{				
		Date_room_selection_fr();
	//only one option should be active in script.
		CIB_fr();
		//Card_Payment_fr();
		//Wire_Transfer_fr();
}

public void Date_room_selection_fr() throws InterruptedException, IOException
{
		//From and To Date Selection
		WebElement date=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",date);
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
		Thread.sleep(500);
	
		//WebElement navigate=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[7]/span"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", navigate);
		
		//	driver.findElement(By.xpath("//*[text()='29']")).click();
			//driver.findElement(By.xpath("//*[text()='30']")).click();
	
			
		//Room selection
			Thread.sleep(2000); 
			((JavascriptExecutor)driver).executeScript("scroll(0,700)");
			WebElement element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[3]/div[2]/select"));
			Select room_sel=new Select(element);
			room_sel.selectByVisibleText("1");
			WebElement firstEle=room_sel.getFirstSelectedOption();
			multiScreens.multiScreenShot(driver);

			//Book now button verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Book Now button available in booking section");
			} catch (Exception e)
			{
				System.out.println("Book Now button not available in Booking section");
			}
			
		//Click on book now button
			WebElement book_now = driver.findElement (By.xpath("//*[(text()='Reserve maintenant')]"));
					book_now.click();
					multiScreens.multiScreenShot(driver);
	
}					
		//Select payment methods ' CIB '  using radio button.
					public void CIB_fr() throws IOException, InterruptedException
					{
						if (driver.findElement(By.id("cib")).isEnabled())
						{
							WebElement radio_cib=driver.findElement(By.id("cib"));
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

				//Select Terms and Conditions - check box
					WebElement checkbox_terms=driver.findElement(By.id("terms"));
					checkbox_terms.click();
					multiScreens.multiScreenShot(driver);
					System.out.println("CIB card selected");
						} else
						{
						//Currency selection
							WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
							Thread.sleep(500);
							cur.click();
							WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='د٠ج DZD')]"));
							cur_sel.click();
						
						//CIB selection and proceed
							Date_room_selection_fr();									
							WebElement radio_cib=driver.findElement(By.id("cib"));
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", radio_cib);
							Thread.sleep(500);
							radio_cib.click();
							multiScreens.multiScreenShot(driver);
							Thread.sleep(3000);

						//Select Terms and Conditions - check box
							WebElement checkbox_terms=driver.findElement(By.id("terms"));
							checkbox_terms.click();
							multiScreens.multiScreenShot(driver);
							System.out.println("CIB card selected");
						}
						}
						
					
			public void Card_Payment_fr() throws IOException, InterruptedException
			{
				WebElement radio_card=driver.findElement(By.id("card"));
				radio_card.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Card Payment selected");
			}
			
			public void Wire_Transfer_fr() throws IOException, InterruptedException
			{
				WebElement radio_wt=driver.findElement(By.id("wt"));
				radio_wt.click();
				multiScreens.multiScreenShot(driver);
				Thread.sleep(3000);
				System.out.println("Wire Transfer selected");
			}

public void various_section_Validation_fr() throws InterruptedException
{
		// Booking section verification
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div")).isDisplayed();
				System.out.println("Booking section available");
			} catch (Exception e)
			{
				System.out.println("Booking section not available");
			}
			
			//Pricing
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[1]/div[1]")).isDisplayed();
				System.out.println("Pricing details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Pricing details not available in Booking section");
			}
			
			// Calender
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).isDisplayed();
				System.out.println("Calender details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Calender details not available in Booking section");
			}
			
			//Guest
			try
			{
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div")).isDisplayed();
				System.out.println("Guest details available in booking section");
			} catch (Exception e)
			{
				System.out.println("Guest details not available in Booking section");
			}
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,2000)");

		//Navigating to various section
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/a")).click();
			Thread.sleep(1000);
			//((JavascriptExecutor)driver).executeScript("scroll(0,400)");

			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[2]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[3]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div[4]/a")).click();
			Thread.sleep(2000);
			
		/* //Select reCAPTCHA Check box
			driver.switchTo().defaultContent().findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[4]"));
			System.out.println("test");
			WebElement checkbox_recaptcha=driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]"));
			checkbox_recaptcha.click();
			System.out.println("reCAPTCHA check box is selected"); 
		

		//Click on Proceed To Pay button
			WebElement Proceed_element =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[5]/div"));
			Proceed_element.click(); */
}

	
}
