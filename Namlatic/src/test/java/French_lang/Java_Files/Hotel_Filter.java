package French_lang.Java_Files;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import French_lang.E2E_Files.Landing_Page_Fr;
import junit.framework.Assert;
public class Hotel_Filter extends Landing_Page_Fr{
	
void Hotel_sel() throws InterruptedException, IOException
{
	/*filter();
	popular_sr();
	Amenities();*/
	
	Landing_Page_Fr Nam_Login = new Landing_Page_Fr();
	Nam_Login.screenshots(); 
	
	search();
}
	public void filter() throws IOException, InterruptedException
	{
	 // Price filter verification
		//Title Verification
			String act_Price_title =driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div")).getText();
			String exp_Price_title = "prix";
			try {
				Assert.assertEquals(exp_Price_title, act_Price_title);	
				System.out.println("Price filter menu available");
				} catch (AssertionError e)
				{
				System.out.println("Price filter menu not available");	
				}
			multiScreens.multiScreenShot(driver);

		//Lowest price verification
			WebElement lowprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[5]"));
			
			try {
				System.out.println("Lowest price is enabled: " +lowprice.isDisplayed());
				} catch (AssertionError e)
				{
				System.out.println("Lowest price is not enabled");	
				}
			multiScreens.multiScreenShot(driver);
			
		//Highest price verifcation
			WebElement highprice = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[7]"));
			
			try {
				System.out.println("Highest price is enabled: " +highprice.isDisplayed());
				} catch (AssertionError e)
				{
				System.out.println("Highest price is not enabled");	
				}
			multiScreens.multiScreenShot(driver);
			
		//Slider verification
			WebElement slider = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div"));
			//WebElement slider2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div[6]"));
			try {
				System.out.println("Price slider1 is enabled: " +slider.isDisplayed());
				} catch (AssertionError e)
				{
				System.out.println("Price slider is not enabled");	
				}
			multiScreens.multiScreenShot(driver);
		
		// Slider navigation
			Actions act = new Actions(driver);
			act.dragAndDropBy(slider, 0, 80).build().perform();
			//act.dragAndDropBy(slider, -20, 0).build().perform();
			multiScreens.multiScreenShot(driver);
			
		//Price low to high
			String act_lowtohigh = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/label")).getText();
			String exp_lowtohigh = "prix croissant";
			WebElement lowtohigh = driver.findElement(By.id("filter_price_low_to_high"));
		
			try {
				Assert.assertEquals(exp_lowtohigh, act_lowtohigh);
				System.out.println("Low to high option button is displayed:" +lowtohigh.isDisplayed());
				System.out.println("Low to high option button is selected:" +lowtohigh.isSelected());				
				System.out.println("Price - low to high is displaying and enabled");
				driver.findElement(By.xpath("//*[@id=\"filter_price_low_to_high\"]")).click();	
				} catch (AssertionError e)
				{
				System.out.println("Price - low to high is missing or not enabled to select");	
				}
			multiScreens.multiScreenShot(driver);
			
		//Price high to low
			String act_hightolow = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[3]/label")).getText();
			String exp_hightolow = "Prix d�croissant";
			WebElement hightolow = driver.findElement(By.id("filter_price_high_to_low"));
			System.out.println(act_hightolow);
			try {
				Assert.assertEquals(exp_hightolow, act_hightolow);
				System.out.println("High to Low option button is displayed:" +hightolow.isDisplayed());
				System.out.println("High to Low option button is selected:" +hightolow.isSelected());				
				System.out.println("Price - high to low is displaying and enabled");
				driver.findElement(By.xpath("//*[@id=\"filter_price_high_to_low\"]")).click();
				} catch (AssertionError e)
				{
				System.out.println("Price - high to low is missing or not enabled to select");	
				}
			multiScreens.multiScreenShot(driver);

		//Price filter enable and disable
			//Deselect
				WebElement pricefilter = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[2]/div[1]/div"));
				pricefilter.click();
				Thread.sleep(3000);
			//Select
				pricefilter.click();
				multiScreens.multiScreenShot(driver);
	}

	public void popular_sr() throws InterruptedException, IOException
	{
		//Popular search popular filter enable and disable
			  //Deselect
				WebElement popularfilter = driver.findElement(By.xpath("//*[contains(text(),'Recherches populaires')]"));
				popularfilter.click();
				Thread.sleep(3000);
			  //Select
				popularfilter.click();
				multiScreens.multiScreenShot(driver);
				
			//Title verification
				String act_pop_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[1]/div")).getText();
				String exp_pop_title = "recherches populaires";
				try {
					Assert.assertEquals(exp_pop_title, act_pop_title);
					System.out.println("Popular search title is matching");
					} catch (AssertionError e)
					{
					System.out.println("Popular search title is mismatching");
					}
				multiScreens.multiScreenShot(driver);
				
			//Couple Friendly verification
				String act_pop1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[1]/label")).getText();
				String exp_pop1 = "couples";
				try {
					Assert.assertEquals(exp_pop1, act_pop1);
					System.out.println("Couple friendly is available in Popular search");
					driver.findElement(By.xpath("//*[@id=\"filter_couple_friendly\"]")).click();
					} catch (AssertionError e)
					{
					System.out.println("Couple friendly is not available in Popular search");
					}
				multiScreens.multiScreenShot(driver);
			
			//Air Conditioning verification
				String act_pop2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[2]/label")).getText();
				String exp_pop2 = "climatisation";
				try {
					Assert.assertEquals(exp_pop2, act_pop2);
					System.out.println("Air Conditioning is available in Popular search");
					driver.findElement(By.xpath("//*[@id=\"filter_air_conditioning\"]")).click();				
					} catch (AssertionError e)
					{
					System.out.println("Air Conditioning is not available in Popular search");
					}
				multiScreens.multiScreenShot(driver);
			
			//Free Breakfast verification
				String act_pop3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[3]/label")).getText();
				String exp_pop3 = "petit d�jeuner gratuit";
				try {
					Assert.assertEquals(exp_pop3, act_pop3);
					System.out.println("Free Breakfast is available in Popular search");
					driver.findElement(By.xpath("//*[@id=\"filter_free_breakfast\"]")).click();	
					} catch (AssertionError e)
					{
					System.out.println("Free Breakfast is not available in Popular search");
					}	
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
			
			//Free Cancellation Policy verification
				String act_pop4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[4]/label")).getText();
				String exp_pop4 = "politique d'annulation gratuite";
				try {
					Assert.assertEquals(exp_pop4, act_pop4);
					System.out.println("Free Cancellation is available in Popular search");
					driver.findElement(By.xpath("//*[@id=\"filter_free_cancellation_policy\"]")).click();
					} catch (AssertionError e)
					{
					System.out.println("Free Cancellation is not available in Popular search");
					}	
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");

			//Pay at hotel verification
				String act_pop5 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[3]/div[2]/div[5]/label")).getText();
				String exp_pop5 = "payez � l'h�tel";
				try {
					Assert.assertEquals(exp_pop5, act_pop5);
					System.out.println("Pay at hotel is available in Popular search");
					driver.findElement(By.xpath("//*[@id=\"filter_pay_at_hotel\"]")).click();
					} catch (AssertionError e)
					{
					System.out.println("Pay at hotel is not available in Popular search");
					}		
				multiScreens.multiScreenShot(driver);
				
				//Deselect
					popularfilter.click();
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");
					Thread.sleep(2000);
	}
	
	public void Amenities() throws InterruptedException, IOException
	{
		//Amenities filter enable and disable
			//Deselect
				WebElement amenitiesfilter = driver.findElement(By.xpath("//*[contains(text(),'Equipements')]"));
				amenitiesfilter.click();
				Thread.sleep(3000);
			//Select
				amenitiesfilter.click();
				multiScreens.multiScreenShot(driver);
				((JavascriptExecutor)driver).executeScript("scroll(0,500)");
					
			//Title verification
				String act_amn_title = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[1]/div")).getText();
					String exp_amn_title = "equipements";
					try {
						Assert.assertEquals(exp_amn_title, act_amn_title);
						System.out.println("Amenities title is matching");
						} catch (AssertionError e)
						{
						System.out.println("Amenities title is mismatching");
						}
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");

			//Free Wi-fi verification
					String act_amn1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[1]/label")).getText();
					String exp_amn1 = "wi-fi gratuit";
					try {
						Assert.assertEquals(exp_amn1, act_amn1);
						System.out.println("Free Wifi is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_free_wi_fi\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Free wifi is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");
					
			//Power Backup verification
					Thread.sleep(2000);
					String act_amn2 = driver.findElement(By.xpath("//*[contains(text(),'Sauvegarde d')]")).getText();
					//html/body/div[1]/div/div[3]/div[1]/div/div[1]/div[2]/div[4]/div[2]/div[2]/label")).getText();
					String exp_amn2 = "sauvegarde d'alimentation";
					System.out.println(act_amn2);
					try {
						Assert.assertEquals(exp_amn2, act_amn2);
						System.out.println("Power Backup is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_power_backup\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Power Backup is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");
					
				//Gardern verification
					Thread.sleep(2000);
					String act_amn3 = driver.findElement(By.xpath("//*[contains(text(),'Jardin/cour arri�re')]")).getText();
					String exp_amn3 = "jardin/cour arri�re";
					System.out.println(act_amn3);

					try {
						Assert.assertEquals(exp_amn3, act_amn3);
						System.out.println("Gardern is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_garden__backyard\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Gardern is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");
			
				//SPA verification
					Thread.sleep(2000);
					String act_amn4 = driver.findElement(By.xpath("//*[contains(text(),'Spa')]")).getText();
					String exp_amn4 = "spa";
					try {
						Assert.assertEquals(exp_amn4, act_amn4);
						System.out.println("SPA is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_spa\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("SPA is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

				//Swimming pool verification
					Thread.sleep(2000);
					String act_amn5 = driver.findElement(By.xpath("//*[contains(text(),'Piscine')]")).getText();
					String exp_amn5 = "piscine";
					try {
						Assert.assertEquals(exp_amn5, act_amn5);
						System.out.println("Swimming pool is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_swimming_pool\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Swimming pool+ is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	


				//Gym verification
					Thread.sleep(2000);
					String act_amn6 = driver.findElement(By.xpath("//*[contains(text(),'Gymnase')]")).getText();
					String exp_amn6 = "gymnase";
					try {
						Assert.assertEquals(exp_amn6, act_amn6);
						System.out.println("GYM is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_gym\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("GYM is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

				//Laundry verification
					Thread.sleep(2000);
					String act_amn7 = driver.findElement(By.xpath("//*[contains(text(),'Service de blanchisserie')]")).getText();
					String exp_amn7 = "service de blanchisserie";
					try {
						Assert.assertEquals(exp_amn7, act_amn7);
						System.out.println("Laundry service is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_laundry_services__washing_machine\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Laundry is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

				//Cafe verification
					Thread.sleep(2000);
					String act_amn8 = driver.findElement(By.xpath("//*[contains(text(),'Caf�')]")).getText();
					String exp_amn8 = "caf�";
					try {
						Assert.assertEquals(exp_amn8, act_amn8);
						System.out.println("Caf� is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_caf_\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Caf� is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	


				//Pet friendly verification
					Thread.sleep(2000);
					String act_amn9 = driver.findElement(By.xpath("//*[contains(text(),'Animaux accept�s')]")).getText();
					String exp_amn9 = "animaux accept�s";
					try {
						Assert.assertEquals(exp_amn9, act_amn9);
						System.out.println("Pet friendly is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_pet_friendly\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Pet friendly is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,500)");	

				//In House restaurant verification
					Thread.sleep(2000);
					String act_amn10 = driver.findElement(By.xpath("//*[contains(text(),'Restaurant sur place')]")).getText();
					String exp_amn10 = "restaurant sur place";
					try {
						Assert.assertEquals(exp_amn10, act_amn10);
						System.out.println("In-House restaurant is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_in_house_restaurant\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("In-House restaurant is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,750)");	

				//Card Payment verification
					Thread.sleep(2000);
					String act_amn11 = driver.findElement(By.xpath("//*[contains(text(),'Paiement par carte')]")).getText();
					String exp_amn11 = "paiement par carte";
					try {
						Assert.assertEquals(exp_amn11, act_amn11);
						System.out.println("Card Payment is available in Amenities");
						driver.findElement(By.xpath("//*[@id=\"filter_card_payment\"]")).click();
						} catch (AssertionError e)
						{
						System.out.println("Card Payment is not available in Amenities");
						}		
					multiScreens.multiScreenShot(driver);
					((JavascriptExecutor)driver).executeScript("scroll(0,0)");
					((JavascriptExecutor)driver).executeScript("scroll(0,750)");
	}
	
	public void search() throws InterruptedException				
	{	
			//Hotel Search
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez o� vous voulez s�journer.')]")).clear();
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				WebElement Hotel = driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez o� vous voulez s�journer.')]"));
				Hotel.sendKeys("Le caf�, Pondich�ry");
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");

				String Hotel_name = driver.findElement(By.xpath("//input[contains(@placeholder,'Choisissez o� vous voulez s�journer.')]")).getText();
				System.out.println(Hotel_name);
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(2000);
				Hotel.sendKeys(Keys.ENTER);
				Thread.sleep(2000);	 
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div[4]/div")).click();
				Thread.sleep(2000);	
				//WebElement book_id = driver.findElement(By.xpath("//div[contains(text(),'Le caf�, Pondich�ry')]/following::div[text()='Voir plus']"));
	}
}
