package French_lang.Java_Files;

import java.awt.AWTException;
import java.io.IOException;

import English_Lang.Java_Files.My_Booking;

public class Main {
		
	public static void main (String args[]) throws Exception 
	{
		//Webdriver declaration
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
			
		/* //Login Profile
			Namlatec_Login Nam_Login = new Namlatec_Login();
			Nam_Login.login(); */
			
		//Hotel Filter
			Hotel_Filter Hotel_Fil = new Hotel_Filter();
			Hotel_Fil.Hotel_sel(); 
				
		//Hotel Booking
			Hotel_Booking_fr HBook_fr = new Hotel_Booking_fr();
			HBook_fr.Hotel_Book_fr(); 
			
		//My Booking
			My_Booking_fr Book_fr = new My_Booking_fr();
			Book_fr.Fr_Booking(); 
			
		//Edit Profile
			Edit_Profile Edit_Prof = new Edit_Profile();
			Edit_Prof.Profileedit(); 
			
		//Cancellation
			Cancel_Booking CanBook = new Cancel_Booking();
			CanBook.Can_Book(); 
			
		//Signoff
			Signout Sgout = new Signout();
			Sgout.Signoff(); 
		  
		
	}
				
			}
	



