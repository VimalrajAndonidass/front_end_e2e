package French_lang.Java_Files;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.mortbay.html.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import English_Lang.Java_Files.Hotel_Booking;
import French_lang.E2E_Files.Landing_Page_Fr;

public class My_Booking_fr extends Landing_Page_Fr{
	
	public void Fr_Booking() throws InterruptedException, AWTException, IOException
	{
		//Selecting view booking ad Book now functionalities		
			view_booking();
			book_now();
		
		//Post execution result section
			System.out.println("My Booking: PASS");	
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			multiScreens.multiScreenShot(driver); 
	}
	
	public void page_launch() throws InterruptedException, IOException
	{
		//selecting my booking menu from right top menu
		Thread.sleep(3000);
		WebElement menu=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span"));
		menu.click();
		WebElement menuoption=driver.findElement(By.xpath("//*[@class='sc-fzoaKM wEvDo']"));
		menuoption.click();
		multiScreens.multiScreenShot(driver);
	}
	
	public void view_booking() throws IOException, InterruptedException
	{	
		page_launch();
	//View Booking
		Thread.sleep(3000);
		WebElement view_book_id = driver.findElement(By.xpath("//div[contains(text(),'N0AIX1V1UZN')]/following::div[text()='Voir la réservation']")); 
		
		view_book_id.click();
		multiScreens.multiScreenShot(driver);
		
		
	//View Voucher details
		//Get handles of the windows
			String MainWindow1 = driver.getWindowHandle();			
			String mainWindowHandle1 = driver.getWindowHandle();
			Set<String> allWindowHandles1 = driver.getWindowHandles();
			Iterator<String> iterator1 = allWindowHandles1.iterator();
		//Here we will check if child window has other child windows and will fetch the heading of the child window
			 while (iterator1.hasNext()) {
			     String ChildWindow1 = iterator1.next();
			           if (!mainWindowHandle1.equalsIgnoreCase(ChildWindow1)) {
					            driver.switchTo().window(ChildWindow1);
					      
							        }}
			//Scroll Down in View Booking Details page
			 ((JavascriptExecutor)driver).executeScript("scroll(0,2000)");
		//WebDriverWait wait=new WebDriverWait(driver,50);
		Thread.sleep(5000);
		multiScreens.multiScreenShot(driver);
							
	//Click on Download Voucher link
	
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
		//WebElement download_Vou =driver.findElement(By.xpath("//*[(@class, 'sc-plWPA hwKzKa']"));
		Thread.sleep(5000); 
		WebElement download_Vou =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
		download_Vou.click();
		
		Thread.sleep(5000); 
						
	//Click on Email Voucher link
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
		Thread.sleep(5000); 
		WebElement send_email=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
		send_email.click();
			
		Thread.sleep(5000);
		multiScreens.multiScreenShot(driver);
						
	/* //Click on Print Voucher link	
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,2000)","");
		Thread.sleep(5000); 
		WebElement print_Vou=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
		print_Vou.click(); 
			
		Thread.sleep(5000);
		Robot esc_print_window = new Robot();
		esc_print_window.keyPress(KeyEvent.VK_ESCAPE);
		esc_print_window.keyRelease(KeyEvent.VK_ESCAPE);
			*/
		
		
	}
	public void book_now() throws InterruptedException, IOException
	{
		page_launch();
		Thread.sleep(3000);
		WebElement Bk_now = driver.findElement(By.xpath("//div[contains(text(),'N8SREUE8863')]/following::div[text()='Réservez maintenant']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Bk_now);
		Thread.sleep(500);
		Bk_now.click();
		Hotel_Booking_fr bking_fr = new Hotel_Booking_fr();
		bking_fr.book_now_fr();		
	}

}
