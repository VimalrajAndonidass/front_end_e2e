package French_lang.Payment_Methods;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.apache.maven.wagon.providers.http.org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import multiScreenShot.MultiScreenShot;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;


public class Wire_Transfer {
public static WebDriver driver = null;


public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Front_End\\French\\Booking\\","Wire Transfer");
static ExtentTest test;
static ExtentReports report;
	

@BeforeTest	
	public void Launch() throws Exception 
	{	
	System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
	driver = new ChromeDriver();
	report = new ExtentReports("C:\\Automation\\Reports\\Front_End\\French\\Booking\\"+"Wire_Transfer.html");	
    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}
	
@Test (priority = 0, invocationCount = 1)
    public void startTest() throws IOException
	{
	test = report.startTest("Scenario");
	try
	{
		test.log(LogStatus.PASS, "Booking via Wire Transfer(French)");
	} catch (AssertionError e)
	{
		
	}
	}

@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
	test = report.startTest("URL Verification");
	driver.get("chrome://settings/clearBrowserData");
	driver.navigate().to("https://test.namlatic.com/");
	driver.manage().window().maximize();
	multiScreens.multiScreenShot(driver);		
	try {
			AssertJUnit.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	// Language change
		WebElement language = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div"));
		if (language.getText().equals("English") || language.getText().equals("Arabic"))
		{
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='Fran�ais')]"));
		chglang.click();
		}
		else
		System.out.println("French");
		
		
	}
		
@Test (priority = 2, invocationCount = 1)
	public void Login() throws IOException, InterruptedException
	{
	// Login 		
	test = report.startTest("Login Functionality and Elastic search verification");
	try
	{
		WebElement nam_Login = driver.findElement(By.xpath("//*[contains(text(),'identifier')]"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
		uname.sendKeys("9047232893");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
		pwd.sendKeys("Test@123");
		multiScreens.multiScreenShot(driver);
		pwd.sendKeys(Keys.ENTER);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		test.log(LogStatus.PASS, "Login Pass");
	}catch (AssertionError e)
	{
		test.log(LogStatus.FAIL, "Login Failed");
	}
	
	//Hotel search
	try
		{
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		WebElement Hotel = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[1]/div/div/div/input"));
		Hotel.sendKeys("City Hotel Alger");
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Hotel.sendKeys(Keys.ENTER);
		Thread.sleep(2000);	 
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement btnclick1 = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div"));
		Hotel.click();
		Hotel.sendKeys(Keys.ENTER);

		//btnclick1.click();
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "Hotel search working fine");
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Unable to proceed hotel search with given keyword");
		}

	}

@Test(priority = 3, invocationCount = 1)
    void booking() throws IOException, InterruptedException, AWTException
	{
	test = report.startTest("Hotel details page laoding and Hotel name verification");
	Thread.sleep(2000);
	WebElement Hotel_name = driver.findElement(By.xpath("//div[contains(@class,'sc-pIhhe hdbrr') or contains(@class,'sc-qPIWj eWdkhq') or contains(@class,'sc-pdkfH eUKPuy')]"));
	WebElement Hotel_sel = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div[1]/div[1]/img"));
	Hotel_sel.click();
	String exp_hot_name = Hotel_name.getText();
	System.out.println(exp_hot_name);
	multiScreens.multiScreenShot(driver);
	//Get handles of the windows
		String mainWindowHandle = driver.getWindowHandle();
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iterator = allWindowHandles.iterator();
	
	// Here we will check if child window has other child windows and will fetch the heading of the child window
		while (iterator.hasNext()) {
			String ChildWindow = iterator.next();
            	if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
            		driver.switchTo().window(ChildWindow);
        }}
		multiScreens.multiScreenShot(driver);
	
	// Hotel name verification
		//Hotel_Filter hot = new Hotel_Filter();
		Thread.sleep(2000);
		String act_hot_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[1]/section/div/div[1]/div[1]")).getText();
		try
		{
			AssertJUnit.assertEquals(exp_hot_name, act_hot_name);
			test.log(LogStatus.PASS, "Hotel name matching:" + exp_hot_name);
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Hotel name mismatching");	
		}
		
}

   @Test(priority = 4, invocationCount = 1)

    public void booking_confirmation() throws InterruptedException, IOException
   	{				
		test = report.startTest("Verification of Language, Booking date, an Room selection");			
		//Currency selection
		try
		{
			WebElement cur = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div/div/div/div"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cur);
			Thread.sleep(500);
			cur.click();
			WebElement cur_sel = cur.findElement(By.xpath("//*[(text()='$ USD')]"));
			cur_sel.click();
			test.log(LogStatus.PASS, "Language changed successfully to USD");	
		} catch (AssertionError e)
			{
			test.log(LogStatus.PASS, "Unable to change Language");
			}
	
	//Date Selection
	   try
	   {
		   Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,501)");

		//Date validation
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_YEAR, 4);
			Date futureDateTime = calendar.getTime();
			System.out.println(futureDateTime);
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div")).click();
			Thread.sleep(1000);
			
			WebElement sel_start_date = driver.findElement(By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')]"));
			WebElement sel_end_date = driver.findElement(By.xpath("//*[contains(@class,'DateRangePicker__Date DateRangePicker__Date--is-selected')][2]"));
			
			System.out.println(sel_start_date.getText());
			System.out.println(sel_end_date.getText());
			
			int set_start_date = Integer.parseInt(sel_start_date.getText())+ 4;
			System.out.println(set_start_date);
			
			int set_end_date = Integer.parseInt(sel_end_date.getText())+ 4;
			System.out.println(set_end_date);
			
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[5]/td[2]/span")).click();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div[3]/div[2]/table/tbody/tr[6]/td[1]/span")).click();

			/*WebElement month = driver.findElement(By.xpath("//*[contains(@class,'sc-fzoOEf dfzouv')]"));
			Select month_pick=new Select(month);
			month_pick.selectByVisibleText("October");
			month_pick.getFirstSelectedOption();
	
			Thread.sleep(500);
			WebElement start_date = driver.findElement(By.xpath("//*[@text()='1']"));
			start_date.click();
	
			Thread.sleep(500);
			WebElement end_date = driver.findElement(By.xpath("//*[@text()='10']"));
			end_date.click(); */

			test.log(LogStatus.PASS, "Start and End date selected properly");	
	   } catch(Exception e)
	   {
			test.log(LogStatus.FAIL, "Unable to select start/ end date");	
			//driver.close();
	   }
	   
	//Room selection
	   try
	   {
		Thread.sleep(3000);
		WebElement scroll =driver.findElement(By.xpath("//*[text()='Types de chambres']"));
		Thread.sleep(2000); 
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll);
		WebElement element =driver.findElement(By.xpath("//*[contains(@class,'sc-oTpcS hKhvoq') or contains(@class,'sc-pJjas fvjHub') or contains(@class,'sc-qQKPx ksBkya')]"));
		Select room_sel=new Select(element);
		room_sel.selectByVisibleText("1");
		room_sel.getFirstSelectedOption();
		
		//WebElement element1 =driver.findElement(By.xpath("//select[contains(@class,'sc-oTpcS hKhvoq')][2]"));
		//Select room_sel1=new Select(element1);
		//room_sel1.selectByVisibleText("2");
		//room_sel1.getFirstSelectedOption(); 
		
		multiScreens.multiScreenShot(driver);
		Thread.sleep(2000);

		test.log(LogStatus.PASS,"Rooms selected successfully");

	   }
	   catch (Exception e)
	   {
		   test.log(LogStatus.FAIL,"Unable to select Rooms");
	   }

	   try
	   {
	//Book now button verification
		((JavascriptExecutor)driver).executeScript("scroll(0,500)");
		Thread.sleep(2000);
		//WebElement book_scroll =driver.findElement(By.xpath("//select[contains(@class,'sc-pTUKB hbNNSd')]"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",book_scroll);
		Thread.sleep(2000); 
		WebElement book_now = driver.findElement (By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/div/div/div/div/div[4]/div"));
		Thread.sleep(2000); 	
		book_now.click();
		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS,"Book Now button available and able to proceed booking");
	   }
	   catch (Exception e)
	   {
		   test.log(LogStatus.FAIL,"Book Now button is not available or unable to proceed Booking");
	   }
   }	

   @Test(priority = 5, invocationCount = 1)

    public void WT() throws IOException, InterruptedException, AWTException
	{
	test = report.startTest("Payment Method verification");
	try
	{
		WebElement radio_wt=driver.findElement(By.id("wt"));
		radio_wt.click();
		multiScreens.multiScreenShot(driver);
		Thread.sleep(3000);
		test.log(LogStatus.PASS,"Wire Transfer Payment selected");
		
		//actions.contextClick(elementLocator).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();

		System.out.println("Test");
		
		} catch(AssertionError e)
		{
			test.log(LogStatus.FAIL,"Unable to select Wire Transfer");
		}

//Click on Proceed To Pay button
	try
	{
	//WebElement book_now_scroll =driver.findElement(By.id("//id[contains(text(),'Sharing')]"));
	//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",book_now_scroll);
	((JavascriptExecutor)driver).executeScript("scroll(0,700)");
	Thread.sleep(2000);
	WebElement Proceed_element = driver.findElement (By.xpath("//*[text()='R�servez maintenant']"));
	multiScreens.multiScreenShot(driver);
	Proceed_element.click(); 
		test.log(LogStatus.PASS,"Payment done successfully via Wire Transfer Payment");
	}
	catch(AssertionError e)
	{
		test.log(LogStatus.FAIL,"Payment failed");
	}	
	
	// Voucher details verification

		Thread.sleep(8000);
		WebElement Booking_ID_web = driver.findElement(By.xpath("//div[contains(@class,'sc-oVeeF NgKHZ')]"));
		String Booking_ID = Booking_ID_web.getText();
		try
		{
			multiScreens.multiScreenShot(driver);
			String Trans_time_date = driver.findElement(By.xpath("//div[contains(text(),'CET')]")).getText();
			WebElement Hot_name_scroll =driver.findElement(By.xpath("//div[contains(@class,'sc-pYQRR coZWsy') or contains(@class,'sc-ptUam QNhqv')]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",Hot_name_scroll);
			multiScreens.multiScreenShot(driver);
			String Hotel_Name_voucher = Hot_name_scroll.getText();
			String Check_in = driver.findElement(By.xpath("//div[contains(@class,'sc-pKJXg gnvEA') or contains(@class,'sc-pdNVt bBXhFy')]")).getText();
			String Check_out = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[1]/div[2]/div[2]")).getText();
			String Guest_count = driver.findElement(By.xpath("//div[contains(@class,'sc-plXmT cxLvr') or contains(@class,'sc-pALlw ldfshs')]")).getText();
			String Room_count = driver.findElement(By.xpath("//div[contains(@class,'sc-puHdH XrpUX') or contains(@class,'sc-pJvck jQCsMK')]")).getText();
			String Guest_Name = driver.findElement(By.xpath("//div[contains(@class,'sc-pTSRm cJMMUy') or contains(@class,'sc-pjVoC hjQhoq')]")).getText();
			String Room_Type = driver.findElement(By.xpath("//div[contains(@class,'sc-pjVoC hjQhoq')]")).getText();
			String Booking_amt = driver.findElement(By.xpath("//div[contains(@class,'sc-pRQdf eaSvPy')]")).getText();
			String Pay_Mode = driver.findElement(By.xpath("//*[contains(@class,'sc-pRfvo llXBTr')]")).getText();
			String Cancellation_Type = driver.findElement(By.xpath("//*[contains(@class,'sc-ptEpz gcuWaT')]")).getText();
			multiScreens.multiScreenShot(driver);

			test.log(LogStatus.PASS,"Payment done successfully via CIB Payment and following are the booking details" + "<br/>  Booking ID: " +Booking_ID + "<br/> Transaction Time and date: "+Trans_time_date + "<br/>  Hotel Name: "+ Hotel_Name_voucher + "<br/>  Check In(Time and Date): " + Check_in + "<br/>  Check Out(Time and Date): " + Check_out + "<br/>  Guest Count: " +Guest_count + "<br/>  Room Count: " +Room_count + "<br/>  Guest name: "+Guest_Name +"<br/>  Room Type: " +Room_Type + "<br/>  Booking Amount: " +Booking_amt + "<br/>  Payment Type: "+Pay_Mode +"<br/>"+Cancellation_Type);
		}
		catch (AssertionError e)
		{
			test.log(LogStatus.FAIL,"Payment done successfully via CIB Payment but voucher not loaded properly and details are mismatching");		
		}

	//Download Payment receipt
		try {
			WebElement download_PR =driver.findElement(By.xpath("//*[contains(text(),'T�l�charger le re�u de paiement')]"));
			download_PR.click();
			multiScreens.multiScreenShot(driver);	
			test.log(LogStatus.PASS, "Download payment receipt working fine");				
//			downloadPdf();
		} catch(Exception e)
			{	
				test.log(LogStatus.FAIL, "Unable to Download Payment receipt");
			}

		//Download Voucher
		try {

			WebElement download_Vou =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[3]"));
			download_Vou.click();
			multiScreens.multiScreenShot(driver);	
			test.log(LogStatus.PASS, "Download voucher working fine");				
		} catch(Exception e)
		{	
			test.log(LogStatus.FAIL, "Unable to Download voucher");
		}
		
		//Email Voucher
		try {
			Thread.sleep(5000); 
			WebElement send_email=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[2]"));
			send_email.click();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Email voucher working fine");				
		} catch(Exception e)
		{
			test.log(LogStatus.FAIL, "Unable to Email voucher");		
		}
		
		 //Print Voucher
		try {
			WebElement print_Vou=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/div[1]"));
			print_Vou.click(); 		
			Thread.sleep(5000);
			Robot print_window = new Robot();
			print_window.keyPress(KeyEvent.VK_ENTER);
			print_window.keyRelease(KeyEvent.VK_ENTER);	test.log(LogStatus.PASS, "Print voucher working fine");	
		} catch(Exception e)
		{
			test.log(LogStatus.FAIL, "Unable to Print voucher");				
		}

	//Upload receipt
		//Navigating to My Booking
	try
	{
	((JavascriptExecutor)driver).executeScript("scroll(0,10)");
	Thread.sleep(3000);
	WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[1]/div/div/div/span/span"));
	menu.click();
	   
	WebElement menuoption=driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div[2]/ul/li[2]"));
	menuoption.click();
	multiScreens.multiScreenShot(driver);
	
	Thread.sleep(3000);
	WebElement scroll =driver.findElement(By.xpath("//*[text()='Chargez votre preuve de paiement et gardez-la attentivement. Un seul chargement possible.']"));
	Thread.sleep(2000); 
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll);
	WebElement BKingId = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div"));
	String[] Bookingid_Time = BKingId.getText().split(" ");
	String Bookingid = Bookingid_Time[0];
	Assert.assertEquals(Booking_ID, Bookingid);
			System.out.println(Bookingid);
			
			test.log(LogStatus.PASS, "Hotel booking done successfully and verified "+Bookingid);
	}
	catch(Exception e)
	{
		test.log(LogStatus.FAIL, "Booking details not reflecting in My Booking page");
	}
	try
	{
	Thread.sleep(3000);
	WebElement scroll1 =driver.findElement(By.xpath("//*[text()='Uploader le re�u']"));
	Thread.sleep(2000); 
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll1);
	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div/button")).click(); 
	Thread.sleep(6000);	
	
	WebElement WTType = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[1]/div/div[2]/select"));
	WTType.click();
	WTType.sendKeys("Bank");
	WTType.click();
	
	WebElement WTrcno = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[3]/div/div/div[2]/input"));
	WTrcno.sendKeys("123456");
	
	WebElement doc_upload = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[2]/div/label/span[2]"));
	doc_upload.click();
	Thread.sleep(3000);
	Robot rb= new Robot();
	// copying File path to Clipboard
	 	StringSelection str = new StringSelection("C:\\Automation\\Downloads\\NZOWO6X168X.pdf");
	 	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
	// press Contol+V for pasting
	 	rb.keyPress(KeyEvent.VK_CONTROL);
	 	rb.keyPress(KeyEvent.VK_V); 
	// release Control+V for pasting
	 	rb.keyRelease(KeyEvent.VK_CONTROL);
	 	rb.keyRelease(KeyEvent.VK_V); 
	// for pressing and releasing Enter
	 	rb.keyPress(KeyEvent.VK_ENTER);
	 	rb.keyRelease(KeyEvent.VK_ENTER);
	 	multiScreens.multiScreenShot(driver);
	
	 	WebElement scroll_sub =driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[4]/div[1]/div"));
		Thread.sleep(2000); 
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll_sub);
		
	 	WebElement submit = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/form/div[4]/div[1]/div"));
	 	multiScreens.multiScreenShot(driver);
	 	submit.click();
	 	
	 	Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("scroll(0,10)");
		Thread.sleep(3000);;
	 	WebElement okay = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/section/main/section/div/div"));
	 	multiScreens.multiScreenShot(driver);
	 	okay.click();

		Thread.sleep(3000);
		WebElement scroll_1 =driver.findElement(By.xpath("//*[text()='R�servations � venir']"));
		Thread.sleep(2000); 
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",scroll_1);
	 	multiScreens.multiScreenShot(driver);

		WebElement BKingId_1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div/div[4]/div"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",BKingId_1);
		driver.findElement(By.xpath("//*[text()='Voir le re�u']")).click();
		///html/body/div[1]/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div/span/span")).click();
	 	multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "Receipt uploaded successfully");
	}
	catch (AssertionError e1)
	{
		test.log(LogStatus.FAIL, "Unable to upload receipt");
	}
	}
		 	
 /*

private void downloadPdf() {
	// TODO Auto-generated method stub
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	
	String URL = driver.getCurrentUrl();
	System.out.print(URL);
	//driver.get("https://www.learningcontainer.com/sample-pdf-files-for-testing");
	
	//locator to click the pdf download link
	// contextClick() method for right click to an element after moving the //mouse to with the          moveToElement()
    Actions a = new Actions(driver);
    a.contextClick(driver.findElement(By.xpath("*[@id=’plugin’])"))).build().perform();
  }
*/
@AfterMethod
public static void endMethod()
{
	report.endTest(test);	
}

@AfterClass
public static void endTest()
{
	System.out.println("End");
report.flush();
report.close();
//driver.quit();
}
}

